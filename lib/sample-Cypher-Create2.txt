var newComponentProfile = {
            //       _id                  **  assigned by server 
            keyId:        uuid.v1(),                          //** Required  
            groupLabel:   req.body.groupLabel || "Errors",    // ** Required,,   "Component" ,   ** Required,  "Component"
            typeLabel :   req.body.typeLabel  || "Errors",    // ** Required,  "Mbrsite",      ** Required, "mbrsite"
            partLabel:    req.body.partLabel  || "Errors",    // ** Required,,   "Profile"       ** Required, "Profile"
            lblArray :    req.body.lblArray   || "Errors" ,   // ** Required,,  'groupLabel:typeLabel:partlabel' 
            userKeyId:    req.body.userKeyId || "Errors",    // ** Required,,   "",      ** Required, derived from "member" user who is will be "Owner" 
            createdByKeyId: req.body.createdByKeyId  || "Errors",         // ** Required,,    "", ** Required, derived from "Creator" user  Sysadmin, Owner
            dateCreated:  req.body.dateCreated || new Date(),         // ** Required set at time of obj creation
            status:       req.body.status  || "Errors",    // ** Required,   ** Required, set at system level-relates to validity of
            approveDate: req.body.approveDate || new Date(),         // ** Required, set at time of obj creation
            approvedById: req.body.approvedById  || "Errors",   //  ** Required, set at time of obj creation
            lastUpdated: req.body.lastUpdated || new Date(),         // ** Required but updated as changes occur
            updatedById: req.body.updatedById  || "Errors",     //   Required, but updated as changes occur
            isActive: req.body.isActive  || false,             // ** Required, set false at creation-relates to state of
            name: req.body.name || "Errors",           // NOTE: Each component type has a more or less the same profile 
            // optional properties  from form 
            street: req.body.street || "",   
            street2: req.body.street2 || "",
            city: req.body.city || "",
            state: req.body.state || "",
            zipCode: req.body.zipCode || "",
            phone: req.body.phone || "",
            fax: req.body.fax || "",
            webSite: req.body.webSite || "",
            email: req.body.email || "",
            identityId: req.body.identityId || "",
            category: req.body.category || [],             
            keywords: req.body.keywords || [],
           
            tabPaneLastUpdated: req.body.tabPaneLastUpdated ||  ["Errors"],
            tabPaneUpdatedById: req.body.tabPaneUpdatedById ||  ["Errors"], 
            tabPaneIsActive: req.body.tabPaneIsActive ||  [false],            
            tabPaneStatus: req.body.tabPaneStatus ||  ["Inactive"],  // dflt: Inactive Public,Internal Only Private,Inactive
                            
            tabPaneBarId: req.body.tabPaneBarId ||  [],  //  [100,200,300,400,500,600];
            tabPaneBarHref: req.body.tabPaneBarHref ||  [],  // ['href="#tab0-1"','href="#tab1-1"','href="#tab1-2"','href="#tab1-3"','href="#tab2-1"','href="#tab3-1"'];
            tabPaneBarLabelText: req.body.tabPaneBarLabelText ||  [],  //["My Home","Blank 1","Blank 2","Blank 3","Grid 1","Google Map"];
            tabPaneBarIsActive: req.body.tabPaneBarIsActive ||  [],  //[true,false,false,false,false,false];
            tabPaneBarOrder: req.body.tabPaneBarOrder ||  [],  //[1,2,3,4,5,6] ;
            // tabPane for each paneTab there can be several ContentModel entries
            tabPaneTabIds: req.body.tabPaneTabIds ||  [],  //["tab1-1","tab1-2"];
            tabPaneTabTitles: req.body.tabPaneTabTitles ||  [],  //["Blank Template One","Blank Template Two"];
            // the following arrays are filled with the _id and KeyId of each contentModel created as described
            // below.
            tabPaneTabContentIds: req.body.tabPaneTabContentIds ||  [],  //
            tabPaneTabContentKeyIds: req.body.tabPaneTabContentKeyIds ||  []  //
