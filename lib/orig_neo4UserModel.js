// user.js
// User model logic.

var neo4j = require('neo4j');
var Util = require('util');
var bcrypt = require('bcrypt');

var db = new neo4j.GraphDatabase(process.env.NEO4J_URL || 'http://localhost:7474');

// constants:

var INDEX_NAME = 'nodes';
var INDEX_KEY = 'type';
var INDEX_VAL = 'user';

var FOLLOWS_REL = 'follows';

// private constructor:

var User = module.exports = function User(_node) {
    // all we'll really store is the node; the rest of our properties will be
    // derivable or just pass-through properties (see below).
    this._node = _node;
};

// public instance properties:

Object.defineProperty(User.prototype, 'id', {
    get: function () { return this._node.id; }
});

Object.defineProperty(User.prototype, 'exists', {
    get: function () { return this._node.exists; }
});

/* ***
   authentication
** */
Object.defineProperty(User.prototype, 'accountnbr', {
    get: function () {
        return this._node.data['accountnbr'];
    },
    set: function (accountnbr) {
        this._node.data['accountnbr'] = accountnbr;
    }
});

Object.defineProperty(User.prototype, 'username', {
    get: function () {
        return this._node.data['username'];
    },
    set: function (username) {
        this._node.data['username'] = username;
    }
});

Object.defineProperty(User.prototype, 'password', {
    get: function () {
        return this._node.data['password'];
    },
    set: function (password) {
        this._node.data['password'] = password;
    }
});

Object.defineProperty(User.prototype, 'key', {
    get: function () {
        return this._node.data['key'];
    },
    set: function (key) {
        this._node.data['key'] = key;
    }
});

Object.defineProperty(User.prototype, 'algorithm', {
    get: function () {
        return this._node.data['algorithm'];
    },
    set: function (algorithm) {
        this._node.data['algorithm'] = algorithm;
    }
});

Object.defineProperty(User.prototype, 'social', {
    get: function () {
        return this._node.data['social'];
    },
    set: function (social) {
        this._node.data['social'] = social;
    }
});
/* ***
 contact info
 ** */
Object.defineProperty(User.prototype, 'name', {
    get: function () {
        return this._node.data['name'];
    },
    set: function (name) {
        this._node.data['name'] = name;
    }
});

Object.defineProperty(User.prototype, 'street', {
    get: function () {
        return this._node.data['street'];
    },
    set: function (street) {
        this._node.data['street'] = street;
    }
});

Object.defineProperty(User.prototype, 'city', {
    get: function () {
        return this._node.data['city'];
    },
    set: function (city) {
        this._node.data['city'] = city;
    }
});

Object.defineProperty(User.prototype, 'state', {
    get: function () {
        return this._node.data['state'];
    },
    set: function (state) {
        this._node.data['state'] = state;
    }
});
Object.defineProperty(User.prototype, 'zipcode', {
    get: function () {
        return this._node.data['zipcode'];
    },
    set: function (zipcode) {
        this._node.data['zipcode'] = zipcode;
    }
});
Object.defineProperty(User.prototype, 'contactphone', {
    get: function () {
        return this._node.data['contactphone'];
    },
    set: function (contactphone) {
        this._node.data['contactphone'] = contactphone;
    }
});

Object.defineProperty(User.prototype, 'mobilephone', {
    get: function () {
        return this._node.data['mobilephone'];
    },
    set: function (mobilephone) {
        this._node.data['mobilephone'] = mobilephone;
    }
});

Object.defineProperty(User.prototype, 'email', {
    get: function () {
        return this._node.data['email'];
    },
    set: function (email) {
        this._node.data['email'] = email;
    }
});

Object.defineProperty(User.prototype, 'website', {
    get: function () {
        return this._node.data['website'];
    },
    set: function (website) {
        this._node.data['website'] = website;
    }
});

Object.defineProperty(User.prototype, 'bioinfo', {
    get: function () {
        return this._node.data['bioinfo'];
    },
    set: function (bioinfo) {
        this._node.data['bioinfo'] = bioinfo;
    }
});


// private instance methods:

User.prototype._getFollowingRel = function (other, callback) {
    var query = [
        'START user=node({userId}), other=node({otherId})',
        'MATCH (user) -[rel?:FOLLOWS_REL]-> (other)',
        'RETURN rel'
    ].join('\n')
        .replace('FOLLOWS_REL', FOLLOWS_REL);

    var params = {
        userId: this.id,
        otherId: other.id
    };

    db.query(query, params, function (err, results) {
        if (err) return callback(err);
        var rel = results[0] && results[0]['rel'];
        callback(null, rel);
    });
};

// public instance methods:

User.prototype.save = function (callback) {
    this._node.save(function (err) {
        callback(err);
    });
};

User.prototype.del = function (callback) {
    this._node.del(function (err) {
        callback(err);
    }, true);   // true = yes, force it (delete all relationships)
};

User.prototype.follow = function (other, callback) {
    this._node.createRelationshipTo(other._node, 'follows', {}, function (err, rel) {
        callback(err);
    });
};

User.prototype.unfollow = function (other, callback) {
    this._getFollowingRel(other, function (err, rel) {
        if (err) return callback(err);
        if (!rel) return callback(null);
        rel.del(function (err) {
            callback(err);
        });
    });
};

// calls callback w/ (err, following, others) where following is an array of
// users this user follows, and others is all other users minus him/herself.
User.prototype.getFollowingAndOthers = function (callback) {
    // query all users and whether we follow each one or not:
    var query = [
        'START user=node({userId}), other=node:INDEX_NAME(INDEX_KEY="INDEX_VAL")',
        'MATCH (user) -[rel?:FOLLOWS_REL]-> (other)',
        'RETURN other, COUNT(rel)'  // COUNT(rel) is a hack for 1 or 0
    ].join('\n')
        .replace('INDEX_NAME', INDEX_NAME)
        .replace('INDEX_KEY', INDEX_KEY)
        .replace('INDEX_VAL', INDEX_VAL)
        .replace('FOLLOWS_REL', FOLLOWS_REL);

    var params = {
        userId: this.id
    };

    var user = this;
    db.query(query, params, function (err, results) {
        if (err) return callback(err);

        var following = [];
        var others = [];

        for (var i = 0; i < results.length; i++) {
            var other = new User(results[i]['other']);
            var follows = results[i]['COUNT(rel)'];

            if (user.id === other.id) {
                continue;
            } else if (follows) {
                following.push(other);
            } else {
                others.push(other);
            }
        }

        callback(null, following, others);
    });
};

// static methods:
//Query:
//    MATCH (a:Actor { name:"Laurence Fishburne" })
//RETURN a

//User.findUniqueUsername = function ( queryparms, callback) {
//   // console.log('db user find initial qparms' + Util.inspect( queryparms.valueOf() , { showHidden: false, depth: 5 } ));
//    // query all usernames matching search term:
//    var query = [
//        'START a=node(*)',mongo
//        'RETURN a'  //
//    ];
//
//
//    var params = {
//        userID: "george"
//    };
//    console.log('db user find parms: ' + Util.inspect( params.valueOf() , { showHidden: false, depth: 5 } ));
//    var user = this;
//    console.log('db user find username: ' + Util.inspect( user.userId , { showHidden: false, depth: 10 } ));
//    db.query(query, params, function (err, results) {
//        var terr = String( err);
//        console.log('db user find err: ' + Util.inspect( terr.valueOf() , { showHidden: false, depth: 5 } ));
//        var tresults = String(results);
//        console.log('db user find results: ' + Util.inspect( tresults.valueOf() , { showHidden: false, depth: 5 } ));
//        err = '';
//        if (err) return callback(err);
//
//        console.log('db user find qparms' + Util.inspect( queryparms.valueOf() , { sh

//           return new User(node);
//        });
// TO DO:
//        RegExp.escape = function(text) {owHidden: false, depth: 5 } ));
//
//        if ( queryparms.smode == 'canCreate') {
//              var canCreate = false;
//              if ( !results.length ) canCreate = true;
//                callback(null, canCreate);
//        }
//        else if ( queryparms.smode == 'filter') {
//           var userslist = [];
//           for (var i = 0; i < results.length; i++) {
//              var tuser = new User(results[i]);
//              userslist.push( tuser );
//           }
//           callback(null, userslist);
//        }
//
//    });
//};

User.registerCheck = function (filterparms, callback) {
    // TO DO: replace with query lookup using username
    db.getIndexedNodes(INDEX_NAME, INDEX_KEY, INDEX_VAL, function (err, nodes) {
        // if (err) return callback(err);
        // FIXME the index might not exist in the beginning, so special-case
        // this error detection. warning: this is super brittle!
        // the better and correct thing is to either ensure the index exists
        // somewhere by creating it, or just use Neo4j's auto-indexing.
        // (the Heroku Neo4j add-on doesn't support auto-indexing currently.)
        if (err) return callback('regCheck dbError', null);
        var hits = nodes.map(function (node) {

            return new User(node);
        });
// TO DO:
//        RegExp.escape = function(text) {
//            return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
//        }
        var OkPswd = false;
        for (var i = 0, len = hits.length; i < len; i++) {
                 // TO DO: do unescape  add compare logic to include password as well
                 console.log('db registerCheck canRegister test: ' + hits[i].username + ' ' + filterparms.username + ' ' + OkPswd);
                 if (hits[i].username === filterparms.username) {
                      //TO DO: var PswdHit = Un escape filterparms
                      OkPswd = bcrypt.compareSync(filterparms.userpswd,hits[i].password);
                      console.log('db user registerCheck canRegister as: ' + filterparms.username + ' ' + filterparms.userpswd + ' ' + OkPswd );
                 };mongo
                 if ( OkPswd ){
                     break;
                 }
        }
        console.log('db user registerCheck registerCheck exit as: ' + filterparms.username + ' ' + filterparms.userpswd + ' ' + OkPswd );
        if ( OkPswd ){
            // callback(err, hits ) ; // callback(err, []); username / password already used
            console.log('db user registerCheck canRegister exists as: ' + filterparms.username + ' ' + filterparms.userpswd + ' ' + OkPswd );
            // callback( 'username/password must not exist', '/users/register' );
            err = 'username/password must not exist';
            //break;
            callback( err, null);
        } else {
            callback(err, []);
        }
    });
    console.log('db user findAll');
};

User.loginCheck = function (filterparms, callback) {
    // TO DO: replace with query lookup using username
    db.getIndexedNodes(INDEX_NAME, INDEX_KEY, INDEX_VAL, function (err, nodes) {
        // if (err) return callback(err);
        // FIXME the index might not exist in the beginning, so special-case
        // this error detection. warning: this is super brittle!
        // the better and correct thing is to either ensure the index exists
        // somewhere by creating it, or just use Neo4j's auto-indexing.
        // (the Heroku Neo4j add-on doesn't support auto-indexing currently.)
        if (err) return callback(null, []);
        var hits = nodes.map(function (node) {

            return new User(node);
        });
//        RegExp.escape = function(text) {
//            return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
//        }
        var OkPswd = false;
        var outResults = [];

        for (var i = 0, len = hits.length; i < len; i++) {
                console.log('db loginCheck canLogin compare: ' + hits[i].username + ' ' + hits[i].id  + ' ' + OkPswd );
                if (hits[i].username === filterparms.username ) {
                    console.log('db user loginCheck canlogin as: ' + filterparms.username + ' ' + filterparms.userpswd + ' ' + OkPswd );
                    OkPswd = bcrypt.compareSync(filterparms.userpswd,hits[i].password);

                };
                if ( OkPswd ){
                    break;
                }
        };
        console.log('db user loginCheck exit as: ' + filterparms.username + ' ' + filterparms.userpswd + ' ' + OkPswd );
        if ( OkPswd ){
            callback( "" , hits[i] );
        } else {
            callback( "Error", null);
        }


    });
    console.log('db user loginCheck');
};

User.get = function (id, callback) {
    db.getNodeById(id, function (err, node) {
        if (err) return callback(err);
        callback(null, new User(node));
    });
};

User.getAll = function (callback) {
    db.getIndexedNodes(INDEX_NAME, INDEX_KEY, INDEX_VAL, function (err, nodes) {
        // if (err) return callback(err);
        // FIXME the index might not exist in the beginning, so special-case
        // this error detection. warning: this is super brittle!
        // the better and correct thing is to either ensure the index exists
        // somewhere by creating it, or just use Neo4j's auto-indexing.
        // (the Heroku Neo4j add-on doesn't support auto-indexing currently.)
        if (err) return callback(null, []);
        var users = nodes.map(function (node) {
            return new User(node);
        });
        callback(null, users);
    });
    console.log('db server called');
};

// creates the user and persists (saves) it to the db, incl. indexing it:
User.create = function (data, callback) {
    var node = db.createNode(data);
    console.log( 'db user createNode');
    var user = new User(node);
    console.log('db user new User' + Util.inspect( user.valueOf() , { showHidden: false, depth: 5 } ));
    node.save(function (err) {
        var tstring = new String( err );
        console.log('db user create save err: ' + Util.inspect( tstring.valueOf() , { showHidden: false, depth: 5 } ));
        //err = null;
        if (err) return callback(err);
        console.log( 'db saved');
        node.index(INDEX_NAME, INDEX_KEY, INDEX_VAL, function (err) {
            var tsring = new String( err );
            console.log('db user create index err: ' + Util.inspect( tsring.valueOf() , { showHidden: false, depth: 5 } ));
            err = null;
            if (err) return callback(err);
              var tring = new String( err );
              console.log('db user create callback err: ' + Util.inspect( tring.valueOf() , { showHidden: false, depth: 5 } ));

            callback(null, user);
        });
    });
};