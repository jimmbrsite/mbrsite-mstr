// neo4ComponentApi.js
// Neo4j - component api

var neo4api = require('./neo4');
var uuid = require('node-uuid');

//var dater = require('./apiUtils');

/* THINGS TO DO:

   20150505 -  whereSetProperties - Need to inprove logic to allow where obj that are ''
                                    to be searched.
   20150506 -  whereSetProperties - Need to imporve overall handling of where ( req.body[].where ) criteria submitted to this routine.
                                    Adding logic to detect and return 'or' and other logic constructs found in criteria as part
                                    of the  returned where

                ""                  need to capture REF name from  whereSetProperties and use as part of MATCH Clause.
                                    example : ref = ['pro','rel,'con']
                                             'MATCH (pro:Components )''  could be: 'MATCH  (' + ref[0] + ':Components )'
                                                                         or maybe: 'MATCH ( {xQ_1}:Components )'





*/
/* POST COMPONENT PROFILE AND CONTENT SET:
*
****  Initial Create of Component Profile, ContentSet and Relationship data elements. This process is
*     launched from admin console "addUser" tab. At a minimum this process will create
      1 Profle element (node) , 1 Content element (node) and 1 RelationShip element ( relationship ) per request.
      Detecting multiple contentSets the process will proceed to create pairs of ContentSets and Relationship elements until
      the contentSets have been fully processed. The query set and params that would have been populated durning this time
      will them be submitted to Ne04j Api. If while processing this query/parms set an error occurs cypher will terminate processing
      this query set and return an error message. All db data will be rolled back to before update state.

****
*/

function postProfileContentSetV1(req, res, next) {

    var parms = "";
    var include_stats = true;

    //var d = new Date();
    //d_string = d.format("m/d/Y h:i:s");
    //var wrkDt = new Date();
    //var dfltDate = wrkDt.dater.format("Y/m/d");
    //var today = new Date();
    //var dateString = today.format("dd-m-yy");
    // alert(dateString);

    // CONTENTSET ORIG - Delete if new process used below works
    // keyId: cKeyId,                                                 // var content =
    // groupLabel: req.body.contentgroupLabel || "Components",
    // typeLabel: req.body.contenttypeLabel  || typeLabel,
    // partLabel: req.body.contentpartLabel || "ContentSet",
    // userKeyId:  req.body.contentuserKeyId  || "Errors",
    // createdByKeyId: req.body.contentcreatedByKeyId  || "Errors",
    // dateCreated: req.body.contentdateCreated  || "Errors",
    // lastUpdated: req.body.contentlastUpdated  || "Errors",
    // updatedById: req.body.contentupdatedById  || "Errors",
    // modelId: req.body.modelId  || 'Empty',           // "tab0-1";
    // modelName: req.body.modelName || 'Empty',        //  "My Home";
    // modelSrcs:  req.body.modelSrcs  || [],     // ["Model",..];
    // modelObjs: req.body.modelObjs  || [],       // ["cOne", "cTwo", "cThree"];
    // isOk: req.body.isOk  || [],      // ["Support","Support"];

    // titleText: req.body.titleText   || 'Empty' ,

    // textAreaTitle: req.body.areaTitle ||[], // ["Sample Title 1",...];
    // textArea: req.body.textArea ||[],   // ['mbrs-Etiam sit nec nunc auctor',..]
    // imgSrcs: req.body.imgSrcs || [],          // ["../../../common/images/homepage-photo3.jpg"];
    // imgTextArea: req.body.imgTextArea ||[],  // ["This is a great picture",...]
    // linkTarget: req.body.linkTarget || [],   // ["href='#'",...];
    // linkTextArea: req.body.linkTextArea ||[] // ["Click here for more",...];


    var now = new Date();
    //var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var dfltDtFrmt = now.getFullYear()+"-"+now.getMonth() + "-" + now.getDate();  // this can be used in Date( dfltDtFrmt ) to get back to a date Object

    var pKeyId = uuid.v1();
    var cKeyId = uuid.v1();
    var rKeyId = uuid.v1();
    var typeLabel = req.body.typeLabel || "missing";

    var query = "CREATE ( p01:Components:" + typeLabel + ":Profile { profile } ), (c01:Components:" + typeLabel + ":ContentSet { contentSet } ), ((p01)-[ :HAS { relationShip }]->(c01))" ;

    var parms = { profile: {
                    keyId:  pKeyId,
                    groupLabel: req.body.groupLabel  ||  "Components",
                    typeLabel: req.body.typeLabel  ||  typeLabel,
                    partLabel: req.body.partLabel  || "Profile",
                    lblArray : req.body.lblArray  ||  "Errors",             // ** Required,,  'groupLabel:typeLabel:partlabel'
                    userKeyId: req.body.userKeyId  ||  "Errors",            // ** Required,,   "",      ** Required, derived from "member" user who is will be "Owner"
                    createdByKeyId: req.body.createdByKeyId  ||  "Errors",  // ** Required,,    "", ** Required, derived from "Creator" user  Sysadmin, Owner
                    dateCreated: req.body.dateCreated  ||  new Date(),    // ** Required set at time of obj creation
                    status: req.body.status  ||  "Errors",
                    approveDate: req.body.approveDate  ||  new Date(),   // ** Required, set at time of obj creation
                    approvedById: req.body.approvedById  ||  "Errors",   //  ** Required, set at time of obj creation
                    lastUpdated: req.body.lastUpdated  ||  new Date(),         // ** Required but updated as changes occur
                    updatedById: req.body.updatedById  ||  "Errors",        //   Required, but updated as changes occur
                    isActive: req.body.isActive  ||  false,                // ** Required, set false at creation-relates to state of
                    name: req.body.name  ||  "missing",                          // NOTE: Each component type has a more or less the same profile
                    street: req.body.street  ||  "missing",
                    street2:  req.body.street2  || "missing",
                    city: req.body.city  ||  "missing",
                    state: req.body.state  || "missing",
                    zipCode: req.body.zipCode || "missing",
                    phone: req.body.phone  || "missing",
                    fax: req.body.fax  || "missing",
                    webSite: req.body.webSite || "missing",
                    email: req.body.email  || "missing",
                    identityId: req.body.identityId || "missing",
                    description: req.body.description || "missing",
                    category: req.body.category || [],
                    keywords: req.body.keywords || []
                    },
                contentSet:   req.body.contentSet ,
                relationShip: {
                   keyId: rKeyId,
                   groupLabel: req.body.groupLabel || "Components",    // var relationShip =
                   typeLabel: req.body.typeLabel || typeLabel,
                   status: req.body.relShipStatus || "Inactive",             // dflt: Inactive Public,Internal Only Private,Inactive
                   isActive: req.body.relShipIsActive || false,             // true || false
                   profileKeyId: pKeyId,
                   contentKeyId: cKeyId,
                   lastUpdated: req.body.relShipLastUpdated || dfltDtFrmt.toString(),
                   updatedById: req.body.relShipUpdatedById || "Errors",
                   setId:  req.body.relShipSetId || "Errors",                //  "tab0-1";
                   setName:  req.body.relShipSetName || "Errors",            //  "My Home"
                   setKey:  req.body.relShipSetKey || "Errors",              // "100";
                   setHref:  req.body.relShipSetHref || "Errors",            // "href='#tab0-1'"
                   setLabelText:  req.body.relShipSetLabelText || "Errors",  // "My Home"
                   setTemplate:  req.body.relShipSetTemplate || "Errors",  // "My Home"
                   setOrder:  req.body.relShipSetOrder || "Errors",          // 1   the tab order as an example

                   }
                };
    // assign keyId to content
    parms.contentSet.keyId = cKeyId;

    console.log("DEBUG: neo4Components.postProfileContentSet : query params: " );
    console.log( query );
    console.log( parms );

    neo4api.cypherRunQuery( query, parms , include_stats, function (err, results) {
        if(err)
        {
                res.send(500, "ERROR: neo4Components.postProfileContentSet : create set : err : " + err.message);
                console.log("ERROR: neo4Components.postProfileContentSet : create set  : err :", err);
                return next();
        }
        else if ( results )
        {
            //      we get back an array of relationships that matched the query      0
            //      next loop over the array testing for otherId
            //      then delete that relationship by id

            //var tmpFld =  results["data"].toString();
            console.log("DEBUG: neo4Components.postProfileContentSet : return component set from create data ", results );
            //console.log( tmpFld);
            if ( results['data'].length ) {

                res.send(200, results );
            }
            else {
                res.send(200, null );
            }

            return next();
        }
    });
}


/* POST COMPONENT CONTENT SET:
*
****  New Component ContentSet and Relationship data elements. This process is
*     launched from the Component admin ContentSet Manager. At a minimum this process will create
      1 ContentSet element (node) and 1 RelationShip element ( relContentSet ) per request.
      Detecting multiple contentSets the process will proceed to create pairs of ContentSets and Relationship elements until
      the contentSets have been fully processed. The query set and params that would have been populated durning this time
      will them be submitted to Ne04j Api. If while processing this query/parms set an error occurs cypher will terminate processing
      this query set and return an error message. All db data will be rolled back to before update state.

****
*/

function postContentSetV1(req, res, next) {

    var parms = "";
    var include_stats = true;

    // var cLabels = ['Components','Mbrsite',part];     // part: Profile,Tabset or Panes

    //  var parms = { where: {
    //                  profile: {
    //                      userKeyId : user.keyId,
    //                      name : '',
    //                      pId : '',
    //                      isActive : true,
    //                      status : '',
    //                      permission : ''
    //                      },
    //                  relationShip: {
    //                      isActive : true,
    //                      status : ''
    //                      }
    //                  },
    //                  cLabels: cLabels
    //                  };


    var now = new Date();
    //var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var dfltDtFrmt = now.getFullYear()+"-"+now.getMonth() + "-" + now.getDate();  // this can be used in Date( dfltDtFrmt ) to get back to a date Object

   // var pKeyId = uuid.v1();
    var cKeyId = uuid.v1();
    var rKeyId = uuid.v1();
    var typeLabel = req.body.typeLabel || "missing";



    // init query with a match that gets us the profile
    var query = "MATCH (pro:Components:" + typeLabel + ":Profile) ";
    // create the where clause for profile match query
    var whereProps =  { profile: { keyId: req.body.profileKeyId }};   // req.query.where;
    var wrkWSP =  whereSetProperties( whereProps, true , '');
    var where = " WHERE " + wrkWSP.where;

    console.log("DEBUG: neo4Components.postContentSetV1 : where: ", where );
    //
    var create = " CREATE (con:Components:" + typeLabel + ":ContentSet { contentSet } ), (pro)-[ rel:HAS { relationShip }]->(con)" ;

     // assemble return data
    var returnData = " RETURN pro,rel, con ";
    // assemble where to query
    query = query.concat( where ) ;
    // assemble create to query
    query = query.concat( create ) ;
    // assemble return data to query
    query = query.concat( returnData ) ;

    // NOTE PROFILE KEYID COMMING FROM EXISTING PROFILE FOUND WITH MATCH PART OF QUERY
    var parms = { contentSet: req.body.contentSet,

                relationShip: {
                   keyId: rKeyId,
                   groupLabel: req.body.groupLabel || "Components",    // var relationShip =
                   typeLabel: req.body.typeLabel || typeLabel,
                   status: req.body.relShipStatus || "Inactive",             // dflt: Inactive Public,Internal Only Private,Inactive
                   isActive: req.body.relShipIsActive || false,             // true || false
                   profileKeyId: req.body.profileKeyId,            // NOTE PROFILE KEYID FROM EXISTING PROFILE
                   contentKeyId: cKeyId,
                   lastUpdated: req.body.relShipLastUpdated || dfltDtFrmt.toString(),
                   updatedById: req.body.relShipUpdatedById || "Errors",
                   setId:  req.body.relShipSetId || "Errors",                //  "tab0-1";
                   setName:  req.body.relShipSetName || "Errors",            //  "My Home"
                   setKey:  req.body.relShipSetKey || "Errors",              // "100";
                   setHref:  req.body.relShipSetHref || "Errors",            // "href='#tab0-1'"
                   setLabelText:  req.body.relShipSetLabelText || "Errors",  // "My Home"
                   setTemplate:  req.body.relShipSetTemplate || "Errors",  // "My Home"
                   setOrder:  req.body.relShipSetOrder || "Errors",          // 1   the tab order as an example

                   }
                };

    // assign keyId value for contentset;
    parms.contentSet.keyId = cKeyId;


    console.log("DEBUG: neo4Components.postContentSetV1 : query params: " );
    console.log( query );
    console.log( parms );

    neo4api.cypherRunQuery( query, parms , include_stats, function (err, results) {
        if(err)
        {
                res.send(500, "ERROR: neo4Components.postContentSetV1 : create set : err : " + err.message);
                console.log("ERROR: neo4Components.postContentSetV1 : create set  : err :", err);
                return next();
        }
        else if ( results )
        {
            //      we get back an array of relationships that matched the query      0
            //      next loop over the array testing for otherId
            //      then delete that relationship by id

            //var tmpFld =  results["data"].toString();
            console.log("DEBUG: neo4Components.postContentSetV1 : return component set from create data ", results );
            //console.log( tmpFld);
            if ( results['data'].length ) {

                res.send(200, results );
            }
            else {
                res.send(200, null );
            }

            return next();
        }
    });
}



/* GET COMPONENT PROFILE and CONTENT SET:
*
****  Get a Components data set which at a minimum will consis of  1 Profle element (node) , 1 Content element (node) and
      1 RelationShip element ( relationship ).

****
*/

function getProfileContentSetsV1(req, res, next) {

    console.log("DEBUG: neo4Components.getProfileContentSetsV1: req.query pre ", req.query  );

    /* parse req.body into workable vars */
    // N/A
    // var setProps = '';
    // setProps = { contentSet: req.query[0] };

    var whereProps =  JSON.parse(req.query.where);   // req.query.where;
    //var temp = cLabels = {req.body[2];
    console.log("DEBUG: neo4Components.getProfileContentSetsV1: json.parse whereProps ", whereProps );
    var groupLabel = req.query.cLabels[0];
    var typeLabel = req.query.cLabels[1];
    var partLabel = req.query.cLabels[2];



    // assemble where/set/properties for cypher query
    var wrkWSP =  whereSetProperties( whereProps, true , '');

    console.log("DEBUG: neo4Components.getProfileContentSetsV1: post wrkWSP=whereSet  ", wrkWSP );

    // begin to assemble where claues
    var where = " WHERE " + wrkWSP.where;

    // N/A
    //var set = " SET " + wrkWSP.set; // + "c.textAreaTitle=['" +  tempBody.textAreaTitle + "']";

    // assemble parms for params
    var parms = wrkWSP.properties;


    /* need to capture REF name from  whereSetProperties and use here ex: ref = ['pro','rel,'con'] */
    var query = "MATCH ( pro:Components:"+typeLabel+":Profile)-[ rel:HAS ]->(con:"+typeLabel+":ContentSet)";

    // assemble return data
    var returnData = " RETURN pro as ps, rel as rs, con as cs ORDER BY rel.isActive, rel.status, rel.setOrder";

    // assemble query
    query = query.concat( where ) ;
    // n/a query = query.concat( set );
    query = query.concat( returnData ) ;

    //var parms = "";
    var include_stats = false;
    // audit the whole works at the console
    console.log("DEBUG: neo4Components.getProfileContentSetsV1 : query where parms: " );
    console.log( query );
    console.log( where );
    // n/a console.log( set );
    console.log( parms );


   /***************************************/

    // var groupLabel = req.query.cLabels[0];
    // var typeLabel = req.query.cLabels[1];
    // var partLabel = req.query.cLabels[2];

    // var query = "MATCH ( pro:Components:" + typeLabel + ":" + partLabel + ")" + "-[ rel:HAS ]->(con:)";
    // var returnData = " RETURN p,r,c ";

    // var where = " WHERE ";
    // var wName = 'p';
    // var wProps =   JSON.parse(req.query.profile );
    // var wAppendToken = " AND "
    // var wStartToken = "'"
    // var wEndToken = "'"
    // console.log("DEBUG: neo4Components.getProfileContentSetsV1: preWhere.match wProps ", wProps );
    // // build where for match
    // where = where.concat( objToPredicate( wName,wProps,wAppendToken, wStartToken, wEndToken ) );
    // var wName = 'r';
    // var wProps =  JSON.parse(req.query.relationShip);
    // // build where for relationship
    // console.log("DEBUG: neo4Components.getProfileContentSetsV1: preWhere.rel wProps ", wProps );

    // where = where.concat( wAppendToken + objToPredicate( wName,wProps,wAppendToken, wStartToken, wEndToken ) );


    // query = query.concat( where ) ;
    // query = query.concat( returnData ) ;

    // var parms = "";
    // var include_stats = true;

    // console.log("DEBUG: neo4Components.getProfileContentSetsV1 : query params: " );
    // console.log( query );

    neo4api.cypherRunQuery( query, parms , include_stats, function (err, results) {
        if(err)
        {
                res.send(500, "ERROR: neo4Components.getProfileContentSetsV1 : get set : err : " + err.message);
                console.log("ERROR: neo4Components.getProfileContentSetsV1 : get set  : err :", err);
                return next();
        }
        else if ( results )
        {
            /*  using this query:

                query = "MATCH ( p:Components:" + typeLabel + ":" + partLabel + ")" + "-[ r:HAS ]->(c) RETURN p,r,c ";

                if data is returned the results will appear as three Objects which contain arrays:
                  { Columns ['p', 'r', 'c'] },
                    data : [[[object],[object],[object]]],
                    stats: { stuff refer to manual }
                  }

                  There are at a minimum three data objects returned. The relationShips between then is One Profile [p] to
                  to many RelationShip [r] and ContentSet [c] pairs of objects ( see columns for names ).

            */



            if ( results['data'].length ) {
                // clean up retdata
                console.log("DEBUG: neo4Components.getProfileContentSetsV1 : return component set data ", results['data'] );  //
                var first = true;
                var retDat = {};
                var row = {};

                for ( var key in results['data'] ) {
                    row = results['data'][key];
                    if (first) {
                      //  profile
                      retDat.profile = row[0] ;
                      // others
                      retDat.relShip = {};
                      retDat.contentSet = {};
                      retDat.relShip[key] =   new Object(  row[1] );
                      retDat.contentSet[key] =  new Object(  row[2] );
                      //retDat.contentSet[row[2]['keyId']] =  new Object(  row[2] );
                      //console.log("DEBUG: neo4Components.getComponentSetV1 : return component set row[2] ", row[2]['keyId']);
                    } else {
                     retDat.relShip[key] =  new Object(  row[1] );
                     retDat.contentSet[key] =  new Object(  row[2] );
                     //console.log("DEBUG: neo4Components.getComponentSetV1 : return component set row[2] ", row[2]['keyId']);
                    }
                    first = false;
                }
                console.log("DEBUG: neo4Components.getProfileContentSetsV1 : return component set data ", retDat );  //
                res.send(200, retDat);  // sults['data'] );  //['data']
            }
            else {
                res.send(200, null );
            }

            return next();
        }
    });
}

/* GET COMPONENT PROFILE(s) for specific context :
*
****  Get a Components Profie data which will consis of  1 Profle element (node)

****
*/

function getContextProfileV1(req, res, next) {

    console.log("DEBUG: neo4Components.getContextProfileV1: req.query pre ", req.query  );

    /* parse req.body into workable vars */
    // N/A
    // var setProps = '';
    // setProps = { contentSet: req.query[0] };

    var whereProps =  JSON.parse(req.query.where);   // req.query.where;
    //var temp = cLabels = {req.body[2];

    var groupLabel = req.query.cLabels[0];
    var typeLabel = req.query.cLabels[1];
    var partLabel = req.query.cLabels[2];



    // assemble where/set/properties for cypher query
    var wrkWSP =  whereSetProperties( whereProps, true, '');

    console.log("DEBUG: neo4Components.getContextProfileV1: post wrkWSP=whereSet  ", wrkWSP );

    // begin to assemble where claues
    var where = " WHERE " + wrkWSP.where;

    // N/A
    //var set = " SET " + wrkWSP.set; // + "c.textAreaTitle=['" +  tempBody.textAreaTitle + "']";

    // assemble parms for params
    var parms = wrkWSP.properties;


    /* need to capture REF name from  whereSetProperties and use here ex: ref = ['pro','rel,'con'] */
    // var query = "MATCH ( pro:Components:"+typeLabel+":Profile)-[ rel:HAS ]->(con:"+typeLabel+":ContentSet)";
    var query = "MATCH ( pro:Components:"+typeLabel+":Profile)";

    // assemble return data
    var returnData = " RETURN pro as ps ORDER BY ps.name";

    // assemble query
    query = query.concat( where ) ;
    // n/a query = query.concat( set );
    query = query.concat( returnData ) ;

    //var parms = "";
    var include_stats = false;
    // audit the whole works at the console
    console.log("DEBUG: neo4Components.getContextProfileV1 : query where parms: " );
    console.log("query: ", query );
    console.log("where: ", where );
    // n/a console.log( set );
    console.log("param: ", parms );


   /***************************************/

    // var groupLabel = req.query.cLabels[0];
    // var typeLabel = req.query.cLabels[1];
    // var partLabel = req.query.cLabels[2];

    // var query = "MATCH ( pro:Components:" + typeLabel + ":" + partLabel + ")" + "-[ rel:HAS ]->(con:)";
    // var returnData = " RETURN p,r,c ";

    // var where = " WHERE ";
    // var wName = 'p';
    // var wProps =   JSON.parse(req.query.profile );
    // var wAppendToken = " AND "
    // var wStartToken = "'"
    // var wEndToken = "'"
    // console.log("DEBUG: neo4Components.getProfileContentSetsV1: preWhere.match wProps ", wProps );
    // // build where for match
    // where = where.concat( objToPredicate( wName,wProps,wAppendToken, wStartToken, wEndToken ) );
    // var wName = 'r';
    // var wProps =  JSON.parse(req.query.relationShip);
    // // build where for relationship
    // console.log("DEBUG: neo4Components.getProfileContentSetsV1: preWhere.rel wProps ", wProps );

    // where = where.concat( wAppendToken + objToPredicate( wName,wProps,wAppendToken, wStartToken, wEndToken ) );


    // query = query.concat( where ) ;
    // query = query.concat( returnData ) ;

    // var parms = "";
    // var include_stats = true;

    // console.log("DEBUG: neo4Components.getProfileContentSetsV1 : query params: " );
    // console.log( query );

    neo4api.cypherRunQuery( query, parms , include_stats, function (err, results) {
        if(err)
        {
                res.send(500, "ERROR: neo4Components.getContextProfileV1 : get set : err : " + err.message);
                console.log("ERROR: neo4Components.getContextProfileV1 : get set  : err :", err);
                return next();
        }
        else if ( results )
        {
            /*  using this query:

                query = "MATCH ( p:Components:" + typeLabel + ":" + partLabel + ")" + "-[ r:HAS ]->(c) RETURN p,r,c ";

                if data is returned the results will appear as three Objects which contain arrays:
                  { Columns ['p', 'r', 'c'] },
                    data : [[[object],[object],[object]]],
                    stats: { stuff refer to manual }
                  }

                  There are at a minimum three data objects returned. The relationShips between then is One Profile [p] to
                  to many RelationShip [r] and ContentSet [c] pairs of objects ( see columns for names ).

            */



            if ( results['data'].length ) {
                // clean up retdata
                console.log("DEBUG: neo4Components.getContextProfileV1 : return component set data ", results['data'] );  //
                var first = true;
                var retDat = {};
                var row = {};

                //for ( var key in results['data'] ) {
                    //row = results['data'][key];
                    //if (first) {
                      //  profile
                      retDat = results['data'] ;
                      // others
                      // retDat.relShip = {};
                      // retDat.contentSet = {};
                      // retDat.relShip[key] =   new Object(  row[1] );
                      // retDat.contentSet[key] =  new Object(  row[2] );
                      //retDat.contentSet[row[2]['keyId']] =  new Object(  row[2] );
                      //console.log("DEBUG: neo4Components.getComponentSetV1 : return component set row[2] ", row[2]['keyId']);
                    //} else {
                    // retDat.relShip[key] =  new Object(  row[1] );
                    // retDat.contentSet[key] =  new Object(  row[2] );
                     //console.log("DEBUG: neo4Components.getComponentSetV1 : return component set row[2] ", row[2]['keyId']);
                    //}
                    //first = false;
                //}
                console.log("DEBUG: neo4Components.getContextProfileV1 : return component profile data ", retDat );  //
                res.send(200, retDat);  // sults['data'] );  //['data']
            }
            else {
                res.send(200, null );
            }

            return next();
        }
    });
}




// ******************************
// MATCH (:Mbrsite { keyId:'1888f720-f129-11e4-8bfd-2574a4428637' })-[r {contentKeyId:'1888f721-f129-11e4-8bfd-2574a4428637'}]->(content)
// SET r.setKey = 101
// RETURN r




// Neo4j.prototype.updateNodeByFltrKey = function(labelsArray, filterPropertyName, filterPropertyValue, node_data, callback){
//   var query =  'MATCH ( wrk:' + labelsArray + ') WHERE wrk.' + filterPropertyName + " = " + "'" + filterPropertyValue + "'" + ' SET ' + cypher.set('wrk', node_data) + ' RETURN wrk';
//   this.cypherQuery(query , node_data, function(err, res) {
//     err? callback(err): callback(err, res.data[0]);
//   });
// };


// Neo4j.prototype.updateNodeById = function(node_id, node_data, callback){
//   var query = 'START data=node({_id}) SET ' + cypher.set('data', node_data) + ' RETURN data';
//   node_data._id = node_id;
//   this.cypherQuery(query , node_data, function(err, res) {
//     err? callback(err): callback(err, res.data[0]);
//   });
// };

// Sample console query update
//
//  MATCH ( p:Components:Mbrsite:Profile)-[ r:HAS ]->(c)
//  where r.contentKeyId = 'fe237521-f2b0-11e4-9974-d99a6f5a39b1'
//  SET c.textAreaTitle = ['About Us']
//  RETURN p,r,c


//**********************************

/*
 * REST PUT : Update Component
   server.put('/api/v1/components', componentApi.putComponentUpdateV1);
*/

function putComponentUpdateV1(req, res, next) {

    console.log("DEBUG: neo4Components.putComponentUpdateV1: pre ", req.body);
    /* parse req.body into workable vars */
    var setProps = new Object();
    setProps[ req.body[1].set ] =  req.body[0] ;   // setProps = { contentSet: req.body[0] };

    var whereProps = req.body[1].where;
    var groupLabel = req.body[1].cLabels[0];
    var typeLabel =  req.body[1].cLabels[1];
    var partLabel =  req.body[1].cLabels[2];

    console.log("DEBUG: neo4Components.putComponentUpdateV1: post body ", setProps, whereProps );

    // assemble where/set/properties for cypher query
    var wrkWSP =  whereSetProperties( whereProps, true ,setProps);

    // begin to assemble where claues
    var where = " WHERE " + wrkWSP.where;

    // assemble set clause fro contentSet ( creates custom property names for property values )
    // set: "user.firstname={xQ_3}",
    //var wrkClause = whereSetProperties ('c', '',  setProps );
    var set = " SET " + wrkWSP.set; // + "c.textAreaTitle=['" +  tempBody.textAreaTitle + "']";

    // assemble parms for params
    var parms = wrkWSP.properties;

    /* need to capture REF name from  whereSetProperties and use here ex: ref = ['pro','rel,'con'] */
    var query = "MATCH ( pro:Components:"+typeLabel+":Profile)-[ rel:HAS ]->(con:"+typeLabel+":ContentSet)";

    // assemble return data
    var returnData = " RETURN pro,rel,con ";

    // assemble query
    query = query.concat( where ) ;
    query = query.concat( set );
    query = query.concat( returnData ) ;

    //var parms = "";
    var include_stats = true;
    // audit the whole works at the console
    console.log("DEBUG: neo4Components.putComponentUpdateV1 : query where set parms: " );
    console.log( query );
    console.log( where );
    console.log( set );
    console.log( parms );


    neo4api.cypherRunQuery( query, parms , include_stats, function (err, results) {
        if(err)
        {
                res.send(500, "ERROR: neo4Components.putComponentUpdateV1 : get set : err : " + err.message);
                console.log("ERROR: neo4Components.putComponentUpdateV1 : get set  : err :", err);
                return next();
        }
        else if ( results )
        {
            /*  using this query:

                query = "MATCH ( p:Components:" + typeLabel + ":" + partLabel + ")" + "-[ r:HAS ]->(c) RETURN p,r,c ";

                if data is returned the results will appear as three Objects which contain arrays:
                  { Columns ['p', 'r', 'c'] },
                    data : [[[object],[object],[object]]],
                    stats: { stuff refer to manual }
                  }

                  There are at a minimum three data objects returned. The relationShips between then is One Profile [p] to
                  to many RelationShip [r] and ContentSet [c] pairs of objects ( see columns for names ).

            */

            console.log("DEBUG: neo4Components.putComponentUpdateV1 : return component set data ", results['data'] );

            if ( results['data'].length ) {

                res.send(200, results['data'] );
            }
            else {
                res.send(200, null );
            }

            return next();
        }
    });
}


// ******************************
// BELOW THIS POINT IS OLDER CODE FOR NODE RETRIEVAL - may not have used cypher as retrieve process





//**********************************

/*
 * REST PUT : Update Component
   server.put('/api/v1/users', api.putUserV1);
*/
function putComponentV1(req, res, next) {
    var  targetIdName = "_id" ;
    var  targetId = req.body._id;

    //console.log("DEBUG: putComponentV1 : pre keyId :************ ", req.body,  targetKeyId);

    var node_data =  req.body ;

    // don't allow user updates to nodes with null or '' keyId
    if ( req.body.keyId === '' | req.body.keyId === null  ) {
        //because keyId was '' or null then use _id for lookup but create and update keyId
        //  targetId and targetIdName already set;
        //targetKeyId =  uuid.v1();
        node_data.keyId =  uuid.v1();
    }
    else {
        // because there was a keyId value present then use it instead of _id
        targetIdName = "keyId" ;
        targetId =  req.body.keyId;

    };

    console.log("DEBUG: putComponentV1 : post keyId :************ ", targetId, req.body.keyId );
    console.log("DEBUG: putComponentV1 : pre  nodedata req.body :************ ", req.body );
    // properties that are not updateable by "member" user
    // *******
    //        labelId: "mbrsite",     ** Required  "mbrsite"
    //        _id  : "";              ** Required, assigned by server
    //        keyId: "",              ** Required, assigned by server
    //        userKeyId: "",          ** Required, derived from user
    //        datecreated: "",        ** Required, set at time of obj creation
    //        status: "",             ** Required, set at system level-relates to validity of
    //        approveDate: "",

    // var node_data = {
    //         keyId : req.body.keyId || targetKeyId,
    //         lastUpdated: req.body.lastUpdated || "",
    //         updatedById: req.body.updatedById || "",
    //         isActive: req.body.isActive || "",
    //         name : req.body.name || "",
    //         street : req.body.street || "",
    //         street2 : req.body.street2 || "",
    //         city : req.body.city || "",
    //         state : req.body.state || "",
    //         zipCode : req.body.zipCode || "",
    //         phone : req.body.phone || "",
    //         fax: req.body.fax || "",
    //         website: req.body.website || "",
    //         category: req.body.category || "",
    //         description: req.body.description ||  "",
    //         identityId : req.body.indentityId || ""


    //     };

    // obs moved to top of routine:  var node_data =  req.body ;
    // obs moved to top of routine:  node_data.keyId =  targetKeyId;

    var lblArray =  req.body.lblArray;  // 'Components:Mbrsite:Profile'
    //[ groupLabel , typeLabel, partLabel ]
    neo4api.updateByFldKey( lblArray , targetIdName , targetId, node_data , function(err, user) {

            var resultsCnt = [user].length ;
            console.log("DEBUG: putComponentV1.updateByFldKey : resultsCnt w/err keyId: ", resultsCnt, err, targetId );
            if(err) {
                console.log("ERROR: putComponentV1.updateByFldKey err :", err);
                res.send(500, "ERROR: putComponentV1.updateByFldKey : update");
                return next();
            }
            else if(   1 === resultsCnt ) {

                res.send(200);
                return next();
            }
            else {

                res.send(400, "ERROR: putComponentV1.updateByFldKey: Was not able to update user with keyId : " + user.keyId);
                return next();
        }
    });


}



// ********************************










/*
 * REST POST : Create component
   server.post('/api/v1/users', api.postUserV1);
*/
function postComponentV1(req, res, next) {

    var lblArrayProfile = req.body.lblArray;

	var newComponentProfile = {
            //       _id                  **  assigned by server
            keyId:        uuid.v1(),                          //** Required
            groupLabel:   req.body.groupLabel || "Errors",    // ** Required,,   "Component" ,   ** Required,  "Component"
            typeLabel :   req.body.typeLabel  || "Errors",    // ** Required,  "Mbrsite",      ** Required, "mbrsite"
            partLabel:    req.body.partLabel  || "Errors",    // ** Required,,   "Profile"       ** Required, "Profile"
            lblArray :    req.body.lblArray   || "Errors" ,   // ** Required,,  'groupLabel:typeLabel:partlabel'
            userKeyId:    req.body.userKeyId || "Errors",    // ** Required,,   "",      ** Required, derived from "member" user who is will be "Owner"
            createdByKeyId: req.body.createdByKeyId  || "Errors",         // ** Required,,    "", ** Required, derived from "Creator" user  Sysadmin, Owner
            dateCreated:  req.body.dateCreated || new Date(),         // ** Required set at time of obj creation
            status:       req.body.status  || "Errors",    // ** Required,   ** Required, set at system level-relates to validity of
            approveDate: req.body.approveDate || new Date(),         // ** Required, set at time of obj creation
            approvedById: req.body.approvedById  || "Errors",   //  ** Required, set at time of obj creation
            lastUpdated: req.body.lastUpdated || new Date(),         // ** Required but updated as changes occur
            updatedById: req.body.updatedById  || "Errors",     //   Required, but updated as changes occur
            isActive: req.body.isActive  || false,             // ** Required, set false at creation-relates to state of
            name: req.body.name || "Errors",           // NOTE: Each component type has a more or less the same profile
            // optional properties  from form
            street: req.body.street || "",
            street2: req.body.street2 || "",
            city: req.body.city || "",
            state: req.body.state || "",
            zipCode: req.body.zipCode || "",
            phone: req.body.phone || "",
            fax: req.body.fax || "",
            webSite: req.body.webSite || "",
            email: req.body.email || "",
            identityId: req.body.identityId || "",
            category: req.body.category || [],
            keywords: req.body.keywords || [],
            /* obs 2050317
            // tabSetBar
            tabBarId: req.body.tabBarId || [],
            tabBarHref: req.body.tabBarHref || [],
            tabBarLabelText: req.body.tabBarLabelText || [],
            tabBarisActive: req.body.tabBarisActive || [],
            tabBarOrder: req.body.tabBarOrder || [],
            // tabPanes for each tabPane there can be several ContentModel entries
            paneTabIds: req.body.paneTabIds || [],                         // ["tab0-1","tab0-2"];
            paneTabTitles: req.body.paneTabTitles || [],                    // ["Blank Template Two", "Blank Template Three"]
            paneTabContentIds: req.body.paneTabContentIds || [],
            paneTabContentKeyIds: req.body.paneTabContentKeyIds || [],
            */
            tabPaneLastUpdated: req.body.tabPaneLastUpdated ||  ["Errors"],
            tabPaneUpdatedById: req.body.tabPaneUpdatedById ||  ["Errors"],
            tabPaneIsActive: req.body.tabPaneIsActive ||  [false],
            tabPaneStatus: req.body.tabPaneStatus ||  ["Inactive"],  // dflt: Inactive Public,Internal Only Private,Inactive

            tabPaneBarId: req.body.tabPaneBarId ||  [],  //  [100,200,300,400,500,600];
            tabPaneBarHref: req.body.tabPaneBarHref ||  [],  // ['href="#tab0-1"','href="#tab1-1"','href="#tab1-2"','href="#tab1-3"','href="#tab2-1"','href="#tab3-1"'];
            tabPaneBarLabelText: req.body.tabPaneBarLabelText ||  [],  //["My Home","Blank 1","Blank 2","Blank 3","Grid 1","Google Map"];
            tabPaneBarIsActive: req.body.tabPaneBarIsActive ||  [],  //[true,false,false,false,false,false];
            tabPaneBarOrder: req.body.tabPaneBarOrder ||  [],  //[1,2,3,4,5,6] ;
            // tabPane for each paneTab there can be several ContentModel entries
            tabPaneTabIds: req.body.tabPaneTabIds ||  [],  //["tab1-1","tab1-2"];
            tabPaneTabTitles: req.body.tabPaneTabTitles ||  [],  //["Blank Template One","Blank Template Two"];
            // the following arrays are filled with the _id and KeyId of each contentModel created as described
            // below.
            tabPaneTabContentIds: req.body.tabPaneTabContentIds ||  [],  //
            tabPaneTabContentKeyIds: req.body.tabPaneTabContentKeyIds ||  []  //

    };

     /* TAB PANE SET DATA

                           NOTE: New Biz Form data loaded earlier in this function with this
                           $scope.savedComponent = angular.copy(addNuUser.nucomponent);

    */
     //  var lblArrayTP = req.body.tabPaneLblArray;
    // var newComponentTabPane = {

    //     // tabPaneGroupLabel: req.body.tabPaneGroupLabel || "Components",  // "Components";
    //     // tabPaneLblArray: req.body.tabPaneLblArray ||  "Errors",  // "Components";

    //     // tabPaneTypeLabel: req.body.tabPaneTypeLabel ||  "Errors",
    //     // tabPanePartLabel: req.body.tabPanePartLabel ||  "Errors",
    //     // tabPaneUserKeyId: req.body.tabPaneUserKeyId ||  "Errors",
    //     // tabPaneCreatedByKeyId: req.body.tabPaneCreatedByKeyId ||  "Errors",
    //     // tabPaneDateCreated: req.body..tabPaneDateCreated ||  "Errors",

    //     tabPaneLastUpdated: req.body.tabPaneLastUpdated ||  "Errors",
    //     tabPaneUpdatedById: req.body.tabPaneUpdatedById ||  "Errors",
    //     tabPaneIsActive: req.body.tabPaneIsActive ||  false,
    //     tabPaneStatus: req.body.tabPaneStatus ||  "Inactive",  // dflt: Inactive Public,Internal Only Private,Inactive

    //     tabPaneBarId: req.body.tabPaneBarId ||  [],  //  [100,200,300,400,500,600];
    //     tabPaneBarHref: req.body.tabPaneBarHref ||  [],  // ['href="#tab0-1"','href="#tab1-1"','href="#tab1-2"','href="#tab1-3"','href="#tab2-1"','href="#tab3-1"'];
    //     tabPaneBarLabelText: req.body.tabPaneBarLabelText ||  [],  //["My Home","Blank 1","Blank 2","Blank 3","Grid 1","Google Map"];
    //     tabPaneBarIsActive: req.body.tabPaneBarIsActive ||  [],  //[true,false,false,false,false,false];
    //     tabPaneBarOrder: req.body.tabPaneBarOrder ||  [],  //[1,2,3,4,5,6] ;
    //     // tabPane for each tabPane there can be several ContentModel content entries per tabPane which are written to DB in one Node/document
    //     tabPaneTabIds: req.body.tabPaneTabIds ||  [],  //["tab1-1","tab1-2"];
    //     tabPaneTabTitles: req.body.tabPaneTabTitles ||  [],  //["Blank Template One","Blank Template Two"];
    //     // the following arrays are filled with the _id and KeyId of each contentModel created as described
    //     // below.
    //     tabPaneTabContentIds: req.body.tabPaneTabContentIds ||  [],  // each Id / KeyId entry represent all contentModels for a tabPane
    //     tabPaneTabContentKeyIds: req.body.tabPaneTabContentKeyIds ||  []  //

    // };


    /* TAB CONTENT MODEL DATA
    */
    // ContentModels: for each contentModel there are four content entries title,text,imgsrc, imgTextArea, linkTarget, LinkTextArea
    /* 20150317 : Note:    Currently for testing purposes this process creates a single node/document per page.
    *
    *             in the code below the contentModel Array will be broken into individual
    *             node/documents based on the contentTabIds. In sample data below four contentModel sets are represented.
    *             One of the modelLabels (tab0-1) uses the "model" key word to identify the source set Member:Profile
    *             that contains data extracted from the profile. Other Modellables use a "This: key word indentifing the source which is
    *             found in this content array element. These samples will result in one contentModel node/document being written to the
    *             database. If the contentModel. Had additonal data (contentModels) to persist in the DB then an additional array element is created and inserted
    *             following the last occurance of the matching tabId.


    *
    // keyId   assigned by DB
    // ** required properties
    */
    var lblArrayTabContent = req.body.contentLblArray;
    var newComponentTabContent = {
        //       _id                  **  assigned by server
        keyId:        uuid.v1(),      //** Required
        contentGroupLabel:     req.body.contentGroupLabel ||  "Components",  //
        contentLblArray:       req.body.contentLblArray || "Errors",
        contentTypeLabel:      req.body.contentTypeLabel  || "Errors",
        contentPartLabel:      req.body.contentPartLabel || "Errors",
        contentUserKeyId:      req.body.contentUserKeyId   || "Errors",
        contentCreatedByKeyId:   req.body.contentCreatedByKeyId   || "Errors",
        contentDateCreated:    req.body.contentDateCreated  || "Errors",
        contentLastUpdated:    req.body.contentLastUpdated  || "Errors",
        contentUpdatedById:    req.body.contentUpdatedById   || "Errors",
        contentIsActive:       req.body.contentIsActive  || false,
        contentStatus:         req.body.contentStatus  || "Inactive",      // dflt: Inactive Public,Internal Only Private,Inactive
        contentModelSrcs:       req.body.contentModelSrcs  || [],            // ["Model", "This","This"];    // dflt: "This"  [ This, Model, ..]
        contentModelIds:       req.body.contentModelIds  || [],            // ["mbrsite.profile,_null, null];
        contentModelLabels:     req.body.contentModelLabels  || [],          // ["tab0-1","tab1-1","tab1-2"];
        contentModelNames:     req.body.contentModelNames   || [],         // ["contentOne","contentTWO"];
        contentModelIsOk:      req.body.contentModelIsOk   || [],          // ["Support","Support"];
        contentModelTitleText:    req.body.contentModelTitleText  || [],   // ["Sample Title 1","Sample Title 2"];
        contentModelTextArea:  req.body.contentModelTextArea  || [],   // ['mbrs-Etiam sit amet leo nec nunc auctor convallis vel at est. Aenean at metus id nisl posuere varius. In vel ipsum eu risus iaculis volutpat in ut metus. Integer eget sem eu massa pharetra vehicula ut id elit. Nam sagittis malesuada est, ut tempus diam ultricies eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla urna libero, finibus in metus mollis, aliquam interdum elit. Phasellus at sagittis lectus. Vestibulum scelerisque libero eget efficitur venenatis. Etiam nunc magna, maximus vel vehicula eget, bibendum eu urna. Phasellus quis turpis id lacus mollis dictum. Ut dui elit, efficitur vel orci eu, elementum interdum erat. Cras at leo volutpat, commodo metus in, dictum justo. Cras id vulputate ex. Nullam et aliquet arcu, id elementum magna. Phasellus faucibus sapien at dolor ultrices pellentesque.','mbrs-Etiam sit amet leo nec nunc auctor convallis vel at est. Aenean at metus id nisl posuere varius. In vel ipsum eu risus iaculis volutpat in ut metus. Integer eget sem eu massa pharetra vehicula ut id elit. Nam sagittis malesuada est, ut tempus diam ultricies eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla urna libero, finibus in metus mollis, aliquam interdum elit. Phasellus at sagittis lectus. Vestibulum scelerisque libero eget efficitur venenatis. Etiam nunc magna, maximus vel vehicula eget, bibendum eu urna. Phasellus quis turpis id lacus mollis dictum. Ut dui elit, efficitur vel orci eu, elementum interdum erat. Cras at leo volutpat, commodo metus in, dictum justo. Cras id vulputate ex. Nullam et aliquet arcu, id elementum magna. Phasellus faucibus sapien at dolor ultrices pellentesque.'];
        contentModelImgSrcs:   req.body.contentModelImgSrcs  || [],   //["../../../common/images/homepage-photo3.jpg","common/bootstrap/images/customer_service2.jpg"];
        contentModelImgTextArea:  req.body.contentModelImgTextArea  ||[],   // ["This is a great picture","Another great picture"];
        contentModelLinkTarget:   req.body.contentModelLinkTarget   || [],   // ["href='#'","href='#'"];
        contentModelLinkTextArea: req.body.contentModelLinkTextArea   ||[]   // ["Click here for more information","Click here for more information"];
     };
    /*
            NOTE: This is sample model for above component as used in the Blank Template Two
                      paneTabId : localMbrsite.paneTabIds[0],
                paneTabTitleText: localMbrsite.paneTabTitles[0],
                        contentOne: {
                          tabId : localMbrsite.contentTabIds[0],
                          tabNameId: localMbrsite.contentTabNameIds[0],
                          isOK: localMbrsite.contentIsOk[0],
                          titleText: localMbrsite.contentModelTitleText[0],
                          textArea: localMbrsite.contentModelTextArea[0],
                          imgSrc:  localMbrsite.contentModelImgSrcs[0],
                          imgTextArea: localMbrsite.contentModelImgTextArea[0],
                          linkTarget:  localMbrsite.contentModelLinkTarget[0],
                          linkTextArea: localMbrsite.contentModelLinkTextArea[0]
                        }

    */




    /*********** Profile insert

      in the following logic the first err bails everyone out.

      NOTE: we really need to rework logic to use the commit rollback methods



    NOTE: NOTE: This logic or different logic needs to be made smarter so that aany conbination of Profile or Content requests can be
          handeled. This only handles one profile ( w/all tab bars) and one tabPane of Content.

    ****************/

    var returnResults = [];

    console.log("DEBUG: postComponentsV1: insert : newComponentTabContent pre  ***: ", newComponentTabContent );

    neo4api.insert(newComponentTabContent, lblArrayTabContent, function(err, contentResults) {
        if(err) {

            console.log("DEBUG: postComponentsV1: insert : newComponentTabContent ***: ", err );
            res.send(500, "ERROR: postComponentsV1 : insert : newComponentTabContent");
            return next();
        }
        else if(contentResults) {

            // note: 20150320  create obj and push to returnResults
            var tmpContent = { 'contentIds': {
                       '_id': contentResults._id,
                       'keyId': contentResults.keyId
                   }
                };
            returnResults.push( tmpContent );

            /********************** profile  ****************/
            // NOTE: add keyId and _id values from Content to Profile
            newComponentProfile.tabPaneTabContentIds.push(contentResults._id );
            newComponentProfile.tabPaneTabContentKeyIds.push(contentResults.keyId );
            console.log("DEBUG: postComponentsV1: insert : newComponentProfile pre  ***: ", newComponentProfile );

            neo4api.insert(newComponentProfile, lblArrayProfile, function(err, profileResults) {

                if(err) {

                    console.log("DEBUG: postComponentsV1: insert : newComponentProfile *****: ", err );
                    res.send(500, "ERROR: postComponentsV1 : insert : newComponentProfile");
                    return next();
                }
                else if(profileResults) {


                    // note: 20150320  create obj and push to returnResults
                    var tmpProfile = { 'profileIds': {
                       '_id': profileResults._id,
                       'keyId': profileResults.keyId
                        }
                    };
                    returnResults.push( tmpProfile );
                    /********************** tab contnet ****************/
                             // orig  res.send(201);
                    // note: 20150235: changed neo4j.js to return result content in addition to status
                    //                 returned values of result  are  result._id and result.keyId
                    //var rresult = result[1];
                    // note: 20150320: changed this process to retun an array of returnResults consisting of
                    //                 _id and keyId for each content and profile item created
                    console.log("DEBUG: postComponentsV1: insert : results *****: ", returnResults );

                    res.json(201, returnResults );
                    return next();

                }

            })
        }
    })
}





/*
 * REST GET : Get components
   server.post('/api/v1/users', api.getUsersV1);
*/

function getComponentsV1(req, res, next) {

/**
 *  Get all nodes with label
 *  Given one label (non-empty string) or multiple labels (array of strings) and one or
 *  more properties in json returns an array of nodes with these labels and properties

    readNodesWithLabelsAndProperties('User',{ firstname: 'Sam', male: true }, callback);
 *       returns an array with nodes with the label 'User' and properties firstname='Sam' and male=true
 **/

    // Neo4j.prototype.findAllNodesWith = function(label, callback)
    //
    // obslete original code - 20140916
    // neo4api.findAllNodesByLabel( ['Users'] , function(err, results) {

    //     //console.log("DEBUG: getUsersV1: findAllNodes : results ");
    //     //console.log(results);

    //     if(err) {

    //         res.send(500, "ERROR: getNodesV1 : findAllNodes");
    //         return next();
    //     }
    //     else {

    //         if (results.length === 0) console.log('nada');

    //         //console.log("Query results: ", results)

    //         res.json(200, results);
    //         return next();
    //     }
    // });

    console.log("DEBUG: neo4Components.getComponentsV1: findNodesWithQuery : req.body pre ", req.query.cLabels  );


    var cLabels = req.query.cLabels;    // ['Components', 'Member' ]
    //var labelId = req.query.labelId ;
    //console.log("DEBUG: getUsersV1: findNodesWithQuery : req.body : " + req.body );
    //var uProperties = {"keyId": "IS NOT NULL", "lastName": "IS NOT NULL"};  lastName : 'Woods', firstName: 'Kimberlie'
    var cUserKeyId  = req.query.userKeyId;
    var cProperties = { userKeyId: cUserKeyId } ;
    console.log("DEBUG: neo4Components.getComponentsV1: findNodesWithQuery : req.body post ", cLabels );
    neo4api.findNodesWithQuery( cLabels , cProperties , function(err, results) {

        console.log("DEBUG: getComponentsV1: findNodesWithQuery :" );
        console.log(results, err);

        if(err) {
            res.send(500, "ERROR: getComponentsV1 : findNodesWithQuery");
            return next();
        }
        else {

            if (results.length === 0) console.log('nada');

            res.json(200, results);
            return next();
        }
    });

}

//********************************

function componentFetchNodesByQueryV1(req, res, next) {


    // example query:
    // MATCH (n:`Users`) WHERE (n.lastName = 'Jones' AND n.firstName <> '' ) RETURN n,COUNT(n) ORDER BY n.lastName, n. firstName SKIP 14  LIMIT 7

   //console.log("DEBUG: userFetchNodesByQueryV1 : req params ID", req.body.fetchBy.fetchLastName);
   // console.log("DEBUG: userFetchNodesByQueryV1 : req params ID", req.body.fetchBy.fetchFirstName);

    //var rootNodeId = req.params.id;
    var newPage =  req.body.newPage - 1;
    var pageSize = req.body.pageSize;
    // build ORDER BY and WHERE Clauses
    var fetchQuery = "WHERE ";
    var fetchOrderBy = "ORDER BY " ;
    var fetchReturns = " n " ;
    // if ( req.body.fetchBy.fetchLastName ) {
    //     fetchOrderBy = fetchOrderBy.concat( "n.lastName, ");
    //     fetchQuery = fetchQuery.concat( "( LOWER (n.lastName ) = LOWER('" + req.body.fetchBy.fetchLastName + "')" );
    // };
    //
    // else {
    //     fetchQuery = fetchQuery.concat( " and n.lastName <> " + "'')"  );
    // };

    if ( req.body.fetchBy.fetchLastName != "" & req.body.fetchBy.fetchFirstName != "" ) {
        fetchOrderBy = fetchOrderBy.concat( " n.lastName, n.firstName " );
        fetchQuery = fetchQuery.concat( " LOWER( n.lastName ) =~ LOWER('" + req.body.fetchBy.fetchLastName + "')" );
        fetchQuery = fetchQuery.concat(" and LOWER( n.firstName ) =~ LOWER('" + req.body.fetchBy.fetchFirstName + "')" );

    } else if ( req.body.fetchBy.fetchLastName == ""  &  req.body.fetchBy.fetchFirstName != "" ) {
        // NOTE: may want query logic to work this
        // fetchQuery = fetchQuery.concat( " n.lastName <> " + "'')"  );
        fetchOrderBy = fetchOrderBy.concat( " n.firstName " );
        fetchQuery = fetchQuery.concat(" LOWER( n.firstName) =~ LOWER('" + req.body.fetchBy.fetchFirstName + "')" );

    } else if ( req.body.fetchBy.fetchLastName != ""  &  req.body.fetchBy.fetchFirstName == "" ){
        fetchOrderBy = fetchOrderBy.concat( " n.lastName ");
        fetchQuery = fetchQuery.concat( " LOWER( n.lastName ) =~ LOWER('" + req.body.fetchBy.fetchLastName + "')" );
        fetchQuery = fetchQuery.concat( " and ( n.firstName <> " + "'')"  );
    };
    console.log( req.body.fetchBy.fetchLastName );

    // if ( req.body.fetchBy.fetchLastName != "" & req.body.fetchBy.fetchFirstName != "" ) {
    //     fetchOrderBy = fetchOrderBy.concat( " n.lastName, n.firstName " );
    //     fetchQuery = fetchQuery.concat( " LOWER('" + req.body.fetchBy.fetchLastName + "') =~ LOWER( n.lastName )" );
    //     fetchQuery = fetchQuery.concat(" AND LOWER('" + req.body.fetchBy.fetchFirstName + "') =~ LOWER( n.firstName )" );

    // } else if ( req.body.fetchBy.fetchLastName == ""  &  req.body.fetchBy.fetchFirstName != "" ) {
    //     // NOTE: may want query logic to work this
    //     // fetchQuery = fetchQuery.concat( " n.lastName <> " + "'')"  );
    //     fetchOrderBy = fetchOrderBy.concat( " n.firstName " );
    //     fetchQuery = fetchQuery.concat(" LOWER( n.firstName) =~ LOWER('" + req.body.fetchBy.fetchFirstName + "')" );

    // } else if ( req.body.fetchBy.fetchLastName != ""  &  req.body.fetchBy.fetchFirstName == "" ){
    //     fetchOrderBy = fetchOrderBy.concat( " n.lastName ");
    //     fetchQuery = fetchQuery.concat( " LOWER( n.lastName ) =~ LOWER('" + req.body.fetchBy.fetchLastName + "')" );
    //     fetchQuery = fetchQuery.concat( " and ( n.firstName <> " + "'')"  );
    // };

    // build WHERE Clause

    console.log("DEBUG: userFetchNodesByQueryV1 : query params: " );
    console.log( fetchQuery);
    console.log( fetchOrderBy);
    // get the relationships for this node
    // "START user = node(2) MATCH user-[:FOLLOWS]->followed RETURN followed"
    //var query = "MATCH (user:'Users' ) " + fetchQuery + " RETURN user, COUNT(user) " + fetchOrderBY +  " SKIP " + newPage + " LIMIT " + pageSize ;
    var query = "MATCH (n:Components ) " + fetchQuery + " RETURN " + fetchReturns + fetchOrderBy   +  " SKIP " + newPage + " LIMIT " + pageSize;
    var params = "";
    var include_stats = true;
    neo4api.cypherRunQuery(query, params, include_stats, function (err, results) {
        if(err)
        {
                res.send(500, "ERROR: userFetchNodesByQueryV1 : findRelationships : err : " + err.message);
                console.log("ERROR: userFetchNodesByQueryV1 : findRelationships : err :", err);
                return next();
        }
        else if ( results )
        {
            //      we get back an array of relationships that matched the query
            //      next loop over the array testing for otherId
            //      then delete that relationship by id

            //var tmpFld =  results["data"].toString();
            console.log("DEBUG: userFetchNodesByQueryV1 : findRelationshipsOfNode ", results );
            //console.log( tmpFld);
            if ( results['data'].length ) {

                res.send(200, results );
            }
            else {
                res.send(200, null );
            }

            return next();
        }
    });
}




//**********************************



/*
 * REST PUT : Update Component
   server.put('/api/v1/users', api.putUserV1);
*/
// function putComponentV1(req, res, next) {
//     var  targetIdName = "_id" ;
//     var  targetId = req.body._id;

//     //console.log("DEBUG: putComponentV1 : pre keyId :************ ", req.body,  targetKeyId);

//     var node_data =  req.body ;

//     // don't allow user updates to nodes with null or '' keyId
//     if ( req.body.keyId === '' | req.body.keyId === null  ) {
//         //because keyId was '' or null then use _id for lookup but create and update keyId
//         //  targetId and targetIdName already set;
//         //targetKeyId =  uuid.v1();
//         node_data.keyId =  uuid.v1();
//     }
//     else {
//         // because there was a keyId value present then use it instead of _id
//         targetIdName = "keyId" ;
//         targetId =  req.body.keyId;

//     };

//     console.log("DEBUG: putComponentV1 : post keyId :************ ", targetKeyId, req.body.keyId );
//     console.log("DEBUG: putComponentV1 : pre  nodedata req.body :************ ", req.body );
//     // properties that are not updateable by "member" user
//     // *******
//     //        labelId: "mbrsite",     ** Required  "mbrsite"
//     //        _id  : "";              ** Required, assigned by server
//     //        keyId: "",              ** Required, assigned by server
//     //        userKeyId: "",          ** Required, derived from user
//     //        datecreated: "",        ** Required, set at time of obj creation
//     //        status: "",             ** Required, set at system level-relates to validity of
//     //        approveDate: "",

//     // var node_data = {
//     //         keyId : req.body.keyId || targetKeyId,
//     //         lastUpdated: req.body.lastUpdated || "",
//     //         updatedById: req.body.updatedById || "",
//     //         isActive: req.body.isActive || "",
//     //         name : req.body.name || "",
//     //         street : req.body.street || "",
//     //         street2 : req.body.street2 || "",
//     //         city : req.body.city || "",
//     //         state : req.body.state || "",
//     //         zipCode : req.body.zipCode || "",
//     //         phone : req.body.phone || "",
//     //         fax: req.body.fax || "",
//     //         website: req.body.website || "",
//     //         category: req.body.category || "",
//     //         description: req.body.description ||  "",
//     //         identityId : req.body.indentityId || ""


//     //     };

//     // obs moved to top of routine:  var node_data =  req.body ;
//     // obs moved to top of routine:  node_data.keyId =  targetKeyId;

//     var lblArray =  req.body.lblArray;  // 'Components:Mbrsite:Profile'
//     //[ groupLabel , typeLabel, partLabel ]
//     neo4api.updateByFldKey( lblArray , targetIdName , targetId, node_data , function(err, user) {

//             var resultsCnt = [user].length ;
//             console.log("DEBUG: putComponentV1.updateByFldKey : resultsCnt w/err keyId: ", resultsCnt, err, targetKeyId );
//             if(err) {
//                 console.log("ERROR: putComponentV1.updateByFldKey err :", err);
//                 res.send(500, "ERROR: putComponentV1.updateByFldKey : update");
//                 return next();
//             }
//             else if(   1 === resultsCnt ) {

//                 res.send(200);
//                 return next();
//             }
//             else {

//                 res.send(400, "ERROR: putComponentV1.updateByFldKey: Was not able to update user with keyId : " + user.keyId);
//                 return next();
//         }
//     });


// }



function deleteComponentV1(req, res, next) {

    var queryStrings = req.getQuery();
    var idType = queryStrings.idType;
    var id = req.params.id;

    var properties = {};

    //console.log("DEBUG: idType : ", idType);
    //console.log("DEBUT: id : ", id);

    if("loginId" === idType) {

        properties["loginId"] = id;
    }
    else if("keyId" === idType) {

        //properties["_id"] = id;
        properties["keyId"] = id;
    }
    else if("objectId" === idType) {

        //properties["_id"] = id;
        properties["_id"] = id;
    }
    else {

        res.send(400, "ERROR: deleteUserV1 : Invalid id type");
        return next();
    }

    neo4api.removeWithQuery( ['Components'], properties,function(err, numRemovedDocs) {
        if(err) {

            res.send(500, "ERROR: deleteUserV1 : remove");
            return next();
        }
        else if(1 === numRemovedDocs) {

            res.send(200);
            return next();
        }
        else {

            res.send(400, "ERROR: Was not able to delete user with id : " + id);
            return next();
        }
    });
}


/**
    * GET user RELATIONSHIPS

    /api/v1/users/:id/connect

    // options : direction: "in"  or "out"  or  dflt= "All"
    //           types:         ex: "FOLLOWS"
*/
// function  userFindConnectionsActionV1(req, res, next) {

//     console.log("DEBUG: userFindConnectionsActionV1 : req params ID", req.params.id);
//     var rootNodeId = req.params.id;
//     var options =  req.body.options;


//     neo4api.findRelationshipsOfNode( rootNodeId, options, function (err, results) {

//         if(err)
//         {
//                 res.send(500, "ERROR: userFindConnectionsActionV1 : findRelationships : err : " + err.message);
//                 console.log("ERROR: userFindConnectionsActionV1 : findRelationships : err :", err);
//                 return next();
//         }
//         else if ( results )
//         {
//             //      we get back an array of relationships that matched the query
//             //      next loop over the array testing for otherId
//             //      then delete that relationship by id

//             console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode " + results.length );
//             console.log( results );

//             var connections = [];

//             for (var i = 0; i < results.length; i++) {
//                  // extract the followed Id of this relationship
//                 var followedId = results[i]['_end'];

//                 console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode : followedId  " + followedId );

//                 neo4api.findById( followedId, function( err, node ) {

//                     if(err) {

//                        // res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                        // console.log("ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                        // console.log(node)
//                        // return next();
//                     }
//                     else if( node ) {

//                         connections.push( node );

//                         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after connections.push" );
//                         console.log( connections );
//                     }
//                     else
//                     {
//                         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: no connections.push node" );

//                     }

//                 });


//             }



//        }
//          console.log("DEBUG: userFindConnectionsActionV1 : findRelationships : exiting  " );
//             console.log( connections );
//             res.send(200, connections);
//             return next();

//     });

// //neo4api.findWithQuery(
// //        ['Users'],
// //        { 'loginId': loginUser.loginId, 'password' : loginUser.password },
// //        function(err, user) {

// }




// function  userFindConnectionsActionV1(req, res, next) {

//     console.log("DEBUG: userFindConnectionsActionV1 : req params ID", req.params.id);
//     var rootNodeId = req.params.id;
//     var options =  req.body.options;

//     // get the relationships for this node
//     neo4api.findRelationshipsOfNode( rootNodeId, options, function (err, results) {

//         if(err)
//         {
//                 res.send(500, "ERROR: userFindConnectionsActionV1 : findRelationships : err : " + err.message);
//                 console.log("ERROR: userFindConnectionsActionV1 : findRelationships : err :", err);
//                 return next();
//         }
//         else if ( results )
//         {
//             //      we get back an array of relationships that matched the query
//             //      next loop over the array testing for otherId
//             //      then delete that relationship by id

//             console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode " + results.length );
//             console.log( results );

//             var connections = "";
//             var ids = [ ] ;
//             for (var i = 0; i < results.length; i++) {
//                  // extract the followed Id of this relationship
//                 var followedId = results[i]['_end'];
//                 ids.push( { 'ID' : followedId } ) ;

//                 console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after ids" );
//                 console.log(ids );

//                 //console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode : followedId  " + followedId );

//                 // neo4api.findById( followedId, function( err, node ) {

//                 //     if(err) {

//                 //        // res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                 //        // console.log("ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                 //        // console.log(node)
//                 //        // return next();
//                 //     }
//                 //     else if( node ) {

//                 //         connections.push( node );

//                 //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after connections.push" );
//                 //         console.log( connections );
//                 //     }
//                 //     else
//                 //     {
//                 //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: no connections.push node" );

//                 //     }

//                 // });


//             }

//            // { ID: 10 }, { ID: 5 }, { ID: 11 }

//             // now get the nodes data
//             var idsWrk = { keyId : "ef957de8-3d29-493b-9e91-806475fca4d1" };
//             // "5aa913a1-137c-11e4-9c59-a3ed62d316b1"
//               //  , { keyId: "ef957de8-3d29-493b-9e91-806475fca4d1"  }
//              // ids.slice(0,ids.length);  0a5d0be1-137c-11e4-9c59-a3ed62d316b1
//             console.log( idsWrk );


//             neo4api.findWithQuery( ['Users'], idsWrk , function(err, nodes ) {

//                 if(err) {

//                   res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                   console.log( nodes );
//                   return next();
//                 }
//                 else {

//                     console.log("DEBUG: userFindConnectionsActionV1 : findRelationships : exiting  " );
//                     console.log( nodes );
//                     res.send(200, nodes);
//                     return next();
//                 }
//             });
//         }
//     });

// }

  //  query
  //var query = "START user = node(" + 123) MATCH user-[:RELATED_TO]->friends RETURN friends"
  //
  // add "LIMIT 100"  to query to reduce working data
  //var query = "START user = node(" + 123) MATCH user-[:RELATED_TO]->friends RETURN friends LIMIT 25"

function  componentFindConnectionsActionV1(req, res, next) {

    console.log("DEBUG: userFindConnectionsActionV1 : req params ID", req.params.id);
    var rootNodeId = req.params.id;
    var options =  req.body.options;

    // get the relationships for this node
    // "START user = node(2) MATCH user-[:FOLLOWS]->followed RETURN followed"
    var query = "START user = node(" + rootNodeId + ") MATCH user-[:FOLLOWS]->followed RETURN followed" ;
    var params = "";
    var include_stats = true;
    neo4api.cypherRunQuery(query, params, include_stats, function (err, results) {
        if(err)
        {
                res.send(500, "ERROR: userFindConnectionsActionV1 : findRelationships : err : " + err.message);
                console.log("ERROR: userFindConnectionsActionV1 : findRelationships : err :", err);
                return next();
        }
        else if ( results )
        {
            //      we get back an array of relationships that matched the query
            //      next loop over the array testing for otherId
            //      then delete that relationship by id

            console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode " + results['data'].length );
            console.log( results['data'] );
            if ( results['data'].length ) {
                res.send(200, results['data']);
            }
            else {
                res.send(200, null );
            }

            return next();
           //***************************************************************
           //  var connections = "";
           //  var ids = [ ] ;
           //  for (var i = 0; i < results.length; i++) {
           //       // extract the followed Id of this relationship
           //      var followedId = results[i]['_end'];
           //      ids.push( { 'ID' : followedId } ) ;

           //      console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after ids" );
           //      console.log(ids );

           //      //console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode : followedId  " + followedId );

           //      // neo4api.findById( followedId, function( err, node ) {

           //      //     if(err) {

           //      //        // res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
           //      //        // console.log("ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
           //      //        // console.log(node)
           //      //        // return next();
           //      //     }
           //      //     else if( node ) {

           //      //         connections.push( node );

           //      //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after connections.push" );
           //      //         console.log( connections );
           //      //     }
           //      //     else
           //      //     {
           //      //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: no connections.push node" );

           //      //     }

           //      // });


           //  }

           // // { ID: 10 }, { ID: 5 }, { ID: 11 }

           //  // now get the nodes data
           //  var idsWrk = { keyId : "ef957de8-3d29-493b-9e91-806475fca4d1" };
           //  // "5aa913a1-137c-11e4-9c59-a3ed62d316b1"
           //    //  , { keyId: "ef957de8-3d29-493b-9e91-806475fca4d1"  }
           //   // ids.slice(0,ids.length);  0a5d0be1-137c-11e4-9c59-a3ed62d316b1
           //  console.log( idsWrk );


           //  neo4api.findWithQuery( ['Users'], idsWrk , function(err, nodes ) {

           //      if(err) {

           //        res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
           //        console.log( nodes );
           //        return next();
           //      }
           //      else {

           //          console.log("DEBUG: userFindConnectionsActionV1 : findRelationships : exiting  " );
           //          console.log( nodes );
           //          res.send(200, nodes);
           //          return next();
           //      }
           //  });
            //****************************************************



        }
    });

}


/**
    * SERVER PUT   server.put('/api/v1/users/:id/connect', api.userConnectActionV1);

        NOTES: after creating new relationships we must repopulate the user relationship data their browser.
               we can do this by retrieving an updated list of relationships for this user based on the type of
              relationship just created. This updated list would be returned to browser client. Sample results

             Sample of returned results of insert operation :
               [ { since: 2014, more usersupplied properties... , _start: 1, _end: 10, _id: 8, _type: 'FOLLOWS' } ]


        req.params: req.params.id
        req.body  : 'otherId',
                    'relationType' : 'FOLLOWS'      other type can be used
                    'relationData' :  object properties of relationship. diff for each relationship been nodes


*/
function componentConnectActionV1(req, res, next) {

    console.log("DEBUG: userConnectActionV1 : connect from ID", req.params.id);
    console.log( req.body );

    console.log(' need to setup id test like in delete')
    var rootNodeId = req.params.id;
    var otherNodeId = req.body.otherId;
    var relationType =  req.body.relationType;
    var relationData   = req.body.relationData ;
    neo4api.insertRelationship(rootNodeId, otherNodeId, relationType, relationData, function (err, results) {

        var resultswrk = [ results ];
        console.log("DEBUG: userConnectActionV1 : connect : resultsCnt : " + resultswrk.length );
        console.log( resultswrk );
        if(err) {
              res.send(500, "ERROR:  userConnectActionV1 : connect: err : " + err.message);
              console.log("ERROR: userConnectActionV1 : connect : err :", err);
              return next();
        }
        else if( 1 === resultswrk.length) {

                console.log("DEBUG: userConnectActionV1 : connect : count :", resultswrk);
                var followedId = resultswrk[0]['_end'];
                console.log( followedId );
                neo4api.findById( followedId , function(err, node){

                        if(err) {
                            console.log("DEBUG: userConnectActionV1 : findById : node : err", err );
                            callback(err, null);
                            return next();
                        }
                        else if( node ) {
                            console.log("DEBUG: userConnectActionV1 : findById : node :", node );
                            res.send(200, node);
                            return next();

                        }
                        else {
                            console.log("DEBUG: userConnectActionV1 : findById : node : else ", node );
                            callback(null, null);
                            return next();
                        }
                });

        }
        else {

                console.log("ERROR: userConnectActionV1 : connect : Was not able to set relationship");
                console.log(results)
                res.send(400, "ERROR: userConnectActionV1 : connect  : Was not able to set relationship");
                return next();
        }

    });


}

/**
    * POST /users/:id/disconnect

    // options : direction: "in"  or "out"  or  dflt= "All"
    //           types:         ex: "FOLLOWS"
*/
function  componentDisconnectActionV1(req, res, next) {

    console.log("DEBUG: postUserDisconnectActionV1 : Disconnect from ID", req.params.id);
    var rootNodeId = req.params.id;
    var connectedNodeId = req.body.connectedId;
    var options =  req.body.options;

    neo4api.deleteRelationship(rootNodeId, connectedNodeId, options, function (err, result) {

        // what we get back is a result of true /number deleted or false indicating relationship wasn't found
        if (err) {
                res.send(500, "ERROR:  userDisconnectActionV1 : disconnect: err : " + err.message);
                console.log("ERROR: userDisconnectActionV1 : disconnect : err :", err);
              return next();
        }
        else if( 1  === [result].length ) {
               console.log("DEBUG: userDisconnectActionV1 : disconnect : result :", result );
               // id of followed Node is returned
               res.send(200,  connectedNodeId  );
               return next();
        }
        else {

                res.send(400, "ERROR: userDisconnectActionV1 : disconnect  : Was not able to delete / drop relationship");
                console.log("ERROR: userDisconnectActionV1 : disconnect : Was not able to delete / drop relationship");
                console.log(result)
                return next();
        }
    });
}

/*  LOCAL USE FUNCTIONS */

/*
WORKING EXAMPLE of APPEND execute at http://repl.it/languages/JavaScript

// create predicate from obj for use in an expression
*******************
refer to  Dropbox/documents for more "js snipits.txt" examples

var wName = 'City';
var wProps =  { name: 'Aalst', postalcode: 9300};
var wAppendToken = ","  ex:  ' and ' or ',' etc
var wStartToken = "'"   ex:  '{' or '[' etc
var wEndToken = "'"     ex:  '}' or ']' etc
var wReturn = objToPredicate( wName,wProps,wAppendToken, wStartToken, wEndToken);

//  returns city.name={name or value } appendToken city.postalcode={postalcode}
*/

function objToPredicate( fieldName, props, appendToken, startToken, endToken) {
     var params = '';
     var notFirst = false;
     fieldName += '.';
     for (var key in props) {
        var obj = props[key];
        console.log("DEBUG: neo4jComponentjs.objToPredicate : key,obj :", key,obj );
        if ( obj !== ''  ) {
            if(notFirst)
              params += appendToken;
            else
              notFirst = true;

            if (obj === true || obj === false || !isNaN(obj)  )
              params += fieldName + key + '=' + obj ;
            else
              params += fieldName + key + '=' + startToken + obj + endToken ;

        }
      }
      return params;
}



// function whereSetProperties ( refNames, whereProps, setProps) {
//   var prefix = 'xQ_',
//     whereClause = '',
//     setClause = '',
//     notFirst = false,
//     props = {},
//     i = 0,
//     obj;

//     for (var fieldNames in whereProps) {
//       var ref = fieldNames.slice(0,3);
//       ref += '.';

//          for ( var fieldName in whereProps[fieldNames] ) {
//           // Build WHERE
//             // for (var k in oldProps) {
//             // obj = oldProps[k];
//             obj = whereProps[fieldName];
//             if(notFirst)
//               whereClause += ' AND ';
//             else
//               notFirst = true;
//             whereClause += ref + fieldName + '={' + prefix + (++i) + '}';

//             props[prefix + i] = obj;
//           }

//           notFirst = false;

//           // Build SET
//           for (var key in setProps) {
//             obj = setProps[key];
//             if(notFirst)
//               setClause += ',';
//             else
//               notFirst = true;
//             // Create unique placeholder {xx1} {xx2} ...
//             setClause += ref + key + '={' + prefix + (++i) + '}';
//             // Build new properties object
//             props[prefix + i] = obj;
//           }

//     }

//   // Return stringified `where` and `set` clause and a new object with unique property names
//   // So there are no name collisions
//   whereProps = property objs to convert to where clause format ( WHERE pro.name='Jim' and rel.setOrder=3 )
//   whereVal = false/true  false: return where clause as aliased values, else where clause returned as normal values
//   setProps = property objs to convert to set clause format  ( SET  pro.name='jim', con.textArea='Hello worls')
//
//     return {
//     where: whereClause,
//     set: setClause,
//     properties: props
//   };
// }

// ver 20151017
// function whereSetProperties ( whereProps, whereVal, setProps) {
//   var prefix = 'xQ_',
//     whereClause = '',
//     setClause = '',
//     notFirst = false,
//     props = {},
//     i = 0,
//     obj;
//
//     for (var whereNames in whereProps) {
//       var ref = whereNames.slice(0,3);
//       ref += '.';
//
//       for ( var whereName in whereProps[whereNames] ) {
//         // Build WHERE
//         obj = whereProps[whereNames][whereName];
//         // NOTE: if '' obj is allowed then cypher where clauses search for '' fields
//         //       probably need to fix
//         if ( obj !==  '') {
//           if(notFirst)
//               whereClause += ' AND ';
//           else
//               notFirst = true;
//
//           if ( whereVal ) {
//             //if ( typeof obj === 'number' ) {
//             if ( typeof obj === 'number' ||  typeof obj === 'boolean') {
//               whereClause += ref + whereName + "=" + obj ;
//             } else {
//               whereClause += ref + whereName + " =~ '" + obj + "'";
//               //whereClause +=  " LOWER( " + ref +  whereName + ") =~ LOWER('" + obj + ".*')";
//             }
//           } else {
//                whereClause += ref + whereName + ' =~ {' + prefix + (++i) + '}';
//                //whereClause +=  " LOWER( " + ref +  whereName + ") =~ LOWER({" + prefix + (++i) + "})";
//           }
//           props[prefix + i] = obj;
//
//         }
//       }
//     }
//
//     notFirst = false;
//
//     for (var setNames in setProps) {
//       var ref = setNames.slice(0,3);
//       ref += '.';
//
//
//           // Build SET
//           // for (var key in setProps) {
//           // obj = setProps[key];
//           for ( var setName in setProps[setNames] ) {
//             obj = setProps[setNames][setName];
//             if(notFirst)
//               setClause += ',';
//             else
//               notFirst = true;
//             // Create unique placeholder {xx1} {xx2} ...
//             //if ( obj !==  '') {
//              setClause += ref + setName + '={' + prefix + (++i) + '}';
//              props[prefix + i] = obj;
//             //}
//           }
//
//
//     }
//
//   // Return stringified `where` and `set` clause and a new object with unique property names
//   // So there are no name collisions
//   return {
//     where: whereClause,
//     set: setClause,
//     properties: props
//   };
// }

function whereSetProperties ( whereProps, whereVal, setProps) {

  /* Create a `where` and `set` string and a new object with unique propertynames
  // Example:
  // whereSetProperties('user', {userid: 123, firstname: 'foo'}, { firstname: 'bar' })
  // returns {
  //   where: "user.userid={xQ_1} AND user.firstname={xQ_2}",
  //   set: "user.firstname={xQ_3}",
  //   props { xQ_1: 123, xQ_2: 'foo', xQ_2: 'bar'}
  // }
  */

  /* NOTE:  20151017 REMOVE whereVal
            whereProps has two formats. If whereVal true then use new format else old format
            Use whereVal test until all calls to this function are converted new format usage
            then remove whereVal parm, associated whereVal else logic and this note.
  */

  var prefix = 'xQ_',
    whereClause = '',
    setClause = '',
    notFirst = false,
    props = {},
    i = 0,
    whereValue;
    /* BUILD WHERE clause if whereProps exist */
    for (var whereSet in whereProps) {
      var ref = whereSet.slice(0,3);
      ref += '.';

      for ( var whereName in whereProps[whereSet] ) {

        whereValue = whereProps[whereSet][whereName];
        // NOTE: if '' obj is allowed then cypher where clauses search for '' fields
        //       probably need to fix
        if ( whereValue !==  '') {
          if(notFirst)
              whereClause += ' AND ';
          else
              notFirst = true;

          // REMOVE THIS TEST see whereVal note at top of function.
          if ( whereVal ) {
            //if ( typeof obj === 'number' ) {
            if ( typeof whereValue === 'number' ||  typeof whereValue === 'boolean') {
              whereClause += ref + whereName + "=" + whereValue ;
            } else {
              whereClause += ref + whereName + " =~ '" + whereValue + "'";
              //whereClause +=  " LOWER( " + ref +  whereName + ") =~ LOWER('" + obj + ".*')";
            }
          } else {
               // REMOVE THIS ELSE LOGIC see note at top of function
               whereClause += ref + whereName + ' =~ {' + prefix + (++i) + '}';
               //whereClause +=  " LOWER( " + ref +  whereName + ") =~ LOWER({" + prefix + (++i) + "})";
          }
          props[prefix + i] = whereValue;

        }
      }
    }
    /* BUILD SET clause if setProps exist */
    notFirst = false;
    for (var setNames in setProps) {
      var ref = setNames.slice(0,3);
      ref += '.';
      for ( var setName in setProps[setNames] ) {
            obj = setProps[setNames][setName];
            if(notFirst)
              setClause += ',';
            else
              notFirst = true;
            // Create unique placeholder {xx1} {xx2} ...
            //if ( obj !==  '') {
             setClause += ref + setName + '={' + prefix + (++i) + '}';
             props[prefix + i] = obj;
            //}
          }


    }

  // Return stringified `where` and `set` clause and a new object with unique property names
  // So there are no name collisions
  return {
    where: whereClause,
    set: setClause,
    properties: props
  };
}


//exports.componentInitialPCSCreateByQueryV1 = componentInitialPCSCreateByQueryV1
// obs exports.getComponentsV1 = getComponentsV1;
exports.getProfileContentSetsV1 = getProfileContentSetsV1;
exports.getContextProfileV1 =  getContextProfileV1;
exports.componentFetchNodesByQueryV1 = componentFetchNodesByQueryV1;

// obs exports.postComponentV1 = postComponentV1;
exports.postProfileContentSetV1 = postProfileContentSetV1;
exports.postContentSetV1 = postContentSetV1;

//exports.putComponentV1 = putComponentV1;
exports.putComponentUpdateV1 = putComponentUpdateV1;

exports.deleteComponentV1 = deleteComponentV1;
//exports.postComponentLoginV1 = postComponentLoginV1;
exports.componentConnectActionV1 = componentConnectActionV1;
exports.componentDisconnectActionV1 = componentDisconnectActionV1;
exports.componentFindConnectionsActionV1 = componentFindConnectionsActionV1;
