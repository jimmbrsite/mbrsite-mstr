// Neo4j - user api 

var neo4api = require('./neo4');
var uuid = require('node-uuid');

/* NOTES:
*  
****  setting up first User in Neo4j:
*  a.  comment user-service line 20ish user.signin
*  b.  use bypass logic in server.js line 29ish
*  c.  start nodejs
*  d.  execute   http://localhost:8080/#/users/create  to create first user
*
****  added updateNodeByFltrKey function to main.js in node-neo4j module
* 
****  need to refine the data returned in neo4.js to qappear more like mongo data. neo4j data is in an array [{flda:valuea,fldb:valeub}] mongo is {flda:valuea,fldb:valeub}
*     the difference requires handling the services and controls logic slightly different.
*
****  need to better understand how _id works my not need updateNodeByFltrKey ( use my own unique key vale ex: uuid() )
*
****   
*/

/*
 * REST POST : Create user
   server.post('/api/v1/users', api.postUserV1);
*/
function postUserV1(req, res, next) {

	var newUser = req.body;
    //console.log("DEBUG: postUserV1: newUser pre: ", newUser);
    newUser['keyId'] = uuid.v1(); 
    //console.log("DEBUG: postUserV1: newUser post: ");
    //console.log( newUser );
    // insertNode({ name: 'Kristof' }, ['Student'], callback);
    neo4api.insert(newUser, ['Users'], function(err, result) {

            if(err) {

                res.send(500, "ERROR: postUserV1 : insert : newUser");
                return next();
            }
            else if(result) {

                res.send(201);
                return next();
            }
        }
    );
}

/*
 * REST GET : Get user  ( only One user )
   server.post('/api/v1/users', api.getUserV1);
*/




/*
 * REST GET : Get users
   server.post('/api/v1/users', api.getUsersV1);
*/

function getUsersV1(req, res, next) {
   
/**
 *  Get all nodes with label
 *  Given one label (non-empty string) or multiple labels (array of strings) and one or
 *  more properties in json returns an array of nodes with these labels and properties
     
    readNodesWithLabelsAndProperties('User',{ firstname: 'Sam', male: true }, callback);
 *       returns an array with nodes with the label 'User' and properties firstname='Sam' and male=true
 **/

    // Neo4j.prototype.findAllNodesWith = function(label, callback)
    //  
    // obslete original code - 20140916
    // neo4api.findAllNodesByLabel( ['Users'] , function(err, results) {
        
    //     //console.log("DEBUG: getUsersV1: findAllNodes : results ");  
    //     //console.log(results);
        
    //     if(err) {

    //         res.send(500, "ERROR: getNodesV1 : findAllNodes");
    //         return next();
    //     }
    //     else {

    //         if (results.length === 0) console.log('nada');

    //         //console.log("Query results: ", results)

    //         res.json(200, results);
    //         return next();
    //     }
    // });
    

    console.log("DEBUG: getUsersV1: findNodesWithQuery : req.query" ); 
   

    var uProperties = {lastName: "Brewer"} ;
    //neo4api.findNodesWithQuery( ['Users'] , uProperties, function(err, results) {
    neo4api.findNodesWithQuery( ['Users'], uProperties , function(err, results) {    
        console.log("DEBUG: getUsersV1: findNodesWithQuery : results ");  
        console.log(results);
        
        if(err) {
            res.send(500, "ERROR: getNodesV1 : findNodesWithQuery");
            return next();
        }
        else {

            if (results.length === 0) console.log('nada');

            //console.log("Query results: ", results)

            res.json(200, results);
            return next();
        }
    });
}

// function getsUsersV1(req, res, next) {
      
//     //console.log("DEBUG: fetchUsersV1 : req params ID", req.params);
//     // //req.params.newPage;
//     // //var options =  req.body.options;
//     // // var whereClauss = 
//     // // get the relationships for this node
//     // // "START user = node(2) MATCH user-[:FOLLOWS]->followed RETURN followed"
//     // var query = "MATCH (user:Users) RETURN user LIMIT 25 " ;
//     // var params = "";
//     // var include_stats = true;
//     // neo4api.cypherRunQuery(query, params, include_stats, function (err, results) {
//     //     if(err)
//     //     {
//     //             res.send(500, "ERROR: fetchUsersV1 : err : " + err.message);
//     //             console.log("ERROR: fetchUsersV1 : err :", err);
//     //             return next();
//     //     }
//     //     else if ( results )
//     //     { 
//     //         //      we get back an array of relationships that matched the query      
//     //         //      next loop over the array testing for otherId 
//     //         //      then delete that relationship by id
              
//     //         console.log("DEBUG: fetchUsersV1 : count: " + results.length );
//     //         console.log( results );
//     //         if ( results.length ) {
//     //             res.send(200, results);
//     //         }
//     //         else {
//     //             res.send(200, null );
//     //         }

//     //         return next();   
           


//     //     }
//     // }); 
        

// }





/*
 * REST PUT : Update user
   server.put('/api/v1/users', api.putUserV1);
*/
function putUserV1(req, res, next) {

    var targetKeyId = req.body.keyId;
    
    // console.log("DEBUG: putUserV1 : update : targetKeyId :", targetKeyId);
   
    // *******
      // 'loginId' : '',
      // 'firstName' : '',
      // 'lastName' : '',
      // 'password' : '',
      // 'userRole': ''  // admin', 'manager', 'user', ...
      // 'keyId' : ''
    // ******* 
    //loginId : req.body.loginId,
    //authToken : req.body.authToken,
    //labelName : req.body.labelName ,
    //userType : req.body.userType  // admin, member, subscriber
    //password : req.body.password || "",

    var node_data = {

            keyId : req.body.keyId,            
            
            firstName : req.body.firstName,
            lastName : req.body.lastName,
            street : req.body.street || "",
            street2 : req.body.street2 || "",
            city : req.body.city || "",
            state : req.body.state || "",
            zipcode : req.body.zipcode || "",
            contactphone : req.body.contactPhone || "",
            mobilephone : req.body.mobilePhone || "",
            email : req.body.email || "",
            password : req.body.password || "",
            userRole : req.body.userRole || "user",
            providerId : req.body.providerId || "",
            bioinfo : req.body.bioinfo || "",
            website : req.body.website || "",
            organizationName : ""
           
        
    };

    
    // Update a Node properties
    // This will update all existing properties on the node with the new set of attributes using a user specified property as the key. */

    //var node_data = {firstName : req.body.firstName, lastName : req.body.lastName, password : req.body.password, userRole : req.body.userRole };

    neo4api.updateByFldKey( ['Users'], 'keyId', targetKeyId, node_data, function(err, user) {
            //var resultsCnt = Object.keys([user]).length ;
            var resultsCnt = [user].length ;
       //     console.log("DEBUG: putUserV1 : updateByFld : resultsCnt", resultsCnt );
            if(err) {
                console.log("ERROR: putUserV1 : err :", err);
                res.send(500, "ERROR: putUserV1 : update");
                return next();
            }
            else if(   1 === resultsCnt ) {

                res.send(200);
                return next();
            }
            else {

                res.send(400, "ERROR: Was not able to update user with keyId : " + user.keyId);
                return next();
        }
    });
}



function deleteUserV1(req, res, next) {

    var queryStrings = req.getQuery();
    var idType = queryStrings.idType;
    var id = req.params.id;

    var properties = {};

    //console.log("DEBUG: idType : ", idType);
    //console.log("DEBUT: id : ", id);

    if("loginId" === idType) {

        properties["loginId"] = id;
    }
    else if("keyId" === idType) {

        //properties["_id"] = id;
        properties["keyId"] = id;
    }
    else if("objectId" === idType) {

        //properties["_id"] = id;
        properties["_id"] = id;
    }
    else {

        res.send(400, "ERROR: deleteUserV1 : Invalid id type");
        return next();
    }

    neo4api.removeWithQuery( ['Users'], properties,function(err, numRemovedDocs) {
        if(err) {

            res.send(500, "ERROR: deleteUserV1 : remove");
            return next();
        }
        else if(1 === numRemovedDocs) {

            res.send(200);
            return next();
        }
        else {

            res.send(400, "ERROR: Was not able to delete user with id : " + id);
            return next();
        }
    });
}

/*
 * REST POST : User Login
   server.post('/api/v1/users/login', api.postUserLoginV1);
*/
function postUserLoginV1(req, res, next) {

    var loginUser = req.body;

    /*
     * Compare user login info 
     */
    neo4api.findNodesWithQuery(
        ['Users'],
        { 'loginId': loginUser.loginId, 'password' : loginUser.password },
        function(err, user) {

            if(err) {

                res.send(500, "ERROR: postUserLoginV1 : find : err : " + err.message);
                console.log("ERROR: postUserLoginV1 : find : err : ", err);
                return next();
            }
            else if( user ) {

                /*
                 * Create auth token
                 *
                */
                var  newProperties = { 'authToken': uuid.v1() };
                
                /* 
                 * Update user with new authToken
                */  
                var tmp = user[0];   // remove array [] and extract the _Id value to update
                var nodeId =  tmp._id;
                neo4api.updateById( nodeId , newProperties , function(err, results) {        
                    var tmp = "";
                    //var resultsCnt = Object.keys(results).length; 
                    var resultsCnt = [results].length; 
                    if(err) {
                        res.send(500, "ERROR: postUserLoginV1 : updateById : err : " + err.message);
                        console.log("ERROR: postUserLoginV1 : updateById : err :", err);
                        return next();
                    }
                    else if( 1 === resultsCnt) {
         //               console.log("DEBUG: postUserLoginV1 : update : count :", resultsCnt);
                        res.send(200, results);
                        return next();
                    }
                    else {
                        
                        res.send(400, "ERROR: postUserLoginV1 : update : Was not able to update user with session info");
                        console.log("ERROR: postUserLoginV1 : update : Was not able to update user with session info");
                        console.log(results)
                        return next();
                    }
                });

            }
            else {

                res.send(403, "ERROR: postUserLoginV1 : User authentication error");
                console.log("ERROR: postUserLoginV1 : User authentication error");
                return next();
            }
        }
    );
}

/**
    * GET user RELATIONSHIPS 

    /api/v1/users/:id/connect

    // options : direction: "in"  or "out"  or  dflt= "All"
    //           types:         ex: "FOLLOWS"    
*/
// function  userFindConnectionsActionV1(req, res, next) {
      
//     console.log("DEBUG: userFindConnectionsActionV1 : req params ID", req.params.id);
//     var rootNodeId = req.params.id;
//     var options =  req.body.options;
   

//     neo4api.findRelationshipsOfNode( rootNodeId, options, function (err, results) {
        
//         if(err)
//         {
//                 res.send(500, "ERROR: userFindConnectionsActionV1 : findRelationships : err : " + err.message);
//                 console.log("ERROR: userFindConnectionsActionV1 : findRelationships : err :", err);
//                 return next();
//         }
//         else if ( results )
//         { 
//             //      we get back an array of relationships that matched the query      
//             //      next loop over the array testing for otherId 
//             //      then delete that relationship by id
              
//             console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode " + results.length );
//             console.log( results );
   
//             var connections = [];           
            
//             for (var i = 0; i < results.length; i++) {
//                  // extract the followed Id of this relationship
//                 var followedId = results[i]['_end'];
                
//                 console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode : followedId  " + followedId );
  
//                 neo4api.findById( followedId, function( err, node ) {

//                     if(err) {

//                        // res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                        // console.log("ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                        // console.log(node)
//                        // return next();
//                     }
//                     else if( node ) {

//                         connections.push( node );

//                         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after connections.push" );
//                         console.log( connections );
//                     }
//                     else
//                     {
//                         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: no connections.push node" );
                        
//                     }
                
//                 });    

  
//             }
              
            
           
//        }
//          console.log("DEBUG: userFindConnectionsActionV1 : findRelationships : exiting  " );
//             console.log( connections );
//             res.send(200, connections);
//             return next();
       
//     }); 
        
// //neo4api.findWithQuery(
// //        ['Users'],
// //        { 'loginId': loginUser.loginId, 'password' : loginUser.password },
// //        function(err, user) {

// }




// function  userFindConnectionsActionV1(req, res, next) {
      
//     console.log("DEBUG: userFindConnectionsActionV1 : req params ID", req.params.id);
//     var rootNodeId = req.params.id;
//     var options =  req.body.options;
   
//     // get the relationships for this node
//     neo4api.findRelationshipsOfNode( rootNodeId, options, function (err, results) {
        
//         if(err)
//         {
//                 res.send(500, "ERROR: userFindConnectionsActionV1 : findRelationships : err : " + err.message);
//                 console.log("ERROR: userFindConnectionsActionV1 : findRelationships : err :", err);
//                 return next();
//         }
//         else if ( results )
//         { 
//             //      we get back an array of relationships that matched the query      
//             //      next loop over the array testing for otherId 
//             //      then delete that relationship by id
              
//             console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode " + results.length );
//             console.log( results );
   
//             var connections = "";           
//             var ids = [ ] ;
//             for (var i = 0; i < results.length; i++) {
//                  // extract the followed Id of this relationship
//                 var followedId = results[i]['_end'];
//                 ids.push( { 'ID' : followedId } ) ;

//                 console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after ids" );
//                 console.log(ids );

//                 //console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode : followedId  " + followedId );
  
//                 // neo4api.findById( followedId, function( err, node ) {

//                 //     if(err) {

//                 //        // res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                 //        // console.log("ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                 //        // console.log(node)
//                 //        // return next();
//                 //     }
//                 //     else if( node ) {

//                 //         connections.push( node );

//                 //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after connections.push" );
//                 //         console.log( connections );
//                 //     }
//                 //     else
//                 //     {
//                 //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: no connections.push node" );
                        
//                 //     }
                
//                 // });    

  
//             }
            
//            // { ID: 10 }, { ID: 5 }, { ID: 11 } 

//             // now get the nodes data 
//             var idsWrk = { keyId : "ef957de8-3d29-493b-9e91-806475fca4d1" };
//             // "5aa913a1-137c-11e4-9c59-a3ed62d316b1"
//               //  , { keyId: "ef957de8-3d29-493b-9e91-806475fca4d1"  }  
//              // ids.slice(0,ids.length);  0a5d0be1-137c-11e4-9c59-a3ed62d316b1
//             console.log( idsWrk );
           

//             neo4api.findWithQuery( ['Users'], idsWrk , function(err, nodes ) {
              
//                 if(err) {

//                   res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                   console.log( nodes );
//                   return next();
//                 }
//                 else { 
                    
//                     console.log("DEBUG: userFindConnectionsActionV1 : findRelationships : exiting  " );
//                     console.log( nodes );
//                     res.send(200, nodes);
//                     return next();
//                 }    
//             });
//         }
//     }); 
        
// }

  //  query
  //var query = "START user = node(" + 123) MATCH user-[:RELATED_TO]->friends RETURN friends"
  // 
  // add "LIMIT 100"  to query to reduce working data
  //var query = "START user = node(" + 123) MATCH user-[:RELATED_TO]->friends RETURN friends LIMIT 25"

function  userFindConnectionsActionV1(req, res, next) {
      
    console.log("DEBUG: userFindConnectionsActionV1 : req params ID", req.params.id);
    var rootNodeId = req.params.id;
    var options =  req.body.options;
   
    // get the relationships for this node
    // "START user = node(2) MATCH user-[:FOLLOWS]->followed RETURN followed"
    var query = "START user = node(" + rootNodeId + ") MATCH user-[:FOLLOWS]->followed RETURN followed" ;
    var params = "";
    var include_stats = true;
    neo4api.cypherRunQuery(query, params, include_stats, function (err, results) {
        if(err)
        {
                res.send(500, "ERROR: userFindConnectionsActionV1 : findRelationships : err : " + err.message);
                console.log("ERROR: userFindConnectionsActionV1 : findRelationships : err :", err);
                return next();
        }
        else if ( results )
        { 
            //      we get back an array of relationships that matched the query      
            //      next loop over the array testing for otherId 
            //      then delete that relationship by id
              
            console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode " + results.length );
            console.log( results );
            if ( results.length ) {
                res.send(200, results);
            }
            else {
                res.send(200, null );
            }

            return next();   
           //*************************************************************** 
           //  var connections = "";           
           //  var ids = [ ] ;
           //  for (var i = 0; i < results.length; i++) {
           //       // extract the followed Id of this relationship
           //      var followedId = results[i]['_end'];
           //      ids.push( { 'ID' : followedId } ) ;

           //      console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after ids" );
           //      console.log(ids );

           //      //console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode : followedId  " + followedId );
  
           //      // neo4api.findById( followedId, function( err, node ) {

           //      //     if(err) {

           //      //        // res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
           //      //        // console.log("ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
           //      //        // console.log(node)
           //      //        // return next();
           //      //     }
           //      //     else if( node ) {

           //      //         connections.push( node );

           //      //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after connections.push" );
           //      //         console.log( connections );
           //      //     }
           //      //     else
           //      //     {
           //      //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: no connections.push node" );
                        
           //      //     }
                
           //      // });    

  
           //  }
            
           // // { ID: 10 }, { ID: 5 }, { ID: 11 } 

           //  // now get the nodes data 
           //  var idsWrk = { keyId : "ef957de8-3d29-493b-9e91-806475fca4d1" };
           //  // "5aa913a1-137c-11e4-9c59-a3ed62d316b1"
           //    //  , { keyId: "ef957de8-3d29-493b-9e91-806475fca4d1"  }  
           //   // ids.slice(0,ids.length);  0a5d0be1-137c-11e4-9c59-a3ed62d316b1
           //  console.log( idsWrk );
           

           //  neo4api.findWithQuery( ['Users'], idsWrk , function(err, nodes ) {
              
           //      if(err) {

           //        res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
           //        console.log( nodes );
           //        return next();
           //      }
           //      else { 
                    
           //          console.log("DEBUG: userFindConnectionsActionV1 : findRelationships : exiting  " );
           //          console.log( nodes );
           //          res.send(200, nodes);
           //          return next();
           //      }    
           //  });
            //****************************************************



        }
    }); 
        
}


/**
    * SERVER PUT   server.put('/api/v1/users/:id/connect', api.userConnectActionV1);
        
        NOTES: after creating new relationships we must repopulate the user relationship data their browser.
               we can do this by retrieving an updated list of relationships for this user based on the type of
              relationship just created. This updated list would be returned to browser client. Sample results 
             
             Sample of returned results of insert operation :
               [ { since: 2014, more usersupplied properties... , _start: 1, _end: 10, _id: 8, _type: 'FOLLOWS' } ]
    

        req.params: req.params.id
        req.body  : 'otherId',
                    'relationType' : 'FOLLOWS'      other type can be used
                    'relationData' :  object properties of relationship. diff for each relationship been nodes  

        
*/
function userConnectActionV1(req, res, next) {
        
    console.log("DEBUG: userConnectActionV1 : connect from ID", req.params.id);
    console.log( req.body );
    
    console.log(' need to setup id test like in delete')
    var rootNodeId = req.params.id;
    var otherNodeId = req.body.otherId;
    var relationType =  req.body.relationType;
    var relationData   = req.body.relationData ;
    neo4api.insertRelationship(rootNodeId, otherNodeId, relationType, relationData, function (err, results) {
       
        var resultswrk = [ results ];
        console.log("DEBUG: userConnectActionV1 : connect : resultsCnt : " + resultswrk.length );
        console.log( resultswrk );
        if(err) {
              res.send(500, "ERROR:  userConnectActionV1 : connect: err : " + err.message);
              console.log("ERROR: userConnectActionV1 : connect : err :", err);
              return next();
        }
        else if( 1 === resultswrk.length) {
               
                console.log("DEBUG: userConnectActionV1 : connect : count :", resultswrk);
                var followedId = resultswrk[0]['_end'];
                console.log( followedId );
                neo4api.findById( followedId , function(err, node){

                        if(err) {
                            console.log("DEBUG: userConnectActionV1 : findById : node : err", err );    
                            callback(err, null);
                            return next(); 
                        }
                        else if( node ) {
                            console.log("DEBUG: userConnectActionV1 : findById : node :", node );
                            res.send(200, node);
                            return next();
                
                        }
                        else {
                            console.log("DEBUG: userConnectActionV1 : findById : node : else ", node );
                            callback(null, null);
                            return next();
                        }
                });

        }
        else {
                        
                console.log("ERROR: userConnectActionV1 : connect : Was not able to set relationship");
                console.log(results)
                res.send(400, "ERROR: userConnectActionV1 : connect  : Was not able to set relationship");
                return next();
        }
  
    });


}

/**
    * POST /users/:id/disconnect

    // options : direction: "in"  or "out"  or  dflt= "All"
    //           types:         ex: "FOLLOWS"    
*/
function  userDisconnectActionV1(req, res, next) {
      
    console.log("DEBUG: postUserDisconnectActionV1 : Disconnect from ID", req.params.id);
    var rootNodeId = req.params.id;
    var connectedNodeId = req.body.connectedId;
    var options =  req.body.options;
   
    neo4api.deleteRelationship(rootNodeId, connectedNodeId, options, function (err, result) {
    
        // what we get back is a result of true /number deleted or false indicating relationship wasn't found  
        if (err) {
                res.send(500, "ERROR:  userDisconnectActionV1 : disconnect: err : " + err.message);
                console.log("ERROR: userDisconnectActionV1 : disconnect : err :", err);
              return next();
        }
        else if( 1  === [result].length ) {
               console.log("DEBUG: userDisconnectActionV1 : disconnect : result :", result );
               // id of followed Node is returned
               res.send(200,  connectedNodeId  );
               return next();
        }
        else {
                        
                res.send(400, "ERROR: userDisconnectActionV1 : disconnect  : Was not able to delete / drop relationship");
                console.log("ERROR: userDisconnectActionV1 : disconnect : Was not able to delete / drop relationship");
                console.log(result)
                return next();
        }          
    }); 
}



exports.getUsersV1 = getUsersV1;
exports.postUserV1 = postUserV1;
exports.putUserV1 = putUserV1;
exports.deleteUserV1 = deleteUserV1;
exports.postUserLoginV1 = postUserLoginV1;
exports.userConnectActionV1 = userConnectActionV1;
exports.userDisconnectActionV1 = userDisconnectActionV1;
exports.userFindConnectionsActionV1 = userFindConnectionsActionV1;