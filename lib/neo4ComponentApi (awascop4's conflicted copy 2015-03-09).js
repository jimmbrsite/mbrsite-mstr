// neo4ComponentApi.js
// Neo4j - component api 

var neo4api = require('./neo4');
var uuid = require('node-uuid');

/* NOTES:
*  
****  setting up first User in Neo4j:
*  a.  comment user-service line 20ish user.signin
*  b.  use bypass logic in server.js line 29ish
*  c.  start nodejs
*  d.  execute   http://localhost:8080/#/users/create  to create first user
*
****  added updateNodeByFltrKey function to main.js in node-neo4j module
* 
****  need to refine the data returned in neo4.js to appear more like mongo data. neo4j data is in an array [{flda:valuea,fldb:valeub}] mongo is {flda:valuea,fldb:valeub}
*     the difference requires handling the services and controls logic slightly different.
*
****  need to better understand how _id works may not need updateNodeByFltrKey ( use my own unique key vale ex: uuid() )
*
****   
*/

/*
 * REST POST : Create component
   server.post('/api/v1/users', api.postUserV1);
*/
function postComponentV1(req, res, next) {

	var newComponent = req.body;
    var newLabels =  [ newComponent.groupLabel, newComponent.typeLabel, newComponent.partLabel ];
    // var newLabels =  ['Components'];
    //console.log("DEBUG: postUserV1: newUser pre: ", newUser);
    newComponent['keyId'] = uuid.v1(); 
    //console.log("DEBUG: postUserV1: newUser post: ");
    //console.log( newUser );
    // insertNode({ name: 'Kristof' }, ['Student'], callback);
    
    neo4api.insert(newComponent, newLabels, function(err, result) {

            if(err) {

                console.log("DEBUG: postComponentsV1: insert : err *****: ", err );
                res.send(500, "ERROR: postComponentsV1 : insert : newComponent");
                return next();
            }
            else if(result) {

                // orig  res.send(201);
                // note: 20150235: changed neo4j.js to return result content in addition to status
                //                 returned values of result  are  result._id and result.keyId    
                console.log("DEBUG: postComponentsV1: insert : result *****: ", result );
                res.json(201, result );
                return next();
            }
        }
    );
}

/*
 * REST GET : Get components
   server.post('/api/v1/users', api.getUsersV1);
*/

function getComponentsV1(req, res, next) {
   
/**
 *  Get all nodes with label
 *  Given one label (non-empty string) or multiple labels (array of strings) and one or
 *  more properties in json returns an array of nodes with these labels and properties
     
    readNodesWithLabelsAndProperties('User',{ firstname: 'Sam', male: true }, callback);
 *       returns an array with nodes with the label 'User' and properties firstname='Sam' and male=true
 **/

    // Neo4j.prototype.findAllNodesWith = function(label, callback)
    //  
    // obslete original code - 20140916
    // neo4api.findAllNodesByLabel( ['Users'] , function(err, results) {
        
    //     //console.log("DEBUG: getUsersV1: findAllNodes : results ");  
    //     //console.log(results);
        
    //     if(err) {

    //         res.send(500, "ERROR: getNodesV1 : findAllNodes");
    //         return next();
    //     }
    //     else {

    //         if (results.length === 0) console.log('nada');

    //         //console.log("Query results: ", results)

    //         res.json(200, results);
    //         return next();
    //     }
    // });
    
    console.log("DEBUG: neo4Components.getComponentsV1: findNodesWithQuery : req.body pre ", req.query.cLabels  ); 
    

    var cLabels = req.query.cLabels;    // ['Components', 'Member' ]
    //var labelId = req.query.labelId ;
    //console.log("DEBUG: getUsersV1: findNodesWithQuery : req.body : " + req.body ); 
    //var uProperties = {"keyId": "IS NOT NULL", "lastName": "IS NOT NULL"};  lastName : 'Woods', firstName: 'Kimberlie' 
    var cUserKeyId  = req.query.userKeyId;
    var cProperties = { userKeyId: cUserKeyId } ;
    console.log("DEBUG: neo4Components.getComponentsV1: findNodesWithQuery : req.body post ", cLabels ); 
    neo4api.findNodesWithQuery( cLabels , cProperties , function(err, results) {    

        console.log("DEBUG: getComponentsV1: findNodesWithQuery :" );  
        console.log(results, err);
        
        if(err) {
            res.send(500, "ERROR: getComponentsV1 : findNodesWithQuery");
            return next();
        }
        else {

            if (results.length === 0) console.log('nada');

            res.json(200, results);
            return next();
        }
    });

}

//********************************

function componentFetchNodesByQueryV1(req, res, next) {
   

    // example query:
    // MATCH (n:`Users`) WHERE (n.lastName = 'Jones' AND n.firstName <> '' ) RETURN n,COUNT(n) ORDER BY n.lastName, n. firstName SKIP 14  LIMIT 7

   //console.log("DEBUG: userFetchNodesByQueryV1 : req params ID", req.body.fetchBy.fetchLastName);
   // console.log("DEBUG: userFetchNodesByQueryV1 : req params ID", req.body.fetchBy.fetchFirstName);
    
    //var rootNodeId = req.params.id;
    var newPage =  req.body.newPage - 1;
    var pageSize = req.body.pageSize;
    // build ORDER BY and WHERE Clauses
    var fetchQuery = "WHERE ";
    var fetchOrderBy = "ORDER BY " ;
    var fetchReturns = " n " ;
    // if ( req.body.fetchBy.fetchLastName ) {
    //     fetchOrderBy = fetchOrderBy.concat( "n.lastName, ");
    //     fetchQuery = fetchQuery.concat( "( LOWER (n.lastName ) = LOWER('" + req.body.fetchBy.fetchLastName + "')" );
    // };
    // 
    // else {
    //     fetchQuery = fetchQuery.concat( " and n.lastName <> " + "'')"  ); 
    // };

    if ( req.body.fetchBy.fetchLastName != "" & req.body.fetchBy.fetchFirstName != "" ) {
        fetchOrderBy = fetchOrderBy.concat( " n.lastName, n.firstName " );
        fetchQuery = fetchQuery.concat( " LOWER( n.lastName ) =~ LOWER('" + req.body.fetchBy.fetchLastName + "')" );  
        fetchQuery = fetchQuery.concat(" and LOWER( n.firstName ) =~ LOWER('" + req.body.fetchBy.fetchFirstName + "')" );

    } else if ( req.body.fetchBy.fetchLastName == ""  &  req.body.fetchBy.fetchFirstName != "" ) {
        // NOTE: may want query logic to work this 
        // fetchQuery = fetchQuery.concat( " n.lastName <> " + "'')"  ); 
        fetchOrderBy = fetchOrderBy.concat( " n.firstName " );
        fetchQuery = fetchQuery.concat(" LOWER( n.firstName) =~ LOWER('" + req.body.fetchBy.fetchFirstName + "')" );

    } else if ( req.body.fetchBy.fetchLastName != ""  &  req.body.fetchBy.fetchFirstName == "" ){
        fetchOrderBy = fetchOrderBy.concat( " n.lastName ");
        fetchQuery = fetchQuery.concat( " LOWER( n.lastName ) =~ LOWER('" + req.body.fetchBy.fetchLastName + "')" );  
        fetchQuery = fetchQuery.concat( " and ( n.firstName <> " + "'')"  ); 
    };
    console.log( req.body.fetchBy.fetchLastName );
    
    // if ( req.body.fetchBy.fetchLastName != "" & req.body.fetchBy.fetchFirstName != "" ) {
    //     fetchOrderBy = fetchOrderBy.concat( " n.lastName, n.firstName " );
    //     fetchQuery = fetchQuery.concat( " LOWER('" + req.body.fetchBy.fetchLastName + "') =~ LOWER( n.lastName )" );  
    //     fetchQuery = fetchQuery.concat(" AND LOWER('" + req.body.fetchBy.fetchFirstName + "') =~ LOWER( n.firstName )" );

    // } else if ( req.body.fetchBy.fetchLastName == ""  &  req.body.fetchBy.fetchFirstName != "" ) {
    //     // NOTE: may want query logic to work this 
    //     // fetchQuery = fetchQuery.concat( " n.lastName <> " + "'')"  ); 
    //     fetchOrderBy = fetchOrderBy.concat( " n.firstName " );
    //     fetchQuery = fetchQuery.concat(" LOWER( n.firstName) =~ LOWER('" + req.body.fetchBy.fetchFirstName + "')" );

    // } else if ( req.body.fetchBy.fetchLastName != ""  &  req.body.fetchBy.fetchFirstName == "" ){
    //     fetchOrderBy = fetchOrderBy.concat( " n.lastName ");
    //     fetchQuery = fetchQuery.concat( " LOWER( n.lastName ) =~ LOWER('" + req.body.fetchBy.fetchLastName + "')" );  
    //     fetchQuery = fetchQuery.concat( " and ( n.firstName <> " + "'')"  ); 
    // };

    // build WHERE Clause
    
    console.log("DEBUG: userFetchNodesByQueryV1 : query params: " );
    console.log( fetchQuery);
    console.log( fetchOrderBy);
    // get the relationships for this node
    // "START user = node(2) MATCH user-[:FOLLOWS]->followed RETURN followed"
    //var query = "MATCH (user:'Users' ) " + fetchQuery + " RETURN user, COUNT(user) " + fetchOrderBY +  " SKIP " + newPage + " LIMIT " + pageSize ;
    var query = "MATCH (n:Components ) " + fetchQuery + " RETURN " + fetchReturns + fetchOrderBy   +  " SKIP " + newPage + " LIMIT " + pageSize;
    var params = "";
    var include_stats = true;
    neo4api.cypherRunQuery(query, params, include_stats, function (err, results) {
        if(err)
        {
                res.send(500, "ERROR: userFetchNodesByQueryV1 : findRelationships : err : " + err.message);
                console.log("ERROR: userFetchNodesByQueryV1 : findRelationships : err :", err);
                return next();
        }
        else if ( results )
        { 
            //      we get back an array of relationships that matched the query      
            //      next loop over the array testing for otherId 
            //      then delete that relationship by id
              
            //var tmpFld =  results["data"].toString();  
            console.log("DEBUG: userFetchNodesByQueryV1 : findRelationshipsOfNode ", results );
            //console.log( tmpFld);
            if ( results['data'].length ) {

                res.send(200, results );
            }
            else {
                res.send(200, null );
            }

            return next();   
        }
    }); 
}




//**********************************



/*
 * REST PUT : Update Component
   server.put('/api/v1/users', api.putUserV1);
*/
function putComponentV1(req, res, next) {
    var  targetId = req.body._id;    

    var  targetKeyId = ""; 
    console.log("DEBUG: putComponentV1 : pre keyId :************ ", req.body,  targetKeyId);
    // don't allow user updates to nodes with null or '' targetkeyId
    if ( req.body.keyId === '' | req.body.keyId === null  ) {

        targetKeyId =  uuid.v1(); 
    }
    else {
   
        targetKeyId =  req.body.keyId; 
    };
   
    console.log("DEBUG: putComponentV1 : post keyId :************ ", targetKeyId, req.body.keyId );
    // properties that are not updateable by "member" user
    // *******
    //        labelId: "mbrsite",     ** Required  "mbrsite"
    //        _id  : "";              ** Required, assigned by server 
    //        keyId: "",              ** Required, assigned by server 
    //        userKeyId: "",          ** Required, derived from user   
    //        datecreated: "",        ** Required, set at time of obj creation
    //        status: "",             ** Required, set at system level-relates to validity of
    //         approveDate: "",
    // var whichComp =  req.body._id;    
    // switch( req.body.labelId ) {
    //             case 'mbrsite':
    //                  $scope.urole = $scope.selRoles[choice];
    //                 break;
    //             case 'utype':
    //                 $scope.utype =  $scope.selTypes[choice];
    //                 break;
    //             case 'uperm':
    //                 $scope.uperm = $scope.selPermissions[choice];
    //                 break;    
    //             default:
    //                 console.log("ERROR: UserProfileCtrl setUSChoices  *************************", choice );    
    //         } 

    var node_data = { 

            keyId : req.body.keyId || targetKeyId, 
            lastUpdated: req.body.lastUpdated || "",        
            updatedById: req.body.updatedById || "",
            isActive: req.body.isActive || "",      // ** Required, set false at creation-relates to state
            name : req.body.name || "",
            street : req.body.street || "",
            street2 : req.body.street2 || "",
            city : req.body.city || "",
            state : req.body.state || "",
            zipCode : req.body.zipCode || "",
            phone : req.body.phone || "",
            fax: req.body.fax || "",
            website: req.body.website || "",
            category: req.body.category || "", 
            description: req.body.description ||  "",
            identityId : req.body.indentityId || ""
            
          
        };

    // 20150305 NOTE temp DEBUG point to return from. See below commented section that needs more work to accomidate multiple component types.  to 
    // 20150305    console.log("DEBUG: putComponentV1 : updateByFldKey : nodeDatad", node_data );
    // 20150305    res.send(200);
    // 20150305 return next();

    // Update a Node properties
    // This will update all existing properties on the node with the new set of attributes using a user specified property as the key. */

    //var node_data = {firstName : req.body.firstName, lastName : req.body.lastName, password : req.body.password, userRole : req.body.userRole };
    //function updateById( node_id, node_data, callback){
    
    // Components have a three part label structure 
    var groupLabel = req.body.groupLabel;    // ['Components' ........ ]
    var typeLabel = req.body.typeLabel;    // ['Components', 'Mbrsite', ........ ]
    var partLabel = req.body.partLabel;    // ['Components', 'Mbrsite', 'Profile' ] 
    //[ groupLabel , typeLabel, partLabel ]
    neo4api.updateByFldKey(  ['Components', 'Mbrsite', 'Profile' ] , 'keyId', targetKeyId, node_data, function(err, user) {
    
            var resultsCnt = [user].length ;
            console.log("DEBUG: putComponentV1.updateByFldKey : resultsCnt w/err keyId: ", resultsCnt, err, targetKeyId );
            if(err) {
                console.log("ERROR: putComponentV1.updateByFldKey err :", err);
                res.send(500, "ERROR: putComponentV1.updateByFldKey : update");
                return next();
            }
            else if(   1 === resultsCnt ) {

                res.send(200);
                return next();
            }
            else {

                res.send(400, "ERROR: putComponentV1.updateByFldKey: Was not able to update user with keyId : " + user.keyId);
                return next();
        }
    });


}



function deleteComponentV1(req, res, next) {

    var queryStrings = req.getQuery();
    var idType = queryStrings.idType;
    var id = req.params.id;

    var properties = {};

    //console.log("DEBUG: idType : ", idType);
    //console.log("DEBUT: id : ", id);

    if("loginId" === idType) {

        properties["loginId"] = id;
    }
    else if("keyId" === idType) {

        //properties["_id"] = id;
        properties["keyId"] = id;
    }
    else if("objectId" === idType) {

        //properties["_id"] = id;
        properties["_id"] = id;
    }
    else {

        res.send(400, "ERROR: deleteUserV1 : Invalid id type");
        return next();
    }

    neo4api.removeWithQuery( ['Components'], properties,function(err, numRemovedDocs) {
        if(err) {

            res.send(500, "ERROR: deleteUserV1 : remove");
            return next();
        }
        else if(1 === numRemovedDocs) {

            res.send(200);
            return next();
        }
        else {

            res.send(400, "ERROR: Was not able to delete user with id : " + id);
            return next();
        }
    });
}


/**
    * GET user RELATIONSHIPS 

    /api/v1/users/:id/connect

    // options : direction: "in"  or "out"  or  dflt= "All"
    //           types:         ex: "FOLLOWS"    
*/
// function  userFindConnectionsActionV1(req, res, next) {
      
//     console.log("DEBUG: userFindConnectionsActionV1 : req params ID", req.params.id);
//     var rootNodeId = req.params.id;
//     var options =  req.body.options;
   

//     neo4api.findRelationshipsOfNode( rootNodeId, options, function (err, results) {
        
//         if(err)
//         {
//                 res.send(500, "ERROR: userFindConnectionsActionV1 : findRelationships : err : " + err.message);
//                 console.log("ERROR: userFindConnectionsActionV1 : findRelationships : err :", err);
//                 return next();
//         }
//         else if ( results )
//         { 
//             //      we get back an array of relationships that matched the query      
//             //      next loop over the array testing for otherId 
//             //      then delete that relationship by id
              
//             console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode " + results.length );
//             console.log( results );
   
//             var connections = [];           
            
//             for (var i = 0; i < results.length; i++) {
//                  // extract the followed Id of this relationship
//                 var followedId = results[i]['_end'];
                
//                 console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode : followedId  " + followedId );
  
//                 neo4api.findById( followedId, function( err, node ) {

//                     if(err) {

//                        // res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                        // console.log("ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                        // console.log(node)
//                        // return next();
//                     }
//                     else if( node ) {

//                         connections.push( node );

//                         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after connections.push" );
//                         console.log( connections );
//                     }
//                     else
//                     {
//                         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: no connections.push node" );
                        
//                     }
                
//                 });    

  
//             }
              
            
           
//        }
//          console.log("DEBUG: userFindConnectionsActionV1 : findRelationships : exiting  " );
//             console.log( connections );
//             res.send(200, connections);
//             return next();
       
//     }); 
        
// //neo4api.findWithQuery(
// //        ['Users'],
// //        { 'loginId': loginUser.loginId, 'password' : loginUser.password },
// //        function(err, user) {

// }




// function  userFindConnectionsActionV1(req, res, next) {
      
//     console.log("DEBUG: userFindConnectionsActionV1 : req params ID", req.params.id);
//     var rootNodeId = req.params.id;
//     var options =  req.body.options;
   
//     // get the relationships for this node
//     neo4api.findRelationshipsOfNode( rootNodeId, options, function (err, results) {
        
//         if(err)
//         {
//                 res.send(500, "ERROR: userFindConnectionsActionV1 : findRelationships : err : " + err.message);
//                 console.log("ERROR: userFindConnectionsActionV1 : findRelationships : err :", err);
//                 return next();
//         }
//         else if ( results )
//         { 
//             //      we get back an array of relationships that matched the query      
//             //      next loop over the array testing for otherId 
//             //      then delete that relationship by id
              
//             console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode " + results.length );
//             console.log( results );
   
//             var connections = "";           
//             var ids = [ ] ;
//             for (var i = 0; i < results.length; i++) {
//                  // extract the followed Id of this relationship
//                 var followedId = results[i]['_end'];
//                 ids.push( { 'ID' : followedId } ) ;

//                 console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after ids" );
//                 console.log(ids );

//                 //console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode : followedId  " + followedId );
  
//                 // neo4api.findById( followedId, function( err, node ) {

//                 //     if(err) {

//                 //        // res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                 //        // console.log("ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                 //        // console.log(node)
//                 //        // return next();
//                 //     }
//                 //     else if( node ) {

//                 //         connections.push( node );

//                 //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after connections.push" );
//                 //         console.log( connections );
//                 //     }
//                 //     else
//                 //     {
//                 //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: no connections.push node" );
                        
//                 //     }
                
//                 // });    

  
//             }
            
//            // { ID: 10 }, { ID: 5 }, { ID: 11 } 

//             // now get the nodes data 
//             var idsWrk = { keyId : "ef957de8-3d29-493b-9e91-806475fca4d1" };
//             // "5aa913a1-137c-11e4-9c59-a3ed62d316b1"
//               //  , { keyId: "ef957de8-3d29-493b-9e91-806475fca4d1"  }  
//              // ids.slice(0,ids.length);  0a5d0be1-137c-11e4-9c59-a3ed62d316b1
//             console.log( idsWrk );
           

//             neo4api.findWithQuery( ['Users'], idsWrk , function(err, nodes ) {
              
//                 if(err) {

//                   res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                   console.log( nodes );
//                   return next();
//                 }
//                 else { 
                    
//                     console.log("DEBUG: userFindConnectionsActionV1 : findRelationships : exiting  " );
//                     console.log( nodes );
//                     res.send(200, nodes);
//                     return next();
//                 }    
//             });
//         }
//     }); 
        
// }

  //  query
  //var query = "START user = node(" + 123) MATCH user-[:RELATED_TO]->friends RETURN friends"
  // 
  // add "LIMIT 100"  to query to reduce working data
  //var query = "START user = node(" + 123) MATCH user-[:RELATED_TO]->friends RETURN friends LIMIT 25"

function  componentFindConnectionsActionV1(req, res, next) {
      
    console.log("DEBUG: userFindConnectionsActionV1 : req params ID", req.params.id);
    var rootNodeId = req.params.id;
    var options =  req.body.options;
   
    // get the relationships for this node
    // "START user = node(2) MATCH user-[:FOLLOWS]->followed RETURN followed"
    var query = "START user = node(" + rootNodeId + ") MATCH user-[:FOLLOWS]->followed RETURN followed" ;
    var params = "";
    var include_stats = true;
    neo4api.cypherRunQuery(query, params, include_stats, function (err, results) {
        if(err)
        {
                res.send(500, "ERROR: userFindConnectionsActionV1 : findRelationships : err : " + err.message);
                console.log("ERROR: userFindConnectionsActionV1 : findRelationships : err :", err);
                return next();
        }
        else if ( results )
        { 
            //      we get back an array of relationships that matched the query      
            //      next loop over the array testing for otherId 
            //      then delete that relationship by id
              
            console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode " + results['data'].length );
            console.log( results['data'] );
            if ( results['data'].length ) {
                res.send(200, results['data']);
            }
            else {
                res.send(200, null );
            }

            return next();   
           //*************************************************************** 
           //  var connections = "";           
           //  var ids = [ ] ;
           //  for (var i = 0; i < results.length; i++) {
           //       // extract the followed Id of this relationship
           //      var followedId = results[i]['_end'];
           //      ids.push( { 'ID' : followedId } ) ;

           //      console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after ids" );
           //      console.log(ids );

           //      //console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode : followedId  " + followedId );
  
           //      // neo4api.findById( followedId, function( err, node ) {

           //      //     if(err) {

           //      //        // res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
           //      //        // console.log("ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
           //      //        // console.log(node)
           //      //        // return next();
           //      //     }
           //      //     else if( node ) {

           //      //         connections.push( node );

           //      //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after connections.push" );
           //      //         console.log( connections );
           //      //     }
           //      //     else
           //      //     {
           //      //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: no connections.push node" );
                        
           //      //     }
                
           //      // });    

  
           //  }
            
           // // { ID: 10 }, { ID: 5 }, { ID: 11 } 

           //  // now get the nodes data 
           //  var idsWrk = { keyId : "ef957de8-3d29-493b-9e91-806475fca4d1" };
           //  // "5aa913a1-137c-11e4-9c59-a3ed62d316b1"
           //    //  , { keyId: "ef957de8-3d29-493b-9e91-806475fca4d1"  }  
           //   // ids.slice(0,ids.length);  0a5d0be1-137c-11e4-9c59-a3ed62d316b1
           //  console.log( idsWrk );
           

           //  neo4api.findWithQuery( ['Users'], idsWrk , function(err, nodes ) {
              
           //      if(err) {

           //        res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
           //        console.log( nodes );
           //        return next();
           //      }
           //      else { 
                    
           //          console.log("DEBUG: userFindConnectionsActionV1 : findRelationships : exiting  " );
           //          console.log( nodes );
           //          res.send(200, nodes);
           //          return next();
           //      }    
           //  });
            //****************************************************



        }
    }); 
        
}


/**
    * SERVER PUT   server.put('/api/v1/users/:id/connect', api.userConnectActionV1);
        
        NOTES: after creating new relationships we must repopulate the user relationship data their browser.
               we can do this by retrieving an updated list of relationships for this user based on the type of
              relationship just created. This updated list would be returned to browser client. Sample results 
             
             Sample of returned results of insert operation :
               [ { since: 2014, more usersupplied properties... , _start: 1, _end: 10, _id: 8, _type: 'FOLLOWS' } ]
    

        req.params: req.params.id
        req.body  : 'otherId',
                    'relationType' : 'FOLLOWS'      other type can be used
                    'relationData' :  object properties of relationship. diff for each relationship been nodes  

        
*/
function componentConnectActionV1(req, res, next) {
        
    console.log("DEBUG: userConnectActionV1 : connect from ID", req.params.id);
    console.log( req.body );
    
    console.log(' need to setup id test like in delete')
    var rootNodeId = req.params.id;
    var otherNodeId = req.body.otherId;
    var relationType =  req.body.relationType;
    var relationData   = req.body.relationData ;
    neo4api.insertRelationship(rootNodeId, otherNodeId, relationType, relationData, function (err, results) {
       
        var resultswrk = [ results ];
        console.log("DEBUG: userConnectActionV1 : connect : resultsCnt : " + resultswrk.length );
        console.log( resultswrk );
        if(err) {
              res.send(500, "ERROR:  userConnectActionV1 : connect: err : " + err.message);
              console.log("ERROR: userConnectActionV1 : connect : err :", err);
              return next();
        }
        else if( 1 === resultswrk.length) {
               
                console.log("DEBUG: userConnectActionV1 : connect : count :", resultswrk);
                var followedId = resultswrk[0]['_end'];
                console.log( followedId );
                neo4api.findById( followedId , function(err, node){

                        if(err) {
                            console.log("DEBUG: userConnectActionV1 : findById : node : err", err );    
                            callback(err, null);
                            return next(); 
                        }
                        else if( node ) {
                            console.log("DEBUG: userConnectActionV1 : findById : node :", node );
                            res.send(200, node);
                            return next();
                
                        }
                        else {
                            console.log("DEBUG: userConnectActionV1 : findById : node : else ", node );
                            callback(null, null);
                            return next();
                        }
                });

        }
        else {
                        
                console.log("ERROR: userConnectActionV1 : connect : Was not able to set relationship");
                console.log(results)
                res.send(400, "ERROR: userConnectActionV1 : connect  : Was not able to set relationship");
                return next();
        }
  
    });


}

/**
    * POST /users/:id/disconnect

    // options : direction: "in"  or "out"  or  dflt= "All"
    //           types:         ex: "FOLLOWS"    
*/
function  componentDisconnectActionV1(req, res, next) {
      
    console.log("DEBUG: postUserDisconnectActionV1 : Disconnect from ID", req.params.id);
    var rootNodeId = req.params.id;
    var connectedNodeId = req.body.connectedId;
    var options =  req.body.options;
   
    neo4api.deleteRelationship(rootNodeId, connectedNodeId, options, function (err, result) {
    
        // what we get back is a result of true /number deleted or false indicating relationship wasn't found  
        if (err) {
                res.send(500, "ERROR:  userDisconnectActionV1 : disconnect: err : " + err.message);
                console.log("ERROR: userDisconnectActionV1 : disconnect : err :", err);
              return next();
        }
        else if( 1  === [result].length ) {
               console.log("DEBUG: userDisconnectActionV1 : disconnect : result :", result );
               // id of followed Node is returned
               res.send(200,  connectedNodeId  );
               return next();
        }
        else {
                        
                res.send(400, "ERROR: userDisconnectActionV1 : disconnect  : Was not able to delete / drop relationship");
                console.log("ERROR: userDisconnectActionV1 : disconnect : Was not able to delete / drop relationship");
                console.log(result)
                return next();
        }          
    }); 
}



exports.getComponentsV1 = getComponentsV1;
exports.componentFetchNodesByQueryV1 = componentFetchNodesByQueryV1;
exports.postComponentV1 = postComponentV1;
exports.putComponentV1 = putComponentV1;
exports.deleteComponentV1 = deleteComponentV1;
//exports.postComponentLoginV1 = postComponentLoginV1;
exports.componentConnectActionV1 = componentConnectActionV1;
exports.componentDisconnectActionV1 = componentDisconnectActionV1;
exports.componentFindConnectionsActionV1 = componentFindConnectionsActionV1;