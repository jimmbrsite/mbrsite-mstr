//var neo4jDb = require('node-neo4j');
//var neo4 = require('node-neo4j');
var neo4 = require('node-neo4j');
var url = 'http://localhost:7474';  // 'http://username:password@domain:port';
var neo4db = new neo4(url);
//var Util = require('util');
//var bcrypt = require('bcrypt');

//var db; 
//= new neo4j.GraphDatabase(process.env.NEO4J_URL || 'http://localhost:7474');
//var neo4jUrl = 'http://localhost:7474';
//var neo4j = new neo4jDb(neo4jUrl);
//var Util = require('util');
//var bcrypt = require('bcrypt');

//var db = new neo4j.GraphDatabase(process.env.NEO4J_URL || 'http://localhost:7474');

/*
 * Connecting to Neo4j
 */
function init(callback) {

    //neo4('http://localhost:7474'),
        // {
        //     db: {
        //         native_parser: false,
        //         w: 1,
        //         journal: true
        //     },
        //     server: {
        //         auto_reconnect: true
        //     },
        //     replSet: {},
        //     mongos: {}
        // },
        // function(err, newDb) {

        //     if(err) {

        //         console.log("ERROR: db.open : err = " + err.message);
        //     }
        //     else {

        //         db = newDb;
        //         callback();
        //     }
        // }
    callback();
};


/**
 * Query
*/
// function cypherRunQuery(query, params, callback) {

//     neo4db
//     .cypherQuery(query, params, function (err, results) {
//         if (err) {callback(err, null)}
//         else {callback(null, results.data)}
//      });
// };

/*  ONE EXAMPLE  of how a query might be contructeds
db.cypherQuery("START user = node(123) MATCH user-[:RELATED_TO]->friends RETURN friends", function(err, result){
    if(err) throw err;

    console.log(result.data); // delivers an array of query results
    console.log(result.columns); // delivers an array of names of objects getting returned
});
*/
// params, include_status,
function cypherRunQuery(query, params, include_stats, callback) {
    // neo4db.cypherQuery( query , params, include_stats,  function(err, results){
    //.cypherQuery( query ,function(err, results){
    neo4db
      .cypherQuery( query , params, include_stats,  function(err, results){
        // console.log("DEBUG: cypherRunQuery: results ", results.data[0] );
        if (err) {callback(err, null)}
        else {
          
          //console.log(results ); // delivers an array of query results
          //console.log(results.columns); // delivers an array of names of objects getting returned
          callback(null, results )}
      
    });
};

/**
 * INSERT / CREATE
 *  During the create process we assign the label and value to identify this node
 *  as belonging to set of nodes.application
 * 
 *  Examples:
 *   Insert a Node with no label:
 *      insertNode({ name: 'Kristof' }, callback);
 *   Insert a Node with one label:   
 *       insertNode({ name: 'Kristof' }, ['Student'], callback);
 *       insertNode({ name: 'Kristof' }, 'Student', callback);
 *       returns { _id: 14, name: 'Kristof' }
 * Insert a Node with three labels:
 *       insertNode({ name:'Darth Vader', level: 99, hobbies: ['lightsaber fighting', 'cycling in space'], shipIds: [123, 321] }, ['User', 'Evil' ,'Man'], callback);
 *       returns { _id: 17, name:'Darth Vader', level: 99, hobbies: ['lightsaber fighting', 'cycling in space'], shipIds: [123, 321] }   
 **/

function insert( params, labelsArray , callback) {

    neo4db.insertNode(params, labelsArray,  function (err, results) {
        console.log("DEBUG: neo4 : insert : err :", err ); 
        if (err) {callback(err, null)}

        else {
             
          callback(null, results )}  /* for 2.0.0-RC6, use: console.log(node._id) */
          // orig callback(null, results._id)}  /* for 2.0.0-RC6, use: console.log(node._id) */
    });
};

/**
 * findById 
 *   
 *  Examples:
 *    readNode(node_id, callback)
 *
 * 
 **/
function findById( nodeId, callback) {

    neo4db.readNode( nodeId , function(err, node){
                
        if(err) {
                
                console.log("DEBUG: neo4 : findById : err :", err);
                callback(err, null);
            }
            else if( node ) {
                
                console.log("DEBUG:  neo4 : findById : node : ", node);
                callback(null, node);  /* for 2.0.0-RC6, use: console.log(node._id) */
            }
            else {
                
                console.log("DEBUG: neo4: findById: else: ", node + " " + err );
                callback(null, null);
            }

        // Output node properties.
        //console.log(node.data);
       
        // Output node id.
        //console.log(node._id); /* for 2.0.0-RC6, use: console.log(node._id) */
    });

};

/** 
  *  FIND all Nodes within a LABEL 

*/
function findAllNodesByLabel( labelsArray, callback) {

    var tmp = labelsArray.toString();
    console.log( tmp); 
    neo4db.readNodesWithLabel( tmp , function(err, results){
                
        if(err) {

                callback(err, null);
            }
            else if( results ) {

                callback(null, results);  /* for 2.0.0-RC6, use: console.log(node._id) */
            }
            else {

                callback(null, null);
            }

        // Output node properties.
        //console.log(node.data);
       
        // Output node id.
        //console.log(node._id); /* for 2.0.0-RC6, use: console.log(node._id) */
    });

};



/**
 * FIND Nodes With Query
 *
 *  Get all nodes with labels and properties
 *  Given one label (non-empty string) or multiple labels (array of strings) and one or
 *  more properties in json returns an array of nodes with these labels and properties
 *   
 *   Examples:
 *   readNodesWithLabelsAndProperties('User',{ firstname: 'Sam', male: true }, callback);
 *       returns an array with nodes with the label 'User' and properties firstname='Sam' and male=true
 *   readNodesWithLabelsAndProperties(['User','Admin'], { 'name': 'DoesNotExist'}, callback); 
 *       returns an empty array
 **/

function findNodesWithQuery( labelsArray, properties, callback){
   
   //console.log("DEBUG: neo4 : findWith pre neo4db properties" );   
   //console.log( properties ); 
   neo4db.readNodesWithLabelsAndProperties( labelsArray, properties, function(err, results){
        if(err) {callback(err, null)}
         else { 
     //       console.log("DEBUG: neo4 : findWithQuery " );
     //      console.log( results) ;  
            callback(null, results);
        }

   });         
};

/*  Update a Node properties
    This will update all existing properties on the node with the new set of attributes using the _id property. */

function updateById( node_id, node_data, callback){
   neo4db.updateNodeById( node_id, node_data, function( err, node ) {

        if(err) {callback(err, null)}
        else { 
      //      console.log("DEBUG: neo4 : updateById" );
      //      console.log( node ) ;  
            callback(null, node );
        }

   });
};    


/* REST  
   Update a Node properties
    This will update all existing properties on the node with the new set of attributes using a user specified property as the key. */
function updateByFldKey( labelsArray, fltrFldName, fltrFldValue, node_data, callback){

    neo4db.updateNodeByFltrKey( labelsArray, fltrFldName, fltrFldValue, node_data, function( err, node ) {

        if(err) {callback(err, null)}
        else { 
        //    console.log("DEBUG: neo4 : updateByFldKey" );
        //    console.log( node ) ;  
            callback(null, node );
        }

   });
};    

/* REST updateWith
 *
 *
 * 
 * removeProperties , returnUpdatedNodes are optional args
 * function updateWith( labelsArray, oldProperties, newProperties, removeProperties , returnUpdatedNodes, callback){    
 */
function updateWith( labelsArray, oldProperties, newProperties, callback){        
  
  //console.log("DEBUG: neo4 : findWith updateNodeswith params" );
  //console.log( oldProperties );
  //console.log( newProperties );
  
  // var query = 'START data=node({_id}) SET ' + cypher.set('data', node_data) + ' RETURN data';

  
  //neo4db.updateNodesWithLabelsAndProperties( labelsArray, oldProperties, newProperties, removeProperties, returnUpdatedNodes , function(err, results){
  neo4db.updateNodesWithLabelsAndProperties( labelsArray, oldProperties, newProperties, function(err, results){ 
        if(err) {callback(err, null)}
         else { 
//            console.log("DEBUG: neo4 : updateWithQuery results" );
//            console.log( results) ;  
            callback(null, results);
        }

   });          
};

/**
 * REMOVE Node With Query
 *
 *  // Examples:
//  removeWithQuery('User',{ keyId: '0a5d0be1-137c-11e4-9c59-a3ed62d316b1' }, callback);
// 
**/

function removeWithQuery( labelsArray, properties, callback){
   
//   console.log("DEBUG: neo4 : reomveWithQuery pre neo4db properties" );   
//   console.log( properties ); 
   neo4db.deleteNodesWithLabelsAndProperties( labelsArray, properties, function(err, results){
        if(err) {callback(err, null)}
         else { 
//            console.log("DEBUG: neo4 : removeWithQuery " );
//            console.log( results) ;  
            callback(null, results);
        }

   });         
};

function findRelationshipsOfNode(node_id, options, callback) {


    neo4db.readRelationshipsOfNode( node_id, options, function ( err, results) {

        if(err) {callback(err, null)}
        else { 
              console.log("DEBUG: neo4 : read Relationships of node by type "  );
              console.log( results) ;  
              callback(null, results);
        }

    }); 

};


function insertRelationship( root_node_id, other_node_id, relationship_type, relationship_data, callback){

    neo4db.insertRelationship( root_node_id, other_node_id, relationship_type, relationship_data, function ( err, results) {

        if(err) {callback(err, null)}
        else { 
              console.log("DEBUG: neo4 : insertRelationship " );
              console.log( results) ;  
              callback(null, results);
        }

    });  

};  

// DELETE Relationship 
//    need Relationship _id in order to delete it.
//    options : direction: "in"  or "out"  or  dflt= "All"
//           types:         ex: "FOLLOWS"    
    
function deleteRelationship( rootNodeId, connectedNodeId , options, callback){

    // options : direction: "in"  or "out"  or  dflt= "All"
    //           types:         ex: "FOLLOWS"    
    // var options = [{ "direction" : "out", "types" : relationship_type } ]; 

    neo4db.readRelationshipsOfNode(rootNodeId, options, function( err, results) {
            if(err) {callback(err, null)}
            else { 
              console.log("DEBUG: neo4 : find relationship by type and direction" );
              console.log( results) ;  
        //      we get back an array of nodes that matched the query      
        //      next loop over the array testing for connetedNodeId vs the connetedRelId of the relationship  
        //      then delete that relationship by id
              
              console.log("DEBUG: neo4 : rootNode Id " + rootNodeId );
              
              for (var i = 0; i < results.length; i++) {
                 
                 var connectedRelId = results[i]['_end'];
                
                 console.log("DEBUG: neo4 : connected relationship Id  " + connectedRelId );

                 if (connectedRelId === connectedNodeId ) {
                      // extractg the relationship id and delete the relationship
                    var relationshipId = results[i]['_id'];
                   
                    neo4db.deleteRelationship( relationshipId , function( err, flag ) {

                           if(err) {

                                callback(err, null);
                            }
                            else if( flag ) {

                                 console.log("DEBUG: neo4 : dropped connected Node Id: " + connectedNodeId );
                                 callback(null,true);
                            }
                            else {

                                callback(null, false);
                            }

                    }); 
                    break;  

                 }
                 else 
                 {
                    console.log("DEBUG: neo4 : not dropping this relationship ",  connectedRelId );
                    
                 }
              }
            }  
            //callback(null, results);

    });
};

 

exports.init = init;     // initialize db vars etc
exports.insert = insert;  // create a single node
exports.findById = findById; // find by _id 
exports.findNodesWithQuery = findNodesWithQuery; // find by query 'keyId'
exports.findAllNodesByLabel = findAllNodesByLabel;  // find all node in 'User' label
exports.updateById = updateById;   // update by _id
exports.updateByFldKey = updateByFldKey; // update by 'keyId'
exports.updateWith = updateWith; 
exports.removeWithQuery = removeWithQuery; // remove node by 'keyId'
exports.insertRelationship = insertRelationship;  // create / insert relationship
exports.deleteRelationship = deleteRelationship;  // delete / drop relationship
exports.findRelationshipsOfNode = findRelationshipsOfNode; // find relationships of node matching type
exports.cypherRunQuery = cypherRunQuery;

