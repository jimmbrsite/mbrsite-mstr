// users.js
// Routes to CRUD users.


var _ = require('lodash')
    , HapiMeal = require('hapi-meal')
    , bcrypt = require('bcrypt')
    , Util = require('util')
    , Sjcl = require('sjcl');

var User = require(__dirname + '/../models/user');

/**
 *
 * Users controller
 * @type {Function}
 */

var UsersController = module.exports = function UsersController () {
    HapiMeal.Controller.prototype.constructor.apply(this, arguments);
};

_.extend(
    UsersController.prototype,
    HapiMeal.Controller.prototype
);

_.extend(
  UsersController.prototype,
  {
    /* *****
      * Login action
      * @param request
      * @param data
      * @return {*}
    */
    userregisterAction: function (request,  data , next) {
        console.log('testing for unique user id ' + request.payload.username );
        // TO DO: perhaps add logic to track attempts using this user id over short period
        if ( request.payload.username  && request.payload.password ) {

            // TO DO: finish implimentation of esc chars process  including rename of userID --> uername
            //          RegExp.escape = function(text) {
            //            return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
            //          }

            var UsernameEsc = request.payload.username; // RegExp.escape(request.payload.userID );
            var UserPswdEsc = request.payload.password; // RegExp.escape(request.payload.password );


            //  smode canRegister returns true if userID didn't exist
            var filterparms = { username: UsernameEsc,
                                userpswd: UserPswdEsc,
                               smode: 'canRegister'  // canRegister returns true  enable create this user
            };
            var salt = bcrypt.genSaltSync(10);
            User.registerCheck( filterparms, function (err, hits) {
                console.log('return from regCheck status err / hits: ' + err + ' ' + hits );
                 //if (err) return next(err);
                if (err) return request.reply.redirect('/register' );
                    console.log('unique user id: ' + request.payload.username );
                    User.create({
                        name: request.payload.name,
                        street: '',
                        city: '',
                        state: '',
                        zipcode: '',
                        homephone: '',
                        mobilephone: '',
                        contactphone: request.payload.contactphone,
                        email: request.payload.email,
                        username: UsernameEsc,
                        password: bcrypt.hashSync(request.payload.password, salt),
                        key: salt,
                        algorithm: 'sha256',
                        social: '{ facebook: null, twitter: null }',
                        website: '',
                        bioinfo: ''
                    },
                    function (err, user) {
                        if ( user ) {
                            console.log('db back from create user user: ' + Util.inspect( user.valueOf() , { showHidden: false, depth: 1 } ));
                        }
                        if ( err ) {
                            console.log('db back from create user err: ' + Util.inspect( err.valueOf() , { showHidden: false, depth: 1 } ));
                            console.log('db back from create user err: ' + err.toString() );
                        }
                        if (err) return next(err);
                        console.log('registered new user: ' + user.id );
                        data.title = 'Register';
                        //extra = user.id;
                        //return request.reply.redirect('/users/_/' + user.id );
                        console.log('register create : ' + user.username );
                        return request.reply.redirect('/' );
                    });
           });
        };
        return request.reply.redirect('/register' );
    },
    loginAction:function (request, data) {
        console.log('start login:');
        if (!data.user) {
          data.title = 'Login';

          if (request.method === 'post') {
              // Validate query
              var Passport = this.app.server.plugins.travelogue.passport;
              Passport.authenticate('local', {
                  successRedirect: this.app.settings.plugins.travelogue.urls.successRedirect,
                  failureRedirect: this.app.settings.plugins.travelogue.urls.failureRedirect
              })(request);

                //console.log('post user login ' + Util.inspect( request.users.valueOf() , { showHidden: false, depth: 4 } ));

          } else {
              console.log('not post user login');
              return request.reply.view(data.viewName, data);
          }

        // Redirect logged users
        } else {
          return request.reply.redirect('/');
        }
    },

    socialAuth: function (request, type, options) {
          var Passport = this.app.server.plugins.travelogue.passport;
          Passport.authenticate(type, options)(request);
    },

    facebookLoginAction: function(request, data) { this.socialAuth(request, 'facebook', { scope: ['email', 'user_location'] }); },
    twitterLoginAction: function(request, data) { this.socialAuth(request, 'twitter'); },

    socialAuthCallback: function (request, type) {
        try {
              var Passport = this.app.server.plugins.travelogue.passport;
              Passport.authenticate(
                  type, this.app.settings.plugins.travelogue.urls
              )(request);

        } catch (error) {
              request.reply.redirect('/users/login?auth_error');
        }
    },
    facebookLoginCallbackAction: function(request, data) { this.socialAuthCallback(request, 'facebook'); },
    twitterLoginCallbackAction: function(request, data) { this.socialAuthCallback(request, 'twitter'); },

    /**
     * GET /users
     */
    userslistAction: function (request, data ) {
        console.log('userLISTAction');
        User.getAll(function (err, users) {
            if (err) return next(err);
            data.title = 'Users List';
            data.userslist = users;
            //data.viewName = '/user/users.html';
            //return request.reply.view('./users/userslist.html', { title: 'Users List',  userslist: users }, data  );
            return request.reply.view(data.viewName, data );
        });
    },
    /**
    * POST /users
    */
    userscreateAction: function (request,  data, next ) {
        User.create({
          name: request.payload.name,
          street: request.payload.street,
          city: request.payload.city,
          state: request.payload.state,
          zipcode: request.payload.zipcode,
          homephone: request.payload.homephone,
          mobilephone: request.payload.mobilephone,
          email: request.payload.email,
          website: request.payload.website,
          bioinfo: request.payload.bioinfo
          },
        function (err, user) {
          if (err) return next(err);
          extra = user.id;
          return request.reply.redirect('/users/' + user.id );
        });
    },
    /**
       * GET /users/:id
    */
    usersshowAction: function (request, data ) {
      User.get( request.params.ID , function (err, user) {
          if (err) {
              try {
                    throw {name: 'usersshow',
                           message: err
                    }
              }
              catch (e) {
                console.log( e)
              };

              return err;
          };
          // TODO also fetch and show followers? (not just follow*ing*)
          user.getFollowingAndOthers(function (err, following, others) {
              if (err) return err;
              //var myfollowing = following;
              //console.log('usersSHOW following: ' + Util.inspect( following[1].valueOf() , { showHidden: false, depth: null } ));
              data.title = 'Users Show';
              data.viewName = 'users/usersshow.html';
              data.usersdat = user;
              data.following = following;
              data.others = others;
              return request.reply.view(data.viewName, data , next);
          });
      });
    },
    /**
    * POST /users/:id
    */
    userseditAction: function (request, data, next) {
      console.log('paramsID: ' + request.params.ID);
      User.get(request.params.ID, function (err, user) {
          if (err) return next(err);
          user.name = request.payload['name'];
          user.street = request.payload['street'];
          user.city = request.payload['city'];
          user.state =  request.payload['state'];
          user.zipcode =  request.payload['zipcode'];
          user.homephone =  request.payload['homephone'];
          user.mobilephone =  request.payload['mobilephone'];
          user.email =  request.payload['email'];
          user.website =  request.payload['website'];
          user.bioinfo =  request.payload['bioinfo'];
          user.save(function (err) {
              if (err) return next(err);
              console.log('userid: ' + user.id + ' paramsID: ' + request.params.ID)
              return request.reply.redirect('/users/' + user.id);
          });
      });
    },
    /**
      * DELETE /users/:id
    */
    usersdelAction: function (request, data, next) {
        console.log('paramsID: ' + request.params.ID);
      User.get(request.params.ID, function (err, user) {
          if (err) return next(err);
          user.del(function (err) {
              if (err) return next(err);
              return request.reply.redirect('/users/listusers', data );
          });
      });
    },
    /**
      * POST /users/:id/follow
    */
    usersfollowAction: function (request, data, next) {
      console.log('paramsID: ' + request.params.ID);
      User.get(request.params.ID, function (err, user) {
          if (err) return next(err);
          User.get(request.payload.userId, function (err, other) {
              if (err) return next(err);
              user.follow(other, function (err) {
                  if (err) return next(err);
                  return request.reply.redirect('/users/' + user.id);
              });
          });
      });

    },
    /**
      * POST /users/:id/unfollow
    */
    usersunfollowAction: function (request, data, next) {
      console.log('paramsID: ' + request.params.ID);
      User.get(request.params.ID, function (err, user) {
          if (err) return next(err);
          User.get(request.payload.otherId, function (err, other) {
              if (err) return next(err);
              user.unfollow(other, function (err) {
                  if (err) return next(err);
                  return request.reply.redirect('/users/' + user.id);
              });
          });
      });
    }
  }
);
