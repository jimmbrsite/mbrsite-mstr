
var neo4api = require('./neo4');


function validateAuthZ(req, res, next) {
    // decorated req set during prior process  (authentication ) 
    var userRole = req['trainingUserRole'];
    //    console.log("DEBUG: validateAuthZ : userRole : ", userRole);

    if("Owner" === userRole || "Manager" === userRole || "Staff" === userRole || "Volunteer" === userRole) {
        
        // types:
        // Root
        // Sysadmin
        // Admin
        // Member
        // Follower
        // Subscriber

        return next();
    }
    else {

        res.send(403, "ERROR: validateAuthZ : invalid role");
        console.log("ERROR: validateAuthZ : invalid role : ", userRole);
        return;
    }
}

function validateAuthN(req, res, next) {

    var authorization = req.authorization;

   if(authorization && authorization.scheme && authorization.scheme === 'Session' && authorization.credentials ) {
        // use auth token to find user and extract  authToken  userRole 
        // { 'fields' : { 'authToken' : 1, 'userRole' : 1 }},
        // orig: findAllNodesByLabel
        // 20140916: findNodesWithQuery
        neo4api.findNodesWithQuery(
            ['Users'],
            { 'authToken' : authorization.credentials },
            function(err, user) {
    //          console.log("DEBUG: validateAuthN : return from findWith : user ");    
    //          console.log( user );    
                if(err) {

                    res.send(500, "ERROR: validateAuthN : err : " + err.message);
                    console.log("ERROR: validateAuthN : err : ", err);
                    return;
                }
                else if(user[0].authToken && "" !== user[0].authToken) {
                    // decorate req with role/permissions - used in next process ( authorization )
                    req['trainingUserRole'] = user[0].role;
                    return next();
                }
                else {

                    res.send(403, "ERROR: validateAuthN : invalid AuthToken");
                    console.log("ERROR: validateAuthN : invalid AuthToken");
                    return;
                }
            }
        );
    }
    else {

        res.send(400, "ERROR: validateAuthN : Invalid authorization headers");
        console.log("ERROR: validateAuthN : Invalid authorization headers");
        return;
    }
}

exports.validateAuthN = validateAuthN;
exports.validateAuthZ = validateAuthZ;