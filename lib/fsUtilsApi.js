var fs = require('fs');
var url = require('url');
var mime = require('mime');

var http = require("http");
//var multipart = require("multipart");
//var multiparty = require("multiparty");
var formidable = require("formidable");
var util = require("util");
var sys = require("sys");


/* launch image form */
// fsApi.putImageV1
function putImageV1(req,res,next) {
    console.log("DEBUG: FS: putImageV1: req : ", req.data  );
    display_form(req, res);
    // fs.writeFileSync("../test.jpg",req.data,"binary",function(err){
    //         if(err) throw err;
    //         res.send("OK")
    // })
    //fs.createWriteStream('test.png').pipe(req).on('end', function() {  console.log("DEBUG: FS: postImagesV1: pipe end : " ) });

}

/* Upload file Images to server FS ( fileStream ) */
// fsApi.postImagesV1
function postImagesV1(req,res,next) {
    console.log("DEBUG: FS: postImagesV1: req.route : ", req.route  );
     // // a file upload using upload ( this )   
     // upload_file(req, res);
   
     // a file upload using multiparty
     //useMultipartyFileUpload( req, res);
    
     // a file upload using formidable 
     useFormidableFileUpload( req, res );
}    
/*
 * Display upload form
 */
function display_form(req, res) {
    res.sendHeader(200, {"Content-Type": "text/html"});
    res.write(
        '<form action="/api/v1/images/upload" method="post" enctype="multipart/form-data">'+
        '<input type="file" name="upload-file">'+
        '<input type="submit" value="Upload">'+
        '</form>'
    );
    res.close();
}

/*
 * Create multipart parser to parse given request
 */
function parse_multipart(req) {

    console.log("DEBUG: FS: postImagesV1.upload_file parse_multipart req.headers : *  * * * * * * * * * * * ", req.headers ); 
    
    var parser = multipart.parser();

    // Make parser use parsed request headers
    parser.headers = req.headers;

    // Add listeners to request, transfering data to parser
 
    req.addListener("data", function(chunk) {
        parser.write(chunk);
    });

    req.addListener("end", function() {
        parser.close();
    });

    return parser;
}

/* Upload a File using  Formidable */

function useFormidableFileUpload( req, res ) {

    console.log('onStart useFormidableFileUpload start: ****************** ', req.path );
    
    var form = new formidable.IncomingForm( { keepExtensions: true, multiples: true, msg:'hereweare' }),
        files = [],
        fields = [];

    form.uploadDir = 'app/common/uploads';

    form
      .on('field', function(field, value) {
        console.log('onField: ****************** ', field, value);
        fields.push([field, value]);


      })
      .on('file', function(field, file) {
        console.log('onFile field : ****************** ', field);
        console.log('onFile file  : ****************** ', file);
        //var jpath = file.path.pop();
              
        //var file.path = jpath.
        //console.log('onFile jbs path name: ****************** ', jpath);
       // var jgh= util.inspect(files[0][1].path);
       // console.log('onFile :  ******************-> upload done', res ); 


// HERE



        files.push([field, file]);
      })
      .on('end', function() {
        var wrkPath = util.inspect(files[0][1].path);
        
        console.log('onEnd :  ******************-> upload done', res );
        //res.setHeader('resPath', jgh);
        res.writeHead(200, {'content-type': 'text/plain' });
        res.write( wrkPath + ':' );   
        res.write(' received fields:\n\n '+util.inspect(fields));
        res.write('\n\n');
        res.end('received files:\n\n '+util.inspect(files));
      });
    form.parse(req); 
}

/* Upload A File  */
function useMultipartyFileUpload( req, res) {

    var form = new multiparty.Form();
 
    form.parse(req, function(err, fields, files) {
      res.writeHead(200, {'content-type': 'text/plain'});
      res.write('received upload:\n\n');
      res.end(util.inspect({fields: fields, files: files}));
    });
}


/*
 * Handle file upload
 */
function upload_file(req, res) {
     console.log("DEBUG: FS: postImagesV1.upload_file start : *  * * * * * * * * * * * ", req.route  );
    // Request body is binary
    
    // no such method 20150612 change to setEncoding Jim
    //req.setBodyEncoding("binary");
   
   // req.setEncoding("binary");    // "binary"  utf8
   


    // Handle request as multipart
    var stream = parse_multipart(req);

    var fileName = null;
    var fileStream = null;
    
    console.log("DEBUG: FS: postImagesV1.upload_file stream : *  * * * * * * * * * * * ", stream );
    
    // Set handler for a request part received
    stream.onPartBegin = function(part) {
        sys.debug("Started part, name = " + part.name + ", filename = " + part.filename);

        // Construct file name
        fileName = "./uploads/" + stream.part.filename;

        // Construct stream used to write to file
        fileStream = fs.createWriteStream(fileName);

        // Add error handler
        fileStream.addListener("error", function(err) {
            sys.debug("Got error while writing to file '" + fileName + "': ", err);
        });

        // Add drain (all queued data written) handler to resume receiving request data
        fileStream.addListener("drain", function() {
            req.resume();
        });
    };

    // Set handler for a request part body chunk received
    stream.onData = function(chunk) {
        // Pause receiving request data (until current chunk is written)
        req.pause();

        // Write chunk to file
        // Note that it is important to write in binary mode
        // Otherwise UTF-8 characters are interpreted
        sys.debug("Writing chunk");
        fileStream.write(chunk, "binary");
    };

    // Set handler for request completed
    stream.onEnd = function() {
        // As this is after request completed, all writes should have been queued by now
        // So following callback will be executed after all the data is written out
        fileStream.addListener("drain", function() {
            // Close file stream
            fileStream.end();
            // Handle request completion, as all chunks were already written
            upload_complete(res);
        });
    };
}

function upload_complete(res) {
    sys.debug("Request complete");

    // Render response
    res.sendHeader(200, {"Content-Type": "text/plain"});
    res.write("Thanks for playing!");
    res.end();

    sys.puts("\n=> Done");
}

/*
 * Handles page not found error
 */
function show_404(req, res) {
    res.sendHeader(404, {"Content-Type": "text/plain"});
    res.write("You r doing it rong!");
    res.end();
}



exports.putImageV1  = putImageV1;
exports.postImagesV1  = postImagesV1;