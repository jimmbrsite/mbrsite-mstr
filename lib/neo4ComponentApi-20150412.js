// neo4ComponentApi.js
// Neo4j - component api 

var neo4api = require('./neo4');
var uuid = require('node-uuid');

/* NOTES:
*  
****  setting up an initial Component Page set in Neo4j:
*  
*
****   
*/

function componentInitialPageCreateByQueryV1(req, res, next) {
   
   
    //var rootNodeId = req.params.id;
    var newPage =  req.body.newPage - 1;
    var pageSize = req.body.pageSize;
    // build ORDER BY and WHERE Clauses
    var createQuery = "CREATE ";
    var createParms = "ORDER BY " ;
    // may want to return data from create
    var createReturns = " n " ;
   
    if ( req.body.fetchBy.fetchLastName != "" & req.body.fetchBy.fetchFirstName != "" ) {
        fetchOrderBy = fetchOrderBy.concat( " n.lastName, n.firstName " );
        fetchQuery = fetchQuery.concat( " LOWER( n.lastName ) =~ LOWER('" + req.body.fetchBy.fetchLastName + "')" );  
        fetchQuery = fetchQuery.concat(" and LOWER( n.firstName ) =~ LOWER('" + req.body.fetchBy.fetchFirstName + "')" );

    } else if ( req.body.fetchBy.fetchLastName == ""  &  req.body.fetchBy.fetchFirstName != "" ) {
        // NOTE: may want query logic to work this 
        // fetchQuery = fetchQuery.concat( " n.lastName <> " + "'')"  ); 
        fetchOrderBy = fetchOrderBy.concat( " n.firstName " );
        fetchQuery = fetchQuery.concat(" LOWER( n.firstName) =~ LOWER('" + req.body.fetchBy.fetchFirstName + "')" );

    } else if ( req.body.fetchBy.fetchLastName != ""  &  req.body.fetchBy.fetchFirstName == "" ){
        fetchOrderBy = fetchOrderBy.concat( " n.lastName ");
        fetchQuery = fetchQuery.concat( " LOWER( n.lastName ) =~ LOWER('" + req.body.fetchBy.fetchLastName + "')" );  
        fetchQuery = fetchQuery.concat( " and ( n.firstName <> " + "'')"  ); 
    };
    console.log( req.body.fetchBy.fetchLastName );
    
    
    console.log("DEBUG: componentPageCreateByQueryV1 : query params: " );
    console.log( createQuery);
    console.log( createParms);
    // get the relationships for this node
    // "START user = node(2) MATCH user-[:FOLLOWS]->followed RETURN followed"
    //var query = "MATCH (user:'Users' ) " + fetchQuery + " RETURN user, COUNT(user) " + fetchOrderBY +  " SKIP " + newPage + " LIMIT " + pageSize ;
    //var query = "MATCH (n:Components ) " + fetchQuery + " RETURN " + fetchReturns + fetchOrderBy   +  " SKIP " + newPage + " LIMIT " + pageSize;
    
    var params = "";
    var include_stats = true;
    neo4api.cypherRunQuery(query, params, include_stats, function (err, results) {
        if(err)
        {
                res.send(500, "ERROR: userFetchNodesByQueryV1 : findRelationships : err : " + err.message);
                console.log("ERROR: userFetchNodesByQueryV1 : findRelationships : err :", err);
                return next();
        }
        else if ( results )
        { 
            //      we get back an array of relationships that matched the query      
            //      next loop over the array testing for otherId 
            //      then delete that relationship by id
              
            //var tmpFld =  results["data"].toString();  
            console.log("DEBUG: userFetchNodesByQueryV1 : findRelationshipsOfNode ", results );
            //console.log( tmpFld);
            if ( results['data'].length ) {

                res.send(200, results );
            }
            else {
                res.send(200, null );
            }

            return next();   
        }
    }); 
}





/*
 * REST POST : Create component
   server.post('/api/v1/users', api.postUserV1);
*/
function postComponentV1(req, res, next) {
   
    var lblArrayProfile = req.body.lblArray;

	var newComponentProfile = {
            //       _id                  **  assigned by server 
            keyId:        uuid.v1(),                          //** Required  
            groupLabel:   req.body.groupLabel || "Errors",    // ** Required,,   "Component" ,   ** Required,  "Component"
            typeLabel :   req.body.typeLabel  || "Errors",    // ** Required,  "Mbrsite",      ** Required, "mbrsite"
            partLabel:    req.body.partLabel  || "Errors",    // ** Required,,   "Profile"       ** Required, "Profile"
            lblArray :    req.body.lblArray   || "Errors" ,   // ** Required,,  'groupLabel:typeLabel:partlabel' 
            userKeyId:    req.body.userKeyId || "Errors",    // ** Required,,   "",      ** Required, derived from "member" user who is will be "Owner" 
            createdByKeyId: req.body.createdByKeyId  || "Errors",         // ** Required,,    "", ** Required, derived from "Creator" user  Sysadmin, Owner
            dateCreated:  req.body.dateCreated || new Date(),         // ** Required set at time of obj creation
            status:       req.body.status  || "Errors",    // ** Required,   ** Required, set at system level-relates to validity of
            approveDate: req.body.approveDate || new Date(),         // ** Required, set at time of obj creation
            approvedById: req.body.approvedById  || "Errors",   //  ** Required, set at time of obj creation
            lastUpdated: req.body.lastUpdated || new Date(),         // ** Required but updated as changes occur
            updatedById: req.body.updatedById  || "Errors",     //   Required, but updated as changes occur
            isActive: req.body.isActive  || false,             // ** Required, set false at creation-relates to state of
            name: req.body.name || "Errors",           // NOTE: Each component type has a more or less the same profile 
            // optional properties  from form 
            street: req.body.street || "",   
            street2: req.body.street2 || "",
            city: req.body.city || "",
            state: req.body.state || "",
            zipCode: req.body.zipCode || "",
            phone: req.body.phone || "",
            fax: req.body.fax || "",
            webSite: req.body.webSite || "",
            email: req.body.email || "",
            identityId: req.body.identityId || "",
            category: req.body.category || [],             
            keywords: req.body.keywords || [],
            /* obs 2050317  
            // tabSetBar 
            tabBarId: req.body.tabBarId || [], 
            tabBarHref: req.body.tabBarHref || [], 
            tabBarLabelText: req.body.tabBarLabelText || [], 
            tabBarisActive: req.body.tabBarisActive || [], 
            tabBarOrder: req.body.tabBarOrder || [], 
            // tabPanes for each tabPane there can be several ContentModel entries
            paneTabIds: req.body.paneTabIds || [],                         // ["tab0-1","tab0-2"];
            paneTabTitles: req.body.paneTabTitles || [],                    // ["Blank Template Two", "Blank Template Three"]
            paneTabContentIds: req.body.paneTabContentIds || [],
            paneTabContentKeyIds: req.body.paneTabContentKeyIds || [],
            */
            tabPaneLastUpdated: req.body.tabPaneLastUpdated ||  ["Errors"],
            tabPaneUpdatedById: req.body.tabPaneUpdatedById ||  ["Errors"], 
            tabPaneIsActive: req.body.tabPaneIsActive ||  [false],            
            tabPaneStatus: req.body.tabPaneStatus ||  ["Inactive"],  // dflt: Inactive Public,Internal Only Private,Inactive
                            
            tabPaneBarId: req.body.tabPaneBarId ||  [],  //  [100,200,300,400,500,600];
            tabPaneBarHref: req.body.tabPaneBarHref ||  [],  // ['href="#tab0-1"','href="#tab1-1"','href="#tab1-2"','href="#tab1-3"','href="#tab2-1"','href="#tab3-1"'];
            tabPaneBarLabelText: req.body.tabPaneBarLabelText ||  [],  //["My Home","Blank 1","Blank 2","Blank 3","Grid 1","Google Map"];
            tabPaneBarIsActive: req.body.tabPaneBarIsActive ||  [],  //[true,false,false,false,false,false];
            tabPaneBarOrder: req.body.tabPaneBarOrder ||  [],  //[1,2,3,4,5,6] ;
            // tabPane for each paneTab there can be several ContentModel entries
            tabPaneTabIds: req.body.tabPaneTabIds ||  [],  //["tab1-1","tab1-2"];
            tabPaneTabTitles: req.body.tabPaneTabTitles ||  [],  //["Blank Template One","Blank Template Two"];
            // the following arrays are filled with the _id and KeyId of each contentModel created as described
            // below.
            tabPaneTabContentIds: req.body.tabPaneTabContentIds ||  [],  //
            tabPaneTabContentKeyIds: req.body.tabPaneTabContentKeyIds ||  []  //

    };

     /* TAB PANE SET DATA              
                           
                           NOTE: New Biz Form data loaded earlier in this function with this 
                           $scope.savedComponent = angular.copy(addNuUser.nucomponent);

    */ 
     //  var lblArrayTP = req.body.tabPaneLblArray;                    
    // var newComponentTabPane = {
        
    //     // tabPaneGroupLabel: req.body.tabPaneGroupLabel || "Components",  // "Components";
    //     // tabPaneLblArray: req.body.tabPaneLblArray ||  "Errors",  // "Components";
                         
    //     // tabPaneTypeLabel: req.body.tabPaneTypeLabel ||  "Errors", 
    //     // tabPanePartLabel: req.body.tabPanePartLabel ||  "Errors",
    //     // tabPaneUserKeyId: req.body.tabPaneUserKeyId ||  "Errors",
    //     // tabPaneCreatedByKeyId: req.body.tabPaneCreatedByKeyId ||  "Errors",
    //     // tabPaneDateCreated: req.body..tabPaneDateCreated ||  "Errors",
         
    //     tabPaneLastUpdated: req.body.tabPaneLastUpdated ||  "Errors",
    //     tabPaneUpdatedById: req.body.tabPaneUpdatedById ||  "Errors", 
    //     tabPaneIsActive: req.body.tabPaneIsActive ||  false,            
    //     tabPaneStatus: req.body.tabPaneStatus ||  "Inactive",  // dflt: Inactive Public,Internal Only Private,Inactive
                        
    //     tabPaneBarId: req.body.tabPaneBarId ||  [],  //  [100,200,300,400,500,600];
    //     tabPaneBarHref: req.body.tabPaneBarHref ||  [],  // ['href="#tab0-1"','href="#tab1-1"','href="#tab1-2"','href="#tab1-3"','href="#tab2-1"','href="#tab3-1"'];
    //     tabPaneBarLabelText: req.body.tabPaneBarLabelText ||  [],  //["My Home","Blank 1","Blank 2","Blank 3","Grid 1","Google Map"];
    //     tabPaneBarIsActive: req.body.tabPaneBarIsActive ||  [],  //[true,false,false,false,false,false];
    //     tabPaneBarOrder: req.body.tabPaneBarOrder ||  [],  //[1,2,3,4,5,6] ;
    //     // tabPane for each tabPane there can be several ContentModel content entries per tabPane which are written to DB in one Node/document 
    //     tabPaneTabIds: req.body.tabPaneTabIds ||  [],  //["tab1-1","tab1-2"];
    //     tabPaneTabTitles: req.body.tabPaneTabTitles ||  [],  //["Blank Template One","Blank Template Two"];
    //     // the following arrays are filled with the _id and KeyId of each contentModel created as described
    //     // below.
    //     tabPaneTabContentIds: req.body.tabPaneTabContentIds ||  [],  // each Id / KeyId entry represent all contentModels for a tabPane
    //     tabPaneTabContentKeyIds: req.body.tabPaneTabContentKeyIds ||  []  //

    // };

  
    /* TAB CONTENT MODEL DATA
    */ 
    // ContentModels: for each contentModel there are four content entries title,text,imgsrc, imgTextArea, linkTarget, LinkTextArea
    /* 20150317 : Note:    Currently for testing purposes this process creates a single node/document per page.  
    *
    *             in the code below the contentModel Array will be broken into individual
    *             node/documents based on the contentTabIds. In sample data below four contentModel sets are represented.
    *             One of the modelLabels (tab0-1) uses the "model" key word to identify the source set Member:Profile
    *             that contains data extracted from the profile. Other Modellables use a "This: key word indentifing the source which is
    *             found in this content array element. These samples will result in one contentModel node/document being written to the
    *             database. If the contentModel. Had additonal data (contentModels) to persist in the DB then an additional array element is created and inserted 
    *             following the last occurance of the matching tabId.  
     

    *             
    // keyId   assigned by DB 
    // ** required properties
    */
    var lblArrayTabContent = req.body.contentLblArray;
    var newComponentTabContent = {
        //       _id                  **  assigned by server 
        keyId:        uuid.v1(),      //** Required  
        contentGroupLabel:     req.body.contentGroupLabel ||  "Components",  //
        contentLblArray:       req.body.contentLblArray || "Errors",
        contentTypeLabel:      req.body.contentTypeLabel  || "Errors",
        contentPartLabel:      req.body.contentPartLabel || "Errors",
        contentUserKeyId:      req.body.contentUserKeyId   || "Errors",
        contentCreatedByKeyId:   req.body.contentCreatedByKeyId   || "Errors",
        contentDateCreated:    req.body.contentDateCreated  || "Errors", 
        contentLastUpdated:    req.body.contentLastUpdated  || "Errors",
        contentUpdatedById:    req.body.contentUpdatedById   || "Errors", 
        contentIsActive:       req.body.contentIsActive  || false,            
        contentStatus:         req.body.contentStatus  || "Inactive",      // dflt: Inactive Public,Internal Only Private,Inactive
        contentModelSrcs:       req.body.contentModelSrcs  || [],            // ["Model", "This","This"];    // dflt: "This"  [ This, Model, ..]
        contentModelIds:       req.body.contentModelIds  || [],            // ["mbrsite.profile,_null, null];
        contentModelLabels:     req.body.contentModelLabels  || [],          // ["tab0-1","tab1-1","tab1-2"];
        contentModelNames:     req.body.contentModelNames   || [],         // ["contentOne","contentTWO"];                
        contentModelIsOk:      req.body.contentModelIsOk   || [],          // ["Support","Support"];
        contentModelTitleText:    req.body.contentModelTitleText  || [],   // ["Sample Title 1","Sample Title 2"];
        contentModelTextArea:  req.body.contentModelTextArea  || [],   // ['mbrs-Etiam sit amet leo nec nunc auctor convallis vel at est. Aenean at metus id nisl posuere varius. In vel ipsum eu risus iaculis volutpat in ut metus. Integer eget sem eu massa pharetra vehicula ut id elit. Nam sagittis malesuada est, ut tempus diam ultricies eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla urna libero, finibus in metus mollis, aliquam interdum elit. Phasellus at sagittis lectus. Vestibulum scelerisque libero eget efficitur venenatis. Etiam nunc magna, maximus vel vehicula eget, bibendum eu urna. Phasellus quis turpis id lacus mollis dictum. Ut dui elit, efficitur vel orci eu, elementum interdum erat. Cras at leo volutpat, commodo metus in, dictum justo. Cras id vulputate ex. Nullam et aliquet arcu, id elementum magna. Phasellus faucibus sapien at dolor ultrices pellentesque.','mbrs-Etiam sit amet leo nec nunc auctor convallis vel at est. Aenean at metus id nisl posuere varius. In vel ipsum eu risus iaculis volutpat in ut metus. Integer eget sem eu massa pharetra vehicula ut id elit. Nam sagittis malesuada est, ut tempus diam ultricies eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla urna libero, finibus in metus mollis, aliquam interdum elit. Phasellus at sagittis lectus. Vestibulum scelerisque libero eget efficitur venenatis. Etiam nunc magna, maximus vel vehicula eget, bibendum eu urna. Phasellus quis turpis id lacus mollis dictum. Ut dui elit, efficitur vel orci eu, elementum interdum erat. Cras at leo volutpat, commodo metus in, dictum justo. Cras id vulputate ex. Nullam et aliquet arcu, id elementum magna. Phasellus faucibus sapien at dolor ultrices pellentesque.'];
        contentModelImgSrcs:   req.body.contentModelImgSrcs  || [],   //["../../../common/images/homepage-photo3.jpg","common/bootstrap/images/customer_service2.jpg"];
        contentModelImgTextArea:  req.body.contentModelImgTextArea  ||[],   // ["This is a great picture","Another great picture"];
        contentModelLinkTarget:   req.body.contentModelLinkTarget   || [],   // ["href='#'","href='#'"];
        contentModelLinkTextArea: req.body.contentModelLinkTextArea   ||[]   // ["Click here for more information","Click here for more information"];
     };              
    /*
            NOTE: This is sample model for above component as used in the Blank Template Two 
                      paneTabId : localMbrsite.paneTabIds[0],
                paneTabTitleText: localMbrsite.paneTabTitles[0],
                        contentOne: {
                          tabId : localMbrsite.contentTabIds[0],
                          tabNameId: localMbrsite.contentTabNameIds[0],
                          isOK: localMbrsite.contentIsOk[0],
                          titleText: localMbrsite.contentModelTitleText[0],
                          textArea: localMbrsite.contentModelTextArea[0],
                          imgSrc:  localMbrsite.contentModelImgSrcs[0],
                          imgTextArea: localMbrsite.contentModelImgTextArea[0],
                          linkTarget:  localMbrsite.contentModelLinkTarget[0],
                          linkTextArea: localMbrsite.contentModelLinkTextArea[0]
                        }
                                    
    */




    /*********** Profile insert

      in the following logic the first err bails everyone out. 

      NOTE: we really need to rework logic to use the commit rollback methods


     
    NOTE: NOTE: This logic or different logic needs to be made smarter so that aany conbination of Profile or Content requests can be 
          handeled. This only handles one profile ( w/all tab bars) and one tabPane of Content.

    ****************/
    
    var returnResults = [];
    
    console.log("DEBUG: postComponentsV1: insert : newComponentTabContent pre  ***: ", newComponentTabContent );
    
    neo4api.insert(newComponentTabContent, lblArrayTabContent, function(err, contentResults) {   
        if(err) {

            console.log("DEBUG: postComponentsV1: insert : newComponentTabContent ***: ", err );
            res.send(500, "ERROR: postComponentsV1 : insert : newComponentTabContent");
            return next();
        }
        else if(contentResults) {

            // note: 20150320  create obj and push to returnResults 
            var tmpContent = { 'contentIds': {
                       '_id': contentResults._id,
                       'keyId': contentResults.keyId
                   }
                };
            returnResults.push( tmpContent );
           
            /********************** profile  ****************/
            // NOTE: add keyId and _id values from Content to Profile
            newComponentProfile.tabPaneTabContentIds.push(contentResults._id ); 
            newComponentProfile.tabPaneTabContentKeyIds.push(contentResults.keyId );
            console.log("DEBUG: postComponentsV1: insert : newComponentProfile pre  ***: ", newComponentProfile );
            
            neo4api.insert(newComponentProfile, lblArrayProfile, function(err, profileResults) {

                if(err) {

                    console.log("DEBUG: postComponentsV1: insert : newComponentProfile *****: ", err );
                    res.send(500, "ERROR: postComponentsV1 : insert : newComponentProfile");
                    return next();
                }
                else if(profileResults) {

                      
                    // note: 20150320  create obj and push to returnResults 
                    var tmpProfile = { 'profileIds': {
                       '_id': profileResults._id,
                       'keyId': profileResults.keyId
                        }
                    };
                    returnResults.push( tmpProfile );
                    /********************** tab contnet ****************/
                             // orig  res.send(201);
                    // note: 20150235: changed neo4j.js to return result content in addition to status
                    //                 returned values of result  are  result._id and result.keyId    
                    //var rresult = result[1];
                    // note: 20150320: changed this process to retun an array of returnResults consisting of
                    //                 _id and keyId for each content and profile item created  
                    console.log("DEBUG: postComponentsV1: insert : results *****: ", returnResults );
                 
                    res.json(201, returnResults );
                    return next();
    
                }
    
            })
        }
    })
}

/*
 * REST GET : Get components
   server.post('/api/v1/users', api.getUsersV1);
*/

function getComponentsV1(req, res, next) {
   
/**
 *  Get all nodes with label
 *  Given one label (non-empty string) or multiple labels (array of strings) and one or
 *  more properties in json returns an array of nodes with these labels and properties
     
    readNodesWithLabelsAndProperties('User',{ firstname: 'Sam', male: true }, callback);
 *       returns an array with nodes with the label 'User' and properties firstname='Sam' and male=true
 **/

    // Neo4j.prototype.findAllNodesWith = function(label, callback)
    //  
    // obslete original code - 20140916
    // neo4api.findAllNodesByLabel( ['Users'] , function(err, results) {
        
    //     //console.log("DEBUG: getUsersV1: findAllNodes : results ");  
    //     //console.log(results);
        
    //     if(err) {

    //         res.send(500, "ERROR: getNodesV1 : findAllNodes");
    //         return next();
    //     }
    //     else {

    //         if (results.length === 0) console.log('nada');

    //         //console.log("Query results: ", results)

    //         res.json(200, results);
    //         return next();
    //     }
    // });
    
    console.log("DEBUG: neo4Components.getComponentsV1: findNodesWithQuery : req.body pre ", req.query.cLabels  ); 
    

    var cLabels = req.query.cLabels;    // ['Components', 'Member' ]
    //var labelId = req.query.labelId ;
    //console.log("DEBUG: getUsersV1: findNodesWithQuery : req.body : " + req.body ); 
    //var uProperties = {"keyId": "IS NOT NULL", "lastName": "IS NOT NULL"};  lastName : 'Woods', firstName: 'Kimberlie' 
    var cUserKeyId  = req.query.userKeyId;
    var cProperties = { userKeyId: cUserKeyId } ;
    console.log("DEBUG: neo4Components.getComponentsV1: findNodesWithQuery : req.body post ", cLabels ); 
    neo4api.findNodesWithQuery( cLabels , cProperties , function(err, results) {    

        console.log("DEBUG: getComponentsV1: findNodesWithQuery :" );  
        console.log(results, err);
        
        if(err) {
            res.send(500, "ERROR: getComponentsV1 : findNodesWithQuery");
            return next();
        }
        else {

            if (results.length === 0) console.log('nada');

            res.json(200, results);
            return next();
        }
    });

}

//********************************

function componentFetchNodesByQueryV1(req, res, next) {
   

    // example query:
    // MATCH (n:`Users`) WHERE (n.lastName = 'Jones' AND n.firstName <> '' ) RETURN n,COUNT(n) ORDER BY n.lastName, n. firstName SKIP 14  LIMIT 7

   //console.log("DEBUG: userFetchNodesByQueryV1 : req params ID", req.body.fetchBy.fetchLastName);
   // console.log("DEBUG: userFetchNodesByQueryV1 : req params ID", req.body.fetchBy.fetchFirstName);
    
    //var rootNodeId = req.params.id;
    var newPage =  req.body.newPage - 1;
    var pageSize = req.body.pageSize;
    // build ORDER BY and WHERE Clauses
    var fetchQuery = "WHERE ";
    var fetchOrderBy = "ORDER BY " ;
    var fetchReturns = " n " ;
    // if ( req.body.fetchBy.fetchLastName ) {
    //     fetchOrderBy = fetchOrderBy.concat( "n.lastName, ");
    //     fetchQuery = fetchQuery.concat( "( LOWER (n.lastName ) = LOWER('" + req.body.fetchBy.fetchLastName + "')" );
    // };
    // 
    // else {
    //     fetchQuery = fetchQuery.concat( " and n.lastName <> " + "'')"  ); 
    // };

    if ( req.body.fetchBy.fetchLastName != "" & req.body.fetchBy.fetchFirstName != "" ) {
        fetchOrderBy = fetchOrderBy.concat( " n.lastName, n.firstName " );
        fetchQuery = fetchQuery.concat( " LOWER( n.lastName ) =~ LOWER('" + req.body.fetchBy.fetchLastName + "')" );  
        fetchQuery = fetchQuery.concat(" and LOWER( n.firstName ) =~ LOWER('" + req.body.fetchBy.fetchFirstName + "')" );

    } else if ( req.body.fetchBy.fetchLastName == ""  &  req.body.fetchBy.fetchFirstName != "" ) {
        // NOTE: may want query logic to work this 
        // fetchQuery = fetchQuery.concat( " n.lastName <> " + "'')"  ); 
        fetchOrderBy = fetchOrderBy.concat( " n.firstName " );
        fetchQuery = fetchQuery.concat(" LOWER( n.firstName) =~ LOWER('" + req.body.fetchBy.fetchFirstName + "')" );

    } else if ( req.body.fetchBy.fetchLastName != ""  &  req.body.fetchBy.fetchFirstName == "" ){
        fetchOrderBy = fetchOrderBy.concat( " n.lastName ");
        fetchQuery = fetchQuery.concat( " LOWER( n.lastName ) =~ LOWER('" + req.body.fetchBy.fetchLastName + "')" );  
        fetchQuery = fetchQuery.concat( " and ( n.firstName <> " + "'')"  ); 
    };
    console.log( req.body.fetchBy.fetchLastName );
    
    // if ( req.body.fetchBy.fetchLastName != "" & req.body.fetchBy.fetchFirstName != "" ) {
    //     fetchOrderBy = fetchOrderBy.concat( " n.lastName, n.firstName " );
    //     fetchQuery = fetchQuery.concat( " LOWER('" + req.body.fetchBy.fetchLastName + "') =~ LOWER( n.lastName )" );  
    //     fetchQuery = fetchQuery.concat(" AND LOWER('" + req.body.fetchBy.fetchFirstName + "') =~ LOWER( n.firstName )" );

    // } else if ( req.body.fetchBy.fetchLastName == ""  &  req.body.fetchBy.fetchFirstName != "" ) {
    //     // NOTE: may want query logic to work this 
    //     // fetchQuery = fetchQuery.concat( " n.lastName <> " + "'')"  ); 
    //     fetchOrderBy = fetchOrderBy.concat( " n.firstName " );
    //     fetchQuery = fetchQuery.concat(" LOWER( n.firstName) =~ LOWER('" + req.body.fetchBy.fetchFirstName + "')" );

    // } else if ( req.body.fetchBy.fetchLastName != ""  &  req.body.fetchBy.fetchFirstName == "" ){
    //     fetchOrderBy = fetchOrderBy.concat( " n.lastName ");
    //     fetchQuery = fetchQuery.concat( " LOWER( n.lastName ) =~ LOWER('" + req.body.fetchBy.fetchLastName + "')" );  
    //     fetchQuery = fetchQuery.concat( " and ( n.firstName <> " + "'')"  ); 
    // };

    // build WHERE Clause
    
    console.log("DEBUG: userFetchNodesByQueryV1 : query params: " );
    console.log( fetchQuery);
    console.log( fetchOrderBy);
    // get the relationships for this node
    // "START user = node(2) MATCH user-[:FOLLOWS]->followed RETURN followed"
    //var query = "MATCH (user:'Users' ) " + fetchQuery + " RETURN user, COUNT(user) " + fetchOrderBY +  " SKIP " + newPage + " LIMIT " + pageSize ;
    var query = "MATCH (n:Components ) " + fetchQuery + " RETURN " + fetchReturns + fetchOrderBy   +  " SKIP " + newPage + " LIMIT " + pageSize;
    var params = "";
    var include_stats = true;
    neo4api.cypherRunQuery(query, params, include_stats, function (err, results) {
        if(err)
        {
                res.send(500, "ERROR: userFetchNodesByQueryV1 : findRelationships : err : " + err.message);
                console.log("ERROR: userFetchNodesByQueryV1 : findRelationships : err :", err);
                return next();
        }
        else if ( results )
        { 
            //      we get back an array of relationships that matched the query      
            //      next loop over the array testing for otherId 
            //      then delete that relationship by id
              
            //var tmpFld =  results["data"].toString();  
            console.log("DEBUG: userFetchNodesByQueryV1 : findRelationshipsOfNode ", results );
            //console.log( tmpFld);
            if ( results['data'].length ) {

                res.send(200, results );
            }
            else {
                res.send(200, null );
            }

            return next();   
        }
    }); 
}




//**********************************



/*
 * REST PUT : Update Component
   server.put('/api/v1/users', api.putUserV1);
*/
function putComponentV1(req, res, next) {
    var  targetIdName = "_id" ;
    var  targetId = req.body._id;     
    
    //console.log("DEBUG: putComponentV1 : pre keyId :************ ", req.body,  targetKeyId);

    var node_data =  req.body ;
   
    // don't allow user updates to nodes with null or '' keyId
    if ( req.body.keyId === '' | req.body.keyId === null  ) {
        //because keyId was '' or null then use _id for lookup but create and update keyId
        //  targetId and targetIdName already set;
        //targetKeyId =  uuid.v1(); 
        node_data.keyId =  uuid.v1();
    }
    else {
        // because there was a keyId value present then use it instead of _id
        targetIdName = "keyId" ; 
        targetId =  req.body.keyId; 
        
    };
   
    console.log("DEBUG: putComponentV1 : post keyId :************ ", targetKeyId, req.body.keyId );
    console.log("DEBUG: putComponentV1 : pre  nodedata req.body :************ ", req.body );
    // properties that are not updateable by "member" user
    // *******
    //        labelId: "mbrsite",     ** Required  "mbrsite"
    //        _id  : "";              ** Required, assigned by server 
    //        keyId: "",              ** Required, assigned by server 
    //        userKeyId: "",          ** Required, derived from user   
    //        datecreated: "",        ** Required, set at time of obj creation
    //        status: "",             ** Required, set at system level-relates to validity of
    //        approveDate: "",
    
    // var node_data = { 
    //         keyId : req.body.keyId || targetKeyId, 
    //         lastUpdated: req.body.lastUpdated || "",        
    //         updatedById: req.body.updatedById || "",
    //         isActive: req.body.isActive || "",      
    //         name : req.body.name || "",
    //         street : req.body.street || "",
    //         street2 : req.body.street2 || "",
    //         city : req.body.city || "",
    //         state : req.body.state || "",
    //         zipCode : req.body.zipCode || "",
    //         phone : req.body.phone || "",
    //         fax: req.body.fax || "",
    //         website: req.body.website || "",
    //         category: req.body.category || "", 
    //         description: req.body.description ||  "",
    //         identityId : req.body.indentityId || ""
            
          
    //     };

    // obs moved to top of routine:  var node_data =  req.body ;
    // obs moved to top of routine:  node_data.keyId =  targetKeyId;

    var lblArray =  req.body.lblArray;  // 'Components:Mbrsite:Profile'
    //[ groupLabel , typeLabel, partLabel ]
    neo4api.updateByFldKey( lblArray , targetIdName , targetId, node_data , function(err, user) {
    
            var resultsCnt = [user].length ;
            console.log("DEBUG: putComponentV1.updateByFldKey : resultsCnt w/err keyId: ", resultsCnt, err, targetKeyId );
            if(err) {
                console.log("ERROR: putComponentV1.updateByFldKey err :", err);
                res.send(500, "ERROR: putComponentV1.updateByFldKey : update");
                return next();
            }
            else if(   1 === resultsCnt ) {

                res.send(200);
                return next();
            }
            else {

                res.send(400, "ERROR: putComponentV1.updateByFldKey: Was not able to update user with keyId : " + user.keyId);
                return next();
        }
    });


}



function deleteComponentV1(req, res, next) {

    var queryStrings = req.getQuery();
    var idType = queryStrings.idType;
    var id = req.params.id;

    var properties = {};

    //console.log("DEBUG: idType : ", idType);
    //console.log("DEBUT: id : ", id);

    if("loginId" === idType) {

        properties["loginId"] = id;
    }
    else if("keyId" === idType) {

        //properties["_id"] = id;
        properties["keyId"] = id;
    }
    else if("objectId" === idType) {

        //properties["_id"] = id;
        properties["_id"] = id;
    }
    else {

        res.send(400, "ERROR: deleteUserV1 : Invalid id type");
        return next();
    }

    neo4api.removeWithQuery( ['Components'], properties,function(err, numRemovedDocs) {
        if(err) {

            res.send(500, "ERROR: deleteUserV1 : remove");
            return next();
        }
        else if(1 === numRemovedDocs) {

            res.send(200);
            return next();
        }
        else {

            res.send(400, "ERROR: Was not able to delete user with id : " + id);
            return next();
        }
    });
}


/**
    * GET user RELATIONSHIPS 

    /api/v1/users/:id/connect

    // options : direction: "in"  or "out"  or  dflt= "All"
    //           types:         ex: "FOLLOWS"    
*/
// function  userFindConnectionsActionV1(req, res, next) {
      
//     console.log("DEBUG: userFindConnectionsActionV1 : req params ID", req.params.id);
//     var rootNodeId = req.params.id;
//     var options =  req.body.options;
   

//     neo4api.findRelationshipsOfNode( rootNodeId, options, function (err, results) {
        
//         if(err)
//         {
//                 res.send(500, "ERROR: userFindConnectionsActionV1 : findRelationships : err : " + err.message);
//                 console.log("ERROR: userFindConnectionsActionV1 : findRelationships : err :", err);
//                 return next();
//         }
//         else if ( results )
//         { 
//             //      we get back an array of relationships that matched the query      
//             //      next loop over the array testing for otherId 
//             //      then delete that relationship by id
              
//             console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode " + results.length );
//             console.log( results );
   
//             var connections = [];           
            
//             for (var i = 0; i < results.length; i++) {
//                  // extract the followed Id of this relationship
//                 var followedId = results[i]['_end'];
                
//                 console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode : followedId  " + followedId );
  
//                 neo4api.findById( followedId, function( err, node ) {

//                     if(err) {

//                        // res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                        // console.log("ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                        // console.log(node)
//                        // return next();
//                     }
//                     else if( node ) {

//                         connections.push( node );

//                         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after connections.push" );
//                         console.log( connections );
//                     }
//                     else
//                     {
//                         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: no connections.push node" );
                        
//                     }
                
//                 });    

  
//             }
              
            
           
//        }
//          console.log("DEBUG: userFindConnectionsActionV1 : findRelationships : exiting  " );
//             console.log( connections );
//             res.send(200, connections);
//             return next();
       
//     }); 
        
// //neo4api.findWithQuery(
// //        ['Users'],
// //        { 'loginId': loginUser.loginId, 'password' : loginUser.password },
// //        function(err, user) {

// }




// function  userFindConnectionsActionV1(req, res, next) {
      
//     console.log("DEBUG: userFindConnectionsActionV1 : req params ID", req.params.id);
//     var rootNodeId = req.params.id;
//     var options =  req.body.options;
   
//     // get the relationships for this node
//     neo4api.findRelationshipsOfNode( rootNodeId, options, function (err, results) {
        
//         if(err)
//         {
//                 res.send(500, "ERROR: userFindConnectionsActionV1 : findRelationships : err : " + err.message);
//                 console.log("ERROR: userFindConnectionsActionV1 : findRelationships : err :", err);
//                 return next();
//         }
//         else if ( results )
//         { 
//             //      we get back an array of relationships that matched the query      
//             //      next loop over the array testing for otherId 
//             //      then delete that relationship by id
              
//             console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode " + results.length );
//             console.log( results );
   
//             var connections = "";           
//             var ids = [ ] ;
//             for (var i = 0; i < results.length; i++) {
//                  // extract the followed Id of this relationship
//                 var followedId = results[i]['_end'];
//                 ids.push( { 'ID' : followedId } ) ;

//                 console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after ids" );
//                 console.log(ids );

//                 //console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode : followedId  " + followedId );
  
//                 // neo4api.findById( followedId, function( err, node ) {

//                 //     if(err) {

//                 //        // res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                 //        // console.log("ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                 //        // console.log(node)
//                 //        // return next();
//                 //     }
//                 //     else if( node ) {

//                 //         connections.push( node );

//                 //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after connections.push" );
//                 //         console.log( connections );
//                 //     }
//                 //     else
//                 //     {
//                 //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: no connections.push node" );
                        
//                 //     }
                
//                 // });    

  
//             }
            
//            // { ID: 10 }, { ID: 5 }, { ID: 11 } 

//             // now get the nodes data 
//             var idsWrk = { keyId : "ef957de8-3d29-493b-9e91-806475fca4d1" };
//             // "5aa913a1-137c-11e4-9c59-a3ed62d316b1"
//               //  , { keyId: "ef957de8-3d29-493b-9e91-806475fca4d1"  }  
//              // ids.slice(0,ids.length);  0a5d0be1-137c-11e4-9c59-a3ed62d316b1
//             console.log( idsWrk );
           

//             neo4api.findWithQuery( ['Users'], idsWrk , function(err, nodes ) {
              
//                 if(err) {

//                   res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
//                   console.log( nodes );
//                   return next();
//                 }
//                 else { 
                    
//                     console.log("DEBUG: userFindConnectionsActionV1 : findRelationships : exiting  " );
//                     console.log( nodes );
//                     res.send(200, nodes);
//                     return next();
//                 }    
//             });
//         }
//     }); 
        
// }

  //  query
  //var query = "START user = node(" + 123) MATCH user-[:RELATED_TO]->friends RETURN friends"
  // 
  // add "LIMIT 100"  to query to reduce working data
  //var query = "START user = node(" + 123) MATCH user-[:RELATED_TO]->friends RETURN friends LIMIT 25"

function  componentFindConnectionsActionV1(req, res, next) {
      
    console.log("DEBUG: userFindConnectionsActionV1 : req params ID", req.params.id);
    var rootNodeId = req.params.id;
    var options =  req.body.options;
   
    // get the relationships for this node
    // "START user = node(2) MATCH user-[:FOLLOWS]->followed RETURN followed"
    var query = "START user = node(" + rootNodeId + ") MATCH user-[:FOLLOWS]->followed RETURN followed" ;
    var params = "";
    var include_stats = true;
    neo4api.cypherRunQuery(query, params, include_stats, function (err, results) {
        if(err)
        {
                res.send(500, "ERROR: userFindConnectionsActionV1 : findRelationships : err : " + err.message);
                console.log("ERROR: userFindConnectionsActionV1 : findRelationships : err :", err);
                return next();
        }
        else if ( results )
        { 
            //      we get back an array of relationships that matched the query      
            //      next loop over the array testing for otherId 
            //      then delete that relationship by id
              
            console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode " + results['data'].length );
            console.log( results['data'] );
            if ( results['data'].length ) {
                res.send(200, results['data']);
            }
            else {
                res.send(200, null );
            }

            return next();   
           //*************************************************************** 
           //  var connections = "";           
           //  var ids = [ ] ;
           //  for (var i = 0; i < results.length; i++) {
           //       // extract the followed Id of this relationship
           //      var followedId = results[i]['_end'];
           //      ids.push( { 'ID' : followedId } ) ;

           //      console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after ids" );
           //      console.log(ids );

           //      //console.log("DEBUG: userFindConnectionsActionV1 : findRelationshipsOfNode : followedId  " + followedId );
  
           //      // neo4api.findById( followedId, function( err, node ) {

           //      //     if(err) {

           //      //        // res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
           //      //        // console.log("ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
           //      //        // console.log(node)
           //      //        // return next();
           //      //     }
           //      //     else if( node ) {

           //      //         connections.push( node );

           //      //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: after connections.push" );
           //      //         console.log( connections );
           //      //     }
           //      //     else
           //      //     {
           //      //         console.log("DEBUG: userFindConnectionsActionV1 : findRelationships: no connections.push node" );
                        
           //      //     }
                
           //      // });    

  
           //  }
            
           // // { ID: 10 }, { ID: 5 }, { ID: 11 } 

           //  // now get the nodes data 
           //  var idsWrk = { keyId : "ef957de8-3d29-493b-9e91-806475fca4d1" };
           //  // "5aa913a1-137c-11e4-9c59-a3ed62d316b1"
           //    //  , { keyId: "ef957de8-3d29-493b-9e91-806475fca4d1"  }  
           //   // ids.slice(0,ids.length);  0a5d0be1-137c-11e4-9c59-a3ed62d316b1
           //  console.log( idsWrk );
           

           //  neo4api.findWithQuery( ['Users'], idsWrk , function(err, nodes ) {
              
           //      if(err) {

           //        res.send(400, "ERROR: userFindConnectionsActionV1 : findRelationships : Was not able to find relationsships");
           //        console.log( nodes );
           //        return next();
           //      }
           //      else { 
                    
           //          console.log("DEBUG: userFindConnectionsActionV1 : findRelationships : exiting  " );
           //          console.log( nodes );
           //          res.send(200, nodes);
           //          return next();
           //      }    
           //  });
            //****************************************************



        }
    }); 
        
}


/**
    * SERVER PUT   server.put('/api/v1/users/:id/connect', api.userConnectActionV1);
        
        NOTES: after creating new relationships we must repopulate the user relationship data their browser.
               we can do this by retrieving an updated list of relationships for this user based on the type of
              relationship just created. This updated list would be returned to browser client. Sample results 
             
             Sample of returned results of insert operation :
               [ { since: 2014, more usersupplied properties... , _start: 1, _end: 10, _id: 8, _type: 'FOLLOWS' } ]
    

        req.params: req.params.id
        req.body  : 'otherId',
                    'relationType' : 'FOLLOWS'      other type can be used
                    'relationData' :  object properties of relationship. diff for each relationship been nodes  

        
*/
function componentConnectActionV1(req, res, next) {
        
    console.log("DEBUG: userConnectActionV1 : connect from ID", req.params.id);
    console.log( req.body );
    
    console.log(' need to setup id test like in delete')
    var rootNodeId = req.params.id;
    var otherNodeId = req.body.otherId;
    var relationType =  req.body.relationType;
    var relationData   = req.body.relationData ;
    neo4api.insertRelationship(rootNodeId, otherNodeId, relationType, relationData, function (err, results) {
       
        var resultswrk = [ results ];
        console.log("DEBUG: userConnectActionV1 : connect : resultsCnt : " + resultswrk.length );
        console.log( resultswrk );
        if(err) {
              res.send(500, "ERROR:  userConnectActionV1 : connect: err : " + err.message);
              console.log("ERROR: userConnectActionV1 : connect : err :", err);
              return next();
        }
        else if( 1 === resultswrk.length) {
               
                console.log("DEBUG: userConnectActionV1 : connect : count :", resultswrk);
                var followedId = resultswrk[0]['_end'];
                console.log( followedId );
                neo4api.findById( followedId , function(err, node){

                        if(err) {
                            console.log("DEBUG: userConnectActionV1 : findById : node : err", err );    
                            callback(err, null);
                            return next(); 
                        }
                        else if( node ) {
                            console.log("DEBUG: userConnectActionV1 : findById : node :", node );
                            res.send(200, node);
                            return next();
                
                        }
                        else {
                            console.log("DEBUG: userConnectActionV1 : findById : node : else ", node );
                            callback(null, null);
                            return next();
                        }
                });

        }
        else {
                        
                console.log("ERROR: userConnectActionV1 : connect : Was not able to set relationship");
                console.log(results)
                res.send(400, "ERROR: userConnectActionV1 : connect  : Was not able to set relationship");
                return next();
        }
  
    });


}

/**
    * POST /users/:id/disconnect

    // options : direction: "in"  or "out"  or  dflt= "All"
    //           types:         ex: "FOLLOWS"    
*/
function  componentDisconnectActionV1(req, res, next) {
      
    console.log("DEBUG: postUserDisconnectActionV1 : Disconnect from ID", req.params.id);
    var rootNodeId = req.params.id;
    var connectedNodeId = req.body.connectedId;
    var options =  req.body.options;
   
    neo4api.deleteRelationship(rootNodeId, connectedNodeId, options, function (err, result) {
    
        // what we get back is a result of true /number deleted or false indicating relationship wasn't found  
        if (err) {
                res.send(500, "ERROR:  userDisconnectActionV1 : disconnect: err : " + err.message);
                console.log("ERROR: userDisconnectActionV1 : disconnect : err :", err);
              return next();
        }
        else if( 1  === [result].length ) {
               console.log("DEBUG: userDisconnectActionV1 : disconnect : result :", result );
               // id of followed Node is returned
               res.send(200,  connectedNodeId  );
               return next();
        }
        else {
                        
                res.send(400, "ERROR: userDisconnectActionV1 : disconnect  : Was not able to delete / drop relationship");
                console.log("ERROR: userDisconnectActionV1 : disconnect : Was not able to delete / drop relationship");
                console.log(result)
                return next();
        }          
    }); 
}



exports.getComponentsV1 = getComponentsV1;
exports.componentFetchNodesByQueryV1 = componentFetchNodesByQueryV1;
exports.postComponentV1 = postComponentV1;
exports.putComponentV1 = putComponentV1;
exports.deleteComponentV1 = deleteComponentV1;
//exports.postComponentLoginV1 = postComponentLoginV1;
exports.componentConnectActionV1 = componentConnectActionV1;
exports.componentDisconnectActionV1 = componentDisconnectActionV1;
exports.componentFindConnectionsActionV1 = componentFindConnectionsActionV1;