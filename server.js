
var restify = require('restify');
var fs = require('fs');
// specify DB lib
// var appDb = require('./lib/mongo');
// var api = require('./lib/mongoUserApi');
// var middleware = require('./lib/mongoMiddleware');

var appDb = require('./lib/neo4');
var userApi = require('./lib/neo4UserApi');
var componentApi = require('./lib/neo4ComponentApi');
var middleware = require('./lib/neo4Middleware');
var fsApi = require('./lib/fsUtilsApi');

var www = require('./lib/www');
// Jims adhoc notes
var server = restify.createServer();

server.use(restify.acceptParser(server.acceptable));
server.use(restify.authorizationParser());
server.use(restify.queryParser({ mapParams: false }));
server.use(restify.jsonBodyParser({ mapParams: false }));
//server.use(restify.gzipResponse());

// view engine setup  NEEDS MORE WORK - RESITFY
//server.set('views', path.join(__dirname, 'views'));
//server.engine('html', require('ejs').renderFile);
//server.set('view engine', 'html');


/*
 * REST APIs
*/

/*
 ********************************* USERS *************************
 *****************************************************************
 * REST GETs : Get users
 *
 */
// Use this bypass for first db user by uncommenting  next line and commenting line after that
//server.get('/api/v1/users', api.getUsersV1);
//server.get('/api/v1/users', middleware.validateAuthN, middleware.validateAuthZ, api.getUsersV1);
server.get('/api/v1/users', middleware.validateAuthN, middleware.validateAuthZ, userApi.getUsersV1);


// don't recall why server.put used and not server.get. typo? fix: whenever time to research
server.put('/api/v1/users/fetch', userApi.userFetchNodesByQueryV1 );

/* Get Component set */
server.get('/api/v1/components', middleware.validateAuthN, middleware.validateAuthZ, componentApi.getProfileContentSetsV1);

/* NOTE: Need to question Authorization process */

/* Get Component profile(s) */
//server.get('/api/v1/components/context', middleware.validateAuthN, middleware.validateAuthZ, componentApi.getProfileContentSetsV1);
server.get('/api/v1/components/context', componentApi.getContextProfileV1);

/*
 * REST GET : Serving WWW Files
 * NOTE: Must be last server.get(...) declaration
 *
 * e.g. http://localhost:8080/docs/public/index.html
*/
server.get('/.*', www.serveV1);

/*
 * REST POSTs :  Upload Images ( fileStream )
*/
server.post('/api/v1/images/upload', fsApi.postImagesV1);
/*
 * REST PUTs :  launch image upoad form
 */
server.put('/api/v1/images', fsApi.putImageV1);
/*
 * REST POSTs :  Create user
 */
server.post('/api/v1/users', userApi.postUserV1);
/*
 * REST POST : Login user
*/
server.post('/api/v1/users/login', userApi.postUserLoginV1);
/*
 * REST PUT : Update user
*/
server.put('/api/v1/users', userApi.putUserV1);
/*
 * REST DELETE : Delete user by userId or MondoDb objectId (_id)
 *
 * @request query parameter userId
 * @request query parameter objectId
 */
server.del('/api/v1/users/:id', userApi.deleteUserV1);
/*
 * REST PUT : Disconnect from user user by _Id
 *
 * @request query parameter userId
 * @request query parameter objectId
 */
server.put('/api/v1/users/:id/disconnect', userApi.userDisconnectActionV1);
/*
 * REST PUT : Connect user by _ID
 *
 * @request query parameter userId
 * @request query parameter objectId
 */
server.put('/api/v1/users/:id/connect', userApi.userConnectActionV1);
/*
 * REST GET : Load Connections and Others user by _ID
 *
 * @request query parameter userId
 * @request query parameter objectId
 */
server.put('/api/v1/users/:id/findconnects', userApi.userFindConnectionsActionV1);

/*
******************************* COMPONENTS **********************
*****************************************************************
*/


/* Create component */
// obs server.post('/api/v1/components', componentApi.postComponentV1);

/* Create initial component set ( add new User ) */
server.post('/api/v1/componentsset', componentApi.postProfileContentSetV1);
server.post('/api/v1/contentset', componentApi.postContentSetV1);

/* Put Component  update  "data" */
// obs server.put('/api/v1/components', componentApi.putComponentV1);
server.put('/api/v1/components', componentApi.putComponentUpdateV1);

//console.log("DEBUG: server: startup settings: ");
//console.log( server );

/*
 * Start DB and Node.js server
 */
appDb.init(function() {

    console.log("INFO: DBs is ready");

    server.listen(8080, function() {

        console.log('%s listening at %s', server.name, server.url);
    });
});
