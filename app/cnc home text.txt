 
Cashiers, a beautiful village in western North Carolina known as "The Heart Of The Blue Ridge Mountains", is located in the southern end of Jackson County. Sitting at 3,487 feet in elevation at the crossroads of Highways 107 and 64, there is a little bit of everything here to enjoy for native families, visitors, and part-time residents alike. Our singularly unique geography boasts hundreds of waterfalls, quiet lakes, stone mountains and amazingly beautiful forests.
Life in this mountain community is as multifaceted as its landscape. Cashiers is located on the highest plateau in the Blue Ridge Mountains. The highest peak in the area rises to 5,127 at Yellow Mountain, yet drops to 1,020 feet along the Horsepasture River. This area is spectacularly beautiful, and is unlike any other place in the world. Our elevation coupled with the abundant rainfall combine to create a lush, green rain forest filled with rhododendrons, laurels, mossy rocks, ferns and pine trees. These mountains are some of the oldest in the world and have abundant hiking trails, gorgeous scenery and intriguing legends.
Fact: Because of it's unique geography and altitude the climate on the Cashiers plateau is generally about 20 degrees cooler during the summer months than just about anywhere else in the southeastern US!
Weather is generally cooler up here, regardless of the season. Temperatures average a high of 80 degrees in July, and 40 degrees in January. Recently, Cashiers has received over 100 inches of rain per year.	 
The scenery isn't the only thing unique about the area. Cashiers is also a really great place to live. Our quiet village boasts world-class accommodations, great restaurants and shopping, coupled with a good dose of mountain friendliness and charm. Once you visit you won't want to leave!
 
The population of Cashiers, North Carolina is officially listed as 1,974, but from about Memorial Day until the end of October�s fall leaf season, part-time residents and visitors are estimated to bump that number up to about 10,000-15,000. For locals with children, Cashiers is home to some of the state�s finest schools, and our churches and civic organizations are among the most philanthropic you�ll ever see.
Village life includes access to unique stores and businesses, which accentuate the lifestyle created by the Village Green and Commons park in the center of town. Other staples of the crossroads are the Albert Carlton-Cashiers Community Library, the Cashiers Community Center, the new Cashiers Recreation Center, the Cashiers Area Chamber of Commerce, new pathways to navigate your way around town, and much, much more. Have a look at Cashiers real estate opportunities while you are here.
Cashiers NC is home to many exciting events throughout the year, most of which benefit a charity or non-profit in the area. Popular yearly events include the Leaf Festival, the Rotary Arts & Crafts Shows on Memorial Day and Labor Day weekends, weekly Groovin� on the Green events on Friday�s during the busy season, the Cashiers Designer Showhouse, Cashiers Benefit Antique Show, the Church of the Good Shepherd Auction and Bazaar, the Tour de Cashiers, Car Shows, The Annual Christmas Parade, and so much more.
Surrounding the village are many opportunities for outdoor enthusiasts, including hiking trails, camping opportunities, parks, mountains, waterfalls, fishing, boating, lakes, rivers, nationally-ranked golf courses, bicycling, skiing (water and snow), and horseback riding, just to name a few.	   
People you run into around Cashiers vary from local mountain folk who have lived here their entire lives, to retired doctors and lawyers, to the avid traveler just stopping for a refreshing hike or a plate full of barbecue. Regardless of who you are, or the length of your stay, one thing is for sure, you won�t be disappointed in what Cashiers has to offer.
Please look through this site to see all of the many choices there are for you to stay and play. We are closer than you think! Come on up for a weekend or a week. Bring your family for a reunion or wedding. Find that second home you have always dreamed of or kick back in one of the greatest places to retire anywhere - only in Cashiers, North Carolina, the Heart of the Blue Ridge Mountains.

by Kelly Donaldson/Crossroads Chronicle and Steve Johannessen


 
CashiersGolfing.com is Now Online
Now you can find out more about all of the golf clubs here and see these beautiful courses. Featuring the golf, tennis and croquet communities in the Cashiers, Highlands, Sapphire and Toxaway areas. Each community or club listed has information about the course, pro, photos, directions and other information of interest. 
u8232 Visit CashiersGolfing.com

CashiersNorthCarolina.org web traffic is way up!
Our website continues to be the top website on the plateau for web traffic. Last year we had over 179,000 unique visitors to the site! In page views we had over 850,000! With the acquisition of the cashiersnorthcarolina.com domain (now pointed to cashiersnorthcarolina.org) the site traffic will only increase.

CashiersNorthCarolina.org is #1 in Google, Yahoo and Bing Search!
Our website has been #1 in the search engines for search terms "Cashiers North Carolina" and "Cashiers NC" for three years in a row! A HUGE thank you goes out to all those businesses and individuals who participated from the beginning to make this happen!! Call us about putting you ad up here on the community website! 
u8232 Advertising on This Website

Watch the helicopter video of Cashiers and Highlands!
Fly with us and see the beauty of our area in this YouTube video! See Cashiers, Highlands and the surrounding mountains. Lake Glenville, Lake Toxaway, Whiteside Mountain, Whitewater Falls and much more are here. Fly over Wade Hampton Golf Club, High Hampton Inn and Resort, Country Club of Sapphire Valley and Trillium Golf communities.
u8232 Watch The Helicopter Video

MemberSite Training Classes at the Cashiers Library
The free classes are held at the Albert-Carlton Cashiers Library in Cashiers. Stop by after work and get signed up or ask questions. A MemberSite will be demonstrated and members will learn how to work with and create their own MemberSite, use the shopping events calendar and more. Bring your laptop if you have one! Next scheduled class date will be in May - as always the class is free to everyone.

Don't Have Time to Attend?
Call (828)743-7543 to schedule a visit to your place of business and get help setting up your MemberSite. Don't wait to take advantage of this!

New 3.0 Community Web Software Now Available
GET FOUND!! - All community businesses can get their own free searchable business listing. This also includes services, professionals and independent contractors. As a business listed on this website you will have a free five-page MemberSite Expanded Business Listing(TM) that has 4 pages with your images and text and your business calendar. You will have control over your website text and graphics, and you can log in and make these changes from any browser anywhere. Your changes will appear instantly on your MemberSite pages. With your MemberSite you will have your very own events calendar that shows special event dates and details that can post to the community shopping calendar.
Your MemberSite Expanded Business Listing(TM) is an extremely valuable tool that your business can use to market your business to customers locally and on the web. This software greatly enhances the ablilty of visitors to find and locate your establishment within our community.� If you have any questions about this new software please contact us.
Click Here To Sign Up!	 
	 
