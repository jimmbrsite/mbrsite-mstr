(function() {

    'use strict';

    /*
     * API:
     *
     * getUsers : callback
     * createUser : Object e.g. {"firstName":"Thomas, "lastName":"Amsler}
     */
    angular.module('training.services').factory('training.services.component', ['$http', 'training.services.user', function($http, userService ) {

        var ComponentService = {

            'user' : {},   
            'components' : [],
            'component' : {},     // name:'hello' 
            'fetchBy' :{},
            'componentsList' : [],
            'componentsListItem' : {},
            'componentsEditItem' : {},
            'preferences':[],
            'mbrsite':{}, 
            'profile':{},     
            'dfltTemplates': [],
            'adminTemplates':[],
            'userTemplates':[],

        };

        //  obs UserService.getMenu = function() {
              
        //      return UserService.userMenus;      
             
        // };

        ComponentService.getLocalMbrsite = function() {
                //var siteDat =  ComponentService.mbrsite ;
                console.log("DEBUG: ComponentService.getLocalMbrsite  returned data ********; ", ComponentService.mbrsite );
             return  ComponentService.mbrsite;      
        };

        ComponentService.setLocalMbrsite = function( nuLocal ) {

                ComponentService.mbrsite = nuLocal ;
                console.log("DEBUG: ComponentService.setLocalMbrsite  set data nuLocal *******; ", ComponentService.mbrsite );
              return;      
        };

        ComponentService.getTemplates = function() {
          
             return UserService.userTemplates;      
        };

        ComponentService.getBizCategories = function() {
                var orgCategory = [{"orgtypes": {"Accountants/Bookkeepers":"","Advertising & Marketing":"","Antique Lumber/Flooring":""
                            ,"Antiques & Gifts":"","Architect/Home Design":"","Architects/Home Design":""
                            ,"Art Galleries":"","Art Galleries/Framing":"","Attorneys":""
                            ,"Automobile Sales/Service":"","Balsam/Cullowhee Accommodations":""
                            ,"Beauty Salons/Nails":"","Bedding/Furniture":"","Blasting/Drilling":""
                            ,"Blinds/Shutters":"","Boat Charters & Tours":"","Boat Rentals/Sales":""
                            ,"Builders Supply":"","Building Contractor":""
                            ,"Cable TV/Internet":"","Campgrounds/RV Resorts":"","Camps":""
                            ,"Carpet Cleaners":"","Cashiers Accommodations":"","Chambers of Commerce":""
                            ,"Chiropractic/Accupuncture":"","Christmas Tree Farms":"","Cleaning Services":""
                            ,"Clothing & Accessories":"","Coffee House/Expresso Bar":"","Colleges":""
                            ,"Computer/Internet":"","Consignment Shops":"","Contract Law Enforcement & Security Services":""
                            ,"Custom Home Builders":"","Dentists":"","Developers":""
                            ,"Disc Jockey/Entertainment":"","Dry Cleaners":"","Electrical Contractors":""
                            ,"Electronics":"","Environmental Services/Engineering":"","Event Coordinating":""
                            ,"Facials & Waxing":"","Financial Institutions":"","Financial Planning/Investments":""
                            ,"Floor Coverings":"","Florists/Plants/Gifts":"","Furniture & Home Décor":""
                            ,"Gasoline":"","Gift Shops/Crafts/ Home Décor":"","Gift Shops/Crafts/Home":""
                            ,"Gem Mining":"","Golf Courses - Public":"","Golf/Tennis Private Clubs":""
                            ,"Grading & Excavating":"","Green Energy":"","Groceries/Fresh Vegetables":""
                            ,"Hardware/Housewares/Gifts":"","Highlands Accommodations":"","Home Inspections":""
                            ,"Home Repairs & Maintenance":"","Home Theater":"","Horseback Riding":""
                            ,"Hospitals":"","Insurance/Home Inventory & Valuables":"","Interior Design/Furniture":""
                            ,"Kitchen & Bath/Cabinets":"","Land Surveying":"","Land Trust/Conservation":""
                            ,"Lanscaping/Planning & Design":"","Lawn Maintenance":""
                            ,"Lighting":"","Log Homes":"","Magazines":""
                            ,"Masonry":"","Massage":"","Mortgage Lending":""
                            ,"Newspapers":"","Non Profit Organizations":"","Nusery/Plants":""
                            ,"Office Supplies":"","Outdoor Recreation":"","Outdoor Specialty Stores":""
                            ,"Painting & Pressure Washing":"","Pest Control":"","Pharmacy":""
                            ,"Photography":"","Physicians":"","Preschools":""
                            ,"Printing":"","Propane Gas":"","Property Management/POA":""
                            ,"Publishing":"","Radio Stations":"","Real Estate Appraisal":""
                            ,"Real Estate Sales/Rentals":"","Restaurants":"","Retirement Communities":""
                            ,"Roofing Repair/Sealants":"","Rugs/Oriental Rugs":"","Sapphire/Lake Toxaway Accommodations":""
                            ,"Shipping Services":"","Signs/Woodworking":"","Skeet Range/Field Club":""
                            ,"Spas":"","Stone Hauling":"","Storage Facilities":""
                            ,"Tent Rentals":"","Tourist Attraction":"","Toys & Gifts":""
                            ,"Transportation/Shuttle Service/Concierge":"","Trash Services/Bear Proof Containers":"","Tree Care":""
                            ,"Tree Service":"","Utilities":"","Video Production":"","Web Design":""
                           }}];        
                          
            return  Object.keys(orgCategory[0]["orgtypes"]);      
        };
        
        ComponentService.signedIn = function() {
            if (angular.fromJson(sessionStorage.getItem("user")) !== null)  {
                // use this bypass... if (angular.fromJson(sessionStorage.getItem("user")) === null)  {  
                 console.log("INFO: User is signedIn");
                 return true;
            } else {
                console.log("INFO: User is not signedIn");
                return false;
            };    
           
        };

        ComponentService.logout = function() {
            ComponentService.user = {};
            ComponentService.connected = {};
            ComponentService.other = {};
            ComponentService.connections = [],
            ComponentService.others = [],
            ComponentService.users = [],
            ComponentService.fetchBy = {},
            ComponentService.preferences = [];
            ComponentService.tmplates = [];
            ComponentService.mbrsite = {};
            // seionStorage.removeItem("user");
            // obs sessionStorage.removeItem("userMenus");
            //ssionStorage.removeItem("userTemplates");
            
        };
        
        // ComponentService.saveLocalUsers = function(users) {

        //     console.log("INFO: save local userUser");
        //     console.log(users);
    
        //     this.users = users;
            
        // };

        // ComponentService.getLocalUsers = function() {

        //     return this.users;
        // };

      //   ComponentService.getLocalConnections = function() {
      //       ///* NOTE: needs work more work need to go back to db and get follows relationships for this active user we are editing
      //       //**/
      //       //*
      //       //*
      //       //*/ 
      //       var item= false;  
      //       var dnedata = [];
      // // $scope.dnedata[0] =  nodes;
      // // $scope.dnedata[1] =  lastNodeId;
      // // $scope.dnedata[2] =  links;
      // // $scope.dnedata[3] =  item;


      //       var nodes =  [{id: 0, reflexive: false, name:"Jim", dat: {city:"West Sac", zip:"95603"} }, {id: 1, reflexive: true, name:"Char", dat: {city:"West Sac", zip:"95691"}  }
      //              , {id: 2, reflexive: false, name:"Harry", dat: {city:"Sac", zip:"96774"} }, {id: 3, reflexive: false, name:"Al", dat: {city:"Davis", zip:"95666"} }];
      //       var lastNodeId = 3;
      //       var links =  [{source: nodes[0], target: nodes[1], left: false, right: true },
      //                       {source: nodes[1], target: nodes[2], left: false, right: true },
      //                       {source: nodes[1], target: nodes[3], left: false, right: true }
      //                      ];
      //       //var tjbs = UserService.connections;
      //       dnedata[0] =  nodes;
      //       dnedata[1] =  lastNodeId;
      //       dnedata[2] =  links;
      //       dnedata[3] =  item;

      //       console.log( 'DEBUG UserService.getLocalConnections :', dnedata)

      //       return  dnedata;
      //   };
    
      //   ComponentService.getLocalOthersPage = function( newPage, pageSize, fetchBy, callback ) {
             

      //       console.log("DEBUG: getLocalOthersPage: fetchBy.fetchFirstName: " );
      //       console.log( fetchBy );
            
      //       if(this.user.authToken) {

                   
      //           $http(
      //               {
      //                   "method" : "PUT",
      //                   "url"    : "/api/v1/users/fetch",
      //                   "headers" : { "Authorization" : "Session " + this.user.authToken
      //                   },
      //                   "data"   : { "newPage" : newPage,
      //                               "pageSize" : pageSize,
      //                               "fetchBy" : fetchBy
                                    
      //                   }         
      //               })
      //               .success(function(data, status, headers, config) {

      //                       //$scope.others= data.splice(0,250); 

      //                       callback(null, data );
      //               })
      //               .error(function(data, status, headers, config) {

      //                       callback(true, null);
      //               });
            
      //       } else {

      //               console.log("ERROR: getLocalOthersPage: User is not authenticated");
      //       }
                    
            
      //   };  
      
        ComponentService.getUser = function() {
            
            return this.user;
        };

        ComponentService.init = function() {

            componentService.user = angular.fromJson(sessionStorage.getItem("user"));
            
        };

        // ComponentService.getLocalMbrsite = function( ndx ) {

        //     console.log("INFO: componetService.getLocalUser.userId: from users array: " + ndx );

        //     console.log("DEBUG : " , this.usersList.length );
            
        //     // for(var i = 0; i < this.usersList.length; i++) {
                
        //     //     console.log("DEBUG : " + i );

        //     //     if(this.usersList[i]._id.toString() === userId ) {
                    
        //             UserService.usersEditItem =   UserService.usersList[ndx];

        //             return UserService.usersEditItem ;

        //         // }
        //         // else {

        //         //    console.log( this.usersList[i]._id ); 
        //         // }
        //     //}

        //     // // remove comment slashes from above when retrieve feature is enabled.     
        //     // console.log("NOTE: userService.getLocalUser.userId: remove temp fix when data retrieve is enabled." + userId );                
        //     // return this.usersListItem;
        // };

        ComponentService.getMbrsite = function( userId, cLabels , callback) {

            console.log("DEBUG: componentService.getMbrsite userId : cLabels : ", userId, cLabels );
            
            var user = userService.getUser();

            console.log("DEBUG: componentService.getMbrsite this.user : ", user );

            if( user.authToken) {

                  $http(
                    {
                        "method" : "GET",
                        "url" : "/api/v1/components",                  // componentApi.getComponentsV1)
                        "headers" : {
                            "Authorization" : "Session " + user.authToken
                        },
                        "cache" : false,
                        "params": { 'userKeyId' : userId,
                         'cLabels'  : cLabels
                       }

                    })
                    .success(function(data, status, headers, config) {
                        //ComponentService.mbrsite = [];
                       // ComponentService.mbrsite = data[0];
                        var ttt = data[0];
                        console.log("DEBUG: componentService.getMbrsite mbrsite : ", ComponentService.mbrsite );
                        console.log("DEBUG: componentService.getMbrsite data[0] : ", data[0] );
                        console.log("DEBUG: componentService.getMbrsite ttt     : ", ttt );

                        
                        callback(null, data[0] );
                    })
                    .error(function(data, status, headers, config) {

                        console.log("DEBUG: componentService.getMbrsite status : ", status );
                        ComponentService.mbrsite = {};    
                        callback(true, null);
                    });
            }
            else {

                console.log("ERROR: User is not authenticated");
            }

            // // remove comment slashes from above when retrieve feature is enabled.     
            // console.log("NOTE: userService.getLocalUser.userId: remove temp fix when data retrieve is enabled." + userId );                
            // return this.usersListItem;
        };
        // API : getUsers
        ComponentService.getUsers = function(callback) {

            if(this.user.authToken) {

                $http(
                    {
                        "method" : "GET",
                        "url" : "/api/v1/users",
                        "headers" : {
                            "Authorization" : "Session " + this.user.authToken
                        },
                        "cache" : false


                    })
                    .success(function(data, status, headers, config) {

                        callback(null, data.splice(0,75));
                    })
                    .error(function(data, status, headers, config) {

                        callback(true, null);
                    });
            }
            else {

                console.log("ERROR: User is not authenticated");
            }
        };

        
        // API called from admin console addUser function. 
        // creating basic component set ( 2 parts Profile and Content ) matching user type
        // one or more tab / content models - 
        ComponentService.createComponentSet = function( newComponent, callback) {

            $http(
                {
                    "method" : "POST",
                    "url" : "/api/v1/components",
                    "data" : newComponent
                })
                .success(function(data, status, headers, config) {

                    //  combine returned status and data and return to calling controller
                    var retdMsg = {};
                    retdMsg.msg = status;
                    retdMsg.contentIds =  data[0]; 
                    retdMsg.profileIds =  data[1]; 
                    console.log("DEBUG: createComponentSet:  components created *********** retdMsg ******: ", retdMsg );
                    callback(false, retdMsg );
                    // orig callback(false, status);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });

                console.log("INFO: createComponent(s) being exectuted w/promise");    
        };

        ComponentService.createComponent = function( newComponent, callback) {

            $http(
                {
                    "method" : "POST",
                    "url" : "/api/v1/components",
                    "data" : newComponent
                })
                .success(function(data, status, headers, config) {

                    //  combine returned status and data and return to calling controller
                    var retdMsg = {};
                    retdMsg.msg = status;
                    retdMsg.contentIds =  data[0]; 
                    retdMsg.profileIds =  data[1]; 
                    console.log("DEBUG: Component created ************* retdMsg ******: ", retdMsg );
                    callback(false, retdMsg );
                    // orig callback(false, status);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });

                console.log("INFO: createComponent(s) being exectuted w/promise");    
        };
        
        // API saveComponrnt
        ComponentService.saveComponent = function( component, callback) {
            console.log("DEBUG ComponentService.saveComponent  being updated: " );
            $http(
                {
                    "method" : "PUT",
                    "url" : "/api/v1/components",
                    "data" : component
                })
                .success(function(data, status, headers, config) {
                    callback(false, status);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });
        };

        /* API Delete User
        *
        * NOTE: idType is either:
        *      "keyId" or "objectId"
        *
        *   obs loginId obs
        */

        ComponentService.deleteComponent = function(id, idType, callback) {

            
            $http(
                {
                    "method" : "DELETE",
                    "url" : "/api/v1/components/" + id,
                    "params" : {
                        "idType" : idType
                    }
                })
                .success(function(data, status, headers, config) {

                    callback(false, status);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });
        };

        /* API Connect User
        *
        * NOTE: idType is either:
        *      "keyId" or loginId or "objectId"
        *
        * rootUser = {'Id': $scope.user._id, 'idType': 'objectId' };
        *
        * otherUser = {'otherId': $scope.others[index]._id, 'idType': 'objectId' };
        * 
        * relationShip = { 'relationType' : '????', 'relationData' : '??????' };
        */   
        ComponentService.connectUser = function( rootUser , otherUser , relationShip, callback) {

            console.log("DEBUG UserService : Connect from root user : ", rootUser.Id  ); 
            console.log("DEBUG UserService : Connect to other user : ", otherUser.otherId  );
            console.log( otherUser );

            $http(
                {
                    "method" : "PUT",
                    "url" : "/api/v1/users/" + rootUser.Id + "/connect",
                    "data" : { "idType" : rootUser.idType,
                                 "otherId" : otherUser.otherId,
                                 "otherType" : otherUser.idType,
                                 "relationType" : relationShip.relationType,
                                 "relationData" :  relationShip.relationData 
                    }
                })
                .success(function(data, status, headers, config) {

                  
                    //sessionStorage.setItem("user", angular.toJson(data));

                    //callback(false, status);
                     callback(null, data);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });
        };

        /* API Disconnect User
        *
        * NOTE: idType is either:
        *      "keyId" or loginId or "objectId"
        *
        */
    
        ComponentService.disconnectUser = function(rootUser, connectedUser , options, callback) {

            console.log("DEBUG Userservice : Disconnect from root user : ", rootUser.Id  ); 
            console.log("DEBUG UserService : Disconnect connected user : ", connectedUser.connectedId  );
            console.log( connectedUser );
            
            $http(
                {
                    "method" : "PUT",
                    "url" : "/api/v1/users/" + rootUser.Id + "/disconnect",
                    "data" : { "idType" : rootUser.idType,
                               "connectedId" : connectedUser.connectedId,
                                 "otherType" : connectedUser.idType,
                                 "options" : options
                    }
                })
                .success(function(data, status, headers, config) {

                    console.log("DEBUG disconnectUser : Disconnected connected user : ", data  );    
                    callback( null , data);
                    //callback(false, status);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });
        };
    
        ComponentService.findConnections = function( userId, options, callback) {

            if(this.user.authToken) {

                $http(
                    {
                        "method" : "PUT",
                        "url" : "/api/v1/users/" + userId + "/findconnects" ,
                        "data" : { "options" : options

                        } 
                       
                       // "headers" : {
                       //     "Authorization" : "Session " + this.user.authToken
                       // },
                       // "cache" : false
                        

                    })
                    .success(function(data, status, headers, config) {
                       
                        console.log("DEBUG UserService : findConnections : data : ", data  );  

                        callback(null, data);
                    })
                    .error(function(data, status, headers, config) {

                        callback(true, null);
                    });
            }
            else {

                console.log("ERROR: User is not authenticated");
            }
        };



        return ComponentService;
    }]);

}());

