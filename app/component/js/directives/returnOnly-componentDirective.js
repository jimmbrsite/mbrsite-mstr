(function () {

    "use strict";
    /* git hub  https://github.com/britztopher/genric-modal/blob/master/directives/directives.js */

    //var module = angular.module('training.directives', ['ui.bootstrap'])
    //var module = angular.module('brilliantDirectives', ['ui.bootstrap']);
    //var module = angular.module('training.directives', ['ui.bootstrap']);
    
    //var templateDir = 'app/user/partials/';
    var templateDir = 'component/partials/';

    /*
     custom directive to use as a generic modal window.
     Usage:
     <div>
     <my-modal instance_template='butterflies' use_ctrl="ButterFliesCtrl"></my-modal>
     </div>
     Attributes:
     instance_template: the tempalate name minus the .tpl.html extension.  This assumes the template is in the
     /js/templates/ folder/
     use_ctrl: this is the controller you want to be passed to be used in your modal instance if its located outside of this
     directive then you will need to inject the module it belongs in so this can find it
     The above example decalares a modal window where the template is found at js/templates/modal_instance.html and uses the
     controller MyModalInstanceCtrl
     */
    //module('training.directives', ).directive('usermodal', function( $modal ) {
    angular.module('training.directives').directive('mbrsiteContentset', function(){
        console.log('mbrsiteContentset  ************************** Start-tmpl : ', templateDir );
         /*
         <usermodal  name='UserEditForm' instance_template="user-admin-edit" usersEditItem="{{usersList[$index]}}" use_ctrl="UserModalCtrl" class="btn-danger btn-sm rcs">
          require: "ngModel",
         */
        return {
             scope: {
                mbrsite: '=',
             },
             templateUrl: function(elem, attr){
                   return templateDir+attrs.instanceTemplate +'.html';
             }

            
              
        };
        console.log('mbrsiteContentSet started - $scope mbrsite: ', $scope.mbrsite );
    });

    angular.module('training.controllers').controller('mbrsiteContentCtrl', [ '$scope', 'training.services.user', function($scope, userService) {
       
        console.log('mbrsiteContentCtrl started - $scope itemdata: ', $scope.mbrsite );
        
        // example data  var edItem = {"street2":"","loginId":"349940297","lastName":"Aaron","LabelId":"acd","organizationName":"","website":"","street":"Po Box 62","zipcode":"62915","state":"IL","userRole":"user","password":"Leslie62915","city":"Cambria","userType":"admin","mobilephone":"","email":"","bioinfo":"","providerId":"349940297","firstName":"Leslie ","keyId":"","contacthone":"(618) 985-2803","_id":49700};  
        
        //var userRole = UserService.getPermissions();  



        //var edItem = JSON.parse(itemdata.ctrldata);
       
        //$scope.userCtrl = edItem; 
        
        
            
        //$scope.custDat = custDat;
         
        $scope.ok = function(){
           // $modalInstance.close();
        };

        $scope.cancel = function () {
            //$modalInstance.dismiss('cancel');
        };
        
        // $scope.loadUserItem = function( ndx ) {

        //    var tmp = {};
        //    var tmp = custDat.edititem;
        //    return  tmp;

        // };    
        
    }]);

}()); // eof function

