(function() {

    'use strict';

    /*
     * Users Ctrl
     */
    angular.module('training.controllers').controller('MbrsiteAdminConsoleCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {

    
        $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            
            }         
        };

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
        // deletes user from a list
            }         
        };


        // deletes user from a list
        $scope.delete = function(index) {
             
            /* NOTE: idType is either :
            *      "keyId" or loginId or "objectId"
            *
            ****  2014/07  currently dynamic idType is not enabled for this function
            ****  
            *
            * userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {
            * userService.deleteUser($scope.users[index].id, "loginId", function(err, status) {    
            * userService.deleteUser($scope.users[index].keyId, "keyId", function(err, status) {  
            * 
            */
            
            userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {    

                if(err) {

                    console.log("ERROR: delete user");
                }
                else {

                    console.log("INFO: deleted user");
                    $scope.users.splice(index, 1);
                }
            });
        };

        $scope.getUser = function() {

            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }
        };

        var user = userService.getUser();

        if(user && user.authToken) {

            userService.getUsers(function(err, users) {

                $scope.users = users;
                
                userService.saveLocalUsers(users);

            });
            
        }
        else {

            $location.path('/home');
        };
            
    }]);

    /*** USER PROFILE TABS SECTION 
     *
     * User Admin Profile Ctrl ************************************
     * 
     *   the user edit ctrl collects remote user data and sets different content in $scope.user as determined by userId
    */
    angular.module('training.controllers').controller('MbrsiteAdminProfileCtrl', ['$scope', '$routeParams', '$location', 'training.services.user', 'training.services.component', function($scope, $routeParams, $location, userService, componentService ) {
        // broadcasting test source  BOTTOM Controller
        //  added to UserListCtrl (top) , UserCreateCtrl (Middle),  UserAdminProfileCtrl  (Bottom ) 
        //$scope.mbrProfileEditForm.editItem = '';  
        $scope.topValue = 0;
        $scope.middleValue = 0;
        $scope.bottomValue = 0;

        $scope.$watch('bottomValue', function() {
            userService.updateBottomValue($scope.bottomValue);
        });

        $scope.$on('valuesUpdated', function() {
            $scope.topValue = userService.topValue;
            $scope.middleValue = userService.middleValue;
        });
        // end of Broadcast Test
        // $scope.bizEditItem  = ""; 
        // $scope.$on('$viewContentLoaded', function(){
        //     $scope.getMbrsite(); 
        // });  

        //console.log("DEBUG: MbrsiteAdminProfileCtrl bizEditItem : ", $scope.bizEditItem  );
        

          
        $scope.upricategory = ""; 
        $scope.up2ndcategory = "";
        $scope.up3rdcategory = "";
        //$scope.selOrgCategory = componentService.getBizCategories();
        
        //console.log("DEBUG: MbrsiteAdminProfileCtrl selOrgCategory : ",  componentService.getBizCategories() );
        
        $scope.setUMChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'pri':
                     $scope.upricategory = $scope.selOrgCategory[choice];
                    break;
                case '2nd':
                   $scope.up2ndcategory =  $scope.selOrgCategory[choice];
                    break;
                case '3rd':
                    $scope.up3rdcategory = $scope.selOrgCategory[choice];
                    break;    
                default:
                    console.log("ERROR: MbrsiteAdminProfileCtrl setUSChoices  *************************", choice );    
            }
            return 
        };


       $scope.save = function() {

            console.log("DEBUG: MbrsiteAdminProfileCtrl : ***** pre save *******", $scope.bizEditItem );

            //$scope.usersedititem = angular.copy(nuuser);
            // $scope.bizEditItem.profilelabelId = $scope.utype;
            // $scope.bizEditItem.role = $scope.urole;
            // $scope.bizEditItem.permissions = $scope.uperm;
// ***************
// NEED TO REVISIT ADD USER LOGIC TO INCLUDE NEW PROPERTIES AND LAYOUT FOR MBRSITE COMPONENT
//****************

            // keyId : req.body.keyId || targetKeyId, 
            // lastupdated: req.body.lastupdated || "",        
            // updatedById: req.body.updatedById || "",
            //    isActive: req.body.isActive || "",      // ** Required, set false at creation-relates to state


            // "Member"   user not allowed to alter the following fields
            //$scope.bizEditItem.userKeyId:    ** set to "member" userID at creation = retdIds._id;
            //$scope.bizEditItem.dateCreated:  ** set at creation
            //$scope.bizEditItem.approveDate:  ** Required, set at time of obj creation
            //$scope.bizEditItem.approvedById: ** Required, set at time of obj creation
            // other properties not set in form
           
            
            $scope.bizEditItem.lastUpdated =  new Date();
            $scope.bizEditItem.updatedById =  userService.user.keyId; 
            $scope.bizEditItem.isActive = false;            
            $scope.bizEditItem.approveDate = new Date() ;
            $scope.bizEditItem.status  = 'Yes';    // ** Required, set at system level-relates to validity of   
            $scope.bizEditItem.isActive = 'Yes';   //  ** Required, set false at creation-relates to state of
            //$scope.bizEditItem.category = ''; 

            $scope.bizEditItem.category[0] = $scope.upricategory; 
            $scope.bizEditItem.category[1] = $scope.up2ndcategory;
            $scope.bizEditItem.category[2] = $scope.up3rdcategory;

            $scope.bizEditItem.keywords[0] = $scope.bizEditItem.category[0];   // temp fix for key words

            //$scope.bizEditItem.cFirstName[0] = $scope.bizEditItem.category[0];   // temp fix for key words 
            // console.log("DEBUG: MbrsiteAdminPro
//ProfileCtrl.componentService.saveComponent :  pre save ***", $scope.bizEditItem );
// ***************
// NEED TO REVISIT BEFORE CREATING COMPONET PART tabsetBar and tabsetPanes 
//                        try adding properties at this time for the mbrsite 
//                        BarId relates to PaneId, PaneId relates to ContnetID
//*********
           /*   ThIS iS GOOD CODE JUST NOT WANTING TO OVERWRITE TEST DATA WITH 
            $scope.bizEditItem.tabBarId = [0];
            $scope.bizEditItem.tabBarHref = ["href='#tab0-1'"];
            $scope.bizEditItem.tabBarLabelText = ["Blank 2"];
            $scope.bizEditItem.tabBarisActive = [true];
            $scope.bizEditItem.tabBarOrder = [1] ;
            // tabPanes for each tabPane there can be several ContentModel entries
            $scope.bizEditItem.tabPaneIds = ["tab0-1"];
            $scope.bizEditItem.tabPaneTitleText = ["Blank Template Two"];
            // ContentModels for each contentModel there are thre content entries title,text,imgsrc
            $scope.bizEditItem.contentPaneIds = ["tab0-1"];
            $scope.bizEditItem.contentNameIds = ["contentOne"] ;                 // ["contentOne","contenTwo", ...]
            $scope.bizEditItem.contentIsOk = ["Support"];
            
            $scope.bizEditItem.contentModelTitleText = ["Sample Title 1"];
            $scope.bizEditItem.contentModelTextArea = ['mbrs-Etiam sit amet leo nec nunc auctor convallis vel at est. Aenean at metus id nisl posuere varius. In vel ipsum eu risus iaculis volutpat in ut metus. Integer eget sem eu massa pharetra vehicula ut id elit. Nam sagittis malesuada est, ut tempus diam ultricies eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla urna libero, finibus in metus mollis, aliquam interdum elit. Phasellus at sagittis lectus. Vestibulum scelerisque libero eget efficitur venenatis. Etiam nunc magna, maximus vel vehicula eget, bibendum eu urna. Phasellus quis turpis id lacus mollis dictum. Ut dui elit, efficitur vel orci eu, elementum interdum erat. Cras at leo volutpat, commodo metus in, dictum justo. Cras id vulputate ex. Nullam et aliquet arcu, id elementum magna. Phasellus faucibus sapien at dolor ultrices pellentesque.'];
            $scope.bizEditItem.contentModelImgSrcs = ["common/bootstrap/images/customer_service2.jpg"];
            $scope.bizEditItem.contentModelImgTextArea: req.body.contentModelImgTextArea || [],  // ["common/bootstrap/images/customer_service2.jpg", ...];
            $scope.bizEditItem.contentModelLinkTarget: req.body.contentModelLinkTarget || [],  // ["href='#'"];
            $scope.bizEditItem.contentModelLinkTextArea: req.body.contentModelLinkTextArea || // ["Click here for more information"];
            */

            componentService.saveComponent($scope.bizEditItem, function(err, status) {
               
                if(err) {

                    console.log("ERROR: component not updated *****: ", err);
                }
                else {

                    console.log("DEBUG: component updated with status: ", status );
                    //$scope.usersedititem = '';
                    //$location.path('/#/users/profil/:#tab3-5');
                    //$scope.$apply(); 
                       
                      // commented $scope.usersEditItem = {};
                   // commented  });
                }

            });
        };

        $scope.cancel = function () {
            
            console.log("INFO: canceled user update");
            
            //$location.path('/users/profile/' + userId );
            
                
        };

        $scope.initMbrsite = function ( part  ) {

          console.log("DEBUG: initMbrsite pre getUser :", user );
          
          var user = userService.getUser();
          
          console.log("DEBUG: initMbrsite post getUser:", user );

          if(user && user.authToken) {
            

            var cLabels = ['Components','Mbrsite',part];     // part: Profile,Tabset or Panes 
                            
            componentService.getMbrsite( user.keyId, cLabels , function(err, mbrsite) {

                if(err) {

                    console.log("ERROR: initMbrsite ");
                }
                else {

                    // may want to make this multiple mbrsites   
                    console.log("DEBUG: got mbrsite *********** : ", mbrsite);
                    //$scope.mbrProfileEditForm.editItem = mbrsite;
                     console.log("ERROR: UserProfileCtrl initMbrsite post  part,mbrsite:*************************: ", part, mbrsite ); 
                    switch( part ) {
                        case 'Profile':
                            $scope.bizEditItem = mbrsite;
                            componentService.setLocalMbrsite( mbrsite) ;
                            // fixed $scope.bizEditItem.lblArray = mbrsite.groupLabel + ":" + mbrsite.typeLabel + ":" + mbrsite.partLabel;  // 'Components:Mbrsite:Profile';                              
                            $scope.upricategory = mbrsite.category[0]; 
                            $scope.up2ndcategory = mbrsite.category[1];
                            $scope.up3rdcategory = mbrsite.category[2];
                            $scope.selOrgCategory = componentService.getBizCategories();
                            break;
                        case 'Tabset':
                            //$scope.utype =  $scope.selTypes[choice];
                            break;
                        case 'Panes':
                            //$scope.uperm = $scope.selPermissions[choice];
                            break;    
                        default:
                            $scope.bizEditItem = "";
                            console.log("ERROR: UserProfileCtrl initMbrsite  part: *************************", part );    
                    }

                }
           
            });
            
          }
          else {
            console.log("DEBUG: MbrsiteAdminProfileCtrl.initMbrsite not authenticated ** :", user.authToken );
            $location.path('/users.adminConsole');
          }

        };

       
    }]);
    
    angular.module('training.controllers').controller('b2TabPaneContent', ['$scope', 'training.services.user', 'training.services.component', function($scope, userService, componentService ) {
      
      // 1.) Which template set are we processing
      // 2.) Set validation rules
      

      //
      //var user = userServices.getUser();
      //var wrkConnections = [];
      
      //wrkConnections = userServices.connections;

        $scope.items = ['Sysadmin', 'Member', 'Subscriber', 'Associate', 'Support'];

        var localMbrsite =  componentService.getLocalMbrsite();       
        
        $scope.isAuthorized = userService.getUserPermissions(); //  $scope.items[0];  //
      
        $scope.isOK = userService.getUserPermissions();

        console.log("DEBUG: b2TabPaneContent.localMbrsite data via Service ************** ", localMbrsite );
        try {
       
            $scope.tabPane = {
                paneTabId : localMbrsite.paneTabIds[0],
                paneTabTitleText: localMbrsite.paneTabTitles[0],
                contentOne: {
                          tabId : localMbrsite.contentTabIds[0],
                          tabNameId: localMbrsite.contentTabNameIds[0],
                          isOK: localMbrsite.contentIsOk[0],
                          titleText: localMbrsite.contentModelTitleText[0],
                          textArea: localMbrsite.contentModelTextArea[0],
                          imgSrc:  localMbrsite.contentModelImgSrcs[0],
                          imgTextArea: localMbrsite.contentModelImgTextArea[0],
                          linkTarget:  localMbrsite.contentModelLinkTarget[0],
                          linkTextArea: localMbrsite.contentModelLinkTextArea[0]
                },
                contentTwo: {
                       //    isOK:   localMbrsite.tabContentIsOk[0],
                       // titleone:  localMbrsite.tabContentModelTitles[0],
                       //  spanone:  localMbrsite.tabContentModelSpans[0],
                       // imgsrcone: localMbrsite.tabContentModelImgsSrc[0]
                        titleText:  'tmpl-Ssub title 2',
                        textArea:   'tmpl-Donec blandit suscipit ante, quis viverra urna ornare vitae. Vestibulum dolor ligula, scelerisque at lectus in, suscipit lobortis enim. Nunc at mattis sapien, quis mollis dolor. Donec blandit diam velit, quis pellentesque ante lobortis a. Aliquam et diam eget nibh tempus vulputate non ut tellus. Mauris sagittis vehicula dapibus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Aenean a quam ut nisl sollicitudin fringilla eu faucibus massa. Duis cursus augue at sapien tempus, vitae ornare tortor euismod. Maecenas pharetra tincidunt felis non consectetur. Maecenas condimentum mi sit amet nibh tempor accumsan. In hac habitasse platea dictumst. Duis accumsan, metus dictum fringilla facilisis, purus magna dictum lacus, eget consequat erat purus sit amet quam. Nam luctus ac ipsum non rhoncus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Fusce elementum, erat vitae gravida tincidunt, justo lorem maximus leo, a iaculis purus risus sit amet felis. Donec a lorem sagittis massa consequat tristique.'
                },
                contentThree: {
                       //    isOK:   localMbrsite.tabContentIsOk[0],
                       // titleone:  localMbrsite.tabContentModelTitles[0],
                       //  spanone:  localMbrsite.tabContentModelSpans[0],
                       // imgsrcone: localMbrsite.tabContentModelImgsSrc[0]
                         titleText:  'tmpl-Sub title 3',
                         textArea:   'tmpl-Donec blandit suscipit ante, quis viverra urna ornare vitae. Vestibulum dolor ligula, scelerisque at lectus in, suscipit lobortis enim. Nunc at mattis sapien, quis mollis dolor. Donec blandit diam velit, quis pellentesque ante lobortis a. Aliquam et diam eget nibh tempus vulputate non ut tellus. Mauris sagittis vehicula dapibus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Aenean a quam ut nisl sollicitudin fringilla eu faucibus massa. Duis cursus augue at sapien tempus, vitae ornare tortor euismod. Maecenas pharetra tincidunt felis non consectetur. Maecenas condimentum mi sit amet nibh tempor accumsan. In hac habitasse platea dictumst. Duis accumsan, metus dictum fringilla facilisis, purus magna dictum lacus, eget consequat erat purus sit amet quam. Nam luctus ac ipsum non rhoncus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Fusce elementum, erat vitae gravida tincidunt, justo lorem maximus leo, a iaculis purus risus sit amet felis. Donec a lorem sagittis massa consequat tristique.' 
                }
            };
        }
        catch(err) {
       
         //document.getElementById("demo").innerHTML = err.message;
         localMbrsite =  componentService.getLocalMbrsite();  
        };



        $scope.updateThis = function(){
          
            alert("data saved");
       
        };

        $scope.uploadThis = function(){
          
            alert("file uploaded");
       
        };

        $scope.saveThisTab = function(){
          
            alert("save this tab");
       
        };

        $scope.postChangesThisTab = function(){
          // editable-text="user.name"
            alert("submit this tab: chack with other tabs to see if changes /nare pending. if so, then give warning user");
            console.log("DEBUG: b2TabPaneContent.postChangesThisTab data  ************** ", $scope.tabPane );

            /* One way to submit data on server is to define onbeforesave attribute pointing to some method of scope.
             Useful when you need to send data on server first and only then update local model (e.g. $scope.user).
             New value can be passed as $data parameter (e.g. <a ... onbeforesave="updateUser($data)">).

            The main thing is that local model will be updated only if method returns true or undefined (or promise resolved
            to true/undefined). Commonly there are 3 cases depending on result type:

            true or undefined: Success. Local model will be updated automatically and form will close.
            false:   Success. But local model will not be updated and form will close.   Useful when you want to update local model manually
                                                                                         (e.g. server changed values).
            string: Error. Local model will not be updated, form will not close, string will be shown as error message. Useful for validation and processing errors.

            */

             return " Not updated";
        };      
     

    }]);


    /*
    * Users Ctrl
    */
    angular.module('training.controllers').controller('obs-UserAdminHomeCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {

    
        $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            
            }         
        };

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };


        // deletes user from a list
        $scope.delete = function(index) {
             
            /* NOTE: idType is either :
            *      "keyId" or loginId or "objectId"
            *
            ****  2014/07  currently dynamic idType is not enabled for this function
            ****  
            *
            * userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {
            * userService.deleteUser($scope.users[index].id, "loginId", function(err, status) {    
            * userService.deleteUser($scope.users[index].keyId, "keyId", function(err, status) {  
            * 
            */
            
            userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {    

                if(err) {

                    console.log("ERROR: delete user");
                }
                else {

                    console.log("INFO: deleted user");
                    $scope.users.splice(index, 1);
                }
            });
        };

        $scope.getUser = function() {

            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }
        };

        var user = userService.getUser();

        if(user && user.authToken) {

            userService.getUsers(function(err, users) {

                $scope.users = users;
                
                userService.saveLocalUsers(users);

            });
            
        }
        else {

            $location.path('/home');
        };
            
    }]);
    /***USER ADMIN  CONTROLLER 
    *
    * UserAdminCtrl
    */
    angular.module('training.controllers').controller('obs-UserAdminConsoleCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {

        $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }         
        };

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };


        // deletes user from a list
        $scope.delete = function(index) {
             
            /* NOTE: idType is either :
            *      "keyId" or loginId or "objectId"
            *
            ****  2014/07  currently dynamic idType is not enabled for this function
            ****  
            *
            * userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {
            * userService.deleteUser($scope.users[index].id, "loginId", function(err, status) {    
            * userService.deleteUser($scope.users[index].keyId, "keyId", function(err, status) {  
            * 
            */
            
            userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {    

                if(err) {

                    console.log("ERROR: delete user");
                }
                else {

                    console.log("INFO: deleted user");
                    $scope.users.splice(index, 1);
                }
            });
        };

        $scope.getUser = function() {

            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }
        };

        // NOTE: likley need to revist this logic - Do we want to get all USERS  automatically??? 
        //       is it enough to just bail out if user is not setup properly token etc  or should token also be added to 
        //       url's ???
        var user = userService.getUser();

        if(user && user.authToken) {
            
            var options = { cLabels: ['Components', 'Member']};
                            
            componentService.getMbrsite( user.KeyId, options, function(err, mbrsite) {

                $scope.editItems = users;
                
                userService.saveLocalUsers(users);

            });
            
        }
        else {

            $location.path('/home');
        };
            
    }]);
    
   
 /*** USER PROFILE TABS SECTION 
     *
     * User Profile Ctrl ************************************
     * 
     *   the user edit ctrl collects remote user data and sets different content in $scope.user as determined by userId
    */
    angular.module('training.controllers').controller('obs-UserProfileCtrl', ['$scope', '$routeParams', '$location','training.services.user', function($scope, $routeParams, $location, userService) {
        
        // note: this code section exicutes every time UsersEditCtrl is entered?
        
        // get logged in user info
        $scope.usersedititem = userService.getUser();   

        console.log("DEBUG: UserProfileCtrl params : ", $scope.usersedititem );
        
        var trps = [{"types": { Root: "Root", Sysadmin: "Sysadmin", Admin: "Admin", Member: "Member", Follower: "Follower", Subscriber: "Subscriber"}},
                    {"roles": { Owner: "Owner", Manager: "Manager",  Staff: "Staff", Volunteer: "Volunteer" }},
                    {"permissions": { Publisher: false, Editor: false, Support: true  }}];
                    
        $scope.utype = $scope.usersedititem.type;
        $scope.urole = $scope.usersedititem.role;
        $scope.uperm = $scope.usersedititem.permissions;
        
        $scope.selTypes = Object.keys(trps[0]['types']);
        $scope.selRoles = Object.keys(trps[1]['roles']);
        $scope.selPermissions = Object.keys(trps[2]['permissions']);
        
        
        $scope.setUSChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'urole':
                     $scope.urole = $scope.selRoles[choice];
                    break;
                case 'utype':
                    $scope.utype =  $scope.selTypes[choice];
                    break;
                case 'uperm':
                    $scope.uperm = $scope.selPermissions[choice];
                    break;    
                default:
                    console.log("ERROR: UserProfileCtrl setUSChoices  *************************", choice );    
            }
            return 
        }; 

       $scope.save = function() {

            console.log("DEBUG: UserProfileCtrl : ***** pre save *******", $scope.usersedititem );

            //$scope.usersedititem = angular.copy(nuuser);
            $scope.usersedititem.type = $scope.utype;
            $scope.usersedititem.role = $scope.urole;
            $scope.usersedititem.permissions = $scope.uperm;
            // $scope.savedUser.orgPriCategory = $scope.upricategory;
            // $scope.savedUser.orgSecCategory =  $scope.up2ndcategory;
            // $scope.savedUser.orgThrdCategory = $scope.up3rdcategory;
            
            // update user info in session
            userService.saveUser($scope.usersedititem, function(err, status) {
               
               console.log("DEBUG: UserProfileCtrl : ***** post save *******", $scope.usersedititem );
                
                if(err) {

                    console.log("ERROR: user info not updated");
                }
                else {

                    console.log("INFO: user info updated with status: ", status );
                    //$scope.usersedititem = '';
                    //$location.path('/#/users/profil/:#tab3-5');
                    //$scope.$apply(); 
                       
                      // commented $scope.usersEditItem = {};
                   // commented  });
                }

            });
        };
        // NOTE : If delete is allowed at this button then we need to clean up connections etc.
        //        This user would need to be logged out as well..  
        $scope.delete = function () {
            // using need to fix api function to accept id or object


            // ask if they are sure and explain the consequense
            alert( " Are you sure ");

            userService.deleteUser($scope.user._id, "objectId", function(err, status) {

                if(err) {

                    console.log("ERROR: delete user");
                }
                else {

                    console.log("INFO: deleted user");

                    //$scope.users.splice(index, 1);
                    // SOME WORK HERE: If allowed 
                    $location.path('/home');
                }
            });
        };

      
        $scope.cancel = function () {
            
            console.log("INFO: canceled user update");
            
            //$location.path('/users/profile/' + userId );
            
                
        };

        

    }]);
    /***USER ADMIN  CONTROLLER 
    *
    * UserMemberSiteCtrl
    */
    angular.module('training.controllers').controller('obs-UserMbrsiteConsoleCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {

       // $scope.mbrsite = userService.getMemberSite();

        $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }         
        };

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };


            
    }]);


    /***USER EDIT TEST CONTROLLER 
      *
      * UserEditCtrl
      */
    angular.module('training.controllers').controller('obs-UsersEditCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {
        //*****
        // NOTE: this is obs
       //
       // $xeditable.run(function(editableOptions) {
       //            editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
       //  });
       
        // get logged in user info
       //scope.usersedititem = "";
        
            
    }]);  

    /***USER EDITABLE TEST CONTROLLER 
      *
      * UserEditableCtrl
      */
    angular.module('training.controllers').controller('obs-UsersEditableCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {
        //*****
        // NOTE: this is obs
       //
       // $xeditable.run(function(editableOptions) {
       //            editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
       //  });
       
        // get logged in user info
       //scope.usersedititem = "";
        
            
    }]);
    /*** USER ADMINISTRATION PAGE TABS SECTION 
     *
     *  User Dashboard Ctrl  ****************************************************
     * 
     *   contains the user profile ctrl which allows updating od user info
     */
    angular.module('training.controllers').controller('obs-UserDashboardCtrl', ['$scope', '$routeParams', '$location','training.services.user', function($scope, $routeParams, $location, userService) {
           
        // note: this code section exicutes every time UsersEditCtrl is entered?

        console.log("DEBUG: usersEdit params : ");
        console.log( $routeParams )
        
        var userId = $routeParams.userId;
        // var userTab=  $routeParams.tabs | '';

        //$scope.meals = [];    // from examples

        // 20140926  $scope.user = userService.getLocalUser(userId);
        $scope.user = userService.getUser();
        
        console.log("DEBUG: userEditControl.userTabrouteParams : routeParams  ", $routeParams);
       
        // DEBUG alert( userId );

        
        if (!$scope.user) {

            // 20140917 $location.path('/users');
            console.log("INFO: userController.UsersEditCtrl : !$scope -> /home");    
            $location.path('/home');
        }
        else {

           
            var options = { 'types' : ['FOLLOWS'],          
                            'direction' : "out" };

            userService.findConnections( userId, options, function( err, results ) {
                
                if(err) {

                    console.log("ERROR: userService.findConnections : err  ", err);
                }
                else {

                    console.log("INFO: userService.findConnections : results: ", results );
                    

                    $scope.connections = results;
                    //userService.UserService['connections'].push( results) ;   // = results;
                    
                               
                }
            });            
         
            
        }

      
        // 20140917 console.log("INFO: userEditCtrl: userService.getLocalOthersPage : scope others: ", $scope.others );
        
        
        // load first page into list
            // userService.getResultsPage(1);
        //$scope.others = userService.getLocalOthersPage(1);  // load first page of user not connected to.
        
        // $scope.pageChangeHandler = function(num) {
        //     console.log('header page changed to ' + num);
        //     userService.getResultsPage( num );
        // };
        
        // MOVED TO userNuConnectPageCtrl 20140919
        //  // local User pagination settings
        // $scope.itemsPerPage = 25; 
        // $scope.currentPage = 1;
        // $scope.pageSize =  25;  // this should match however many users you want on one page
        // // asynchronous User pagination settings
        // $scope.totalUsers = 0;
        // $scope.usersPerPage = 25; // this should match however many results your API puts on one page
        // //pageChangeHandler(1);

        //$scope.others = userService.getLocalOthersPage( 1 , function(err, status)  );

        // userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize , $scope.fetchBy , function(err, nxPageResults) {
        //                 if(err) {

        //                     console.log("ERROR: userNuConnect error");

        //                     $scope.message = "Pagination error - userNuConnect";

        //                 } else {

        //                     console.log("INFO: new page loaded");
        //                     // var totCount =   nxPageResults.length - 4850;
        //                     // var nxPage = 
        //                     // for(var i = 0; i < totCount  ; i++) {
                
        //                     $scope.others= nxPageResults;   
        //                     //userService.others= nxPageResults;
        //                     $scope.totalUsers =  $scope.others.length;   
                           
        //                    //}



                            
        //                 }
        // });

        // MOVED TO  userNuConnectPageCtrl  20140919   
        // $scope.pagination = {
        //    current: 1
        // };
  

        // END NOTE: 
        $scope.theseConnections = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };       

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };
        
        $scope.whichUserNotMe = function(beingViewedId) {
            var user = userService.getUser();
             console.log("DEBUG: whichUserNotMe", beingViewedId );
         
            if( beingViewedId ) {

                return true;
            }         
        };

        $scope.save = function() {
            console.log("DEBUG: UserProfileCtrl: ***** pre save *******", $scope.user );
            userService.saveUser($scope.user, function(err, status) {

                if(err) {

                    console.log("ERROR: save user");
                }
                else {

                    console.log("INFO: saved user");
                    
                    //$location.path('/users');

                }
            });
        };
        // NOTE : If delete is allowed at this button then we need to clean up connections etc.
        //        This user would need to be logged out as well..  
        $scope.delete = function () {
            // using need to fix api function to accept id or object


            // ask if they are sure and explain the consequense
            alert( " Are you sure ");

            userService.deleteUser($scope.user._id, "objectId", function(err, status) {

                if(err) {

                    console.log("ERROR: delete user");
                }
                else {

                    console.log("INFO: deleted user");

                    //$scope.users.splice(index, 1);
                    // SOME WORK HERE: If allowed 
                    $location.path('/home');
                }
            });
        };

        // connect user to user

        // $scope.addConnection = function (index) {
        //     console.log("DEBUG: making connection from root user");
        //     console.log( $scope.user._id);
        //     console.log("DEBUG: making connection to other user");
        //     console.log($scope);

        //     var rootUser = {'Id': $scope.user._id,
        //                     'idType': 'objectId' };

        //     var otherUser = {'otherId': $scope.others[index]._id,
        //                       'idType': 'objectId' };

        //     var relationShip = { 'relationType' : 'FOLLOWS',
          
        //                          'relationData' : { 'otherKeyId' : $scope.others[index].keyId } }; 
                              
        //     userService.connectUser( rootUser, otherUser , relationShip, function(err, results ) {
                
        //         if(err) {

        //             console.log("ERROR: making connection to other user");
        //             console.log( results );
        //             console.log( Object.keys(results).length )
        //         }
        //         else if ( [results].length ===  1 ) {

        //             console.log("DEBUG: made connection to other user: results: ", results );

        //             $scope.connections.push( results );
                    
        //             $location.path('/users/edit/' + rootUser.Id );
        //         }
        //         else {
        //             console.log("INFO: made connection to other user");
        //             $location.path('/users');
        //         }

        //     });
        // };

        // drop  user to user connection 
        $scope.dropConnection = function (index) {
           console.log("ERROR: disconnecting from this user");
           console.log($scope.connections[index]._id); 

           var rootUser = {'Id': $scope.user._id,
                            'idType': 'objectId' };

            var connectedUser = {'connectedId': $scope.connections[index]._id,
                              'idType': 'objectId' };

            var options = { 'types' : ['FOLLOWS'],          
                            'direction' : "out" };

           userService.disconnectUser(rootUser, connectedUser , options , function(err, result) {

                console.log("DEBUG: dropConnection : disconnectUser : result :", result );
                console.log("DEBUG: dropConnection : disconnectUser : result :", connectedUser.connectedId );

                if(err) {

                    console.log("ERROR: disconnecting from this connected user");
                }
                else if ( result == connectedUser.connectedId  ) {  //   
                    
                    console.log("DEBUG: dropConnection : disconnectUser : results :", result ); 
                    $scope.connections.splice(index, 1);
                    
                    $location.path('/users/edit/' + rootUser.Id);           
                } 
                else {
                    // $scope.users.splice(index, 1);
                    $location.path('/users');
                }
           });
        };

        $scope.cancel = function () {
            
            console.log("INFO: canceled user update");
            
            $location.path('/home');
            
                
        };

    

    }]);

    
    /*
     * userNuConnectPage Controller
    */
    angular.module('training.controllers').controller('obs-UsersOthersConnectPageCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {
        
        // local User pagination settings
        $scope.itemsPerPage = 25; 
        $scope.currentPage = 1;
        $scope.pageSize =  25;  // this should match however many users you want on one page
        $scope.fetchBy = {
            'fetchLastName' : '',
            'fetchFirstName' : '',
            'fetchCity'  : ''

        };
        // asynchronous User pagination settings
        $scope.othersTotalPossible = 0;
        $scope.othersPerPage = 25; // this should match however many results your API puts on one page
        //pageChangeHandler(1);
        $scope.pagination = {
           current: 1
        };
        $scope.pageChanged = function() {
                console.log('DEBUG: UserService.pageChanged: to nu page ' + $scope.currentPage);
                
                if ( $scope.fetchBy.fetchLastName != ""  ||  $scope.fetchBy.fetchFirstName != "" ) {
                     
                    console.log('DEBUG: UserService.pageChanged: got fetch data:' + $scope.fetchBy);
                    var tmpFetchBy = {
                                        'fetchLastName' : '',
                                        'fetchFirstName' : '',
                                        'fetchCity'  : ''
                                    };    
                    tmpFetchBy.fetchLastName = $scope.fetchBy.fetchLastName + ".*";
                    tmpFetchBy.fetchFirstName = $scope.fetchBy.fetchFirstName + ".*"; 
                    console.log('DEBUG: UserService.pageChanged: got tmp  data:', tmpFetchBy);
                    userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize , tmpFetchBy,  function(err, nxPageResults) {
                        if(err) {

                            console.log("ERROR: userNuConnect error");

                            $scope.message = "Pagination error - userNuConnect";

                        } else {

                            $scope.others = nxPageResults["data"];
                            console.log("DEBUG: new others page contents loaded: ");
                            console.log( $scope.others );
                            //userService.others =  nxPageResults;  
                            $scope.othersTotalPossible =  $scope.others.length;   
                           
                        }
                    });
                };         
        };

        $scope.fetchConnection = function() {
                console.log('getting more others: currentPage: ' + $scope.currentPage );
                console.log('getting more others: fetch: ' + $scope.fetchBy );
                if ( $scope.fetchBy.fetchLastName !== ""  ||  $scope.fetchBy.fetchFirstName !== "" ) {

                    userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize, $scope.fetchBy ,  function(err, nxPageResults) {
                        if(err) {

                            console.log("ERROR: userNuConnect error");

                            $scope.message = "Pagination error - userNuConnect";

                        } else {

                            console.log("INFO: new page loaded: " +  $scope.currentPage   );
                            // var totCount =   nxPageResults.length - 4850;
                            // var nxPage = 
                            // for(var i = 0; i < totCount  ; i++) {
                
                            $scope.others = nxPageResults["data"];

                            // userService.others =  nxPageResults;  
                            $scope.othersTotalPossible =  $scope.others.length;   
                           
                        }

                    });    
                };     
        };

        $scope.addConnection = function (index) {
            console.log("DEBUG: making connection from root user");
            console.log( $scope.user._id);
            console.log("DEBUG: making connection to other user");
            console.log($scope.others[index]._id);

            var rootUser = {'Id': $scope.user._id,
                            'idType': 'objectId' };

            var otherUser = {'otherId': $scope.others[index]._id,
                              'idType': 'objectId' };

            var relationShip = { 'relationType' : 'FOLLOWS',
          
                                 'relationData' : { 'otherKeyId' : $scope.others[index].keyId } }; 
                              
            userService.connectUser( rootUser, otherUser , relationShip, function(err, results ) {
                
                if(err) {

                    console.log("ERROR: making connection to other user");
                    console.log( results );
                    console.log( Object.keys(results).length )
                }
                else if ( [results].length ===  1 ) {

                    console.log("DEBUG: made connection to other user: results: ", results );

                    $scope.connections.push( results );
                    
                    $location.path('/users/profile/' + rootUser.Id + '#tab2-5' );
                }
                else {
                    console.log("INFO: made connection to other users");
                    $location.path('/users/profile/' + rootUser.Id + '#tab2-5');
                }

            });
        };

        
    }]);
    /*
     *    UsersListPageCtrl
    */
    
    angular.module('training.controllers').controller('obs-UsersListPageCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {
        
        // Broadcasting test source controller  TOP CONTROLLER
        //
        //  added to UserListCtrl (top) , UserCreateCtrl (Middle),  UserAdminProfileCtrl  (Bottom ) 
        // $scope.TopController($scope, DemoService) {
        //     $scope.topValue = 0;
        //     $scope.middleValue = 0;
        //     $scope.bottomValue = 0;

        //     $scope.$watch('topValue', function() {
        //         DemoService.updateTopValue($scope.topValue);
        //     });

        //     $scope.$on('valuesUpdated', function() {
        //         $scope.middleValue = DemoService.middleValue;
        //         $scope.bottomValue = DemoService.bottomValue;
        //     });
        // };
        
        $scope.topValue = 0;
        $scope.middleValue = 0;
        $scope.bottomValue = 0;

        $scope.$watch('topValue', function() {
                userService.updateTopValue($scope.topValue);
        });

        $scope.$on('valuesUpdated', function() {
                $scope.middleValue = userService.middleValue;
                $scope.bottomValue = userService.bottomValue;
        });
         // end of Broadcasting test

        // local User pagination settings

        //$scope.usersEditItem = [];
        $scope.itemsPerPage = 25; 
        $scope.currentPage = 1;
        $scope.pageSize =  25;  // this should match however many users you want on one page
        $scope.fetchBy = {
            'fetchLastName' : '',
            'fetchFirstName' : '',
            'fetchCity'  : ''

        };
        // asynchronous User pagination settings
        $scope.usersTotalPossible = 0;
        //$scope.othersPerPage = 25; // this should match however many results your API puts on one page
        //pageChangeHandler(1);
        $scope.pagination = {
           current: 1
        };

        $scope.getSelectedUser = function( ndx ) { 

            console.log("DEBUG: UsersListPageCtrl.getSelectedUser : usersEditItem  ",  ndx ); //, $scope.usersEditItem
           
            var tmp = {};
            var tmp = $scope.usersList[ndx];
            userService.usersEditItem = tmp;
            //$scope.usersEditItem = tmp;
            // userService.usersEditItem = tmp;
            // $scope.connections.push( results );
            $scope.usersEditItem = tmp ;
             //$scope.apply();
            console.log("DEBUG: UsersListPageCtrl.getSelectedUser : usersEditItem  ", $scope.usersEditItem );
            
            // if (!$scope.usersEditItem) {

            // // 20140917 $location.path('/users');
            //  //$scope.usersEditItem.push( $scope.usersList[ndx]);
            //  console.log("INFO: userController.UsersEditCtrl : !$scope -> /home");    
            //   $location.path('/#TAB3-5');
            // }
            // else {

            //     console.log("DEBUG: UsersListPageCtrl.getSelectedUser : usersEditItem  ", $scope.usersEditItem );
            //     console.log("INFO: userController.UsersEditCtrl : !$scope -> /tab3-5");    
              
            //    //$scope.usersEditItem.push( $scope.usersList[ndx]);
            //    $location.path('/#');
            // }    

        };   

        $scope.fetchPage = function() {
                console.log('DEBUG: UsersListPageCtrl.fetchPage: to nu page ' + $scope.currentPage);
                
                if ( $scope.fetchBy.fetchLastName != ""  ||  $scope.fetchBy.fetchFirstName != "" ) {
                     
                    console.log('DEBUG: UsersListPageCtrl.fetchPage: got fetch data:' + $scope.fetchBy);
                    var tmpFetchBy = {
                                        'fetchLastName' : '',
                                        'fetchFirstName' : '',
                                        'fetchCity'  : ''
                                    };  
                    // add wildcard search params                  
                    tmpFetchBy.fetchLastName = $scope.fetchBy.fetchLastName + ".*";
                    tmpFetchBy.fetchFirstName = $scope.fetchBy.fetchFirstName + ".*"; 
                    console.log('DEBUG: UserService.pageChanged: got tmp  data:', tmpFetchBy);
                    userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize , tmpFetchBy,  function(err, nxPageResults) {
                        if(err) {

                            console.log("ERROR: userNuConnect error");

                            $scope.message = "Pagination error - userNuConnect";

                        } else {

                            $scope.usersList = nxPageResults["data"];
                            //userService.usersList = nxPageResults["data"];

                            console.log("DEBUG: new usersList page contents loaded: ");
                            console.log( $scope.usersList );
                            //userService.others =  nxPageResults;  
                            console.log("FIX ERROR: UsersListPageCtrl fetchPage list length error *************** FIX");

                            $scope.usersTotalPossible =  $scope.usersList.length;   
                           
                        }
                    });
                };         
        };

        $scope.fetchConnection = function() {
                console.log('getting more others: currentPage: ' + $scope.currentPage );
                console.log('getting more others: fetch: ' + $scope.fetchBy );
                if ( $scope.fetchBy.fetchLastName !== ""  ||  $scope.fetchBy.fetchFirstName !== "" ) {

                    userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize, $scope.fetchBy ,  function(err, nxPageResults) {
                        if(err) {

                            console.log("ERROR: userNuConnect error");

                            $scope.message = "Pagination error - userNuConnect";

                        } else {

                            console.log("INFO: new page loaded: " +  $scope.currentPage   );
                            // var totCount =   nxPageResults.length - 4850;
                            // var nxPage = 
                            // for(var i = 0; i < totCount  ; i++) {
                
                            $scope.others = nxPageResults["data"];

                            // userService.others =  nxPageResults;  
                            $scope.othersTotalPossible =  $scope.others.length;   
                           
                        }

                    });    
                };     
        };

        $scope.addConnection = function (index) {
            console.log("DEBUG: making connection from root user");
            console.log( $scope.user._id);
            console.log("DEBUG: making connection to other user");
            console.log($scope.others[index]._id);

            var rootUser = {'Id': $scope.user._id,
                            'idType': 'objectId' };

            var otherUser = {'otherId': $scope.others[index]._id,
                              'idType': 'objectId' };

            var relationShip = { 'relationType' : 'FOLLOWS',
          
                                 'relationData' : { 'otherKeyId' : $scope.others[index].keyId } }; 
                              
            userService.connectUser( rootUser, otherUser , relationShip, function(err, results ) {
                
                if(err) {

                    console.log("ERROR: making connection to other user");
                    console.log( results );
                    console.log( Object.keys(results).length )
                }
                else if ( [results].length ===  1 ) {

                    console.log("DEBUG: made connection to other user: results: ", results );

                    $scope.connections.push( results );
                    
                    $location.path('/users/profile/' + rootUser.Id + '#tab2-5' );
                }
                else {
                    console.log("INFO: made connection to other users");
                    $location.path('/users/profile/' + rootUser.Id + '#tab2-5');
                }

            });
        };
       
       
        
    }]);
    /*
    * userAlchemy page contropller
    */
    angular.module('training.controllers').controller('obs-UsersAlchemyPageCtrl', ['$scope', 'training.services.user' ,function($scope, userService ) {
            
            var config = {
                          dataSource: {
                            "nodes": [
                              {
                                "id": 1
                              },
                              {
                                "id": 2
                              },
                              {
                                "id": 3
                              }
                            ],
                            "edges": [
                              {
                                "source": 1,
                                "target": 2
                              },
                              {
                                "source": 1,
                                "target": 3,
                              }
                            ]},
                          graphHeight: function(){ return 400; },
                          graphWidth: function(){ return 400; },
                          
                          linkDistance: function(){ return 40; },

                          nodeTypes: {"node_type":[ "Maintainer",
                                                    "Contributor"]},
                          nodeCaption: function(node){ 
                            return node.caption + " " + "hello" }
                          }; 
            $scope.start = function(){
                 
                // alchemy.begin(config);
                // "dataSource": some_data
                console.log('UsersAlchemyPageCtrl: begin: ' );  //,  config.dataSource.nodes
            };

            
            $scope.fetchConnection = function() {
                    console.log('getting more others: currentPage: ' + $scope.currentPage );
                    console.log('getting more others: fetch: ' + $scope.fetchBy );
                    if ( $scope.fetchBy.fetchLastName !== ""  ||  $scope.fetchBy.fetchFirstName !== "" ) {

                        userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize, $scope.fetchBy ,  function(err, nxPageResults) {
                            if(err) {

                                console.log("ERROR: userNuConnect error");

                                $scope.message = "Pagination error - userNuConnect";

                            } else {

                                console.log("INFO: new page loaded: " +  $scope.currentPage   );
                                // var totCount =   nxPageResults.length - 4850;
                                // var nxPage = 
                                // for(var i = 0; i < totCount  ; i++) {
                    
                                $scope.others = nxPageResults["data"];

                                // userService.others =  nxPageResults;  
                                $scope.othersTotalPossible =  $scope.others.length;   
                               
                            }

                        });    
                    };     
            };
            
    }]);
   
    /** USER ADMINISTRATION PAGE TABS SECTION  add new user
     * User Create Ctrl ****************
     * 
     */
    angular.module('training.controllers').controller('obs-UserCreateCtrl', ['$scope', '$filter', 'training.services.user', 'training.services.component', function($scope, $filter, userService, componentService) {
        // broadcasting test source  MIDDLE Controller
        //  also added to UserListCtrl (top) , UserCreateCtrl (Middle),  UserAdminProfileCtrl  (Bottom ) 
       
        $scope.topValue = 0;
        $scope.middleValue = 0;
        $scope.bottomValue = 0;

        $scope.$watch('middleValue', function() {
            userService.updateMiddleValue($scope.middleValue);
        });

        $scope.$on('valuesUpdated', function() {
            $scope.topValue = userService.topValue;
            $scope.bottomValue = userService.bottomValue;
        });
        // end of Broadcasting test     
       $scope.addNuUser = {
         nuuser: {},
         nucommponent: {}}; 
       //$scope.nucomponent = {};
        // $scope.user = {
        //     '_id' : '',        // system defined
        //     'keyId' : '',      // system defined
        //     'loginId'  : '',   // user denfined
        //     'firstName' : '',  //* req - user defined
        //     'lastName' : '',   //* req - user defined
        //     'password' : '',   //* req - user defined
        //     'passwordPhrase': '', //* req - user defined
        //     'street'   :'',
        //     'street2'  : '',
        //     'city': '',
        //     'state': '',
        //     'zip': '',
        //     'Phone':'',
        //     'Mobile':'',
        //     'Fax':'',
        //     'Role': '',  //* req  'admin', 'manager', 'user', ...
        //     'Type': '',  //* req system defined - 'root', admin', 'member', 'subscriber', 'follower'
        //     'Permissons':'',  //* req  admin defined -  'modify, publish, delete, create, .... 
        //     'Preferences':'', //* req user set - system defined list 
        //     'mbrName':'',
        //     'mbrStreet':'',
        //     'mbrStreet2':'',
        //     'mbrCity':'',
        //     'mbrSate':'',
        //     'mbrPhone':'',
        //     'mbrFax': '',
        //     'mbrWebSite': '',
        //     'mbrIdentity': '',
        //     'mbrContacts':'',
        //     'mbrCategory': '',
        //     'mbrApproveDate':'',
        //     'mbrKeyWords':''
        //};

        
        //MASTER trp logic *************************
        // NOTE: convert to  userService.trpSettings  
        var orgCategory = [{"orgtypes": {"Accountants/Bookkeepers":"","Advertising & Marketing":"","Antique Lumber/Flooring":""
                            ,"Antiques & Gifts":"","Architect/Home Design":"","Architects/Home Design":""
                            ,"Art Galleries":"","Art Galleries/Framing":"","Attorneys":""
                            ,"Automobile Sales/Service":"","Balsam/Cullowhee Accommodations":""
                            ,"Beauty Salons/Nails":"","Bedding/Furniture":"","Blasting/Drilling":""
                            ,"Blinds/Shutters":"","Boat Charters & Tours":"","Boat Rentals/Sales":""
                            ,"Builders Supply":"","Building Contractor":""
                            ,"Cable TV/Internet":"","Campgrounds/RV Resorts":"","Camps":""
                            ,"Carpet Cleaners":"","Cashiers Accommodations":"","Chambers of Commerce":""
                            ,"Chiropractic/Accupuncture":"","Christmas Tree Farms":"","Cleaning Services":""
                            ,"Clothing & Accessories":"","Coffee House/Expresso Bar":"","Colleges":""
                            ,"Computer/Internet":"","Consignment Shops":"","Contract Law Enforcement & Security Services":""
                            ,"Custom Home Builders":"","Dentists":"","Developers":""
                            ,"Disc Jockey/Entertainment":"","Dry Cleaners":"","Electrical Contractors":""
                            ,"Electronics":"","Environmental Services/Engineering":"","Event Coordinating":""
                            ,"Facials & Waxing":"","Financial Institutions":"","Financial Planning/Investments":""
                            ,"Floor Coverings":"","Florists/Plants/Gifts":"","Furniture & Home Décor":""
                            ,"Gasoline":"","Gift Shops/Crafts/ Home Décor":"","Gift Shops/Crafts/Home":""
                            ,"Gem Mining":"","Golf Courses - Public":"","Golf/Tennis Private Clubs":""
                            ,"Grading & Excavating":"","Green Energy":"","Groceries/Fresh Vegetables":""
                            ,"Hardware/Housewares/Gifts":"","Highlands Accommodations":"","Home Inspections":""
                            ,"Home Repairs & Maintenance":"","Home Theater":"","Horseback Riding":""
                            ,"Hospitals":"","Insurance/Home Inventory & Valuables":"","Interior Design/Furniture":""
                            ,"Kitchen & Bath/Cabinets":"","Land Surveying":"","Land Trust/Conservation":""
                            ,"Lanscaping/Planning & Design":"","Lawn Maintenance":""
                            ,"Lighting":"","Log Homes":"","Magazines":""
                            ,"Masonry":"","Massage":"","Mortgage Lending":""
                            ,"Newspapers":"","Non Profit Organizations":"","Nusery/Plants":""
                            ,"Office Supplies":"","Outdoor Recreation":"","Outdoor Specialty Stores":""
                            ,"Painting & Pressure Washing":"","Pest Control":"","Pharmacy":""
                            ,"Photography":"","Physicians":"","Preschools":""
                            ,"Printing":"","Propane Gas":"","Property Management/POA":""
                            ,"Publishing":"","Radio Stations":"","Real Estate Appraisal":""
                            ,"Real Estate Sales/Rentals":"","Restaurants":"","Retirement Communities":""
                            ,"Roofing Repair/Sealants":"","Rugs/Oriental Rugs":"","Sapphire/Lake Toxaway Accommodations":""
                            ,"Shipping Services":"","Signs/Woodworking":"","Skeet Range/Field Club":""
                            ,"Spas":"","Stone Hauling":"","Storage Facilities":""
                            ,"Tent Rentals":"","Tourist Attraction":"","Toys & Gifts":""
                            ,"Transportation/Shuttle Service/Concierge":"","Trash Services/Bear Proof Containers":"","Tree Care":""
                            ,"Tree Service":"","Utilities":"","Video Production":"","Web Design":""
                           }}]; 
        
        $scope.upricategory = "";
        $scope.up2ndcategory = "";
        $scope.up3rdcategory = "";
        $scope.selOrgCategory = Object.keys(orgCategory[0]["orgtypes"]);
        
        $scope.setUMChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'pri':
                     $scope.upricategory = $scope.selOrgCategory[choice];
                    break;
                case '2nd':
                   $scope.up2ndcategory =  $scope.selOrgCategory[choice];
                    break;
                case '3rd':
                    $scope.up3rdcategory = $scope.selOrgCategory[choice];
                    break;    
                default:
                    console.log("ERROR: UsersCreateCtrl setUSChoices  *************************", choice );    
            }
            return 
        };

        
        var trps = [{"types": { Root: "Root", Sysadmin: "Sysadmin", Member: "Member", Associate: "Associate", Subscriber: "Subscriber", Advertiser:"Advertiser"}},
                    {"roles": { Owner: "Owner", Manager: "Manager",  Staff: "Staff", Volunteer: "Volunteer" }},
                    {"permissions": { Publisher: false, Editor: false, Support: true  }}];
                    
        $scope.utype = null;
        $scope.urole = null;
        $scope.uperm = null;
        
        $scope.selTypes = Object.keys(trps[0]['types']);
        $scope.selRoles = Object.keys(trps[1]['roles']);
        $scope.selPermissions = Object.keys(trps[2]['permissions']);
        
        

        $scope.setUSChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'urole':
                     $scope.urole = $scope.selRoles[choice];
                    break;
                case 'utype':
                    $scope.utype =  $scope.selTypes[choice];
                    break;
                case 'uperm':
                    $scope.uperm = $scope.selPermissions[choice];
                    break;    
                default:
                    console.log("ERROR: UsersCreateCtrl setUSChoices  *************************", choice );    
            }
            return 
        }; 

        $scope.status = {
              isopen: false
        };

        $scope.toggled = function(open) {
            //$log.log('Dropdown is now: ', open);
            console.log("DEBUG: UsersCreateCtrl setUtype Dropdown is now: ", open);
        };

        $scope.toggleDropdown = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.status.isopen = !$scope.status.isopen;
        };

        // Wrapup logic ***

        $scope.save = function(addNuUser) {
            
                $scope.savedUser = angular.copy(addNuUser.nuuser);
                $scope.savedComponent = angular.copy(addNuUser.nucomponent);

                console.log("DEBUG: adduser save data  ********** : ",  $scope.savedComponent ,$scope.savedUser );

                $scope.savedUser.labelId = $scope.utype;
                $scope.savedUser.role = $scope.urole;
                $scope.savedUser.permissions = $scope.uperm;
                // obs - moved to component create when ready
                // $scope.savedUser.orgPriCategory = $scope.upricategory;
                //$scope.savedUser.orgSecCategory =  $scope.up2ndcategory;
                //$scope.savedUser.orgThrdCategory = $scope.up3rdcategory;
                
                
                // // do basic crud
                // console.log("DEBUG: saved nuuser scope *** if no error *********************************", $scope );

                userService.createUser( $scope.savedUser ,  function(err, status) {
                    // note: 20150225: status is an obj containing status, _id and keyId data
                    if(err) {

                         console.log("ERROR: UsersCreateCtrl create (save) *************************", err );
                         $scope.message = "User Create error - add User";

                    } else {
                        var retdIds ={};
                        retdIds.status = status.msg; 
                        retdIds._id = status._id;
                        retdIds.keyId = status.keyId;     
                        console.log("INFO: new user created: ",  retdIds.status );
                        console.log("DEBUG: new component data  ********** : ",  $scope.savedComponent );
                        console.log("DEBUG: new component data  scope.utype ** : ",  $scope.utype );

                        // // now create component object (mbrsite, subscribers, etc)
                        // first setup basic 
                        //var savedComponent = {};
                        //savedComponent = nucomponent;  // angular.copy(nucomponent);
                        $scope.savedComponent.labelId = $scope.utype;
                        $scope.savedComponent.userId = retdIds._id;
                        $scope.savedComponent.userKeyId = retdIds.keyId;
                        $scope.savedComponent.dateCreated =  new Date();
                        $scope.savedComponent.lastUpdated =  new Date();
                        $scope.savedComponent.updatedById = retdIds.keyId; 
                        $scope.savedComponent.isActive = false;            
                        $scope.savedComponent.approveDate = '';
                        
                        //$scope.savedComponent.category = [];
                        //var category = ["firstCategory","secondCategory","thirdCategory"];

                        // $scope.savedComponent.category = ["firstCategory","secondCategory","thirdCategory"];
                        // $scope.savedComponent.category["firstCategory"] =  '"' + $scope.upricategory + '"' ;
                        // $scope.savedComponent.category["secondCategory"] =  '"' + $scope.up2ndcategory + '"' ;  
                        // $scope.savedComponent.category["thirdCategory"] =  '"' + $scope.up3rdcategory + '"' ;
                        
                        //$scope.savedComponent.category.firstAlt = $scope.upricategory;
                        //$scope.savedComponent.category.secondAlt =  $scope.up2ndcategory;
                        //$scope.savedComponent.category.thirdAlt = $scope.up3rdcategory;
                
                        //$scope.savedComponent.category = {};   // ["firstCategory","secondCategory","thirdCategory"];
                        $scope.savedComponent.category = [ $scope.upricategory , $scope.up2ndcategory, $scope.up3rdcategory ];
                        
                        componentService.createComponent(  $scope.savedComponent ,  function(err, status) {
                             // note: 20150225: status is an obj containing status, _id and keyId data
                            if(err) {

                                 console.log("ERROR: ComponentCreateCtrl create (save) *************************", err );

                                 $scope.message = "Component Create error - add new User/component";

                            } else {
                                var retdIds ={};
                                retdIds.status = status.msg; 
                                // retdIds._id = status._id;
                                // retdIds.keyId = status.keyId;     
                                console.log("INFO: new user/component created: ",  retdIds.status );

                                
                                
                            }
                            // here because variables were cleared too quickly at original location
                            $scope.clear();
                        }); 
                        
                    }

                });   
                

                // original location 
                // $scope.clear();
                
        };

        $scope.clear = function() {
                 $scope.utype = null;
                 $scope.urole = null;
                 $scope.uperm = null; 
                 $scope.upricategory = null;
                 $scope.addNuUser.nuuser = null;
                 $scope.addNuUser.nucomponent = null;
                 //$scope.savedComponent = null; 
                 //$scope.savedUser = null;
                 $scope.$apply;   
        };
        
    }]);

    /*
     * User login Ctrl
     */
    angular.module('training.controllers').controller('obs-UsersLoginCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location,    userService) {

        $scope.user = {

            'loginId' : '',
            'password' : ''
        };

        $scope.message = "";

        $scope.login = function() {

            $scope.message = "";

            userService.loginUser($scope.user, function(err, status) {

                if(err) {

                    console.log("ERROR: login user");

                    $scope.message = "Authentication Error";
                }
                else {
                       
                    var tmpUser = userService.getUser();   

                    console.log("INFO: Logged In User status : " + tmpUser._id );  //+ '#tab1-5'
                    $location.path('/users/adminhome');
                    //$location.path('/home');
                }
            });
        };
    }]);

    
    
    /*
     * Main Controller
     */
    angular.module('training.controllers').controller('obs-MainCtrl', ['$scope', 'training.services.user', function($scope, userService) {
        //$scope.menus = "";
        userService.init();
        
    }]);
}());