(function() {

    'use strict';

    /* *****************************
       *****************************
       *****************************
     *  MBRSITE ADMIN CONSOLE  Ctrl
    */
    angular.module('training.controllers').controller('MbrsiteAdminConsoleCtrl', ['$scope', '$location', 'training.services.user', 'training.services.component', function($scope, $location, userService, componentService ) {
            

        /*** Component PROFILE SECTION 
         *
         *  Admin Profile Ctrl ************************************
         * 
         *   the component edit ctrl collects remote component data and sets different content in $scope.user as determined by userId
        */

        $scope.profileItem = {};
        $scope.contentSets = {};
        $scope.relationShips = {};

      
       
        $scope.OBS_whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            
            }         
        };

        $scope.OBS_whichUserId = function() {
            var user = userService.getUser();
            if(user) {
            return user._id;
            }         
        };
        
        /* TEST DATA remove when obs */  
        var jbs =  { 100: { 
                    setId: "#tab0-1",
                    setHref:"#/mbrsites/mbrsiteconsole/",
                    setKey: 1,
                    setOrder:1,
                    setName:"My Homes",
                    setLabelText:"My Home",
                    setTemplate: "my-home",
                    status:"active"},
                    200: {
                    setId: "#tab1-1",
                    setHref:"#/mbrsites/mbrsiteconsole/",
                    setKey: 1,
                    setOrder:1,
                    setName:"Blank 1",
                    setLabelText:"Blank 1",
                    setTemplate: "blank-1",
                    status:""}};
        
        // ****************************************************
        //{"setName":"My Sheds","profileKeyId":"4cfdb890-f41e-11e4-8595-0bc68f35e86a","approveDate":"2015-05-17T00:20:33.413Z","setHref":"/mbrsites/mbrsiteconsole/","groupLabel":"Components","keyId":"4cfdb892-f41e-11e4-8595-0bc68f35e86a","updatedById":"4c99edb0-f41e-11e4-8595-0bc68f35e86a","isActive":true,"setOrder":1,"lastUpdated":"2015-05-17T00:20:33.413Z","contentKeyId":"4cfdb891-f41e-11e4-8595-0bc68f35e86a","setLabelText":"My Shed","setId":"#tab0-1","typeLabel":"Mbrsite","setKey":"101","status":"Active","_id":28}
        // *************************************************
        $scope.$watch('relationShips', function( nuValue, oldValue ) {
   
            // create loop to build proper object sets ( see jbs above )
            
            $scope.relShips = {}; // init, new Object("setZero");
            $scope.relShips = componentService.getLocalComponentPart( 'relationShips', 'all');
            //$scope.relShips = $scope.relationShips;
            console.log("WATCH: MbrsiteAdminConsoleCtrl- relationShips :", nuValue, oldValue  );
            
            // var rLen = $scope.relationShips.length;

            // if (rLen) {
            //     for (var i = 0; i < rLen; i++) {
            //      $scope.relShips[i] = $scope.relationShips[i]; 
            //     }
            // }
        });


        $scope.$watch('contentSets', function( nuValue, oldValue ) {
   
            // create loop to build proper object sets ( see jbs above )
            
            $scope.eleSets = {}; // init, new Object("setZero");
            $scope.eleSets = componentService.getLocalComponentPart( 'contentSets', 'all');
            //$scope.relShips = $scope.relationShips;
            console.log("WATCH: MbrsiteAdminConsoleCtrl- contentSets :", nuValue, oldValue  );
            
            // var rLen = $scope.relationShips.length;

            // if (rLen) {
            //     for (var i = 0; i < rLen; i++) {
            //      $scope.relShips[i] = $scope.relationShips[i]; 
            //     }
            // }
        });


        $scope.upricategory = ""; 
        $scope.up2ndcategory = "";
        $scope.up3rdcategory = "";
        //$scope.selOrgCategory = componentService.getBizCategories();
        
        //console.log("DEBUG: MbrsiteAdminProfileCtrl selOrgCategory : ",  componentService.getBizCategories() );
        
        $scope.setUMChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'pri':
                     $scope.upricategory = $scope.selOrgCategory[choice];
                    break;
                case '2nd':
                   $scope.up2ndcategory =  $scope.selOrgCategory[choice];
                    break;
                case '3rd':
                    $scope.up3rdcategory = $scope.selOrgCategory[choice];
                    break;    
                default:
                    console.log("ERROR: MbrsiteAdminProfileCtrl setUSChoices  *************************", choice );    
            }
            return 
        };


     
        $scope.cancelProfile = function () {
            
            console.log("INFO: MbrsiteAdminConsoleCtrl-canceledProfile update");
            
            //$location.path('/users/profile/' + userId );
            
                
        };

        $scope.initMbrsiteConsole = function ( part  ) {

          console.log("DEBUG: MbrsiteAdminConsoleCtrl-initMbrsite pre getUser :", user );
          
          var user = userService.getUser();
          
          console.log("DEBUG: MbrsiteAdminConsoleCtrl-initMbrsite post getUser:", user );

          if(user && user.authToken) {
            
            /* 
            var cLabels = ['Components','Mbrsite',part];     // part: Profile,Tabset or Panes 
                            
            componentService.getMbrsite( user.keyId, cLabels , function(err, mbrsite) {
            */
            var cLabels = ['Components','Mbrsite',part];     // part: Profile,Tabset or Panes 
            
            var parms = { where: {
                            profile: {
                                userKeyId : user.keyId,
                                name : '',
                                pId : '', 
                                isActive : true,
                                status : '',
                                permission : ''
                                },  
                            relationShip: {
                                isActive : true,
                                status : ''
                                }
                            },      
                          cLabels: cLabels
                        };
                          
                         
            console.log("DEBUG: MbrsiteAdminConsoleCtrl-initMbrsite parms *********** : ", parms );

            componentService.getMbrsite( parms, function(err, nuMbrsite) {
                if(err) {

                    console.log("ERROR: initMbrsite ");
                }
                else {

                    console.log("ERROR: MbrsiteAdminConsoleCtrl-initMbrsite nuMbrsite:*************************: ", nuMbrsite ); 
                    
                    $scope.profileItem = componentService.getLocalComponentPart('profile', '');  // nuMbrsite.profile; // componentService.getLocalComponentPart('profile', ''); //nuMbrsite.profile;  
                    $scope.contentSets = componentService.getLocalComponentPart('contentSets', 'all');  //nuMbrsite.contentSets; //componentService.getLocalComponentPart('contentSets', 'all');//nuMbrsite.contentSets;  
                    $scope.relationShips = componentService.getLocalComponentPart('relationShips', 'all'); //nuMbrsite.relationShips;
                   
                    console.log("DEBUG: MbrsiteAdminConsoleCtrl-getLocalComponentPart profile,contentSets via Service ************** ", $scope.relationShips );
                }
           
            });
            
          }
          else {
            console.log("DEBUG: MbrsiteAdminConsoleCtrl.initMbrsite not authenticated ** :", user.authToken );
            $location.path('/home');
          }

        };

    
        $scope.items = ['Sysadmin', 'Member', 'Subscriber', 'Associate', 'Support'];

               
        //$scope.localSiteContentSet =  componentService.getLocalComponentPart('contentsets', 0 );
        $scope.isAuthorized = 'support'       //  userService.getUserPermissions(); //  $scope.items[0];  //
      
        $scope.isOK = userService.getUserPermissions();
     
    }]);
    




    /*  **********************************
        **********************************
        **********************************
         MBRSITE PROFILE MANAGER Ctrl
    */  
    angular.module('training.controllers').controller('MbrsiteAdminProfileManagerCtrl', ['$scope', 'training.services.user', 'training.services.component', function($scope, userService, componentService ) {
        
        $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            
            }         
        };

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) { $scope.$watch('profileItem', function( nuValue, oldValue ) {
   
            console.log("WATCH: MbrsiteAdminConsoleCtrl- profileItem :", nuValue, oldValue );
                
        });


                return user._id;
            }         
        };
        
        console.log("DEBUG: MbrsiteAdminProfileManagerCtrl ************** " );

      
        $scope.$watch('profileItem', function( nuValue, oldValue ) {
   
            console.log("WATCH: MbrsiteAdminProfileManagerCtrl- profileItem :", nuValue, oldValue );
                
            $scope.profileItem =  componentService.getLocalComponentPart('profile', ''); 
            $scope.upricategory = $scope.profileItem.category[0]; 
            $scope.up2ndcategory = $scope.profileItem.category[1];
            $scope.up3rdcategory = $scope.profileItem.category[2];                  //mbrsite.profile
            $scope.selOrgCategory = componentService.getProfileCategories();


        });

        $scope.setUMChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'pri':
                     $scope.upricategory = $scope.selOrgCategory[choice];
                    break;
                case '2nd':
                   $scope.up2ndcategory =  $scope.selOrgCategory[choice];
                    break;
                case '3rd':
                    $scope.up3rdcategory = $scope.selOrgCategory[choice];
                    break;    
                default:
                    console.log("ERROR: MbrsiteAdminProfileCtrl setUSChoices  *************************", choice );    
            }
            return 
        };


       $scope.saveProfile = function() {

            console.log("DEBUG: MbrsiteAdminProfileCtrl : ***** pre save *******", $scope.profileItem );

            
            $scope.profileItem.lastUpdated =  new Date();
            $scope.profileItem.updatedById =  userService.user.keyId; 
            $scope.profileItem.isActive = false;            
            $scope.profileItem.approveDate = new Date() ;
            $scope.profileItem.status  = 'Active';    // ** Required, set at system level-relates to validity of   
            $scope.profileItem.isActive = true;   //  ** Required, set false at creation-relates to state of

            $scope.profileItem.category[0] = $scope.upricategory; 
            $scope.profileItem.category[1] = $scope.up2ndcategory;
            $scope.profileItem.category[2] = $scope.up3rdcategory;


            var cLabels = ['Components','Mbrsite','Profile'];     // part: Profile,Tabset or Panes 
            
            // NOTE: we may want to form cypher query here??
            var parms = {where: {
                            profile: {
                                keyId : $scope.profileItem.keyId,
                                name : '',
                                pId : '', 
                                isActive : true,
                                status : '',
                                permission : ''
                                },  
                            relationShip: {
                                keyId: '',
                                isActive : true,
                                status : ''
                                },  
                            contentSet: {
                                keyId: ''  
                                }
                         },
                         set: 'profile',
                         cLabels: cLabels 
                        };

            componentService.saveComponent($scope.profileItem, parms, function(err, status) {
               
                if(err) {

                    console.log("ERROR: MbrsiteAdminProfileManagerCtrlcomponent-saveComponent not updated *****: ", err);
                }
                else {

                    console.log("DEBUG: MbrsiteAdminProfileManagerCtrlcomponent-saveComponent updated with status: ", status );
                    //$scope.usersedititem = '';
                    //$location.path('/#/users/profil/:#tab3-5');
                    //$scope.$apply(); 
                       
                      // commented $scope.usersEditItem = {};
                   // commented  });
                }

            });
        };

        $scope.cancelProfile = function () {
            
            console.log("INFO: MbrsiteAdminProfileManagerCtrl-canceled profile update");
            
            //$location.path('/users/profile/' + userId );
            
                
        };

        $scope.getMbrsiteProfile = function ( part  ) {

          console.log("DEBUG: MbrsiteAdminProfileManagerCtrl-initMbrsite pre getUser :", user );
          
          var user = userService.getUser();
          
          console.log("DEBUG: MbrsiteAdminProfileManagerCtrl-initMbrsite post getUser:", user );

          if(user && user.authToken) {
            
            var cLabels = ['Components','Mbrsite',part];   // part: Profile,Tabset or Panes 
            //
            // NOTE: we may want to only retrieve PROFILE here??
            //
            //          call to getMbrsite using below parms will bring back all associated mbrsite relationShips
            //          and ContentSets. May need to update parms to be a bit more selective


            var parms = { where: {
                            profile: {
                                userKeyId : user.keyId,     
                                name : '',
                                pId : '', 
                                isActive : true,
                                status : '',
                                permission : ''
                                },  
                            relationShip: {
                                isActive : true,
                                status : ''
                                }
                            },      
                          cLabels: cLabels
                        };
                          
                         
            console.log("DEBUG: MbrsiteAdminProfileManagerCtrl-initMbrsite parms *********** : ", parms );

            componentService.getMbrsite( parms, function(err, nuProfile) {
                if(err) {

                    console.log("ERROR: initMbrsite ");
                }
                else {

                    console.log("ERROR: MbrsiteAdminProfileManagerCtrl-initMbrsiteProfile nuProfile:*************************: ", nuProfile ); 

                    $scope.profileItem = componentService.getLocalComponentPart('profile', '');  // nuMbrsite.profile; // componentService.getLocalComponentPart('profile', ''); //nuMbrsite.profile;  

                    console.log("DEBUG: MbrsiteAdminProfileManagerCtrl-getLocalComponentPart profile,contentSets via Service ************** ", $scope.profileItem );
                }
           
            });
            
          }
          else {
            console.log("DEBUG: MbrsiteAdminProfileManagerCtrl.initMbrsiteProfile not authenticated ** :", user.authToken );
            $location.path('/home');
          }

        };

    
    }]);




    /*  **********************************
        **********************************
        **********************************

         MBRSITE RELATIONSHIP MANAGER Ctrl
    */  
    
    angular.module('training.controllers').controller('MbrsiteAdminRelationShipManagerCtrl', ['$scope', 'training.services.user', 'training.services.component', function($scope, userService, componentService ) {
    

        console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl ************** " );
      
        $scope.isAuthorized =   userService.getUserPermissions(); 

        $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            
            }         
        };

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) { $scope.$watch('profileItem', function( nuValue, oldValue ) {
   
            console.log("WATCH: MbrsiteAdminRelationShipManagerCtrl- profileItem :", nuValue, oldValue );
                
        });


                return user._id;
            }         
        };
        
        console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl ************** " );

      
        $scope.$watch('relationShips', function( nuValue, oldValue ) {
   
            console.log("WATCH: MbrsiteAdminRelationShipManagerCtrl- relationShips :", nuValue, oldValue );
                
            $scope.relShips =  componentService.getLocalComponentPart('relationShips', 'all'); 
       
           try {
            var publish = componentService.getPublish(); 
          
            $scope.selPubOptions = Object.keys(publish[0]['publish']); 
        
            $scope.pub = $scope.relShips.status;   // ???????????  
            
            } catch (err) {
               //null
            }

        });
 
       
        
        $scope.setPUBChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'pub':
                     $scope.pub = $scope.selPubOptions[choice];
                     // update db       
                     $scope.updateThis( choice , 'status');
                    break;
                // case '2nd':
                //    $scope.up2ndcategory =  $scope.selOrgCategory[choice];
                //     break;
                // case '3rd':
                //     $scope.up3rdcategory = $scope.selOrgCategory[choice];
                //     break;    
                default:
                    break;  
                    console.log("ERROR: MbrsiteAdminRelationShipManagerCtrl setUSChoices  *************************", choice );
                      
 
            }

            return 
            console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl setUSChoices  *************************", $scope.pub );
        };

        $scope.updateThis = function(  data, prompt ){
            /* NOTE:
              Because relationsShips and contentSets data is managed with the use of arrays the [] seen
              below are just temp until a better model is formated.

              The code on the next line takes advantage of javasrcipt's everything is executable nature.
              What is happening is the code enclosed in () is interpted as a function and the function is initializing a var as an object,
              followed by an assigment of some data to that obj, which, is followed by the return of the obj.
            */  
            // var nuData = ( nuData={}, nuData[ prompt ] = [data] , nuData );     // Object();
            var nuData = ( nuData={}, nuData[ prompt ] = data , nuData );     // Object();
            nuData['lastUpdated'] =  new Date();
            nuData['updatedById'] =  userService.user.keyId; 
            nuData['approveDate'] = new Date() ;
            nuData['status']  = 'Active';    // ** Required, set at system level-relates to validity of   
            nuData['isActive'] = true;  
              
            var cLabels = ['Components','Mbrsite','ContentSet'];     // part: Profile,Tabset or Panes 
            
            var parms = { where: {
                           profile: {
                            keyId : '',
                            name : '',
                            pId : '', 
                            isActive : true,
                            status : '',
                            permission : ''
                            },  
                           relationShip: {
                            keyId: $scope.relShips[0].keyId,
                            isActive : true,
                            status : ''
                            },  
                           contentSet: {
                            keyId: ''  
                            }
                           },
                         set: 'relationShips',
                         cLabels: cLabels 
                        };
                          
               quickSave( nuData , parms, function ( err, status ) {
               
                    if(err) {

                       console.log("ERROR: MbrsiteAdminRelationShipManagerCtrl-quickSave not updated *****: ", err);
                    }
                    else {
 
                       console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl-quickSave updated with status: ", status );
                   }

                });

       
        };



       $scope.OBS_saveProfile = function() {

            console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl : ***** pre save *******", $scope.profileItem );

            
            $scope.profileItem.lastUpdated =  new Date();
            $scope.profileItem.updatedById =  userService.user.keyId; 
            $scope.profileItem.isActive = false;            
            $scope.profileItem.approveDate = new Date() ;
            $scope.profileItem.status  = 'Active';    // ** Required, set at system level-relates to validity of   
            $scope.profileItem.isActive = true;   //  ** Required, set false at creation-relates to state of

            $scope.profileItem.category[0] = $scope.upricategory; 
            $scope.profileItem.category[1] = $scope.up2ndcategory;
            $scope.profileItem.category[2] = $scope.up3rdcategory;


            var cLabels = ['Components','Mbrsite','Profile'];     // part: Profile,Tabset or Panes 
            
            // NOTE: we may want to form cypher query here??
            var parms = {where: {
                            profile: {
                                keyId : $scope.profileItem.keyId,
                                name : '',
                                pId : '', 
                                isActive : true,
                                status : '',
                                permission : ''
                                },  
                            relationShip: {
                                keyId: '',
                                isActive : true,
                                status : ''
                                },  
                            contentSet: {
                                keyId: ''  
                                }
                         },
                         set: 'relationShip',
                         cLabels: cLabels 
                        };

            componentService.saveComponent($scope.profileItem, parms, function(err, status) {
               
                if(err) {

                    console.log("ERROR: MbrsiteAdminRelationShipManagerCtrl-saveComponent not updated *****: ", err);
                }
                else {

                    console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl-saveComponent updated with status: ", status );
                    //$scope.usersedititem = '';
                    //$location.path('/#/users/profil/:#tab3-5');
                    //$scope.$apply(); 
                       
                      // commented $scope.usersEditItem = {};
                   // commented  });
                }

            });
        };

        $scope.cancelProfile = function () {
            
            console.log("INFO: MbrsiteAdminRelationShipManagerCtrl-canceled profile update");
            
            //$location.path('/users/profile/' + userId );
            
                
        };

        $scope.getMbrsiteRelationShip = function ( part  ) {

          console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl-initMbrsite pre getUser :", user );
          
          var user = userService.getUser();
          
          console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl-initMbrsite post getUser:", user );

          if(user && user.authToken) {
            
            var cLabels = ['Components','Mbrsite',part];   // part: Profile,Tabset or Panes 
            //
            // NOTE: we may want to only retrieve PROFILE here??
            //
            //          call to getMbrsite using below parms will bring back all associated mbrsite relationShips
            //          and ContentSets. May need to update parms to be a bit more selective


            var parms = { where: {
                            profile: {
                                userKeyId : user.keyId,     
                                name : '',
                                pId : '', 
                                isActive : true,
                                status : '',
                                permission : ''
                                },  
                            relationShip: {
                                //isActive : true,
                                status : ''
                                }
                            },      
                          cLabels: cLabels
                        };
                          
                         
            console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl-initMbrsite parms *********** : ", parms );

            componentService.getMbrsite( parms, function(err, nuProfile) {
                if(err) {

                    console.log("ERROR: initMbrsite ");
                }
                else {

                    console.log("ERROR: MbrsiteAdminRelationShipManagerCtrl-initMbrsiteProfile nuProfile:*************************: ", nuProfile ); 

                    $scope.relationShips = componentService.getLocalComponentPart('relationShips', 'all');  // nuMbrsite.profile; // componentService.getLocalComponentPart('profile', ''); //nuMbrsite.profile;  

                    console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl-getLocalComponentPart profile,contentSets via Service ************** ", $scope.profileItem );
                }
           
            });
            
          }
          else {
            console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl.initMbrsiteProfile not authenticated ** :", user.authToken );
            $location.path('/home');
          }

        };

        /* NOT FINISHED */
        function quickSave ( what,  parms, callback ){

            // componentService.saveComponent( $scope.contentSet , parms, function(err, status) {
            componentService.saveComponent( what , parms, function(err, status) {               
                if(err) {

                    callback( true, status);
                }
                else {

                    callback( false, status);
                }

            });
       
        }



              
    }]);



    /*  **********************************
        **********************************
        **********************************

         MBRSITE RELATIONSHIP ITEM Ctrl
    */  
    angular.module('training.controllers').controller('MbrsiteAdminRelationShipItemCtrl', ['$scope', 'training.services.user', 'training.services.component', function($scope, userService, componentService ) {
    

        console.log("DEBUG: MbrsiteAdminRelationShipItemCtrl ************** " );
      
        $scope.isAuthorized =   userService.getUserPermissions(); 

        $scope.OBS_whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            
            }         
        };

        $scope.OBS_whichUserId = function() {
            var user = userService.getUser();
            if(user) { $scope.$watch('profileItem', function( nuValue, oldValue ) {
   
            console.log("WATCH: MbrsiteAdminRelationShipManagerCtrl- profileItem :", nuValue, oldValue );
                
        });


                return user._id;
            }         
        };
        
        console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl ************** " );

      
        $scope.$watch('relationShips', function( nuValue, oldValue ) {
   
            console.log("WATCH: MbrsiteAdminRelationShipManagerCtrl- relationShips :", nuValue, oldValue );
                
            $scope.relShips =  componentService.getLocalComponentPart('relationShips', 'all'); 
       
           try {
            var publish = componentService.getPublish(); 
          
            $scope.selPubOptions = Object.keys(publish[0]['publish']); 
        
            $scope.pub = $scope.relItem.status;   // ???????????  
            
            } catch (err) {
               //null
            }

        });
 
       
        
        $scope.setPUBChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'pub':
                     $scope.pub = $scope.selPubOptions[choice];
                     // update db       
                     $scope.updateThis(  $scope.pub , 'status');
                     console.log("ERROR: MbrsiteAdminRelationShipItemCtrl setPUBChoices  *************************", $scope.pub );
                    break;
                case 'isActive':
                    //$scope.relItem.isActive = $scope.selPubOptions[choice];
                     // update db       
                     $scope.updateThis(  $scope.isActive , 'isActive');
                    break;
                // case '3rd':
                //     $scope.up3rdcategory = $scope.selOrgCategory[choice];
                //     break;    
                default:
                    break;  
                    console.log("ERROR: MbrsiteAdminRelationShipItemCtrl setPUBChoices  *************************", choice );
                      
 
            }

            return 
            console.log("DEBUG: MbrsiteAdminRelationShipItemCtrl setPUBChoices  *************************", $scope.pub );
        };

        $scope.updateThis = function(  data, prompt ){
            /* NOTE:
              Because relationsShips and contentSets data is managed with the use of arrays the [] seen
              below are just temp until a better model is formated.

              The code on the next line takes advantage of javasrcipt's everything is executable nature.
              What is happening is the code enclosed in () is interpted as a function and the function is initializing a var as an object,
              followed by an assigment of some data to that obj, which, is followed by the return of the obj.
            */  
            // var nuData = ( nuData={}, nuData[ prompt ] = [data] , nuData );     // Object();
            console.log("DEBUG: MbrsiteAdminRelationShipItemCtrl updateThis  pre * * * * * * * * * * * * * * *  ", data );
            var nuData;
            if ( isNaN(data)) {
                nuData = ( nuData={}, nuData[ prompt ] = data , nuData );     // Object();
            } else {
                nuData = ( nuData={}, nuData[ prompt ] = Number(data) , nuData );     // Object();  
            }    
            //
            //n = data.valueOf();
            //    
            //n = num.valueOf();
            //var nuData = ( nuData={}, nuData[ prompt ] = 5 , nuData );     // Object();
            //var nuData = { prompt: data.valueOf() };
            
            console.log("DEBUG: MbrsiteAdminRelationShipItemCtrl updateThis  post * * * * * * * * * * * * * *  ", nuData );          
            
            nuData.lastUpdated =  new Date();

            nuData.updatedById =  userService.user.keyId; 
            nuData.approveDate = new Date() ;
            //nuData['status']  = 'Active';    // ** Required, set at system level-relates to validity of   
            //nuData['isActive'] = true;  
              
            var cLabels = ['Components','Mbrsite','ContentSet'];     // part: Profile,Tabset or Panes 
            
            var parms = { where: {
                           profile: {
                            keyId : '',
                            name : '',
                            pId : '', 
                            isActive : true,
                            status : '',
                            permission : ''
                            },  
                           relationShip: {
                            keyId: $scope.relItem.keyId
                            //isActive : true,
                            //status : ''
                            },  
                           contentSet: {
                            keyId: ''  
                            }
                           },
                         set: 'relationShips',
                         cLabels: cLabels 
                        };
               quickSave( nuData , parms, function ( err, status ) {
               
                    if(err) {

                       console.log("ERROR: MbrsiteAdminRelationShipManagerCtrl-quickSave not updated *****: ", err);
                    }
                    else {
 
                       console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl-quickSave updated with status: ", status );
                   }

                });

       
        };



      
        $scope.getMbrsiteRelationShip = function ( part  ) {

          console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl-initMbrsite pre getUser :", user );
          
          var user = userService.getUser();
          
          console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl-initMbrsite post getUser:", user );

          if(user && user.authToken) {
            
            var cLabels = ['Components','Mbrsite',part];   // part: Profile,Tabset or Panes 
            //
            // NOTE: we may want to only retrieve PROFILE here??
            //
            //          call to getMbrsite using below parms will bring back all associated mbrsite relationShips
            //          and ContentSets. May need to update parms to be a bit more selective


            var parms = { where: {
                            profile: {
                                userKeyId : user.keyId,     
                                name : '',
                                pId : '', 
                                isActive : true,
                                status : '',
                                permission : ''
                                },  
                            relationShip: {
                                isActive : true,
                                status : ''
                                }
                            },      
                          cLabels: cLabels
                        };
                          
                         
            console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl-initMbrsite parms *********** : ", parms );

            componentService.getMbrsite( parms, function(err, nuProfile) {
                if(err) {

                    console.log("ERROR: initMbrsite ");
                }
                else {

                    console.log("ERROR: MbrsiteAdminRelationShipManagerCtrl-initMbrsiteProfile nuProfile:*************************: ", nuProfile ); 

                    $scope.relationShips = componentService.getLocalComponentPart('reslationShips', 'all');  // nuMbrsite.profile; // componentService.getLocalComponentPart('profile', ''); //nuMbrsite.profile;  

                    console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl-getLocalComponentPart profile,contentSets via Service ************** ", $scope.profileItem );
                }
           
            });
            
          }
          else {
            console.log("DEBUG: MbrsiteAdminRelationShipManagerCtrl.initMbrsiteProfile not authenticated ** :", user.authToken );
            $location.path('/home');
          }

        };

        /* NOT FINISHED */
        function quickSave ( what,  parms, callback ){

            // componentService.saveComponent( $scope.contentSet , parms, function(err, status) {
            componentService.saveComponent( what , parms, function(err, status) {               
                if(err) {

                    callback( true, status);
                }
                else {

                    callback( false, status);
                }

            });
       
        }
              
    }]);





    /*  **********************************
        **********************************
        **********************************

         MBRSITE RELATIONSHIP ADD CONTENTSET Ctrl
    */  
    angular.module('training.controllers').controller('MbrsiteAdminRelationShipAddContentSetCtrl', ['$scope', 'training.services.user', 'training.services.component', function($scope, userService, componentService ) {
    

        console.log("DEBUG: MbrsiteAdminRelationShipAddContentSetCtrl ************** " );
      
         

        $scope.nuContentSet = {};
          
        
        $scope.OBS_whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            
            }         
        };

        $scope.whichUserId = function() {
            var userid = userService.getUserId();
            if(userid) { 
               return userid;
            }         
        };
        
        console.log("DEBUG: MbrsiteAdminRelationShipAddContentSetCtrl ************** " );

      
        // $scope.$watch('relationShips', function( nuValue, oldValue ) {
   
        //     console.log("WATCH: MbrsiteAdminRelationShipManagerCtrl- relationShips :", nuValue, oldValue );
                
        //     $scope.relShips =  componentService.getLocalComponentPart('relationShips', 'all'); 
       
        //    try {
        //     var publish = componentService.getPublish(); 
          
        //     $scope.selPubOptions = Object.keys(publish[0]['publish']); 
        
        //     $scope.pub = $scope.relShips.status;   // ???????????  
            
        //     } catch (err) {
        //        //null
        //     }

        // });
 
       
        
        $scope.setPUBChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'pub':
                     $scope.pub = $scope.selPubOptions[choice];
                     // update db       
                     $scope.updateThis( choice , 'status');
                    break;
                // case '2nd':
                //    $scope.up2ndcategory =  $scope.selOrgCategory[choice];
                //     break;
                // case '3rd':
                //     $scope.up3rdcategory = $scope.selOrgCategory[choice];
                //     break;    
                default:
                    break;  
                    console.log("ERROR: MbrsiteAdminRelationShipAddContentSetCtrl setUSChoices  *************************", choice );
                      
 
            }

            return 
            console.log("DEBUG: MbrsiteAdminRelationShipAddContentSetCtrl setUSChoices  *************************", $scope.pub );
        };

        $scope.updateThis = function(  data, prompt ){
            /* NOTE:
              Because relationsShips and contentSets data is managed with the use of arrays the [] seen
              below are just temp until a better model is formated.

              The code on the next line takes advantage of javasrcipt's everything is executable nature.
              What is happening is the code enclosed in () is interpted as a function and the function is initializing a var as an object,
              followed by an assigment of some data to that obj, which, is followed by the return of the obj.
            */  
            // var nuData = ( nuData={}, nuData[ prompt ] = [data] , nuData );     // Object();
            var nuData;
            if ( isNaN(data)) {
                nuData = ( nuData={}, nuData[ prompt ] = data , nuData );     // Object();
            } else {
                nuData = ( nuData={}, nuData[ prompt ] = Number(data) , nuData );     // Object();  
            } 
            nuData['lastUpdated'] =  new Date();
            nuData['updatedById'] =  userService.user.keyId; 
            nuData['approveDate'] = new Date() ;
            //nuData['status']  = 'Active';    // ** Required, set at system level-relates to validity of   
            //nuData['isActive'] = true;  
              
            var cLabels = ['Components','Mbrsite','ContentSet'];     // part: Profile,Tabset or Panes 
            
            var parms = { where: {
                           profile: {
                            keyId : '',
                            name : '',
                            pId : '', 
                            isActive : true,
                            status : '',
                            permission : ''
                            },  
                           relationShip: {
                            keyId: $scope.relShips[0].keyId,
                            isActive : true,
                            status : ''
                            },  
                           contentSet: {
                            keyId: ''  
                            }
                           },
                         set: 'relationShips',
                         cLabels: cLabels 
                        };
                          
               quickSave( nuData , parms, function ( err, status ) {
               
                    if(err) {

                       console.log("ERROR: MbrsiteAdminRelationShipAddContentSetCtrl-quickSave not updated *****: ", err);
                    }
                    else {
 
                       console.log("DEBUG: MbrsiteAdminRelationShipAddContentSetCtrl-quickSave updated with status: ", status );
                   }

                });

       
        };



       $scope.addContentSet = function() {

            console.log("DEBUG: MbrsiteAdminRelationShipAddContentSetCtrl : ***** pre save *******", $scope.profileItem );


            $scope.nuContentSet.profileKeyId = componentService.mbrsite.profile.keyId;
            /* relationship Data */
            $scope.nuContentSet.groupLabel = "Components";    // var relationShip =
            $scope.nuContentSet.typeLabel  =  "Mbrsite" ;
           // $scope.nuContentSet.status:  =  "Inactive",             // dflt: Inactive Public,Internal Only Private,Inactive   
           // $scope.nuContentSet.isActive:  = false,             // true || false
    

            $scope.nuContentSet.relShipStatus = "active";  // dflt: Inactive Public,Internal Only Private,Inactive
            $scope.nuContentSet.relShipIsActive = true;            
            $scope.nuContentSet.relShipLastUpdated =  new Date();
            $scope.nuContentSet.relShipUpdatedById = userService.getUserKeyId(); 
            
            $scope.nuContentSet.relShipSetId = "tab9999";
            $scope.nuContentSet.relShipSetName = $scope.nuContentSet.buttonName; 
            $scope.nuContentSet.relShipSetKey = 100 ;
            $scope.nuContentSet.relShipSetHref = "#/mbrsites/mbrsiteconsole/";
            $scope.nuContentSet.relShipSetLabelText = $scope.nuContentSet.buttonName;
            $scope.nuContentSet.relShipSetTemplate = $scope.nuContentSet.template;
            $scope.nuContentSet.relShipSetOrder = 1 ;
            
            
            // Content Data            
            
            $scope.nuContentSet.contentSet = { 
                            keyId: "",             // NOTE:  will be assigned value in db server
                             groupLabel: "Components",
                             typeLabel:  "Mbrsite",
                             partLabel: "ContentSet",
                            userKeyId: userService.getUserKeyId(),
                            createdByKeyId: userService.getUserKeyId(),
                             dateCreated: new Date(),
                             lastUpdated:  new Date(),
                            updatedById: userService.getUserKeyId(), 
                             modelId: "tab9999" ,    //,"tab1-1","tab1-2","tab1-2"]; ["member.profile"];  // "nill","nill","nill"];
                            modelName: $scope.nuContentSet.template,   //"contentOne","contentOne","contentTWO"];
                             modelSrcs: "Model",  //,"This","This","This"];    // dflt: "This"  [ This, Model, ..]
                             modelobjs: "cOne-1",    //"cOne","contentOne","contentTWO"];
                             isOk: "support",   //"Support","Support","Support"];
                            text_0000:  $scope.nuContentSet.buttonName , //dflt "Title" { value:$scope.cpnent.name, isok:"support"};
                             text_0001: "About Our history", //   textAreaTitle;
                             area_0000: "mbrs-tab0-1 sit amet leo nec nunc aucto",   //textArea 
                             imag_0000: "../../../common/images/homepage-photo3.jpg",  // img source
                             area_0001: "This is a great picture", //imgTextArea = 
                             link_0000: "href='#'",   //  link,"href='#'","href='#'";
                             area_0002: "Click here for more information" // linkTextArea

                        };

            var parms = "";
            componentService.addContentSet($scope.nuContentSet, function(err, status) {
               
                if(err) {

                    console.log("ERROR: MbrsiteAdminRelationShipAddContentSetCtrl-saveComponent not updated *****: ", err);
                }
                else {

                    console.log("DEBUG: MbrsiteAdminRelationShipAddContentSetCtrl-saveComponent updated with status: ", status );
                    //$scope.usersedititem = '';
                    //$location.path('/#/users/profil/:#tab3-5');
                    //$scope.$apply(); 
                       
                      // commented $scope.usersEditItem = {};
                   // commented  });
                }

            });
        };

        
        $scope.getMbrsiteRelationShip = function ( part  ) {

          var user = userService.getUser();
        
          if(user && user.authToken) {
            
            var cLabels = ['Components','Mbrsite',part];   // part: Profile,Tabset or Panes 
            //
            // NOTE: we may want to only retrieve PROFILE here??
            //
            //          call to getMbrsite using below parms will bring back all associated mbrsite relationShips
            //          and ContentSets. May need to update parms to be a bit more selective


            var parms = { where: {
                            profile: {
                                userKeyId : user.keyId,     
                                name : '',
                                pId : '', 
                                isActive : true,
                                status : '',
                                permission : ''
                                },  
                            relationShip: {
                                isActive : '',                          // true or false
                                status : ''
                                }
                            },      
                          cLabels: cLabels
                        };
                          
                         
            console.log("DEBUG: MbrsiteAdminRelationShipAddContentSetCtrl-initMbrsite parms *********** : ", parms );

            componentService.getMbrsite( parms, function(err, nuProfile) {
                if(err) {

                    console.log("ERROR: initMbrsite ");
                }
                else {

                    console.log("ERROR: MbrsiteAdminRelationShipAddContentSetCtrl-initMbrsiteProfile nuProfile:*************************: ", nuProfile ); 

                    $scope.relationShips = componentService.getLocalComponentPart('reslationShips', 'all');  // nuMbrsite.profile; // componentService.getLocalComponentPart('profile', ''); //nuMbrsite.profile;  

                    console.log("DEBUG: MbrsiteAdminRelationShipAddContentSetCtrl-getLocalComponentPart profile,contentSets via Service ************** ", $scope.profileItem );
                }
           
            });
            
          }
          else {
            console.log("DEBUG: MbrsiteAdminRelationShipAddContentSetCtrl.initMbrsiteProfile not authenticated ** :", user.authToken );
            $location.path('/home');
          }

        };

        /* NOT FINISHED */
        function quickSave ( what,  parms, callback ){

            // componentService.saveComponent( $scope.contentSet , parms, function(err, status) {
            componentService.saveComponent( what , parms, function(err, status) {               
                if(err) {

                    callback( true, status);
                }
                else {

                    callback( false, status);
                }

            });
       
        }
              
    }]);




    

    /*  **********************************
        **********************************
        **********************************
         MBRSITE CONTENTSETS MANAGER Ctrl
    */ 

    angular.module('training.controllers').controller('MbrsiteAdminContentSetsCtrl', ['$scope', '$window', 'training.services.user', 'training.services.component', function( $scope, $window, userService, componentService ) {
            
       // $scope.profileItem = {};
        $scope.contentSetWrite = {};
        $scope.contentSetNbr = 0;
        $scope.SetNum = 0;
        $scope.contentSetSrc = "";
        $scope.mine = "master";
        $scope.fileNameUploaded = "testA";
        $scope.fileread = "testb";
          
        //$scope.contentSet = {};  
        $scope.$watchCollection('contentSets', function( nuValue, oldValue ) {
   
            console.log("WATCH: MbrsiteAdminContentSetsCtrl- tabSets :", nuValue, oldValue );
            //     var notFirst = false;
            //     if ( $scope.contentSet !== '' ) {
            //       if(notFirst) { 
            //          //$scope.contentSet = $scope.contentSets;  //
            //       } else { 
            //         notFirst = true;
            //         $scope.contentSetWrite = $scope.contentSets;
            //        // $scope.contentSet = $scope.contentSets; 
            //       }
            //     }
            $scope.tabSets = {};
            $scope.tabSets = componentService.getLocalComponentPart('contentSets', 'all'); 

            var row;
            $scope.tabTypes = [];
            for ( var key in $scope.tabSets ) { 
                row = $scope.tabSets[key];
                // if (first) {
                //   //  profile    
                //   retDat.profile = row[0] ;
                //   // others
                //   retDat.relShip = {};
                //   retDat.contentSet = {};
                //   retDat.relShip[key] =   new Object(  row[1] );
                //   retDat.contentSet[key] =  new Object(  row[2] );
                //   //retDat.contentSet[row[2]['keyId']] =  new Object(  row[2] );
                //   //console.log("DEBUG: neo4Components.getComponentSetV1 : return component set row[2] ", row[2]['keyId']);  
                // } else { 
                //  retDat.relShip[key] =  new Object(  row[1] );
                //  retDat.contentSet[key] =  new Object(  row[2] );
                //  //console.log("DEBUG: neo4Components.getComponentSetV1 : return component set row[2] ", row[2]['keyId']);  
                // }
                // if ( key === 'tabSets ') {
                //    console.log("DEBUG: Directives.contentSet - scope ***************** ", key, row);       
                //  } else {
                console.log("DEBUG: MbrsiteAdminContentSetsCtrl- tabTypes.key : ***************** ", key);        
                $scope.tabTypes.push(row.modelName); 
                //$scope.tabSet = {};
                //first = false;
               
            }    
            console.log("DEBUG: MbrsiteAdminContentSetsCtrl- tabTypes.arry: * *  * ************** ", $scope.tabTypes,  $scope.tabSets);    
             
        
        });

        $scope.$on('contentSetNbr Changed', function( event, nuValue ) {
   
            console.log("ON: MbrsiteAdminContentSetsCtrl- contentSetNbr Changed :", nuValue );
            
            $scope.SetNum = nuValue;

            //$scope.contentSetSrc = $scope.contentSets[nuValue];
        });

       // MAY NEED THIS as we transistion functionality to multi element arrys

        $scope.initMbrsiteContentSet = function ( set ) {
         
          console.log("DEBUG: MbrsiteAdminContentSetsCtrl-initMbrsiteContentset-getLocalComponentPart - contentSet :", set );
           
        }; 
        
        $scope.setActive = function (key) {
            
          null

        };  

        $scope.registerThis  = function( parms ) {
          console.log("DEBUG: MbrsiteAdminContentSetsCtrl-registerThis parms   * * * * *: ", parms );
        };

        $scope.updateThis = function( key, prompt, data){
          
             console.log("DEBUG: MbrsiteAdminContentSetsCtrl-updateThis pre key, prompt, data   * * * * *: ", key, prompt, $scope.fileNameUploaded );

            var nuData;
            if ( isNaN(data)) {
                nuData = ( nuData={}, nuData[ prompt ] = data , nuData );     // Object();
            } else {
                nuData = ( nuData={}, nuData[ prompt ] = Number(data) , nuData );     // Object();  
            } 

            nuData['lastUpdated'] =  new Date();
            nuData['updatedById'] =  userService.user.keyId; 
            nuData['approveDate'] = new Date() ;
            nuData['status']  = 'active';    // ** Required, set at system level-relates to validity of   
            nuData['isActive'] = true;  
              
            var cLabels = ['Components','Mbrsite','ContentSet'];     // part: Profile,Tabset or Panes 

            console.log("DEBUG: MbrsiteAdminContentSetsCtrl-updateThis pre $scope.tabSets[key].keyId  * * * * *: ", $scope.tabSets[key].keyId, key  );
            
            var parms = { where: {
                           profile: {
                            keyId : '',
                            name : '',
                            pId : '', 
                            isActive : true,
                            status : '',
                            permission : ''
                            },  
                           relationShip: {
                            keyId: '',
                            isActive : true,
                            status : ''
                            },  
                           contentSet: {
                            keyId: $scope.tabSets[key].keyId 
                            }
                           },
                         set: 'contentSet',
                         cLabels: cLabels 
                        };
                          
               // quickSave( nuData , parms, function ( err, status ) {
               
               //      if(err) {

               //         console.log("ERROR: MbrsiteAdminContentSetsCtrl-saveComponent not updated *****: ", err);
               //      }
               //      else {
 
               //         console.log("DEBUG: MbrsiteAdminContentSetsCtrl-saveComponent updated with status: ", status );
               //     }

               //  });

       
        };

        // $scope.uploadThis = function( key, prompt, data ){
          
        //     alert("content file uploaded");
       
        // };

        $scope.saveThisTab = function(){
          
            alert("save this content");
       
        };

        // function quickSave ( what,  parms, callback ){
        //     // componentService.saveComponent( $scope.contentSet , parms, function(err, status) {
        //     componentService.saveComponent( what , parms, function(err, status) {               
        //         if(err) {

        //             callback( true, status);
        //         }
        //         else {

        //             callback( false, status);
        //         }

        //     });
       
        // };

              
    }]);

    
    //angular.module('training.controllers').controller('contentSetImgCtrl', ['$scope', 'Upload', 'training.services.user', 'training.services.component', function( $scope, Upload, userService, componentService ) {
    angular.module('training.controllers').controller('contentSetImgCtrl', ['$scope', 'Upload', , 'training.services.user', 'training.services.component', function ($scope, Upload, userService, componentService ) {
        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        });
        $scope.log = '';

        $scope.upload = function (files) {
            if (files && files.length) {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    Upload.upload({
                        url: '/api/v1/images/upload',
                        fields: {
                            'username': $scope.username
                        },
                        file: file
                    }).progress(function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        $scope.log = 'progress: ' + progressPercentage + '% ' +
                                    evt.config.file.name + '\n' + $scope.log;
                    }).success(function (data, status, headers, config) {
                        $scope.log = 'file ' + config.file.name + 'uploaded. Response: ' + JSON.stringify(data) + '\n' + $scope.log;
                        $scope.$apply();
                    });
                }
            }
        };

 
    }]);  

}());