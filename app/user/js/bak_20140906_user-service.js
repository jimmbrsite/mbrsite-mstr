(function() {

    'use strict';

    /*
     * API:
     *
     * getUsers : callback
     * createUser : Object e.g. {"firstName":"Thomas, "lastName":"Amsler}
     */
    angular.module('training.services').factory('training.services.user', ['$http', function($http) {

        var UserService = {

            'users' : [],
            'user' : {},     // name:'hello' 
            'connections' : [],
            'connected' : {},
            'others' : [],
            'other' : {}
        };

        UserService.signedIn = function() {
            if (angular.fromJson(sessionStorage.getItem("user")) !== null)  {
            // use this bypass... if (angular.fromJson(sessionStorage.getItem("user")) === null)  {    
                 console.log("INFO: User is signedIn");
                 return true;
            } else {
                console.log("INFO: User is not signedIn");
                return false;
            };    
           
        };

        UserService.logout = function() {
            UserService.user = {};
            UserService.connected = {};
            UserService.other = {};
            UserService.connections = [],
            UserService.others = [],
            UserService.users = [],
            sessionStorage.removeItem("user");
            
        };
        
        UserService.saveLocalUsers = function(users) {

            console.log("INFO: save local userUser");
            console.log(users);
    
            this.users = users;
            
        };

        UserService.getLocalUsers = function() {

            return this.users;
        };

        UserService.getLocalConnections = function() {
/* NOTE: needs work more work need to go back to db and get follows relationships for this active user we are editing
**
*
*
*/       
            return this.connections;
        };
    
        UserService.getLocalOthersPage = function( newPage ) {
             
/*  NOTE: This needs more work:  such as lookup prompt that creates list from query results , pick one or many  to follow
*
*   Sample function for page gets
*   function getResultsPage(pageNumber) {
        // this is just an example, in reality this stuff should be in a service
        $http.get('path/to/api/users?page=' + pageNumber)
            .then(function(result) {
                $scope.users = result.data.Items;
                $scope.totalUsers = result.data.Count
            });
    }
*/           if ( this.others.length === 0 ) {

                // get and load first page of others, store total count of others in pagination variables
                return this.users;
             }
             else {

                // get and load next page of others update pages etc.
                return this.others;      
             }   
             

            
        };  
      
        UserService.getUser = function() {

            return this.user;
        };

        UserService.init = function() {

            UserService.user = angular.fromJson(sessionStorage.getItem("user"));
            
        };

        UserService.getLocalUser = function(userId) {
            
            for(var i = 0; i < this.users.length; i++) {
                
                console.log("DEBUG : " + i );

                if(this.users[i]._id.toString() === userId ) {
                    
                    return this.users[i];
                }
                else {

                   console.log( this.users[i]._id ); 
                }
            }
        };

        // API : getUsers
        UserService.getUsers = function(callback) {

            if(this.user.authToken) {

                $http(
                    {
                        "method" : "GET",
                        "url" : "/api/v1/users",
                        "headers" : {
                            "Authorization" : "Session " + this.user.authToken
                        },
                        "cache" : false

                    })
                    .success(function(data, status, headers, config) {

                        callback(null, data);
                    })
                    .error(function(data, status, headers, config) {

                        callback(true, null);
                    });
            }
            else {

                console.log("ERROR: User is not authenticated");
            }
        };

        
        // API createUser
        UserService.createUser = function(newUser, callback) {

            $http(
                {
                    "method" : "POST",
                    "url" : "/api/v1/users",
                    "data" : newUser
                })
                .success(function(data, status, headers, config) {

                    callback(false, status);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });

             console.log("ERROR: User create here");    
        };

        // API loginUser
        UserService.loginUser = function(user, callback) {

            $http(
                {
                    "method" : "POST",
                    "url" : "/api/v1/users/login",
                    "data" : user
                })
                .success(function(data, status, headers, config) {

                    UserService.user = data;
                    sessionStorage.setItem("user", angular.toJson(data));
                    callback(false, status);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });
        };

        // API saveUser
        UserService.saveUser = function(user, callback) {

            console.log("DEBUG: saveUser called : user : ", user);
            $http(
                {
                    "method" : "PUT",
                    "url" : "/api/v1/users",
                    "data" : user
                })
                .success(function(data, status, headers, config) {

                    callback(false, status);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });
        };

        /* API Delete User
        *
        * NOTE: idType is either:
        *      "keyId" or "objectId"
        *
        *   obs loginId obs
        */

        UserService.deleteUser = function(id, idType, callback) {

            
            $http(
                {
                    "method" : "DELETE",
                    "url" : "/api/v1/users/" + id,
                    "params" : {
                        "idType" : idType
                    }
                })
                .success(function(data, status, headers, config) {

                    callback(false, status);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });
        };

        /* API Connect User
        *
        * NOTE: idType is either:
        *      "keyId" or loginId or "objectId"
        *
        * rootUser = {'Id': $scope.user._id, 'idType': 'objectId' };
        *
        * otherUser = {'otherId': $scope.others[index]._id, 'idType': 'objectId' };
        * 
        * relationShip = { 'relationType' : '????', 'relationData' : '??????' };
        */   
        UserService.connectUser = function( rootUser , otherUser , relationShip, callback) {

            console.log("DEBUG UserService : Connect from root user : ", rootUser.Id  ); 
            console.log("DEBUG UserService : Connect to other user : ", otherUser.otherId  );
            console.log( otherUser );

            $http(
                {
                    "method" : "PUT",
                    "url" : "/api/v1/users/" + rootUser.Id + "/connect",
                    "data" : { "idType" : rootUser.idType,
                                 "otherId" : otherUser.otherId,
                                 "otherType" : otherUser.idType,
                                 "relationType" : relationShip.relationType,
                                 "relationData" :  relationShip.relationData 
                    }
                })
                .success(function(data, status, headers, config) {

                  
                    //sessionStorage.setItem("user", angular.toJson(data));

                    //callback(false, status);
                     callback(null, data);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });
        };

        /* API Disconnect User
        *
        * NOTE: idType is either:
        *      "keyId" or loginId or "objectId"
        *
        */
    
        UserService.disconnectUser = function(rootUser, connectedUser , options, callback) {

            console.log("DEBUG Userservice : Disconnect from root user : ", rootUser.Id  ); 
            console.log("DEBUG UserService : Disconnect connected user : ", connectedUser.connectedId  );
            console.log( connectedUser );
            
            $http(
                {
                    "method" : "PUT",
                    "url" : "/api/v1/users/" + rootUser.Id + "/disconnect",
                    "data" : { "idType" : rootUser.idType,
                               "connectedId" : connectedUser.connectedId,
                                 "otherType" : connectedUser.idType,
                                 "options" : options
                    }
                })
                .success(function(data, status, headers, config) {

                    console.log("DEBUG disconnectUser : Disconnected connected user : ", data  );    
                    callback( null , data);
                    //callback(false, status);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });
        };
    
        UserService.findConnections = function( userId, options, callback) {

            if(this.user.authToken) {

                $http(
                    {
                        "method" : "PUT",
                        "url" : "/api/v1/users/" + userId + "/findconnects" ,
                        "data" : { "options" : options

                        } 
                       
                       // "headers" : {
                       //     "Authorization" : "Session " + this.user.authToken
                       // },
                       // "cache" : false
                        

                    })
                    .success(function(data, status, headers, config) {
                       
                        console.log("DEBUG UserService : findConnections : data : ", data  );  

                        callback(null, data);
                    })
                    .error(function(data, status, headers, config) {

                        callback(true, null);
                    });
            }
            else {

                console.log("ERROR: User is not authenticated");
            }
        };



        return UserService;
    }]);

}());

