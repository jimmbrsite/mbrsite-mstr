(function() {

    'use strict';

    /*
     * Users Ctrl
     */
    angular.module('training.controllers').controller('UsersCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {

        $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }         
        };

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };


        // deletes user from a list
        $scope.delete = function(index) {
             
            /* NOTE: idType is either :
            *      "keyId" or loginId or "objectId"
            *
            ****  2014/07  currently dynamic idType is not enabled for this function
            ****  
            *
            * userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {
            * userService.deleteUser($scope.users[index].id, "loginId", function(err, status) {    
            * userService.deleteUser($scope.users[index].keyId, "keyId", function(err, status) {  
            * 
            */
            
            userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {    

                if(err) {

                    console.log("ERROR: delete user");
                }
                else {

                    console.log("INFO: deleted user");
                    $scope.users.splice(index, 1);
                }
            });
        };

        $scope.getUser = function() {

            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }
        };

        var user = userService.getUser();

        if(user && user.authToken) {

            userService.getUsers(function(err, users) {

                $scope.users = users;
                
                userService.saveLocalUsers(users);

            });
            
        }
        else {

            $location.path('/home');
        };
            
    }]);

    /*
     * User Edit Ctrl
     *   the user edit ctrl collects and sets different content in $scope.user as determined by userId
     */
    angular.module('training.controllers').controller('UsersEditCtrl', ['$scope', '$routeParams', '$location','training.services.user', function($scope, $routeParams, $location, userService) {
        
        // note: this code section exicutes every time UsersEditCtrl is entered?

        console.log("DEBUG: usersEdit params : ");
        console.log( $routeParams )
        
        var userId = $routeParams.userId;
        // var userTab=  $routeParams.tabs | '';

        //$scope.meals = [];    // from examples

        $scope.user = userService.getLocalUser(userId);
        
        console.log("DEBUG: userEditControl.userTabrouteParams : routeParams  ", $routeParams);
       
        // DEBUG alert( userId );

        
        if (!$scope.user) {

            // 20140917 $location.path('/users');
            console.log("INFO: userController.UsersEditCtrl : !$scope -> /home");    
            $location.path('/home');
        }
        else {

           
            var options = { 'types' : ['FOLLOWS'],          
                            'direction' : "out" };

            userService.findConnections( userId, options, function( err, results ) {
                
                if(err) {

                    console.log("ERROR: userService.findConnections : err  ", err);
                }
                else {

                    console.log("INFO: userService.findConnections : results: ", results );
                    

                    $scope.connections = results;
          
                               
                }
            });            
         
            
        }

      
        // 20140917 console.log("INFO: userEditCtrl: userService.getLocalOthersPage : scope others: ", $scope.others );
        
        
        // load first page into list
            // userService.getResultsPage(1);
        //$scope.others = userService.getLocalOthersPage(1);  // load first page of user not connected to.
        
        // $scope.pageChangeHandler = function(num) {
        //     console.log('header page changed to ' + num);
        //     userService.getResultsPage( num );
        // };
        
        // MOVED TO userNuConnectPageCtrl 20140919
        //  // local User pagination settings
        // $scope.itemsPerPage = 25; 
        // $scope.currentPage = 1;
        // $scope.pageSize =  25;  // this should match however many users you want on one page
        // // asynchronous User pagination settings
        // $scope.totalUsers = 0;
        // $scope.usersPerPage = 25; // this should match however many results your API puts on one page
        // //pageChangeHandler(1);

        //$scope.others = userService.getLocalOthersPage( 1 , function(err, status)  );

        // userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize , $scope.fetchBy , function(err, nxPageResults) {
        //                 if(err) {

        //                     console.log("ERROR: userNuConnect error");

        //                     $scope.message = "Pagination error - userNuConnect";

        //                 } else {

        //                     console.log("INFO: new page loaded");
        //                     // var totCount =   nxPageResults.length - 4850;
        //                     // var nxPage = 
        //                     // for(var i = 0; i < totCount  ; i++) {
                
        //                     $scope.others= nxPageResults;   
        //                     //userService.others= nxPageResults;
        //                     $scope.totalUsers =  $scope.others.length;   
                           
        //                    //}



                            
        //                 }
        // });

        // MOVED TO  userNuConnectPageCtrl  20140919   
        // $scope.pagination = {
        //    current: 1
        // };
  

        // END NOTE: 
       

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };
        
        $scope.whichUserNotMe = function(beingViewedId) {
            var user = userService.getUser();
             console.log("DEBUG: whichUserNotMe", beingViewedId );
         
            if( beingViewedId ) {

                return true;
            }         
        };

        $scope.save = function() {

            userService.saveUser($scope.user, function(err, status) {

                if(err) {

                    console.log("ERROR: save user");
                }
                else {

                    console.log("INFO: saved user");
                    
                    $location.path('/users');
                }
            });
        };
        // NOTE : If delete is allowed at this button then we need to clean up connections etc.
        //        This user would need to be logged out as well..  
        $scope.delete = function () {
            // using need to fix api function to accept id or object


            // ask if they are sure and explain the consequense
            alert( " Are you sure ");

            userService.deleteUser($scope.user._id, "objectId", function(err, status) {

                if(err) {

                    console.log("ERROR: delete user");
                }
                else {

                    console.log("INFO: deleted user");

                    //$scope.users.splice(index, 1);
                    // SOME WORK HERE: If allowed 
                    $location.path('/home');
                }
            });
        };

        // connect user to user

        // $scope.addConnection = function (index) {
        //     console.log("DEBUG: making connection from root user");
        //     console.log( $scope.user._id);
        //     console.log("DEBUG: making connection to other user");
        //     console.log($scope);

        //     var rootUser = {'Id': $scope.user._id,
        //                     'idType': 'objectId' };

        //     var otherUser = {'otherId': $scope.others[index]._id,
        //                       'idType': 'objectId' };

        //     var relationShip = { 'relationType' : 'FOLLOWS',
          
        //                          'relationData' : { 'otherKeyId' : $scope.others[index].keyId } }; 
                              
        //     userService.connectUser( rootUser, otherUser , relationShip, function(err, results ) {
                
        //         if(err) {

        //             console.log("ERROR: making connection to other user");
        //             console.log( results );
        //             console.log( Object.keys(results).length )
        //         }
        //         else if ( [results].length ===  1 ) {

        //             console.log("DEBUG: made connection to other user: results: ", results );

        //             $scope.connections.push( results );
                    
        //             $location.path('/users/edit/' + rootUser.Id );
        //         }
        //         else {
        //             console.log("INFO: made connection to other user");
        //             $location.path('/users');
        //         }

        //     });
        // };

        // drop  user to user connection 
        $scope.dropConnection = function (index) {
           console.log("ERROR: disconnecting from this user");
           console.log($scope.connections[index]._id); 

           var rootUser = {'Id': $scope.user._id,
                            'idType': 'objectId' };

            var connectedUser = {'connectedId': $scope.connections[index]._id,
                              'idType': 'objectId' };

            var options = { 'types' : ['FOLLOWS'],          
                            'direction' : "out" };

           userService.disconnectUser(rootUser, connectedUser , options , function(err, result) {

                console.log("DEBUG: dropConnection : disconnectUser : result :", result );
                console.log("DEBUG: dropConnection : disconnectUser : result :", connectedUser.connectedId );

                if(err) {

                    console.log("ERROR: disconnecting from this connected user");
                }
                else if ( result == connectedUser.connectedId  ) {  //   
                    
                    console.log("DEBUG: dropConnection : disconnectUser : results :", result ); 
                    $scope.connections.splice(index, 1);
                    
                    $location.path('/users/edit/' + rootUser.Id);           
                } 
                else {
                    // $scope.users.splice(index, 1);
                    $location.path('/users');
                }
           });
        };

        $scope.cancel = function () {
            
            console.log("INFO: canceled user update");
            
            $location.path('/home');
            
                
        };



    }]);

    /*
     * User Create Ctrl
     */
    angular.module('training.controllers').controller('UsersCreateCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {

        $scope.user = {
            '_id' : '',
            'keyId' : '', 
            'loginId' : '',
            'firstName' : '',
            'lastName' : '',
            'password' : '',
            'userRole': ''  // admin', 'manager', 'user', ...
        };

        $scope.connected = {
            '_id' : '',
            'keyId' : '', 
            'loginId' : '',
            'firstName' : '',
            'lastName' : '',
            'password' : '',
            'userRole': ''  // admin', 'manager', 'user', ...
        };

        $scope.other = {
            '_id' : '',
            'keyId' : '', 
            'loginId' : '',
            'firstName' : '',
            'lastName' : '',
            'password' : '',
            'userRole': ''  // admin', 'manager', 'user', ...
        };
        

        $scope.create = function() {

            userService.createUser($scope.user, function(err, status) {

                if(err) {

                    console.log("ERROR: create user");
                }
                else {

                    console.log("INFO: Created User");
                    $location.path('/users');
                }
            });
        };

        $scope.cancel = function () {
            
            console.log("INFO: canceled user create");
            
            $location.path('/users');
                
        };
    }]);

    /*
     * User login Ctrl
     */
    angular.module('training.controllers').controller('UsersLoginCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location,    userService) {

        $scope.user = {

            'loginId' : '',
            'password' : ''
        };

        $scope.message = "";

        $scope.login = function() {

            $scope.message = "";

            userService.loginUser($scope.user, function(err, status) {

                if(err) {

                    console.log("ERROR: login user");

                    $scope.message = "Authentication Error";
                }
                else {
                       
                    var tmpUser = userService.getUser();   

                    console.log("INFO: Logged In User status : " + tmpUser._id );  //+ '#tab1-5'
                    
                    //$location.path('/users/profile/:' + tmpUser._id );
                    //$location.path('/users');
                    $location.path('/home');
                }
            });
        };
    }]);

    /*
     * userNuConnectPage Controller
     */
    angular.module('training.controllers').controller('UsersOthersConnectPageCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {
        
        // local User pagination settings
        $scope.itemsPerPage = 25; 
        $scope.currentPage = 1;
        $scope.pageSize =  25;  // this should match however many users you want on one page
        $scope.fetchBy = {
            'fetchLastName' : '',
            'fetchFirstName' : '',
            'fetchCity'  : ''

        };
        // asynchronous User pagination settings
        $scope.othersTotalPossible = 0;
        $scope.othersPerPage = 25; // this should match however many results your API puts on one page
        //pageChangeHandler(1);
        $scope.pagination = {
           current: 1
        };
        $scope.pageChanged = function() {
                console.log('DEBUG: UserService.pageChanged: to nu page ' + $scope.currentPage);
                
                if ( $scope.fetchBy.fetchLastName != ""  ||  $scope.fetchBy.fetchFirstName != "" ) {
                     
                    console.log('DEBUG: UserService.pageChanged: got fetch data:' + $scope.fetchBy);
                    var tmpFetchBy = {
                                        'fetchLastName' : '',
                                        'fetchFirstName' : '',
                                        'fetchCity'  : ''
                                    };    
                    tmpFetchBy.fetchLastName = $scope.fetchBy.fetchLastName + ".*";
                    tmpFetchBy.fetchFirstName = $scope.fetchBy.fetchFirstName + ".*"; 
                    console.log('DEBUG: UserService.pageChanged: got tmp  data:', tmpFetchBy);
                    userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize , tmpFetchBy,  function(err, nxPageResults) {
                        if(err) {

                            console.log("ERROR: userNuConnect error");

                            $scope.message = "Pagination error - userNuConnect";

                        } else {

                            $scope.others = nxPageResults["data"];
                            console.log("DEBUG: new others page contents loaded: ");
                            console.log( $scope.others );
                            //userService.others =  nxPageResults;  
                            $scope.othersTotalPossible =  $scope.others.length;   
                           
                        }
                    });
                };         
        };

        $scope.fetchConnection = function() {
                console.log('getting more others: currentPage: ' + $scope.currentPage );
                console.log('getting more others: fetch: ' + $scope.fetchBy );
                if ( $scope.fetchBy.fetchLastName !== ""  ||  $scope.fetchBy.fetchFirstName !== "" ) {

                    userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize, $scope.fetchBy ,  function(err, nxPageResults) {
                        if(err) {

                            console.log("ERROR: userNuConnect error");

                            $scope.message = "Pagination error - userNuConnect";

                        } else {

                            console.log("INFO: new page loaded: " +  $scope.currentPage   );
                            // var totCount =   nxPageResults.length - 4850;
                            // var nxPage = 
                            // for(var i = 0; i < totCount  ; i++) {
                
                            $scope.others = nxPageResults["data"];

                            // userService.others =  nxPageResults;  
                            $scope.othersTotalPossible =  $scope.others.length;   
                           
                        }

                    });    
                };     
        };

        $scope.addConnection = function (index) {
            console.log("DEBUG: making connection from root user");
            console.log( $scope.user._id);
            console.log("DEBUG: making connection to other user");
            console.log($scope.others[index]._id);

            var rootUser = {'Id': $scope.user._id,
                            'idType': 'objectId' };

            var otherUser = {'otherId': $scope.others[index]._id,
                              'idType': 'objectId' };

            var relationShip = { 'relationType' : 'FOLLOWS',
          
                                 'relationData' : { 'otherKeyId' : $scope.others[index].keyId } }; 
                              
            userService.connectUser( rootUser, otherUser , relationShip, function(err, results ) {
                
                if(err) {

                    console.log("ERROR: making connection to other user");
                    console.log( results );
                    console.log( Object.keys(results).length )
                }
                else if ( [results].length ===  1 ) {

                    console.log("DEBUG: made connection to other user: results: ", results );

                    $scope.connections.push( results );
                    
                    $location.path('/users/profile/' + rootUser.Id + '#tab2-5' );
                }
                else {
                    console.log("INFO: made connection to other users");
                    $location.path('/users/profile/' + rootUser.Id + '#tab2-5');
                }

            });
        };

        
    }]);
    /*
     *    UsersListPageCtrl
    */
    
    angular.module('training.controllers').controller('UsersListPageCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {
        
        // local User pagination settings
        $scope.itemsPerPage = 25; 
        $scope.currentPage = 1;
        $scope.pageSize =  25;  // this should match however many users you want on one page
        $scope.fetchBy = {
            'fetchLastName' : '',
            'fetchFirstName' : '',
            'fetchCity'  : ''

        };
        // asynchronous User pagination settings
        $scope.usersTotalPossible = 0;
        //$scope.othersPerPage = 25; // this should match however many results your API puts on one page
        //pageChangeHandler(1);
        $scope.pagination = {
           current: 1
        };
        $scope.fetchPage = function() {
                console.log('DEBUG: UsersListPageCtrl.fetchPage: to nu page ' + $scope.currentPage);
                
                if ( $scope.fetchBy.fetchLastName != ""  ||  $scope.fetchBy.fetchFirstName != "" ) {
                     
                    console.log('DEBUG: UsersListPageCtrl.fetchPage: got fetch data:' + $scope.fetchBy);
                    var tmpFetchBy = {
                                        'fetchLastName' : '',
                                        'fetchFirstName' : '',
                                        'fetchCity'  : ''
                                    };  
                    // add wildcard search params                  
                    tmpFetchBy.fetchLastName = $scope.fetchBy.fetchLastName + ".*";
                    tmpFetchBy.fetchFirstName = $scope.fetchBy.fetchFirstName + ".*"; 
                    console.log('DEBUG: UserService.pageChanged: got tmp  data:', tmpFetchBy);
                    userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize , tmpFetchBy,  function(err, nxPageResults) {
                        if(err) {

                            console.log("ERROR: userNuConnect error");

                            $scope.message = "Pagination error - userNuConnect";

                        } else {

                            $scope.usersList = nxPageResults["data"];
                            console.log("DEBUG: new usersList page contents loaded: ");
                            console.log( $scope.usersList );
                            //userService.others =  nxPageResults;  
                            $scope.usersTotalPossible =  $scope.usersList.length;   
                           
                        }
                    });
                };         
        };

        $scope.fetchConnection = function() {
                console.log('getting more others: currentPage: ' + $scope.currentPage );
                console.log('getting more others: fetch: ' + $scope.fetchBy );
                if ( $scope.fetchBy.fetchLastName !== ""  ||  $scope.fetchBy.fetchFirstName !== "" ) {

                    userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize, $scope.fetchBy ,  function(err, nxPageResults) {
                        if(err) {

                            console.log("ERROR: userNuConnect error");

                            $scope.message = "Pagination error - userNuConnect";

                        } else {

                            console.log("INFO: new page loaded: " +  $scope.currentPage   );
                            // var totCount =   nxPageResults.length - 4850;
                            // var nxPage = 
                            // for(var i = 0; i < totCount  ; i++) {
                
                            $scope.others = nxPageResults["data"];

                            // userService.others =  nxPageResults;  
                            $scope.othersTotalPossible =  $scope.others.length;   
                           
                        }

                    });    
                };     
        };

        $scope.addConnection = function (index) {
            console.log("DEBUG: making connection from root user");
            console.log( $scope.user._id);
            console.log("DEBUG: making connection to other user");
            console.log($scope.others[index]._id);

            var rootUser = {'Id': $scope.user._id,
                            'idType': 'objectId' };

            var otherUser = {'otherId': $scope.others[index]._id,
                              'idType': 'objectId' };

            var relationShip = { 'relationType' : 'FOLLOWS',
          
                                 'relationData' : { 'otherKeyId' : $scope.others[index].keyId } }; 
                              
            userService.connectUser( rootUser, otherUser , relationShip, function(err, results ) {
                
                if(err) {

                    console.log("ERROR: making connection to other user");
                    console.log( results );
                    console.log( Object.keys(results).length )
                }
                else if ( [results].length ===  1 ) {

                    console.log("DEBUG: made connection to other user: results: ", results );

                    $scope.connections.push( results );
                    
                    $location.path('/users/profile/' + rootUser.Id + '#tab2-5' );
                }
                else {
                    console.log("INFO: made connection to other users");
                    $location.path('/users/profile/' + rootUser.Id + '#tab2-5');
                }

            });
        };

        
    }]);
    /*
    * userAlchemy page contropller
    */
    angular.module('training.controllers').controller('UsersAlchemyPageCtrl', ['$scope', 'training.services.user' ,function($scope, userService ) {
            
            var config = {
                          dataSource: {
                            "nodes": [
                              {
                                "id": 1
                              },
                              {
                                "id": 2
                              },
                              {
                                "id": 3
                              }
                            ],
                            "edges": [
                              {
                                "source": 1,
                                "target": 2
                              },
                              {
                                "source": 1,
                                "target": 3,
                              }
                            ]},
                          graphHeight: function(){ return 400; },
                          graphWidth: function(){ return 400; },
                          
                          linkDistance: function(){ return 40; },

                          nodeTypes: {"node_type":[ "Maintainer",
                                                    "Contributor"]},
                          nodeCaption: function(node){ 
                            return node.caption + " " + "hello" }
                          }; 
            $scope.start = function(){
                 
                // alchemy.begin(config);
                // "dataSource": some_data
                console.log('UsersAlchemyPageCtrl: begin: ' );  //,  config.dataSource.nodes
            };

            
            $scope.fetchConnection = function() {
                    console.log('getting more others: currentPage: ' + $scope.currentPage );
                    console.log('getting more others: fetch: ' + $scope.fetchBy );
                    if ( $scope.fetchBy.fetchLastName !== ""  ||  $scope.fetchBy.fetchFirstName !== "" ) {

                        userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize, $scope.fetchBy ,  function(err, nxPageResults) {
                            if(err) {

                                console.log("ERROR: userNuConnect error");

                                $scope.message = "Pagination error - userNuConnect";

                            } else {

                                console.log("INFO: new page loaded: " +  $scope.currentPage   );
                                // var totCount =   nxPageResults.length - 4850;
                                // var nxPage = 
                                // for(var i = 0; i < totCount  ; i++) {
                    
                                $scope.others = nxPageResults["data"];

                                // userService.others =  nxPageResults;  
                                $scope.othersTotalPossible =  $scope.others.length;   
                               
                            }

                        });    
                    };     
            };
            
    }]);
    /*
     * Main Controller
     */
    angular.module('training.controllers').controller('MainCtrl', ['$scope', 'training.services.user', function($scope, userService) {

        userService.init();
        
    }]);
}());