
/* VERSION 1  Couldn't get to display , code was loaded correctly it appeard, but no display  */


(function () {

  'use strict';

  angular.module('training.directives').directive('dirUserProfileEdit', ['$window', function( $window ) {
      console.log("DEBUG:training.directives.dir-userprofileedit start: ");
      return {
  
        template: '<div class="modal show" >' + 
            '<div class="modal-dialog">' + 
              '<div class="modal-content">' + 
                '<div class="modal-header">' + 
                  '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                  '<h4 class="modal-title">{{ title }}</h4>' + 
                '</div>' + 
                '<div class="modal-body" ng-transclude></div>' + 
              '</div>' + 
            '</div>' + 
          '</div>',
        restrict: 'E',
        transclude: true,
        replace:true,
        scope:true,
        link: function postLink(scope, element, attrs) {
          scope.title = attrs.title;
            
          scope.$watch(attrs.visible, function(value){
            console.log("DEBUG: dir-userprofileedit watch elements,attrs.visible, value: **********************",  element, attrs.visible, value );
          
            if(value == true)
              $(element).modal('show');
            else
              $(element).modal('hide');
          });

          $(element).on('shown.bs.modal', function(){
            console.log("DEBUG: dir-userprofileedit element.on shown.bs.modal: ", element);
            scope.$apply(function(){
              scope.$parent[attrs.visible] = true;
            });
          });

          $(element).on('hidden.bs.modal', function(){
            console.log("DEBUG: dir-userprofileedit element.on hidden.bs.modal: ", element);
            scope.$apply(function(){
              scope.$parent[attrs.visible] = false;
            });
          });
        }
      };  // eof return
   
   }]); // eof anglar modual
}()); // eof function
