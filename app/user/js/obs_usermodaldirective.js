(function () {

    "use strict";
    /* git hub  https://github.com/britztopher/genric-modal/blob/master/directives/directives.js */

    //var module = angular.module('training.directives', ['ui.bootstrap'])
    //var module = angular.module('brilliantDirectives', ['ui.bootstrap']);
    //var module = angular.module('training.directives', ['ui.bootstrap']);
    
    //var templateDir = 'app/user/partials/';
    var templateDir = 'user/partials/';

    /*
     custom directive to use as a generic modal window.
     Usage:
     <div>
     <my-modal instance_template='butterflies' use_ctrl="ButterFliesCtrl"></my-modal>
     </div>
     Attributes:
     instance_template: the tempalate name minus the .tpl.html extension.  This assumes the template is in the
     /js/templates/ folder/
     use_ctrl: this is the controller you want to be passed to be used in your modal instance if its located outside of this
     directive then you will need to inject the module it belongs in so this can find it
     The above example decalares a modal window where the template is found at js/templates/modal_instance.html and uses the
     controller MyModalInstanceCtrl
     */
    //module('training.directives', ).directive('usermodal', function( $modal ) {
    angular.module('training.directives').directive('usermodal', function( $modal){
        console.log('Modal loaded at ************************** Start-tmpl : ', templateDir );
         
        return {
            transclude: true,
            restrict: 'EA',
            //replace: true,
            template: '<a ng-click="open()" ng-transclude=true>{{name}}</a>',
            //template: "<div class='ng-modal' ng-click='open()' ng-transclude ><div class='ng-modal-overlay' ng-click='hideModal()'></div><div class='ng-modal-dialog' ng-style='dialogStyle'><div class='ng-modal-close' ng-click='hideModal()'>X</div><div class='ng-modal-dialog-content' ></div></div></div>",
            scope: {
                useCtrl: "@",
                email: "@",
                showModal: "@",
                usersedititem: "@"
            },
            link: function(scope, element, attrs) {

                console.log('Attrs -  scope.usersEditItem: ', attrs, attrs.usersedititem );
                //console.log('SCOPE: ', scope);

                scope.open = function(){

                    console.log('Modal Opened ************************** : ',templateDir, attrs.usersedititem ); 
                    var modalInstance = $modal.open({
                        templateUrl: templateDir+attrs.instanceTemplate +'.html',
                        controller:  scope.useCtrl,
                        size: 'lg',
                        windowClass: 'app-modal-window',
                        backdrop: true,
                        resolve: {
                            custDat: function(){
                                return {email: scope.email, edititem: scope.usersedititem};
                            }
                        }

                    });

                    modalInstance.result.then(function(){
                        console.log('Finished');
                    }, function(){
                        console.log('Modal dismissed at : ' + new Date());
                    });
                };
            }
        };
    });

    angular.module('training.controllers').controller('UserModalCtrl', [ '$scope','$modalInstance',  'training.services.user', 'custDat', function($scope, $modalInstance, userService, custDat){
        //add the scop
       console.log('UserModalCtrl started - $scope custDat: ',$scope, custDat);
       
        $scope.usersEditItem = custDat.edititem ;
        
        console.log("DEBUG: UserModalCtrl : user EDITITEM -> loaded ", $scope.usersEditItem );
            
        //$scope.custDat = custDat;
         
        $scope.ok = function(){
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
        
        function loadUserItem( ndx ) {

           var tmp = {};
           var tmp = custDat.edititem;
           return  tmp;

        };    
        
    }]);


}()); // eof function

