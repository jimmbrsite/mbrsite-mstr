(function () {

    "use strict";
    /* git hub  https://github.com/britztopher/genric-modal/blob/master/directives/directives.js */

    var templateDir = 'user/partials/';

    /*
     custom directive to use as a generic modal window.
     Usage:
     <div>
     <my-modal instance_template='butterflies' use_ctrl="ButterFliesCtrl"></my-modal>
     </div>
     Attributes:
     instance_template: the tempalate name minus the .tpl.html extension.  This assumes the template is in the
     /js/templates/ folder/
     use_ctrl: this is the controller you want to be passed to be used in your modal instance if its located outside of this
               directive then you will need to inject the module it belongs in so directive can find it.
    
     scope:    useCtrl: "@",
               userShow: "@",
               userItem: "@",
               setProperty: '&',
               sel???Var: '='
        varibles passed into directive via scope can include these types:
        (see this link for examples and explainations )  http://www.sitepoint.com/practical-guide-angularjs-directives-part-two/
                                                
           ( @ )  One way text binding, ( = ) Two way Binding to parent scope via , 
           ( & ) Execute functions in Parent scope. Using this example in parent
                    in parent Control:
                        $scope.change=function(){
                           return $scope.colors[Math.floor((Math.random()*3))];
                         }

                    in HTML:
                         <input type="text" ng-model="color" placeholder="Enter a color"/>
                         <hello-world color="color" change-color='change()'/>   
                     
                    in directive:
                         scope it like this:  scope{ changeColor:'&' }
                          
                    in child Control:
                         when call is made use this syntax: scope.changeColor();
                         
                    result: which will execute parent function:  $scope.change() 

     The above example decalares a modal window where the template is found at js/templates/modal_instance.html and uses the
     controller MyModalInstanceCtrl
   
     */

    /*  
     NOTE: Tried to this in user-administration.html to launch modal that would return object of biz categories
     <span class="dropdown" userctrlbtn use_ctrl="UserBtnCtrl" use-item="upricategory" set-item="setUMChoices( )" instance_template="user-category" > 
                                  <input type="text" hidden ng-model="upricategory">
                                  <button ng-click="open()" href class="btn btn-success btn-md rcs"  >
                                      {{upricategory || 'Select Primary Business Category'}}
                                  </button>
                                </span>  
    */
    angular.module('training.directives').directive('userctrlbtn', function( $modal){
        console.log('DEBUG: userctrlbtn directive loaded at ************************** Start-tmpl : ', templateDir );
         
        return {
            transclude: true,
            restrict: 'EA',
            //replace: true,
            template: '<a ng-click="open()" ng-transclude=true>{{job}}</a>',
            //template: '<button type="button" class="btn btn-primary rcs" ng-click="userEditForm.$show()" ng-hide="userEditForm.$visible"  ng-transclude=true>
            //                     Edit This User
            //              </button>',
            scope: {
                useCtrl: "@",
                useItem: "=upricategory",
                setItem: '&'
            },
            link: function(scope, element, attrs) {

                console.log('DEBUG: userctrlbtn Attrs -  scope.userShow: ', attrs );
                //console.log('SCOPE: ', scope);

                scope.open = function(){

                    console.log('DEBUG: userctrlbtn Modal ctrl pre open ************************** : ',templateDir, attrs.instanceTemplate  ); 
                    console.log('DEBUG: userctrlbtn Modal ctrl pre scope ************************** : ', scope ); 
                    

                    var modalInstance = $modal.open({
                        templateUrl: templateDir+attrs.instanceTemplate +'.html',
                        controller:  scope.useCtrl,
                        size: 'lg',
                        windowClass: 'app-modal-window',
                        backdrop: true,
                        resolve: {
                            ctrldata: function(){
                                return { dirdata: scope.useItem};
                            }
                        }

                    });

                    modalInstance.result.then(function(){
                        console.log('Finished');
                    }, function(){
                        console.log('DEBUG: userctrlbtn modal dismissed at : ' + new Date());
                    });
                    
                    //scope.setItem( "param", "hello" );
                    
                    scope.$watch('upricategory',function(changedVal){
                        console.log('DEBUG userctrlbtn Modal dir upricategory  changed detected ************ ', changedVal );
                        // executing parent function
                        scope.setItem( "param", "changedVal" );
                    });
                };    
            }
        };
    });

    angular.module('training.controllers').controller('UserBtnCtrl', [ '$scope','$modalInstance',  'training.services.user', 'ctrldata', function($scope, $modalInstance, userService, ctrldata ){
       
        console.log('DEBUG: userctrlbtn Modal ctrl started - $scope ctrldata: ',$scope, ctrldata);
        
        // example data  var edItem = {"street2":"","loginId":"349940297","lastName":"Aaron","LabelId":"acd","organizationName":"","website":"","street":"Po Box 62","zipcode":"62915","state":"IL","userRole":"user","password":"Leslie62915","city":"Cambria","userType":"admin","mobilephone":"","email":"","bioinfo":"","providerId":"349940297","firstName":"Leslie ","keyId":"","contacthone":"(618) 985-2803","_id":49700};  
        
        //var userRole = UserService.getPermissions();  

        // scope.setUMChoices

       // var catData = JSON.parse(ctrldata);
       
        //$scope.userCtrl = edItem; 
        var orgCategory = ["Accountants/Bookkeepers"
                             ,"Advertising & Marketing"
                             ,"Antique Lumber/Flooring"
                             ,"Balsam/Cullowhee Accommodations"
                             ,"Baot Rentals/Sales"
                             ,"Beauty Salons/Nails"
                             ,"Bedding/Furniture"
                             ,"Blasting/Drilling"
                             ,"Blinds/Shutters"
                             ,"Boat Charters & Tours"
                             ,"Chambers of Commerce"
                             ,"Chiropractic/Accupuncture"
                             ,"Christmas Tree Farms"
                             ,"Cleaning Services"
                             ,"Clothing & Accessories"
                             ,"Coffee House/Expresso Bar"
                             ,"Financial Planning/Investments"
                             ,"Floor Coverings"
                             ,"Florists/Plants/Gifts"
                             ,"Furniture & Home Décor"
                             ,"Gasoline"
                             ,"Gift Shops/Crafts/ Home Décor"
                             ,"Gift Shops/Crafts/Home"
                             ,"Gem Mining"
                             ,"Golf Courses - Public"];
        
        console.log("DEBUG: userctrlbtn ctrl : catdata -> loaded ",  ctrldata);
            
        //$scope.custDat = custDat;
        //$scope.setumchoices
        

        $scope.selTypes = orgCategory;

        

        $scope.setUChoice = function( param, choice) {
            // set user radio style ( single selection) choices
             
           console.log("DEBUG: userctrlbtn ctrl : setUCHoice",  choice);
        }; 
       
          
        $scope.ok = function(){
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };


        $scope.close = function(){
            $modalInstance.close();
        };
                
    }]);


}()); // eof function

