angular.module('training.directives').directive('userTrp', function( $modal ){

    console.log('userTrp Modal loaded:  user/directives/userTrp.js *********** Start-tmpl : ', templateDir );
     
    return{
	    scope:{
	      color: '=',	
	      trp:'=',
	      changeColor:'&',
	    },
	    restrict: 'AE',
	    replace: true,
	    template: '<p style="background-color:{{color}}">{{trp}}</p>',
	    link: function(scope,elem,attrs){
	      elem.bind('click',function(){
	        scope.$apply(function(){
	          scope.color="white";
	        });
	      });
	      elem.bind('mouseover',function(){
	        elem.css('cursor','pointer');
	        scope.$apply(function(){
	          scope.trp=scope.changeColor();
	        });
	      });
	      scope.$watch('trp',function(changedVal){
	        console.log('changed detected');
	      });
	    }
    }
});



// app.controller('MainCtrl', function($scope) {
  
//   $scope.colors=['red','orange','green'];
  
//   $scope.change=function(){
//     return $scope.colors[Math.floor((Math.random()*3))];
//   }
  
// });

