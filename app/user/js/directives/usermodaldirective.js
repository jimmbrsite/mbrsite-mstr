(function () {

    "use strict";
    /* git hub  https://github.com/britztopher/genric-modal/blob/master/directives/directives.js */

    //var templateDir = 'app/user/partials/';
    var templateDir = 'user/partials/';

    /*
     custom directive to use as a generic modal window.
     Usage:
     <div>
     <my-modal instance_template='butterflies' use_ctrl="ButterFliesCtrl"></my-modal>
     </div>
     Attributes:
     instance_template: the tempalate name minus the .tpl.html extension.  This assumes the template is in the
     /js/templates/ folder/
     use_ctrl: this is the controller you want to be passed to be used in your modal instance if its located outside of this
     directive then you will need to inject the module it belongs in so this can find it
     The above example decalares a modal window where the template is found at js/templates/modal_instance.html and uses the
     controller MyModalInstanceCtrl

     <!--  original 
      <usermodal ng-click="getSelectedUser( $index )" name='UserEditForm' instance_template="user-edit" usersEditItem="{{usersList[$index]}}" use_ctrl="UserModalCtrl" class="btn-danger btn-sm rcs">
            <span style="color:#ffffff;text-decoration a:hover:none;">Edit</span>
      </usermodal>
     -->      

     */
    //module('training.directives', ).directive('usermodal', function( $modal ) {
    angular.module('training.directives').directive('usermodal', function( $modal){

        console.log('Modal loaded:  user/directives/usermodaldirectives.js *********** Start-tmpl : ', templateDir );
         
        return {
            transclude: true,
            restrict: 'EA',
            //replace: true,
            template: '<a ng-click="open()" ng-transclude=true{{trp}}</a>',
            scope: {
                useCtrl: "@",
                //usersedititem: "@",
                 usersEditItem: "@" ,
                 setProperty: '&',
                 trp: '='
               
            },
            link: function(scope, element, attrs) {

                console.log('DEBUG Attrs -  scope.usersEditItem: ', attrs, attrs.usersedititem );
               
                scope.open = function(){

                    console.log('DEBUG modal Opened: user/directives/usermodaldirectives.js ******* : ', templateDir, attrs.usersEditItem ); 
                    var modalInstance = $modal.open({
                        templateUrl: templateDir+attrs.instanceTemplate +'.html',
                        controller:  scope.useCtrl,
                        size: 'lg',
                        windowClass: 'app-modal-window',
                        backdrop: true,
                        resolve: {
                            custDat: function(){
                                return { edititem: scope.usersEditItem, trpDat: scope.trp };   //, propertylist: scope.usersproperty
                            }
                        }

                    });

                    modalInstance.result.then(function(){
                        //wrapup for update logic
                        console.log('Finished');
                    }, function(){
                        // add finish up logic here  perhaps clear scope .. etc
                      

                        console.log('Modal dismissed at : ' + new Date());
                    });
                };
            }
        };
    });

    angular.module('training.controllers').controller('UserModalCtrl', [ '$scope', '$location', '$modalInstance',  'training.services.user', 'custDat', function($scope, $location, $modalInstance, userService, custDat){
       
        console.log('DEBUG UserModalCtrl started - $scope custDat: ',$scope, custDat);
        
        // example data  var edItem = {"street2":"","loginId":"349940297","lastName":"Aaron","LabelId":"acd","organizationName":"","website":"","street":"Po Box 62","zipcode":"62915","state":"IL","userRole":"user","password":"Leslie62915","city":"Cambria","userType":"admin","mobilephone":"","email":"","bioinfo":"","providerId":"349940297","firstName":"Leslie ","keyId":"","contacthone":"(618) 985-2803","_id":49700};  
        //var edItem = JSON.parse(custDat.edititem);
          
        var edItem =  userService.usersEditItem;

        $scope.usersedititem = edItem; 
        
        console.log("DEBUG: UserModalCtrl scope.usersedititem **************** : ", $scope.usersedititem );
        
        var trps = [{"types": { Root: "Root", Sysadmin: "Sysadmin", Member: "Member", Associate: "Associate", Subscriber: "Subscriber", Advertiser:"Advertiser"}},
                    {"roles": { Owner: "Owner", Manager: "Manager",  Staff: "Staff", Volunteer: "Volunteer" }},
                    {"permissions": { Publisher: false, Editor: false, Support: true  }}];
                    
        $scope.utype = $scope.usersedititem.typeLabel;
        $scope.urole = $scope.usersedititem.role;
        $scope.uperm = $scope.usersedititem.permissions;
        
        $scope.selTypes = Object.keys(trps[0]['types']);
        $scope.selRoles = Object.keys(trps[1]['roles']);
        $scope.selPermissions = Object.keys(trps[2]['permissions']);
        
        
        $scope.setUSChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'urole':
                     $scope.urole = $scope.selRoles[choice];
                    break;
                case 'utype':
                    $scope.utype =  $scope.selTypes[choice];
                    break;
                case 'uperm':
                    $scope.uperm = $scope.selPermissions[choice];
                    break;    
                default:
                    console.log("ERROR: UserProfileCtrl setUSChoices  *************************", choice );    
            }
            return 
        }; 


        console.log("DEBUG: UserModalCtrl : user EDITITEM -> loaded ",  edItem);
            
        //$scope.custDat = custDat;
         
        $scope.ok = function(){
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $scope.usersedititem = ""; 
            //$scope.$apply();
            $modalInstance.dismiss('cancel');
        };
        
        // // NOTE : If delete is allowed at this button then we need to clean up connections etc.
        // //        This user would need to be logged out as well..  
        $scope.delete = function () {
        //     // using need to fix api function to accept id or object

             console.log("DEBUG: UserModalCtrl : Deleting User firstName, id:   ",  $scope.usersedititem.firstName, $scope.usersedititem._id );
             // ask if they are sure and explain the consequense
             
             if (confirm("DELETING USER WILL HAVE THESE OUTCOMES:\n\nComponent relationships:\n a.) This users registration with components is removed.\n(b.) Components WITHOUT registered users are flagged for deleting.\n\nAre you sure you want to continue with USER DELETE?\n\nNOT COMPLETE YET")){
                userService.deleteUser($scope.usersedititem.keyId, "keyId" , function(err, status) {

                   if(err) {

                       console.log("ERROR: delete user");
                   }
                   else {

                       console.log("INFO: deleted user");

                       //$scope.users.splice(index, 1);
                       // SOME WORK HERE: If allowed 
                       // need to force reload of last search - this user should not be there  


// ******************
//    ADD CLEAN UP LOGIC FOR SEARCH LIST WHEN ROW DELETED
// ******************************
                       //$location.path('/adminconsole');
                       $scope.usersedititem = ""; 
                       //$scope.$apply();
                       $modalInstance.dismiss('cancel');
                   }
                });

              }
              else
              {
                  alert("Cancel clicked");
              } 
        };

        
    }]);


}()); // eof function

