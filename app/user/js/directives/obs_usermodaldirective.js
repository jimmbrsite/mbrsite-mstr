(function () {

    "use strict";
    /* git hub  https://github.com/britztopher/genric-modal/blob/master/directives/directives.js */

    //var module = angular.module('training.directives', ['ui.bootstrap'])
    //var module = angular.module('brilliantDirectives', ['ui.bootstrap']);
    //var module = angular.module('training.directives', ['ui.bootstrap']);
    
    //var templateDir = 'app/user/partials/';
    var templateDir = 'user/partials/';

    /*
     custom directive to use as a generic modal window.
     Usage:
     <div>
     <my-modal instance_template='butterflies' use_ctrl="ButterFliesCtrl"></my-modal>
     </div>
     Attributes:
     instance_template: the tempalate name minus the .tpl.html extension.  This assumes the template is in the
     /js/templates/ folder/
     use_ctrl: this is the controller you want to be passed to be used in your modal instance if its located outside of this
     directive then you will need to inject the module it belongs in so this can find it
     The above example decalares a modal window where the template is found at js/templates/modal_instance.html and uses the
     controller MyModalInstanceCtrl
     */
    //module('training.directives', ).directive('usermodal', function( $modal ) {
    angular.module('training.directives').directive('usermodal', function( $modal){
        console.log('Modal loaded  user/dirctvs/usermodaldirectv.js **************** Start-tmpl : ', templateDir );
         
        return {
            transclude: true,
            restrict: 'EA',
            //replace: true,
            template: '<a ng-click="open()" ng-transclude=true>{{name}}</a>',
            scope: {
                useCtrl: "@",
                usersedititem: "@"
            },
            link: function(scope, element, attrs) {

                console.log('Attrs -  scope.usersEditItem: ', attrs, attrs.usersedititem );
                //console.log('SCOPE: ', scope);

                scope.open = function(){

                    console.log('Modal Opened ************************** : ',templateDir, attrs.usersedititem ); 
                    var modalInstance = $modal.open({
                        templateUrl: templateDir+attrs.instanceTemplate +'.html',
                        controller:  scope.useCtrl,
                        size: 'lg',
                        windowClass: 'app-modal-window',
                        backdrop: true,
                        resolve: {
                            custDat: function(){
                                return { edititem: scope.usersedititem};
                            }
                        }

                    });

                    modalInstance.result.then(function(){
                        console.log('Finished');
                    }, function(){
                        console.log('Modal dismissed at : ' + new Date());
                    });
                };
            }
        };
    });

    angular.module('training.controllers').controller('UserModalCtrl', [ '$scope','$modalInstance',  'training.services.user', 'custDat', function($scope, $modalInstance, userService, custDat){
       
        console.log('UserModalCtrl started - $scope custDat: ',$scope, custDat);
        
        // example data  var edItem = {"street2":"","loginId":"349940297","lastName":"Aaron","LabelId":"acd","organizationName":"","website":"","street":"Po Box 62","zipcode":"62915","state":"IL","userRole":"user","password":"Leslie62915","city":"Cambria","userType":"admin","mobilephone":"","email":"","bioinfo":"","providerId":"349940297","firstName":"Leslie ","keyId":"","contacthone":"(618) 985-2803","_id":49700};  
        var edItem = JSON.parse(custDat.edititem);
       
        $scope.usersedititem = edItem; 
        
        console.log("DEBUG: UserModalCtrl : user EDITITEM -> loaded ",  edItem);
            
        //$scope.custDat = custDat;
         
        $scope.ok = function(){
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
        
        // $scope.loadUserItem = function( ndx ) {

        //    var tmp = {};
        //    var tmp = custDat.edititem;
        //    return  tmp;

        // };    
        
    }]);


}()); // eof function

