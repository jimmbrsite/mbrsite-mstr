(function () {

    "use strict";
    /* git hub  https://github.com/britztopher/genric-modal/blob/master/directives/directives.js */

    //var module = angular.module('training.directives', ['ui.bootstrap'])
    //var module = angular.module('brilliantDirectives', ['ui.bootstrap']);
    //var module = angular.module('training.directives', ['ui.bootstrap']);
    
    //var templateDir = 'app/user/partials/';
    var templateDir = 'user/partials/';

    /*
     custom directive to use as a generic modal window.
     Usage:
     <div>
     <my-modal instance_template='butterflies' use_ctrl="ButterFliesCtrl"></my-modal>
     </div>
     Attributes:
     instance_template: the tempalate name minus the .tpl.html extension.  This assumes the template is in the
     /js/templates/ folder/
     use_ctrl: this is the controller you want to be passed to be used in your modal instance if its located outside of this
     directive then you will need to inject the module it belongs in so this can find it
     The above example decalares a modal window where the template is found at js/templates/modal_instance.html and uses the
     controller MyModalInstanceCtrl
     */
    //module('training.directives', ).directive('usermodal', function( $modal ) {
    angular.module('training.directives').directive('contenteditable', function(){
        console.log('contenteditable loaded at ************************** Start-tmpl : ', templateDir );
         
        return {
            transclude: true,
            restrict: 'A',
            require: "ngModel",
            template: '<a ng-click="open()" class="editor" ng-transclude=true> <i  class="fa fa-pencil"></i></a>', 
            replace: true,
            scope: {
                useCtrl: "@",
                userShow: "@"
            
            },
            link: function(scope, element, attrs, ngModel ) {

                console.log('userctrlbtn Attrs -  scope.userShow: ', attrs );
                //console.log('SCOPE: ', scope);
                
                function read() {
                    ngModel.$setViewValue(element.html());
                }

                ngModel.$render = function() {
                    element.html(ngModel.$viewValue || "");
                };

                element.bind("blur keyup change", function() {
                    scope.$apply(read);
                 });   
                
                //scope.open = function(){

                //    console.log('contenteditable Modal Opened ************************** : ',templateDir, scope.userShow  ); 
                    
                
                
                //     var modalInstance = $modal.open({
                //         templateUrl: templateDir+attrs.instanceTemplate +'.html',
                //         controller:  scope.useCtrl,
                //         size: 'lg',
                //         windowClass: 'app-modal-window',
                //         backdrop: true,
                //         resolve: {
                //             itemdata: function(){
                //                 return { ctrldata: scope.userShow};
                //             }
                //         }

                //     });

                //     modalInstance.result.then(function(){
                //         console.log('Finished');
                //     }, function(){
                //         console.log('userctrlbtn Modal dismissed at : ' + new Date());
                //     });
                // };
            }
        };
    });

    angular.module('training.controllers').controller('UserBtnCtrl', [ '$scope','$modalInstance',  'training.services.user', 'itemdata', function($scope, $modalInstance, userService, itemdata){
       
        console.log('userctrlbtn Modal started - $scope itemdata: ',$scope, itemdata);
        
        // example data  var edItem = {"street2":"","loginId":"349940297","lastName":"Aaron","LabelId":"acd","organizationName":"","website":"","street":"Po Box 62","zipcode":"62915","state":"IL","userRole":"user","password":"Leslie62915","city":"Cambria","userType":"admin","mobilephone":"","email":"","bioinfo":"","providerId":"349940297","firstName":"Leslie ","keyId":"","contacthone":"(618) 985-2803","_id":49700};  
        
        var userRole = UserService.getPermissions();  



        var edItem = JSON.parse(itemdata.ctrldata);
       
        $scope.userCtrl = edItem; 
        
        console.log("DEBUG: userctrlbtn : userRole -> loaded ",  userRole);
            
        //$scope.custDat = custDat;
         
        $scope.ok = function(){
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
        
        // $scope.loadUserItem = function( ndx ) {

        //    var tmp = {};
        //    var tmp = custDat.edititem;
        //    return  tmp;

        // };    
        
    }]);

}()); // eof function

