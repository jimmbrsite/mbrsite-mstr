(function() {

    'use strict';

    /*
     * Users Ctrl
     */
    angular.module('training.controllers').controller('UsersCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {

        $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }         
        };

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };
        // deletes user from a list
        $scope.delete = function(index) {
             
            /* NOTE: idType is either :
            *      "keyId" or loginId or "objectId"
            *
            ****  2014/07  currently dynamic idType is not enabled for this function
            ****  
            *
            * userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {
            * userService.deleteUser($scope.users[index].id, "loginId", function(err, status) {    
            * userService.deleteUser($scope.users[index].keyId, "keyId", function(err, status) {  
            * 
            */
            
            userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {    

                if(err) {

                    console.log("ERROR: delete user");
                }
                else {

                    console.log("INFO: deleted user");
                    $scope.users.splice(index, 1);
                }
            });
        };

        $scope.getUser = function() {

            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }
        };

        var user = userService.getUser();

        if(user && user.authToken) {

            userService.getUsers(function(err, users) {

                $scope.users = users;
                
                userService.saveLocalUsers(users);

            });
            
        }
        else {

            $location.path('/home');
        };
            
    }]);

    /*
     * User Edit Ctrl
     *   the user edit ctrl collects and sets different content in $scope.user as determined by userId
     */
    angular.module('training.controllers').controller('UsersEditCtrl', ['$scope', '$routeParams', '$location','training.services.user', function($scope, $routeParams, $location, userService) {
       
        console.log("DEBUG: usersEdit params : ");
        console.log( $routeParams )
        
        var userId = $routeParams.userId;
        // var userTab=  $routeParams.tabs | '';

        //$scope.meals = [];    // from examples

        $scope.user = userService.getLocalUser(userId);
        
        console.log("DEBUG: userControl.userTabrouteParams : routeParams  ", $routeParams);
       
        // DEBUG alert( userId );

        
        if (!$scope.user) {

            $location.path('/users');
        }
        else {

            var options = { 'types' : ['FOLLOWS'],          
                            'direction' : "out" };

            userService.findConnections( userId, options, function( err, results ) {
                
                if(err) {

                    console.log("ERROR: userService.findConnections : err  ", err);
                }
                else {

                    console.log("INFO: userService.findConnections : results: ", results );
                    

                    $scope.connections = results;
          
                               
                }
            });            

            
        }

      
        console.log("INFO: userEditCtrl: userService.getLocalOthers : scope others: ", $scope.others );
        
        // local User pagination settings
        $scope.currentPage = 1;
        $scope.pageSize =  25;  // this should match however many users you want on one page
        // asynchronous User pagination settings
        $scope.totalUsers = 0;
        $scope.usersPerPage = 25; // this should match however many results your API puts on one page
        // load first page into list
            // userService.getResultsPage(1);
        $scope.others = userService.getLocalOthersPage(1);  // load first page of user not connected to.
        
        $scope.pageChangeHandler = function(num) {
            console.log('header page changed to ' + num);
            userService.getResultsPage( num );
        };
        


        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };
        
        $scope.save = function() {

            userService.saveUser($scope.user, function(err, status) {

                if(err) {

                    console.log("ERROR: save user");
                }
                else {

                    console.log("INFO: saved user");
                    
                    $location.path('/users');
                }
            });
        };
        // delete user using form button etc
        $scope.delete = function () {
            // using need to fix api function to accept id or object
            userService.deleteUser($scope.user._id, "objectId", function(err, status) {

                if(err) {

                    console.log("ERROR: delete user");
                }
                else {

                    console.log("INFO: deleted user");

                    //$scope.users.splice(index, 1);
                    $location.path('/users');
                }
            });
        };

        // connect user to user

        $scope.addConnection = function (index) {
            console.log("DEBUG: making connection from root user");
            console.log( $scope.user._id);
            console.log("DEBUG: making connection to other user");
            console.log($scope.others[index]._id);

            var rootUser = {'Id': $scope.user._id,
                            'idType': 'objectId' };

            var otherUser = {'otherId': $scope.others[index]._id,
                              'idType': 'objectId' };

            var relationShip = { 'relationType' : 'FOLLOWS',
          
                                 'relationData' : { 'otherKeyId' : $scope.others[index].keyId } }; 
                              
            userService.connectUser( rootUser, otherUser , relationShip, function(err, results ) {
                
                if(err) {

                    console.log("ERROR: making connection to other user");
                    console.log( results );
                    console.log( Object.keys(results).length )
                }
                else if ( [results].length ===  1 ) {

                    console.log("DEBUG: made connection to other user: results: ", results );

                    $scope.connections.push( results );
                    
                    $location.path('/users/edit/' + rootUser.Id );
                }
                else {
                    console.log("INFO: made connection to other user");
                    $location.path('/users');
                }

            });
        };

        // drop  user to user connection 
        $scope.dropConnection = function (index) {
           console.log("ERROR: disconnecting from this user");
           console.log($scope.connections[index]._id); 

           var rootUser = {'Id': $scope.user._id,
                            'idType': 'objectId' };

            var connectedUser = {'connectedId': $scope.connections[index]._id,
                              'idType': 'objectId' };

            var options = { 'types' : ['FOLLOWS'],          
                            'direction' : "out" };

           userService.disconnectUser(rootUser, connectedUser , options , function(err, result) {

                console.log("DEBUG: dropConnection : disconnectUser : result :", result );
                console.log("DEBUG: dropConnection : disconnectUser : result :", connectedUser.connectedId );

                if(err) {

                    console.log("ERROR: disconnecting from this connected user");
                }
                else if ( result == connectedUser.connectedId  ) {  //   
                    
                    console.log("DEBUG: dropConnection : disconnectUser : results :", result ); 
                    $scope.connections.splice(index, 1);
                    
                    $location.path('/users/edit/' + rootUser.Id);           
                } 
                else {
                    // $scope.users.splice(index, 1);
                    $location.path('/users');
                }
           });
        };

        $scope.cancel = function () {
            
            console.log("INFO: canceled user update");
            
            $location.path('/users/profile/5');
            
                
        };

    }]);

    /*
     * User Create Ctrl
     */
    angular.module('training.controllers').controller('UsersCreateCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {

        $scope.user = {
            '_id' : '',
            'keyId' : '', 
            'loginId' : '',
            'firstName' : '',
            'lastName' : '',
            'password' : '',
            'userRole': ''  // admin', 'manager', 'user', ...
        };

        $scope.connected = {
            '_id' : '',
            'keyId' : '', 
            'loginId' : '',
            'firstName' : '',
            'lastName' : '',
            'password' : '',
            'userRole': ''  // admin', 'manager', 'user', ...
        };

        $scope.other = {
            '_id' : '',
            'keyId' : '', 
            'loginId' : '',
            'firstName' : '',
            'lastName' : '',
            'password' : '',
            'userRole': ''  // admin', 'manager', 'user', ...
        };
        

        $scope.create = function() {

            userService.createUser($scope.user, function(err, status) {

                if(err) {

                    console.log("ERROR: create user");
                }
                else {

                    console.log("INFO: Created User");
                    $location.path('/users');
                }
            });
        };

        $scope.cancel = function () {
            
            console.log("INFO: canceled user create");
            
            $location.path('/users');
                
        };
    }]);

    /*
     * User login Ctrl
     */
    angular.module('training.controllers').controller('UsersLoginCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location,    userService) {

        $scope.user = {

            'loginId' : '',
            'password' : ''
        };

        $scope.message = "";

        $scope.login = function() {

            $scope.message = "";

            userService.loginUser($scope.user, function(err, status) {

                if(err) {

                    console.log("ERROR: login user");

                    $scope.message = "Authentication Error";
                }
                else {

                    console.log("INFO: Logged In User");
                    
                    $location.path('/users');
                }
            });
        };
    }]);

    /*
     * userPaging Controller
     */
    angular.module('training.controllers').controller('userPagingCtrl', ['$scope', 'training.services.user', function($scope, userService) {

        $scope.pageChangeHandler = function(num) {
                 console.log('going to page ' + num);
                 userService.getResultsPage( num );     
        };
        
    }]);
    /*
     * Main Controller
     */
    angular.module('training.controllers').controller('MainCtrl', ['$scope', 'training.services.user', function($scope, userService) {

        userService.init();
        
    }]);
}());