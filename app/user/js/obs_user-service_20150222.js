(function() {

    'use strict';

    /*
     * API:
     *
     * getUsers : callback
     * createUser : Object e.g. {"firstName":"Thomas, "lastName":"Amsler}
     */
    angular.module('training.services').factory('training.services.user', ['$http', function($http) {

        var UserService = {

            'users' : [],
            'user' : {},     // name:'hello' 
            'connections' : [],
            'connected' : {},
            'others' : [],
            'other' : {},
            'fetchBy' :{},
            'usersList' : [],
            'usersListItem' : {},
            'usersEditItem' : {},
            'preferences':[],
            'userMenus': {},     
            'dfltTemplates': [],
            'adminTemplates':[],
            'userTemplates':[]

        };

        UserService.getMenu = function() {
              
             return UserService.userMenus;      
             
        };

        UserService.getTemplates = function() {
          
             return UserService.userTemplates;      
        };

        UserService.signedIn = function() {
            if (angular.fromJson(sessionStorage.getItem("user")) !== null)  {
                // use this bypass... if (angular.fromJson(sessionStorage.getItem("user")) === null)  {  
                 console.log("INFO: User is signedIn");
                 return true;
            } else {
                console.log("INFO: User is not signedIn");
                return false;
            };    
           
        };

        UserService.logout = function() {
            UserService.user = {};
            UserService.connected = {};
            UserService.other = {};
            UserService.connections = [],
            UserService.others = [],
            UserService.users = [],
            UserService.fetchBy = {},
            UserService.preferences = [];
            UserService.userMenus=[];
            UserService.tmplates = [];
            sessionStorage.removeItem("user");
            sessionStorage.removeItem("userMenus");
            sessionStorage.removeItem("userTemplates");
            
        };
        
        UserService.saveLocalUsers = function(users) {

            console.log("INFO: save local userUser");
            console.log(users);
    
            this.users = users;
            
        };

        UserService.getLocalUsers = function() {

            return this.users;
        };

        UserService.getLocalConnections = function() {
            ///* NOTE: needs work more work need to go back to db and get follows relationships for this active user we are editing
            //**/
            //*
            //*
            //*/ 
            var item= false;  
            var dnedata = [];
      // $scope.dnedata[0] =  nodes;
      // $scope.dnedata[1] =  lastNodeId;
      // $scope.dnedata[2] =  links;
      // $scope.dnedata[3] =  item;


            var nodes =  [{id: 0, reflexive: false, name:"Jim", dat: {city:"West Sac", zip:"95603"} }, {id: 1, reflexive: true, name:"Char", dat: {city:"West Sac", zip:"95691"}  }
                   , {id: 2, reflexive: false, name:"Harry", dat: {city:"Sac", zip:"96774"} }, {id: 3, reflexive: false, name:"Al", dat: {city:"Davis", zip:"95666"} }];
            var lastNodeId = 3;
            var links =  [{source: nodes[0], target: nodes[1], left: false, right: true },
                            {source: nodes[1], target: nodes[2], left: false, right: true },
                            {source: nodes[1], target: nodes[3], left: false, right: true }
                           ];
            //var tjbs = UserService.connections;
            dnedata[0] =  nodes;
            dnedata[1] =  lastNodeId;
            dnedata[2] =  links;
            dnedata[3] =  item;

            console.log( 'DEBUG UserService.getLocalConnections :', dnedata)

            return  dnedata;
        };
    
        UserService.getLocalOthersPage = function( newPage, pageSize, fetchBy, callback ) {
             

            console.log("DEBUG: getLocalOthersPage: fetchBy.fetchFirstName: " );
            console.log( fetchBy );
            
            if(this.user.authToken) {

                   
                $http(
                    {
                        "method" : "PUT",
                        "url"    : "/api/v1/users/fetch",
                        "headers" : { "Authorization" : "Session " + this.user.authToken
                        },
                        "data"   : { "newPage" : newPage,
                                    "pageSize" : pageSize,
                                    "fetchBy" : fetchBy
                                    
                        }         
                    })
                    .success(function(data, status, headers, config) {

                            //$scope.others= data.splice(0,250); 

                            callback(null, data );
                    })
                    .error(function(data, status, headers, config) {

                            callback(true, null);
                    });
            
            } else {

                    console.log("ERROR: getLocalOthersPage: User is not authenticated");
            }
                    
            
        };  
      
        UserService.getUser = function() {
            
            return this.user;
        };

        UserService.init = function() {

            UserService.user = angular.fromJson(sessionStorage.getItem("user"));
            UserService.userMenus = angular.fromJson(sessionStorage.getItem("userMenus"));
            
            
        };

        UserService.getLocalUser = function( ndx ) {

            console.log("INFO: userService.getLocalUser.userId: from users array: " + ndx );

            console.log("DEBUG : " , this.usersList.length );
            
            // for(var i = 0; i < this.usersList.length; i++) {
                
            //     console.log("DEBUG : " + i );

            //     if(this.usersList[i]._id.toString() === userId ) {
                    
                    UserService.usersEditItem =   UserService.usersList[ndx];

                    return UserService.usersEditItem ;

                // }
                // else {

                //    console.log( this.usersList[i]._id ); 
                // }
            //}

            // // remove comment slashes from above when retrieve feature is enabled.     
            // console.log("NOTE: userService.getLocalUser.userId: remove temp fix when data retrieve is enabled." + userId );                
            // return this.usersListItem;
        };

        // API : getUsers
        UserService.getUsers = function(callback) {

            if(this.user.authToken) {

                $http(
                    {
                        "method" : "GET",
                        "url" : "/api/v1/users",
                        "headers" : {
                            "Authorization" : "Session " + this.user.authToken
                        },
                        "cache" : false


                    })
                    .success(function(data, status, headers, config) {

                        callback(null, data.splice(0,75));
                    })
                    .error(function(data, status, headers, config) {

                        callback(true, null);
                    });
            }
            else {

                console.log("ERROR: User is not authenticated");
            }
        };

        
        // API createUser
        UserService.createUser = function(newUser, callback) {

            $http(
                {
                    "method" : "POST",
                    "url" : "/api/v1/users",
                    "data" : newUser
                })
                .success(function(data, status, headers, config) {

                    callback(false, status);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });

             console.log("ERROR: User create here");    
        };

        // API loginUser
        UserService.loginUser = function(user, callback) {

            $http(
                {
                    "method" : "POST",
                    "url" : "/api/v1/users/login",
                    "data" : user
                })
                .success(function(data, status, headers, config) {

                    UserService.user = data;
                    // temp location for admin menu - will be retrived with login get
                    var tmpmnu = {}; 
                    tmpmnu = {title: "Admins", 
                                    action: "", 
                                    menus: [
                                      {
                                        title: "My Profile",
                                        action: "#/users/profile/{{whichUserId()}}#tab1-5"
                                      },
                                      {
                                        title: "Administration",
                                        action: "#/users/administration"
                                      },
                                      {
                                        title: "Member Tabs",
                                        action: "#/users/membersite",
                                        menus: [
                                          {
                                            title: "Connections",
                                            action: "#users/profile/{{whichUserId()}}#tab2-5"
                                          },
                                          {
                                            title: "User List",
                                            action: "#users/profile/{{whichUserId()}}#tab3-5"
                                          },
                                          {
                                            title: "Stuff",
                                            action: "#users/profile/{{whichUserId()}}#tab4-5"
                                          },
                                          {
                                            title: "Stuff Stuff",
                                            action: "#users/profile/{{whichUserId()}}#tab5-5"
                                          }
                                        ]
                                      },
                                      {  title: "Calendar ",
                                        action: "#/users/calendar"
                                      },
                                      {  title: "Jobs",
                                        action: "#/users/jobs"
                                      },
                                      {  title: "Ad Manager",
                                        action: "#/users/admanager"
                                      }
                                    ]
                                  };
                    UserService.userMenus = tmpmnu;   
                    sessionStorage.setItem("userMenus", angular.toJson(tmpmnu));
                    sessionStorage.setItem("user", angular.toJson(data));
                    //setTimeout(function(){ alert("Hello"); }, 3000);
                    callback(false, status) ;   
                    
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });
        };

        // API saveUser
        UserService.saveUser = function( user, callback) {
            console.log("DEBUG UserService.saveUser  being updated: " );
            $http(
                {
                    "method" : "PUT",
                    "url" : "/api/v1/users",
                    "data" : user
                })
                .success(function(data, status, headers, config) {
                    callback(false, status);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });
        };

        /* API Delete User
        *
        * NOTE: idType is either:
        *      "keyId" or "objectId"
        *
        *   obs loginId obs
        */

        UserService.deleteUser = function(id, idType, callback) {

            
            $http(
                {
                    "method" : "DELETE",
                    "url" : "/api/v1/users/" + id,
                    "params" : {
                        "idType" : idType
                    }
                })
                .success(function(data, status, headers, config) {

                    callback(false, status);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });
        };

        /* API Connect User
        *
        * NOTE: idType is either:
        *      "keyId" or loginId or "objectId"
        *
        * rootUser = {'Id': $scope.user._id, 'idType': 'objectId' };
        *
        * otherUser = {'otherId': $scope.others[index]._id, 'idType': 'objectId' };
        * 
        * relationShip = { 'relationType' : '????', 'relationData' : '??????' };
        */   
        UserService.connectUser = function( rootUser , otherUser , relationShip, callback) {

            console.log("DEBUG UserService : Connect from root user : ", rootUser.Id  ); 
            console.log("DEBUG UserService : Connect to other user : ", otherUser.otherId  );
            console.log( otherUser );

            $http(
                {
                    "method" : "PUT",
                    "url" : "/api/v1/users/" + rootUser.Id + "/connect",
                    "data" : { "idType" : rootUser.idType,
                                 "otherId" : otherUser.otherId,
                                 "otherType" : otherUser.idType,
                                 "relationType" : relationShip.relationType,
                                 "relationData" :  relationShip.relationData 
                    }
                })
                .success(function(data, status, headers, config) {

                  
                    //sessionStorage.setItem("user", angular.toJson(data));

                    //callback(false, status);
                     callback(null, data);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });
        };

        /* API Disconnect User
        *
        * NOTE: idType is either:
        *      "keyId" or loginId or "objectId"
        *
        */
    
        UserService.disconnectUser = function(rootUser, connectedUser , options, callback) {

            console.log("DEBUG Userservice : Disconnect from root user : ", rootUser.Id  ); 
            console.log("DEBUG UserService : Disconnect connected user : ", connectedUser.connectedId  );
            console.log( connectedUser );
            
            $http(
                {
                    "method" : "PUT",
                    "url" : "/api/v1/users/" + rootUser.Id + "/disconnect",
                    "data" : { "idType" : rootUser.idType,
                               "connectedId" : connectedUser.connectedId,
                                 "otherType" : connectedUser.idType,
                                 "options" : options
                    }
                })
                .success(function(data, status, headers, config) {

                    console.log("DEBUG disconnectUser : Disconnected connected user : ", data  );    
                    callback( null , data);
                    //callback(false, status);
                })
                .error(function(data, status, headers, config) {

                    callback(true, status);
                });
        };
    
        UserService.findConnections = function( userId, options, callback) {

            if(this.user.authToken) {

                $http(
                    {
                        "method" : "PUT",
                        "url" : "/api/v1/users/" + userId + "/findconnects" ,
                        "data" : { "options" : options

                        } 
                       
                       // "headers" : {
                       //     "Authorization" : "Session " + this.user.authToken
                       // },
                       // "cache" : false
                        

                    })
                    .success(function(data, status, headers, config) {
                       
                        console.log("DEBUG UserService : findConnections : data : ", data  );  

                        callback(null, data);
                    })
                    .error(function(data, status, headers, config) {

                        callback(true, null);
                    });
            }
            else {

                console.log("ERROR: User is not authenticated");
            }
        };



        return UserService;
    }]);

}());

