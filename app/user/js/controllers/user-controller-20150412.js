(function() {

    'use strict';

    /*
     * Users Ctrl
     */
    angular.module('training.controllers').controller('UsersCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {

    
        $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            
            }         
        };

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };


        // deletes user from a list
        $scope.delete = function(index) {
             
            /* NOTE: idType is either :
            *      "keyId" or loginId or "objectId"
            *
            ****  2014/07  currently dynamic idType is not enabled for this function
            ****  
            *
            * userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {
            * userService.deleteUser($scope.users[index].id, "loginId", function(err, status) {    
            * userService.deleteUser($scope.users[index].keyId, "keyId", function(err, status) {  
            * 
            */
            
            userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {    

                if(err) {

                    console.log("ERROR: delete user");
                }
                else {

                    console.log("INFO: deleted user");
                    $scope.users.splice(index, 1);
                }
            });
        };

        $scope.getUser = function() {

            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }
        };

        var user = userService.getUser();

        if(user && user.authToken) {

            userService.getUsers(function(err, users) {

                $scope.users = users;
                
                userService.saveLocalUsers(users);

            });
            
        }
        else {

            $location.path('/home');
        };
            
    }]);
/*
    * Users Ctrl
    */
    angular.module('training.controllers').controller('UserAdminHomeCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {

    
        $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            
            }         
        };

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };


        // deletes user from a list
        $scope.delete = function(index) {
             
            /* NOTE: idType is either :
            *      "keyId" or loginId or "objectId"
            *
            ****  2014/07  currently dynamic idType is not enabled for this function
            ****  
            *
            * userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {
            * userService.deleteUser($scope.users[index].id, "loginId", function(err, status) {    
            * userService.deleteUser($scope.users[index].keyId, "keyId", function(err, status) {  
            * 
            */
            
            userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {    

                if(err) {

                    console.log("ERROR: delete user");
                }
                else {

                    console.log("INFO: deleted user");
                    $scope.users.splice(index, 1);
                }
            });
        };

        $scope.getUser = function() {

            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }
        };

        var user = userService.getUser();

        if(user && user.authToken) {

            userService.getUsers(function(err, users) {

                $scope.users = users;
                
                userService.saveLocalUsers(users);

            });
            
        }
        else {

            $location.path('/home');
        };
            
    }]);
    /***USER ADMIN  CONTROLLER 
    *
    * UserAdminCtrl
    */
    angular.module('training.controllers').controller('UserAdminConsoleCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {

        $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }         
        };

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };


        // deletes user from a list
        $scope.delete = function(index) {
             
            /* NOTE: idType is either :
            *      "keyId" or loginId or "objectId"
            *
            ****  2014/07  currently dynamic idType is not enabled for this function
            ****  
            *
            * userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {
            * userService.deleteUser($scope.users[index].id, "loginId", function(err, status) {    
            * userService.deleteUser($scope.users[index].keyId, "keyId", function(err, status) {  
            * 
            */
            
            userService.deleteUser($scope.users[index]._id, "objectId", function(err, status) {    

                if(err) {

                    console.log("ERROR: delete user");
                }
                else {

                    console.log("INFO: deleted user");
                    $scope.users.splice(index, 1);
                }
            });
        };

        $scope.getUser = function() {

            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }
        };

        // NOTE: likley need to revist this logic - Do we want to get all USERS  automatically??? 
        //       is it enough to just bail out if user is not setup properly token etc  or should token also be added to 
        //       url's ???
        var user = userService.getUser();

        if(user && user.authToken) {

            userService.getUsers(function(err, users) {

                $scope.users = users;
                
                userService.saveLocalUsers(users);

            });
            
        }
        else {

            $location.path('/home');
        };
            
    }]);
    
    /*** USER PROFILE TABS SECTION 
     *
     * User Admin Profile Ctrl ************************************
     * 
     *   the user edit ctrl collects remote user data and sets different content in $scope.user as determined by userId
    */
    angular.module('training.controllers').controller('UserAdminProfileCtrl', ['$scope', '$routeParams', '$location','training.services.user', function($scope, $routeParams, $location, userService) {
        // broadcasting test source  BOTTOM Controller
        //  added to UserListCtrl (top) , UserCreateCtrl (Middle),  UserAdminProfileCtrl  (Bottom ) 

        $scope.topValue = 0;
        $scope.middleValue = 0;
        $scope.bottomValue = 0;

        $scope.$watch('bottomValue', function() {
            userService.updateBottomValue($scope.bottomValue);
        });

        $scope.$on('valuesUpdated', function() {
            $scope.topValue = userService.topValue;
            $scope.middleValue = userService.middleValue;
        });
        // end of Broadcast Test




        // note: this code section exicutes every time UsersEditCtrl is entered?
        
        // get logged in user info
        $scope.usersedititem = userService.getUser();   

        console.log("DEBUG: UserADminProfileCtrl params : ", $scope.usersedititem );
        
        var trps = [{"types": { Root: "Root", Sysadmin: "Sysadmin", Admin: "Admin", Member: "Member", Follower: "Follower", Subscriber: "Subscriber"}},
                    {"roles": { Owner: "Owner", Manager: "Manager",  Staff: "Staff", Volunteer: "Volunteer" }},
                    {"permissions": { Publisher: false, Editor: false, Support: true  }}];
                    
        $scope.utype = $scope.usersedititem.typeLabel;
        $scope.urole = $scope.usersedititem.role;
        $scope.uperm = $scope.usersedititem.permissions;
        
        $scope.selTypes = Object.keys(trps[0]['types']);
        $scope.selRoles = Object.keys(trps[1]['roles']);
        $scope.selPermissions = Object.keys(trps[2]['permissions']);
        
        
        $scope.setUSChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'urole':
                     $scope.urole = $scope.selRoles[choice];
                    break;
                case 'utype':
                    $scope.utype =  $scope.selTypes[choice];
                    break;
                case 'uperm':
                    $scope.uperm = $scope.selPermissions[choice];
                    break;    
                default:
                    console.log("ERROR: UserProfileCtrl setUSChoices  *************************", choice );    
            }
            return 
        }; 

       $scope.save = function() {

            console.log("DEBUG: UserProfileCtrl : ***** pre save *******", $scope.usersedititem );

            //$scope.usersedititem = angular.copy(nuuser);
            $scope.usersedititem.groupLabel = 'Users';
            $scope.usersedititem.typeLabel = $scope.utype;
            $scope.usersedititem.role = $scope.urole;
            $scope.usersedititem.permissions = $scope.uperm;
             
            userService.saveUser($scope.usersedititem, function(err, status) {
               
               console.log("DEBUG: UserProfileCtrl : ***** post save *******", $scope.usersedititem );
                
                if(err) {

                    console.log("ERROR: user info not updated");
                }
                else {

                    console.log("INFO: user info updated with status: ", status );
                    //$scope.usersedititem = '';
                    //$location.path('/#/users/profil/:#tab3-5');
                    //$scope.$apply(); 
                       
                      // commented $scope.usersEditItem = {};
                   // commented  });
                }

            });
        };
        // // NOTE : If delete is allowed at this button then we need to clean up connections etc.
        // //        This user would need to be logged out as well..  
        // $scope.delete = function () {
        //     // using need to fix api function to accept id or object


        //     // ask if they are sure and explain the consequense
        //     alert( " Are you sure ");

        //     userService.deleteUser($scope.user._id, "objectId", function(err, status) {

        //         if(err) {

        //             console.log("ERROR: delete user");
        //         }
        //         else {

        //             console.log("INFO: deleted user");

        //             //$scope.users.splice(index, 1);
        //             // SOME WORK HERE: If allowed 
        //             $location.path('/home');
        //         }
        //     });
        // };

      
        $scope.cancel = function () {
            
            console.log("INFO: canceled user update");
            
            //$location.path('/users/profile/' + userId );
            
                
        };

        

    }]);
 /*** USER PROFILE TABS SECTION 
     *
     * User Profile Ctrl ************************************
     * 
     *   the user edit ctrl collects remote user data and sets different content in $scope.user as determined by userId
    */
    angular.module('training.controllers').controller('UserProfileCtrl', ['$scope', '$routeParams', '$location','training.services.user', function($scope, $routeParams, $location, userService) {
        
        // note: this code section exicutes every time UsersEditCtrl is entered?
        
        // get logged in user info
        $scope.usersedititem = userService.getUser();   

        console.log("DEBUG: UserProfileCtrl params : ", $scope.usersedititem );
        
        var trps = [{"types": { Root: "Root", Sysadmin: "Sysadmin", Admin: "Admin", Member: "Member", Follower: "Follower", Subscriber: "Subscriber"}},
                    {"roles": { Owner: "Owner", Manager: "Manager",  Staff: "Staff", Volunteer: "Volunteer" }},
                    {"permissions": { Publisher: false, Editor: false, Support: true  }}];
                    
        $scope.utype = $scope.usersedititem.type;
        $scope.urole = $scope.usersedititem.role;
        $scope.uperm = $scope.usersedititem.permissions;
        
        $scope.selTypes = Object.keys(trps[0]['types']);
        $scope.selRoles = Object.keys(trps[1]['roles']);
        $scope.selPermissions = Object.keys(trps[2]['permissions']);
        
        
        $scope.setUSChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'urole':
                     $scope.urole = $scope.selRoles[choice];
                    break;
                case 'utype':
                    $scope.utype =  $scope.selTypes[choice];
                    break;
                case 'uperm':
                    $scope.uperm = $scope.selPermissions[choice];
                    break;    
                default:
                    console.log("ERROR: UserProfileCtrl setUSChoices  *************************", choice );    
            }
            return 
        }; 

       $scope.save = function() {

            console.log("DEBUG: UserProfileCtrl : ***** pre save *******", $scope.usersedititem );

            //$scope.usersedititem = angular.copy(nuuser);
            $scope.usersedititem.type = $scope.utype;
            $scope.usersedititem.role = $scope.urole;
            $scope.usersedititem.permissions = $scope.uperm;
            // $scope.savedUser.orgPriCategory = $scope.upricategory;
            // $scope.savedUser.orgSecCategory =  $scope.up2ndcategory;
            // $scope.savedUser.orgThrdCategory = $scope.up3rdcategory;
            
            // update user info in session
            userService.saveUser($scope.usersedititem, function(err, status) {
               
               console.log("DEBUG: UserProfileCtrl : ***** post save *******", $scope.usersedititem );
                
                if(err) {

                    console.log("ERROR: user info not updated");
                }
                else {

                    console.log("INFO: user info updated with status: ", status );
                    //$scope.usersedititem = '';
                    //$location.path('/#/users/profil/:#tab3-5');
                    //$scope.$apply(); 
                       
                      // commented $scope.usersEditItem = {};
                   // commented  });
                }

            });
        };
        // NOTE : If delete is allowed at this button then we need to clean up connections etc.
        //        This user would need to be logged out as well..  
        $scope.delete = function () {
            // using need to fix api function to accept id or object


            // ask if they are sure and explain the consequense
            alert( " Are you sure ");

            userService.deleteUser($scope.user._id, "objectId", function(err, status) {

                if(err) {

                    console.log("ERROR: delete user");
                }
                else {

                    console.log("INFO: deleted user");

                    //$scope.users.splice(index, 1);
                    // SOME WORK HERE: If allowed 
                    $location.path('/home');
                }
            });
        };

      
        $scope.cancel = function () {
            
            console.log("INFO: canceled user update");
            
            //$location.path('/users/profile/' + userId );
            
                
        };

        

    }]);
    /***USER ADMIN  CONTROLLER 
    *
    * UserMemberSiteCtrl
    */
    angular.module('training.controllers').controller('UserMbrsiteConsoleCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {

       // $scope.mbrsite = userService.getMemberSite();

        $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }         
        };

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };


            
    }]);


    /***USER EDIT TEST CONTROLLER 
      *
      * UserEditCtrl
      */
    angular.module('training.controllers').controller('UsersEditCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {
        //*****
        // NOTE: this is obs
       //
       // $xeditable.run(function(editableOptions) {
       //            editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
       //  });
       
        // get logged in user info
       //scope.usersedititem = "";
        
            
    }]);  

    /***USER EDITABLE TEST CONTROLLER 
      *
      * UserEditableCtrl
      */
    angular.module('training.controllers').controller('UsersEditableCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {
        //*****
        // NOTE: this is obs
       //
       // $xeditable.run(function(editableOptions) {
       //            editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
       //  });
       
        // get logged in user info
       //scope.usersedititem = "";
        
            
    }]);
    /*** USER ADMINISTRATION PAGE TABS SECTION 
     *
     *  User Dashboard Ctrl  ****************************************************
     * 
     *   contains the user profile ctrl which allows updating od user info
     */
    angular.module('training.controllers').controller('UserDashboardCtrl', ['$scope', '$routeParams', '$location','training.services.user', function($scope, $routeParams, $location, userService) {
           
        // note: this code section exicutes every time UsersEditCtrl is entered?

        console.log("DEBUG: usersEdit params : ");
        console.log( $routeParams )
        
        var userId = $routeParams.userId;
        // var userTab=  $routeParams.tabs | '';

        //$scope.meals = [];    // from examples

        // 20140926  $scope.user = userService.getLocalUser(userId);
        $scope.user = userService.getUser();
        
        console.log("DEBUG: userEditControl.userTabrouteParams : routeParams  ", $routeParams);
       
        // DEBUG alert( userId );

        
        if (!$scope.user) {

            // 20140917 $location.path('/users');
            console.log("INFO: userController.UsersEditCtrl : !$scope -> /home");    
            $location.path('/home');
        }
        else {

           
            var options = { 'types' : ['FOLLOWS'],          
                            'direction' : "out" };

            userService.findConnections( userId, options, function( err, results ) {
                
                if(err) {

                    console.log("ERROR: userService.findConnections : err  ", err);
                }
                else {

                    console.log("INFO: userService.findConnections : results: ", results );
                    

                    $scope.connections = results;
                    //userService.UserService['connections'].push( results) ;   // = results;
                    
                               
                }
            });            
         
            
        }

      
        // 20140917 console.log("INFO: userEditCtrl: userService.getLocalOthersPage : scope others: ", $scope.others );
        
        
        // load first page into list
            // userService.getResultsPage(1);
        //$scope.others = userService.getLocalOthersPage(1);  // load first page of user not connected to.
        
        // $scope.pageChangeHandler = function(num) {
        //     console.log('header page changed to ' + num);
        //     userService.getResultsPage( num );
        // };
        
        // MOVED TO userNuConnectPageCtrl 20140919
        //  // local User pagination settings
        // $scope.itemsPerPage = 25; 
        // $scope.currentPage = 1;
        // $scope.pageSize =  25;  // this should match however many users you want on one page
        // // asynchronous User pagination settings
        // $scope.totalUsers = 0;
        // $scope.usersPerPage = 25; // this should match however many results your API puts on one page
        // //pageChangeHandler(1);

        //$scope.others = userService.getLocalOthersPage( 1 , function(err, status)  );

        // userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize , $scope.fetchBy , function(err, nxPageResults) {
        //                 if(err) {

        //                     console.log("ERROR: userNuConnect error");

        //                     $scope.message = "Pagination error - userNuConnect";

        //                 } else {

        //                     console.log("INFO: new page loaded");
        //                     // var totCount =   nxPageResults.length - 4850;
        //                     // var nxPage = 
        //                     // for(var i = 0; i < totCount  ; i++) {
                
        //                     $scope.others= nxPageResults;   
        //                     //userService.others= nxPageResults;
        //                     $scope.totalUsers =  $scope.others.length;   
                           
        //                    //}



                            
        //                 }
        // });

        // MOVED TO  userNuConnectPageCtrl  20140919   
        // $scope.pagination = {
        //    current: 1
        // };
  

        // END NOTE: 
        $scope.theseConnections = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };       

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };
        
        $scope.whichUserNotMe = function(beingViewedId) {
            var user = userService.getUser();
             console.log("DEBUG: whichUserNotMe", beingViewedId );
         
            if( beingViewedId ) {

                return true;
            }         
        };

        $scope.save = function() {
            console.log("DEBUG: UserProfileCtrl: ***** pre save *******", $scope.user );
            userService.saveUser($scope.user, function(err, status) {

                if(err) {

                    console.log("ERROR: save user");
                }
                else {

                    console.log("INFO: saved user");
                    
                    //$location.path('/users');

                }
            });
        };
        // NOTE : If delete is allowed at this button then we need to clean up connections etc.
        //        This user would need to be logged out as well..  
        $scope.delete = function () {
            // using need to fix api function to accept id or object


            // ask if they are sure and explain the consequense
            alert( " Are you sure ");

            userService.deleteUser($scope.user._id, "objectId", function(err, status) {

                if(err) {

                    console.log("ERROR: delete user");
                }
                else {

                    console.log("INFO: deleted user");

                    //$scope.users.splice(index, 1);
                    // SOME WORK HERE: If allowed 
                    $location.path('/home');
                }
            });
        };

        // connect user to user

        // $scope.addConnection = function (index) {
        //     console.log("DEBUG: making connection from root user");
        //     console.log( $scope.user._id);
        //     console.log("DEBUG: making connection to other user");
        //     console.log($scope);

        //     var rootUser = {'Id': $scope.user._id,
        //                     'idType': 'objectId' };

        //     var otherUser = {'otherId': $scope.others[index]._id,
        //                       'idType': 'objectId' };

        //     var relationShip = { 'relationType' : 'FOLLOWS',
          
        //                          'relationData' : { 'otherKeyId' : $scope.others[index].keyId } }; 
                              
        //     userService.connectUser( rootUser, otherUser , relationShip, function(err, results ) {
                
        //         if(err) {

        //             console.log("ERROR: making connection to other user");
        //             console.log( results );
        //             console.log( Object.keys(results).length )
        //         }
        //         else if ( [results].length ===  1 ) {

        //             console.log("DEBUG: made connection to other user: results: ", results );

        //             $scope.connections.push( results );
                    
        //             $location.path('/users/edit/' + rootUser.Id );
        //         }
        //         else {
        //             console.log("INFO: made connection to other user");
        //             $location.path('/users');
        //         }

        //     });
        // };

        // drop  user to user connection 
        $scope.dropConnection = function (index) {
           console.log("ERROR: disconnecting from this user");
           console.log($scope.connections[index]._id); 

           var rootUser = {'Id': $scope.user._id,
                            'idType': 'objectId' };

            var connectedUser = {'connectedId': $scope.connections[index]._id,
                              'idType': 'objectId' };

            var options = { 'types' : ['FOLLOWS'],          
                            'direction' : "out" };

           userService.disconnectUser(rootUser, connectedUser , options , function(err, result) {

                console.log("DEBUG: dropConnection : disconnectUser : result :", result );
                console.log("DEBUG: dropConnection : disconnectUser : result :", connectedUser.connectedId );

                if(err) {

                    console.log("ERROR: disconnecting from this connected user");
                }
                else if ( result == connectedUser.connectedId  ) {  //   
                    
                    console.log("DEBUG: dropConnection : disconnectUser : results :", result ); 
                    $scope.connections.splice(index, 1);
                    
                    $location.path('/users/edit/' + rootUser.Id);           
                } 
                else {
                    // $scope.users.splice(index, 1);
                    $location.path('/users');
                }
           });
        };

        $scope.cancel = function () {
            
            console.log("INFO: canceled user update");
            
            $location.path('/home');
            
                
        };

    

    }]);

    
    /*
     * userNuConnectPage Controller
    */
    angular.module('training.controllers').controller('UsersOthersConnectPageCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {
        
        // local User pagination settings
        $scope.itemsPerPage = 25; 
        $scope.currentPage = 1;
        $scope.pageSize =  25;  // this should match however many users you want on one page
        $scope.fetchBy = {
            'fetchLastName' : '',
            'fetchFirstName' : '',
            'fetchCity'  : ''

        };
        // asynchronous User pagination settings
        $scope.othersTotalPossible = 0;
        $scope.othersPerPage = 25; // this should match however many results your API puts on one page
        //pageChangeHandler(1);
        $scope.pagination = {
           current: 1
        };
        $scope.pageChanged = function() {
                console.log('DEBUG: UserService.pageChanged: to nu page ' + $scope.currentPage);
                
                if ( $scope.fetchBy.fetchLastName != ""  ||  $scope.fetchBy.fetchFirstName != "" ) {
                     
                    console.log('DEBUG: UserService.pageChanged: got fetch data:' + $scope.fetchBy);
                    var tmpFetchBy = {
                                        'fetchLastName' : '',
                                        'fetchFirstName' : '',
                                        'fetchCity'  : ''
                                    };    
                    tmpFetchBy.fetchLastName = $scope.fetchBy.fetchLastName + ".*";
                    tmpFetchBy.fetchFirstName = $scope.fetchBy.fetchFirstName + ".*"; 
                    console.log('DEBUG: UserService.pageChanged: got tmp  data:', tmpFetchBy);
                    userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize , tmpFetchBy,  function(err, nxPageResults) {
                        if(err) {

                            console.log("ERROR: userNuConnect error");

                            $scope.message = "Pagination error - userNuConnect";

                        } else {

                            $scope.others = nxPageResults["data"];
                            console.log("DEBUG: new others page contents loaded: ");
                            console.log( $scope.others );
                            //userService.others =  nxPageResults;  
                            $scope.othersTotalPossible =  $scope.others.length;   
                           
                        }
                    });
                };         
        };

        $scope.fetchConnection = function() {
                console.log('getting more others: currentPage: ' + $scope.currentPage );
                console.log('getting more others: fetch: ' + $scope.fetchBy );
                if ( $scope.fetchBy.fetchLastName !== ""  ||  $scope.fetchBy.fetchFirstName !== "" ) {

                    userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize, $scope.fetchBy ,  function(err, nxPageResults) {
                        if(err) {

                            console.log("ERROR: userNuConnect error");

                            $scope.message = "Pagination error - userNuConnect";

                        } else {

                            console.log("INFO: new page loaded: " +  $scope.currentPage   );
                            // var totCount =   nxPageResults.length - 4850;
                            // var nxPage = 
                            // for(var i = 0; i < totCount  ; i++) {
                
                            $scope.others = nxPageResults["data"];

                            // userService.others =  nxPageResults;  
                            $scope.othersTotalPossible =  $scope.others.length;   
                           
                        }

                    });    
                };     
        };

        $scope.addConnection = function (index) {
            console.log("DEBUG: making connection from root user");
            console.log( $scope.user._id);
            console.log("DEBUG: making connection to other user");
            console.log($scope.others[index]._id);

            var rootUser = {'Id': $scope.user._id,
                            'idType': 'objectId' };

            var otherUser = {'otherId': $scope.others[index]._id,
                              'idType': 'objectId' };

            var relationShip = { 'relationType' : 'FOLLOWS',
          
                                 'relationData' : { 'otherKeyId' : $scope.others[index].keyId } }; 
                              
            userService.connectUser( rootUser, otherUser , relationShip, function(err, results ) {
                
                if(err) {

                    console.log("ERROR: making connection to other user");
                    console.log( results );
                    console.log( Object.keys(results).length )
                }
                else if ( [results].length ===  1 ) {

                    console.log("DEBUG: made connection to other user: results: ", results );

                    $scope.connections.push( results );
                    
                    $location.path('/users/profile/' + rootUser.Id + '#tab2-5' );
                }
                else {
                    console.log("INFO: made connection to other users");
                    $location.path('/users/profile/' + rootUser.Id + '#tab2-5');
                }

            });
        };

        
    }]);
    /*
     *    UsersListPageCtrl
    */
    
    angular.module('training.controllers').controller('UsersListPageCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {
        
        // Broadcasting test source controller  TOP CONTROLLER
        //
        //  added to UserListCtrl (top) , UserCreateCtrl (Middle),  UserAdminProfileCtrl  (Bottom ) 
        // $scope.TopController($scope, DemoService) {
        //     $scope.topValue = 0;
        //     $scope.middleValue = 0;
        //     $scope.bottomValue = 0;

        //     $scope.$watch('topValue', function() {
        //         DemoService.updateTopValue($scope.topValue);
        //     });

        //     $scope.$on('valuesUpdated', function() {
        //         $scope.middleValue = DemoService.middleValue;
        //         $scope.bottomValue = DemoService.bottomValue;
        //     });
        // };
        
        $scope.topValue = 0;
        $scope.middleValue = 0;
        $scope.bottomValue = 0;

        $scope.$watch('topValue', function() {
                userService.updateTopValue($scope.topValue);
        });

        $scope.$on('valuesUpdated', function() {
                $scope.middleValue = userService.middleValue;
                $scope.bottomValue = userService.bottomValue;
        });
         // end of Broadcasting test

        // local User pagination settings

        //$scope.usersEditItem = [];
        $scope.itemsPerPage = 25; 
        $scope.currentPage = 1;
        $scope.pageSize =  25;  // this should match however many users you want on one page
        $scope.fetchBy = {
            'fetchLastName' : '',
            'fetchFirstName' : '',
            'fetchCity'  : ''

        };
        // asynchronous User pagination settings
        $scope.usersTotalPossible = 0;
        //$scope.othersPerPage = 25; // this should match however many results your API puts on one page
        //pageChangeHandler(1);
        $scope.pagination = {
           current: 1
        };

        $scope.getSelectedUser = function( ndx ) { 

            console.log("DEBUG: UsersListPageCtrl.getSelectedUser : usersEditItem  ",  ndx ); //, $scope.usersEditItem
           
            var tmp = {};
            var tmp = $scope.usersList[ndx];
            userService.usersEditItem = tmp;
            //$scope.usersEditItem = tmp;
            // userService.usersEditItem = tmp;
            // $scope.connections.push( results );
            $scope.usersEditItem = tmp ;
             //$scope.apply();
            console.log("DEBUG: UsersListPageCtrl.getSelectedUser : usersEditItem  ", $scope.usersEditItem );
            
            // if (!$scope.usersEditItem) {

            // // 20140917 $location.path('/users');
            //  //$scope.usersEditItem.push( $scope.usersList[ndx]);
            //  console.log("INFO: userController.UsersEditCtrl : !$scope -> /home");    
            //   $location.path('/#TAB3-5');
            // }
            // else {

            //     console.log("DEBUG: UsersListPageCtrl.getSelectedUser : usersEditItem  ", $scope.usersEditItem );
            //     console.log("INFO: userController.UsersEditCtrl : !$scope -> /tab3-5");    
              
            //    //$scope.usersEditItem.push( $scope.usersList[ndx]);
            //    $location.path('/#');
            // }    

        };   

        $scope.fetchPage = function() {
                console.log('DEBUG: UsersListPageCtrl.fetchPage: to nu page ' + $scope.currentPage);
                
                if ( $scope.fetchBy.fetchLastName != ""  ||  $scope.fetchBy.fetchFirstName != "" ) {
                     
                    console.log('DEBUG: UsersListPageCtrl.fetchPage: got fetch data:' + $scope.fetchBy);
                    var tmpFetchBy = {
                                        'fetchLastName' : '',
                                        'fetchFirstName' : '',
                                        'fetchCity'  : ''
                                    };  
                    // add wildcard search params                  
                    tmpFetchBy.fetchLastName = $scope.fetchBy.fetchLastName + ".*";
                    tmpFetchBy.fetchFirstName = $scope.fetchBy.fetchFirstName + ".*"; 
                    console.log('DEBUG: UserService.pageChanged: got tmp  data:', tmpFetchBy);
                    userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize , tmpFetchBy,  function(err, nxPageResults) {
                        if(err) {

                            console.log("ERROR: userNuConnect error");

                            $scope.message = "Pagination error - userNuConnect";

                        } else {

                            $scope.usersList = nxPageResults["data"];
                            //userService.usersList = nxPageResults["data"];

                            console.log("DEBUG: new usersList page contents loaded: ");
                            console.log( $scope.usersList );
                            //userService.others =  nxPageResults;  
                            console.log("FIX ERROR: UsersListPageCtrl fetchPage list length error *************** FIX");

                            $scope.usersTotalPossible =  $scope.usersList.length;   
                           
                        }
                    });
                };         
        };

        $scope.fetchConnection = function() {
                console.log('getting more others: currentPage: ' + $scope.currentPage );
                console.log('getting more others: fetch: ' + $scope.fetchBy );
                if ( $scope.fetchBy.fetchLastName !== ""  ||  $scope.fetchBy.fetchFirstName !== "" ) {

                    userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize, $scope.fetchBy ,  function(err, nxPageResults) {
                        if(err) {

                            console.log("ERROR: userNuConnect error");

                            $scope.message = "Pagination error - userNuConnect";

                        } else {

                            console.log("INFO: new page loaded: " +  $scope.currentPage   );
                            // var totCount =   nxPageResults.length - 4850;
                            // var nxPage = 
                            // for(var i = 0; i < totCount  ; i++) {
                
                            $scope.others = nxPageResults["data"];

                            // userService.others =  nxPageResults;  
                            $scope.othersTotalPossible =  $scope.others.length;   
                           
                        }

                    });    
                };     
        };

        $scope.addConnection = function (index) {
            console.log("DEBUG: making connection from root user");
            console.log( $scope.user._id);
            console.log("DEBUG: making connection to other user");
            console.log($scope.others[index]._id);

            var rootUser = {'Id': $scope.user._id,
                            'idType': 'objectId' };

            var otherUser = {'otherId': $scope.others[index]._id,
                              'idType': 'objectId' };

            var relationShip = { 'relationType' : 'FOLLOWS',
          
                                 'relationData' : { 'otherKeyId' : $scope.others[index].keyId } }; 
                              
            userService.connectUser( rootUser, otherUser , relationShip, function(err, results ) {
                
                if(err) {

                    console.log("ERROR: making connection to other user");
                    console.log( results );
                    console.log( Object.keys(results).length )
                }
                else if ( [results].length ===  1 ) {

                    console.log("DEBUG: made connection to other user: results: ", results );

                    $scope.connections.push( results );
                    
                    $location.path('/users/profile/' + rootUser.Id + '#tab2-5' );
                }
                else {
                    console.log("INFO: made connection to other users");
                    $location.path('/users/profile/' + rootUser.Id + '#tab2-5');
                }

            });
        };
       
       
        
    }]);
    /*
    * userAlchemy page contropller
    */
    angular.module('training.controllers').controller('obs-UsersAlchemyPageCtrl', ['$scope', 'training.services.user' ,function($scope, userService ) {
            
            var config = {
                          dataSource: {
                            "nodes": [
                              {
                                "id": 1
                              },
                              {
                                "id": 2
                              },
                              {
                                "id": 3
                              }
                            ],
                            "edges": [
                              {
                                "source": 1,
                                "target": 2
                              },
                              {
                                "source": 1,
                                "target": 3,
                              }
                            ]},
                          graphHeight: function(){ return 400; },
                          graphWidth: function(){ return 400; },
                          
                          linkDistance: function(){ return 40; },

                          nodeTypes: {"node_type":[ "Maintainer",
                                                    "Contributor"]},
                          nodeCaption: function(node){ 
                            return node.caption + " " + "hello" }
                          }; 
            $scope.start = function(){
                 
                // alchemy.begin(config);
                // "dataSource": some_data
                console.log('UsersAlchemyPageCtrl: begin: ' );  //,  config.dataSource.nodes
            };

            
            $scope.fetchConnection = function() {
                    console.log('getting more others: currentPage: ' + $scope.currentPage );
                    console.log('getting more others: fetch: ' + $scope.fetchBy );
                    if ( $scope.fetchBy.fetchLastName !== ""  ||  $scope.fetchBy.fetchFirstName !== "" ) {

                        userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize, $scope.fetchBy ,  function(err, nxPageResults) {
                            if(err) {

                                console.log("ERROR: userNuConnect error");

                                $scope.message = "Pagination error - userNuConnect";

                            } else {

                                console.log("INFO: new page loaded: " +  $scope.currentPage   );
                                // var totCount =   nxPageResults.length - 4850;
                                // var nxPage = 
                                // for(var i = 0; i < totCount  ; i++) {
                    
                                $scope.others = nxPageResults["data"];

                                // userService.others =  nxPageResults;  
                                $scope.othersTotalPossible =  $scope.others.length;   
                               
                            }

                        });    
                    };     
            };
            
    }]);
   
    /** USER ADMINISTRATION PAGE TABS SECTION  add new user
     * User Create Ctrl ****************
     * 
     */
    angular.module('training.controllers').controller('UserCreateCtrl', ['$scope', '$filter', 'training.services.user', 'training.services.component', function($scope, $filter, userService, componentService) {
        // broadcasting test source  MIDDLE Controller
        //  also added to UserListCtrl (top) , UserCreateCtrl (Middle),  UserAdminProfileCtrl  (Bottom ) 
       
        $scope.topValue = 0;
        $scope.middleValue = 0;
        $scope.bottomValue = 0;

        $scope.$watch('middleValue', function() {
            userService.updateMiddleValue($scope.middleValue);
        });

        $scope.$on('valuesUpdated', function() {
            $scope.topValue = userService.topValue;
            $scope.bottomValue = userService.bottomValue;
        });
        // end of Broadcasting test     
       $scope.addNuUser = {
         nuuser: {},
         nucommponent: { }
        }; 
       //$scope.nucomponent = {};
        // $scope.user = {
        //     '_id' : '',        // system defined
        //     'keyId' : '',      // system defined
        //     'loginId'  : '',   // user denfined
        //     'firstName' : '',  //* req - user defined
        //     'lastName' : '',   //* req - user defined
        //     'password' : '',   //* req - user defined
        //     'passwordPhrase': '', //* req - user defined
        //     'street'   :'',
        //     'street2'  : '',
        //     'city': '',
        //     'state': '',
        //     'zip': '',
        //     'Phone':'',
        //     'Mobile':'',
        //     'Fax':'',
        //     'Role': '',  //* req  'admin', 'manager', 'user', ...
        //     'Type': '',  //* req system defined - 'root', admin', 'member', 'subscriber', 'follower'
        //     'Permissons':'',  //* req  admin defined -  'modify, publish, delete, create, .... 
        //     'Preferences':'', //* req user set - system defined list 
        //     'mbrName':'',
        //     'mbrStreet':'',
        //     'mbrStreet2':'',
        //     'mbrCity':'',
        //     'mbrSate':'',
        //     'mbrPhone':'',
        //     'mbrFax': '',
        //     'mbrWebSite': '',
        //     'mbrIdentity': '',
        //     'mbrContacts':'',
        //     'mbrCategory': '',
        //     'mbrApproveDate':'',
        //     'mbrKeyWords':''
        //};

        
        //MASTER trp logic *************************
        // NOTE: convert to  userService.trpSettings  
        // var orgCategory = [{"orgtypes": {"Accountants/Bookkeepers":"","Advertising & Marketing":"","Antique Lumber/Flooring":""
        //                     ,"Antiques & Gifts":"","Architect/Home Design":"","Architects/Home Design":""
        //                     ,"Art Galleries":"","Art Galleries/Framing":"","Attorneys":""
        //                     ,"Automobile Sales/Service":"","Balsam/Cullowhee Accommodations":""
        //                     ,"Beauty Salons/Nails":"","Bedding/Furniture":"","Blasting/Drilling":""
        //                     ,"Blinds/Shutters":"","Boat Charters & Tours":"","Boat Rentals/Sales":""
        //                     ,"Builders Supply":"","Building Contractor":""
        //                     ,"Cable TV/Internet":"","Campgrounds/RV Resorts":"","Camps":""
        //                     ,"Carpet Cleaners":"","Cashiers Accommodations":"","Chambers of Commerce":""
        //                     ,"Chiropractic/Accupuncture":"","Christmas Tree Farms":"","Cleaning Services":""
        //                     ,"Clothing & Accessories":"","Coffee House/Expresso Bar":"","Colleges":""
        //                     ,"Computer/Internet":"","Consignment Shops":"","Contract Law Enforcement & Security Services":""
        //                     ,"Custom Home Builders":"","Dentists":"","Developers":""
        //                     ,"Disc Jockey/Entertainment":"","Dry Cleaners":"","Electrical Contractors":""
        //                     ,"Electronics":"","Environmental Services/Engineering":"","Event Coordinating":""
        //                     ,"Facials & Waxing":"","Financial Institutions":"","Financial Planning/Investments":""
        //                     ,"Floor Coverings":"","Florists/Plants/Gifts":"","Furniture & Home Décor":""
        //                     ,"Gasoline":"","Gift Shops/Crafts/ Home Décor":"","Gift Shops/Crafts/Home":""
        //                     ,"Gem Mining":"","Golf Courses - Public":"","Golf/Tennis Private Clubs":""
        //                     ,"Grading & Excavating":"","Green Energy":"","Groceries/Fresh Vegetables":""
        //                     ,"Hardware/Housewares/Gifts":"","Highlands Accommodations":"","Home Inspections":""
        //                     ,"Home Repairs & Maintenance":"","Home Theater":"","Horseback Riding":""
        //                     ,"Hospitals":"","Insurance/Home Inventory & Valuables":"","Interior Design/Furniture":""
        //                     ,"Kitchen & Bath/Cabinets":"","Land Surveying":"","Land Trust/Conservation":""
        //                     ,"Lanscaping/Planning & Design":"","Lawn Maintenance":""
        //                     ,"Lighting":"","Log Homes":"","Magazines":""
        //                     ,"Masonry":"","Massage":"","Mortgage Lending":""
        //                     ,"Newspapers":"","Non Profit Organizations":"","Nusery/Plants":""
        //                     ,"Office Supplies":"","Outdoor Recreation":"","Outdoor Specialty Stores":""
        //                     ,"Painting & Pressure Washing":"","Pest Control":"","Pharmacy":""
        //                     ,"Photography":"","Physicians":"","Preschools":""
        //                     ,"Printing":"","Propane Gas":"","Property Management/POA":""
        //                     ,"Publishing":"","Radio Stations":"","Real Estate Appraisal":""
        //                     ,"Real Estate Sales/Rentals":"","Restaurants":"","Retirement Communities":""
        //                     ,"Roofing Repair/Sealants":"","Rugs/Oriental Rugs":"","Sapphire/Lake Toxaway Accommodations":""
        //                     ,"Shipping Services":"","Signs/Woodworking":"","Skeet Range/Field Club":""
        //                     ,"Spas":"","Stone Hauling":"","Storage Facilities":""
        //                     ,"Tent Rentals":"","Tourist Attraction":"","Toys & Gifts":""
        //                     ,"Transportation/Shuttle Service/Concierge":"","Trash Services/Bear Proof Containers":"","Tree Care":""
        //                     ,"Tree Service":"","Utilities":"","Video Production":"","Web Design":""
        //                    }}]; 
        
        $scope.upricategory = "";
        $scope.up2ndcategory = "";
        $scope.up3rdcategory = "";
        // $scope.selOrgCategory = Object.keys(orgCategory[0]["orgtypes"]);
         $scope.selOrgCategory = componentService.getBizCategories();
        
        $scope.setUMChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'pri':
                     $scope.upricategory = $scope.selOrgCategory[choice];
                    break;
                case '2nd':
                   $scope.up2ndcategory =  $scope.selOrgCategory[choice];
                    break;
                case '3rd':
                    $scope.up3rdcategory = $scope.selOrgCategory[choice];
                    break;    
                default:
                    console.log("ERROR: UsersCreateCtrl setUSChoices  *************************", choice );    
            }
            return 
        };

        
        var trps = [{"types": { Root: "Root", Sysadmin: "Sysadmin", Member: "Member", Associate: "Associate", Subscriber: "Subscriber", Advertiser:"Advertiser"}},
                    {"roles": { Owner: "Owner", Manager: "Manager",  Staff: "Staff", Volunteer: "Volunteer" }},
                    {"permissions": { Publisher: false, Editor: false, Support: true  }}];
                    
        $scope.utype = null;
        $scope.urole = null;
        $scope.uperm = null;
        
        $scope.selTypes = Object.keys(trps[0]['types']);
        $scope.selRoles = Object.keys(trps[1]['roles']);
        $scope.selPermissions = Object.keys(trps[2]['permissions']);
        
        

        $scope.setUSChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'urole':
                     $scope.urole = $scope.selRoles[choice];
                    break;
                case 'utype':
                    $scope.utype =  $scope.selTypes[choice];
                    break;
                case 'uperm':
                    $scope.uperm = $scope.selPermissions[choice];
                    break;    
                default:
                    console.log("ERROR: UsersCreateCtrl setUSChoices  *************************", choice );    
            }
            return 
        }; 

        $scope.status = {
              isopen: false
        };

        $scope.toggled = function(open) {
            //$log.log('Dropdown is now: ', open);
            console.log("DEBUG: UsersCreateCtrl setUtype Dropdown is now: ", open);
        };

        $scope.toggleDropdown = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.status.isopen = !$scope.status.isopen;
        };

        // Wrapup logic ***
         
        $scope.save = function(addNuUser) {
            
                $scope.savedUser = angular.copy(addNuUser.nuuser);
                $scope.savedComponent = angular.copy(addNuUser.nucomponent);

                console.log("DEBUG: adduser save data  ********** : ",  $scope.savedComponent ,$scope.savedUser );
                
                $scope.savedUser.groupLabel = 'Users';
                $scope.savedUser.typeLabel = $scope.utype;
                $scope.savedUser.lblArray = 'Users:' + $scope.utype;              
                $scope.savedUser.type = $scope.utype;
                $scope.savedUser.role = $scope.urole;
                $scope.savedUser.permissions = $scope.uperm;
                $scope.savedUser.createdByKeyId = userService.getUserKeyId();  // ** Required, set to userID of user doing create   
                $scope.savedUser.dateCreated = new Date();                     // ** Required, set at time of obj creation
                $scope.savedUser.lastUpdated = new Date();        
                $scope.savedUser.updatedById = userService.getUserKeyId();     // ** Required, set to userId doing create/update  
                if ( $scope.utype === "Associate" ) {
                     $scope.savedUser.associateOfKeyId[0] =  userService.getUserKeyId(); // ** an "Associate" type of  user is added to array when "member" type(s) creates them
                     $scope.savedUser.associateOfLabel[0] =  userService.getUserTypeLabel(); // ** set to "member" type(s) type label during above  process  
                } 
                $scope.savedUser.status = "" ;                 //** Required, set at system level-relates to validity of
                $scope.savedUser.isActive = true ;             // ** Required, set true at creation-relates to state of
                $scope.savedUser.approveDate = new Date();     // ** Required, set by Sysadmin user(s) with role of Publish using this process  
                
                // ** keyId:  uuid.v1(),                                // asigned by DB             
                // ** min required properties for all user types 
                // firstName : req.body.firstName || "missing",         // ** Required,
                // lastName : req.body.lastName || "missing",           // ** Required, 
                // email:  req.body.email || "missing",                 // ** Required,
                // loginId: req.body.loginId,
                // password : req.body.password,
                // passPhrase: req.body.passPhrase,
                // ** opt properties which are set/not set by form but are set in DB
                // street : req.body.street || "",
                // street2 : req.body.street2 || "",
                // city : req.body.city || "",
                // state : req.body.state || "",
                // zipCode : req.body.zipCode || "",
                // contactPhone : req.body.contactPhone || "",
                // mobilePhone : req.body.mobilePhone || "",
                // email : req.body.email || "",
                // password : req.body.password || "",
                // passPhrase: req.body.passPhrase || "",
                // identityId : req.body.identityId || "",
                // bioinfo : req.body.bioinfo || "",
                // fax: req.body.fax || ""
                // // do basic crud
                // console.log("DEBUG: saved nuuser scope *** if no error *********************************", $scope );

                userService.createUser( $scope.savedUser ,  function(err, status) {
                    // note: 20150225: status is an obj containing status, _id and keyId data
                    if(err) {

                         console.log("ERROR: UsersCreateCtrl create (save) *************************", err );
                         $scope.message = "User Create error - add User";

                    } else {
                        var retdIds ={};
                        retdIds.status = status.msg; 
                        retdIds._id = status._id;
                        retdIds.keyId = status.keyId;     
                        console.log("INFO:  post:  new user created: ",  retdIds.status );
                        console.log("DEBUG: pre new component data create  ********** : ",  $scope.savedComponent );
                        console.log("DEBUG: pre new component data scope.utype ****** : ",  $scope.utype );

                        /* PROFILE SET DATA              
                           
                           NOTE: New Biz Form data loaded earlier in this function with this 
                           $scope.savedComponent = angular.copy(addNuUser.nucomponent);

                        */ 
                        // // now create component object (mbrsite, subscribers, etc)
                        $scope.savedComponent.groupLabel = "Components";
                        $scope.savedComponent.lblArray  = "Components";
                        switch( $scope.utype ) {
                            case 'Member':
                                $scope.savedComponent.typeLabel = $scope.utype ;   // uTypeValue;
                                $scope.savedComponent.partLabel = "Profile"
                                $scope.savedComponent.lblArray  = $scope.savedComponent.lblArray.concat( ":Mbrsite", ":Profile");
                                $scope.savedComponent.category = [ $scope.upricategory , $scope.up2ndcategory, $scope.up3rdcategory ];
                                break;
                            case 'Advertister':
                                $scope.savedComponent.typeLabel = $scope.utype ;
                                $scope.savedComponent.partLabel = "Profile"
                                $scope.savedComponent.lblArray  = $scope.savedComponent.lblArray.concat( ":", $scope.utype , ":Profile");
                                $scope.savedComponent.category = "";
                                break;
                             case 'Associate':
                                $scope.savedComponent.typeLabel = $scope.utype 
                                $scope.savedComponent.partLabel = "Profile"
                                $scope.savedComponent.lblArray  = $scope.savedComponent.lblArray.concat( ":", $scope.utype , ":Profile");
                                $scope.savedComponent.category = "";
                                break;     
                            case 'Subscriber':
                                $scope.savedComponent.typeLabel = $scope.utype 
                                $scope.savedComponent.partLabel = "Profile"
                                $scope.savedComponent.lblArray  = $scope.savedComponent.lblArray.concat( ":", $scope.utype , ":Profile");
                                $scope.savedComponent.category = "";
                                break;    
                            default:
                                $scope.savedComponent.typeLabel = $scope.utype ;
                                $scope.savedComponent.partLabel = "Profile"
                                $scope.savedComponent.category = "";
                                console.log("INFO: UsersCreateCtrl setComponet Type *************************", $scope.utype );    
                        } 
                        // keyId   assigned by DB 
                        // ** required properties
                        $scope.savedComponent.userKeyId = retdIds.keyId;
                        $scope.savedComponent.createdByKeyId = retdIds._id;
                        $scope.savedComponent.dateCreated =  new Date();
                        $scope.savedComponent.lastUpdated =  new Date();
                        $scope.savedComponent.updatedById = retdIds.keyId; 
                        $scope.savedComponent.isActive = true;            
                        $scope.savedComponent.approveDate = new Date();
                        $scope.savedComponent.approvedById = retdIds._id;       
                        $scope.savedComponent.status = "";
                        
                        /* PANE SET DATA              
                           
                           NOTE: New Biz Form data loaded earlier in this function with this 
                           $scope.savedComponent = angular.copy(addNuUser.nucomponent);

                        */ 
                        /* obs properties after consolidating tabBar and Profile properties 
                        $scope.savedComponent.tabPaneGroupLabel = "Components";
                        $scope.savedComponent.tabPaneLblArray  = "Components";
                        $scope.savedComponent.tabPaneLblArray  = $scope.savedComponent.tabPaneLblArray.concat( ":", $scope.utype , ":Tabsets");
                        $scope.savedComponent.tabPaneTypeLabel = $scope.utype 
                        $scope.savedComponent.tabPanePartLabel = "Tabsets"
                        $scope.savedComponent.tabPaneUserKeyId = retdIds.keyId ;
                        $scope.savedComponent.tabPaneCreatedByKeyId = retdIds._id;
                        $scope.savedComponent.tabPaneDateCreated =  new Date();
                        */
                        $scope.savedComponent.tabPaneLastUpdated =  [new Date()];
                        $scope.savedComponent.tabPaneUpdatedById = [retdIds.keyId]; 
                        $scope.savedComponent.tabPaneIsActive = [true];            
                        $scope.savedComponent.tabPaneStatus = ["Inactive"];  // dflt: Inactive Public,Internal Only Private,Inactive
                        
                        $scope.savedComponent.tabPaneBarId = [100,200,300,400,500,600];
                        $scope.savedComponent.tabPaneBarHref = ['href="#tab0-1"','href="#tab1-1"','href="#tab1-2"','href="#tab1-3"','href="#tab2-1"','href="#tab3-1"'];
                        $scope.savedComponent.tabPaneBarLabelText = ["My Home","Blank 1","Blank 2","Blank 3","Grid 1","Google Map"];
                        $scope.savedComponent.tabPaneBarIsActive = [true,false,false,false,false,false];
                        $scope.savedComponent.tabPaneBarOrder = [1,2,3,4,5,6] ;
                        // tabPane for each paneTab there can be several ContentModel entries
                        $scope.savedComponent.tabPaneTabIds = ["tab1-1","tab1-2"];
                        $scope.savedComponent.tabPaneTabTitles = ["Blank Template One","Blank Template Two"];
                        // the following arrays are filled with the _id and KeyId of each contentModel created as described
                        // below.
                        $scope.savedComponent.tabPaneTabContentIds = [];
                        $scope.savedComponent.tabPaneTabContentKeyIds = [];
                        
                        



                        /* CONTENT MODEL DATA
                        */ 
                        // ContentModels: for each contentModel there are several content entries text, AreaText
                        //                imgsrc, imgAreaText, linktarget, linkAreaText
                        /* 20150317 : Note:    Currently for testing purposes this process creates a single node/document per page.  
                        *
                        *             in the code below the contentModel Array will be broken into individual
                        *             node/documents based on the contentTabIds. In sample data below four contentModel sets are represented.
                        *             One of the modelLabels (tab0-1) uses the "model" key word to identify the source set Member:Profile
                        *             that contains data extracted from the profile. Other Modellables use a "This: key word indentifing the source which is
                        *             found in this content array element. These samples will result in one contentModel node/document being written to the
                        *             database. If the contentModel. Had additonal data (contentModels) to persist in the DB then an additional array element is created and inserted 
                        *             following the last occurance of the matching tabId.  
                         

                        *             
                        // keyId   assigned by DB 
                        // ** required properties
                        */
                        $scope.savedComponent.contentGroupLabel = "Components";
                        $scope.savedComponent.contentLblArray  = "Components";
                        // obs var tmpConcat = ":" + $scope.utype +  ":Tabcontent"; 
                        $scope.savedComponent.contentLblArray  = $scope.savedComponent.contentLblArray.concat( ":", $scope.utype , ":Tabcontent" );
                         
                        $scope.savedComponent.contentTypeLabel = $scope.utype ; 
                        $scope.savedComponent.contentPartLabel = "Tabcontent" ; 
                        $scope.savedComponent.contentUserKeyId = retdIds.keyId;
                        $scope.savedComponent.contentCreatedByKeyId = retdIds._id;
                        $scope.savedComponent.contentDateCreated =  new Date();
                        $scope.savedComponent.contentLastUpdated =  new Date();
                        $scope.savedComponent.contentUpdatedById = retdIds.keyId; 
                        $scope.savedComponent.contentIsActive = true;            
                        $scope.savedComponent.contentStatus = "Inactive";      // dflt: Inactive Public,Internal Only Private,Inactive
                       
                        $scope.savedComponent.contentModelSrcs = ["Model","This","This","This"];    // dflt: "This"  [ This, Model, ..]
                        $scope.savedComponent.contentModelIds = ["member.profile","nill","nill","nill"];
                        $scope.savedComponent.contentModelLabels = ["tab0-1","tab1-1","tab1-2","tab1-2"];
                        $scope.savedComponent.contentModelNames = ["contentOne","contentOne","contentOne","contentTWO"];

                        $scope.savedComponent.contentModelIsOk = ["Support","Support","Support","Support"];
                        $scope.savedComponent.contentModelTitleText = ["{name}","Sample Title 1","Sample Title 1","Sample Title 2"];
                        $scope.savedComponent.contentModelTextArea = ['mbrs-tab0-1 sit amet leo nec nunc auctor convallis vel at est','mbrs-tab1-2 Etiam sit amet leo nec nunc auctor convallis vel at est. Aenean at metus id nisl posuere varius. In vel ipsum eu risus iaculis volutpat in ut metus. Integer eget sem eu massa pharetra vehicula ut id elit. Nam sagittis malesuada est, ut tempus diam ultricies eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla urna libero, finibus in metus mollis, aliquam interdum elit. Phasellus at sagittis lectus. Vestibulum scelerisque libero eget efficitur venenatis. Etiam nunc magna, maximus vel vehicula eget, bibendum eu urna. Phasellus quis turpis id lacus mollis dictum. Ut dui elit, efficitur vel orci eu, elementum interdum erat. Cras at leo volutpat, commodo metus in, dictum justo. Cras id vulputate ex. Nullam et aliquet arcu, id elementum magna. Phasellus faucibus sapien at dolor ultrices pellentesque.','mbrs-Etiam sit amet leo nec nunc auctor convallis vel at est. Aenean at metus id nisl posuere varius. In vel ipsum eu risus iaculis volutpat in ut metus. Integer eget sem eu massa pharetra vehicula ut id elit. Nam sagittis malesuada est, ut tempus diam ultricies eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla urna libero, finibus in metus mollis, aliquam interdum elit. Phasellus at sagittis lectus. Vestibulum scelerisque libero eget efficitur venenatis. Etiam nunc magna, maximus vel vehicula eget, bibendum eu urna. Phasellus quis turpis id lacus mollis dictum. Ut dui elit, efficitur vel orci eu, elementum interdum erat. Cras at leo volutpat, commodo metus in, dictum justo. Cras id vulputate ex. Nullam et aliquet arcu, id elementum magna. Phasellus faucibus sapien at dolor ultrices pellentesque.'];
                        $scope.savedComponent.contentModelImgSrcs = ["../../../common/images/homepage-photo3.jpg","../../../common/images/homepage-photo3.jpg","nill"];
                        $scope.savedComponent.contentModelImgTextArea = ["This is a great picture","This is a great picture also","Another great picture"];
                        $scope.savedComponent.contentModelLinkTarget = ["href='#'","href='#'","href='#'"];
                        $scope.savedComponent.contentModelLinkTextArea = ["Click here for more information","Click here for more information","Click here for more information"];
                        /*
                        NOTE: This is sample model for above component as used in the Blank Template Two 
                                  paneTabId : localMbrsite.paneTabIds[0],
                            paneTabTitleText: localMbrsite.paneTabTitles[0],
                                    contentOne: {
                                      tabId : localMbrsite.contentTabIds[0],
                                      tabNameId: localMbrsite.contentTabNameIds[0],
                                      isOK: localMbrsite.contentIsOk[0],
                                      titleText: localMbrsite.contentModelTitleText[0],
                                      textArea: localMbrsite.contentModelTextArea[0],
                                      imgSrc:  localMbrsite.contentModelImgSrcs[0],
                                      imgTextArea: localMbrsite.contentModelImgTextArea[0],
                                      linkTarget:  localMbrsite.contentModelLinkTarget[0],
                                      linkTextArea: localMbrsite.contentModelLinkTextArea[0]
                                    },
                                    contentTwo: {...}
                         
                        In this instance we are creating the Profile and Content sections of our model with one
                        call to the createComponent process. It can handle any combinations of Profile or Content creation.  

                        */
                           
                        componentService.createComponentSet(  $scope.savedComponent ,  function(err, status) {
                             // note: 20150225: status is an obj containing status, _id and keyId data
                            if(err) {

                                 console.log("ERROR: createComponentSet (save err) *************************", err );

                                 $scope.message = "Component Create error - add new User/component";

                            } else {
                                /* returned data looks like this:
                                 Note: ContentIds can contain one or more entries dependent on
                                       number of tabPanes being added at a time.
                                 retdMsg.msg = status;
                                 retdMsg.contentIds =  data[0];  //  'contentIds': { '_id': contentResults._id,  'keyId': contentResults.keyId }
                                 retdMsg.profileIds =  data[1];  // 'profileIds': { '_id': profileResults._id,  'keyId': profileResults.keyId }
                                 
                                 note: 20150321  only msg data being returned 
                                */
                                var retdIds ={};
                                retdIds.status = status.msg; 
                                //retdIds.status = status.contentIds; 
                                //retdIds.status = status.contentIds; 
                                
                                console.log("INFO: post: new user/component created: ",  retdIds.status );
      
                            }
                            // here because variables were cleared too quickly at original location
                            $scope.clear();
                        }); 
                        
                    }

                });   
                

                // original location 
                // $scope.clear();
                
        };

        $scope.clear = function() {
                 $scope.utype = null;
                 $scope.urole = null;
                 $scope.uperm = null; 
                 $scope.upricategory = null;
                 $scope.addNuUser.nuuser = null;
                 $scope.addNuUser.nucomponent = null;
                 //$scope.savedComponent = null; 
                 //$scope.savedUser = null;
                 $scope.$apply;   
        };
        
    }]);

    /*
     * User login Ctrl
     */
    angular.module('training.controllers').controller('UsersLoginCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location,    userService) {

        $scope.user = {

            'loginId' : '',
            'password' : ''
        };

        $scope.message = "";

        $scope.login = function() {

            $scope.message = "";

            userService.loginUser($scope.user, function(err, status) {

                if(err) {

                    console.log("ERROR: login user");

                    $scope.message = "Authentication Error";
                }
                else {
                       
                    var tmpUser = userService.getUser();   

                    console.log("INFO: Logged In User status : " + tmpUser._id );  //+ '#tab1-5'
                    $location.path('/users/adminhome');
                    //$location.path('/home');
                }
            });
        };
    }]);

    
    
    /*
     * Main Controller
     */
    angular.module('training.controllers').controller('MainCtrl', ['$scope', 'training.services.user', 'training.services.component', function($scope, userService, componentService) {
        //$scope.menus = "";
        userService.init();
        //componentService.init();
        
    }]);
}());