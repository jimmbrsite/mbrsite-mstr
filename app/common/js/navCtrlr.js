(function() {

    'use strict';

    /*
     * Nav Controller
     */
    angular.module('training.controllers').controller('NavCtrl', ['$scope', '$location', 'training.services.user', function($scope, $location, userService) {
        // init services - not sure why but, falls over if we don't
        //$scope.menus = angular.fromJson(sessionStorage.getItem("userMenus"));
        var tmp = userService.signedIn();       
        $scope.signedIn = function() {
             // load admin menu into NavCtrl here
            // if (userService.signedIn() === true ) {
            //     console.log("DEBUG: signedIn scope.menu: ", $scope.menus );     
            //     if ( $scope.menus === undefined ) { 
            //        console.log("DEBUG: signedIn passed test: " );     
                
            //        $scope.menus =  [userService.getMenu()];    //.push(userService.getMenu());
            //        //$scope.$apply( );
            //     } 
            // }
            return userService.signedIn();          
        };
       
       $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            }         
        };
        
        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };
      
        $scope.logout = function() {
            console.log("INFO: User logging out");
            userService.logout();
            $scope.menus = {};
            $location.path('/common/home');
        };

    }]);

}());