(function () {
  'use strict';

  angular.module('training.controllers').controller('DemoCtrl2', ['$scope', function($scope){
      $scope.title = "DemoCtrl2";
      $scope.d3Data = [
        {name: "Greg", score:23},
        {name: "Ari", score:43},
        {name: "Loser", score: 87}
      ];
    }]);

}());