(function() {

    'use strict';
   
    /*
     *    PublicListPageCtrl
    */
    
    angular.module('training.controllers').controller('ContextListPageCtrl', ['$scope', '$location', 'training.services.user', 'training.services.component', function($scope, $location, userService, componentService) {
        // Public List pagination settings
        //console.log("INFO: PUBLIC ACTIVE : "  );

        //$scope.usersEditItem = [];
        $scope.itemsPerPage = 25; 
        $scope.currentPage = 1;
        $scope.pageSize =  25;  // this should match however many users you want on one page
        $scope.fetchBy = {
            'fetchContextValue' : ''
        };
        // asynchronous User pagination settings
        $scope.contextTotalPossible = 0;
        //$scope.othersPerPage = 25; // this should match however many results your API puts on one page
        //pageChangeHandler(1);
        $scope.pagination = {
           current: 1
        };

        $scope.getSelectedUser = function( ndx ) { 

            console.log("DEBUG: ContextListPageCtrl.getSelectedUser : usersEditItem  ",  ndx ); //, $scope.usersEditItem
           
            var tmp = {};
            var tmp = $scope.usersList[ndx];
            userService.usersEditItem = tmp;
            //$scope.usersEditItem = tmp;
            // userService.usersEditItem = tmp;
            // $scope.connections.push( results );
            $scope.usersEditItem = tmp ;
             //$scope.apply();
            console.log("DEBUG: ContextListPageCtrl.getSelectedUser : usersEditItem  ", $scope.usersEditItem );
           
        };   

        $scope.fetchPage = function() {
                console.log('DEBUG: ContextListPageCtrl.fetchPage: to nu page ' + $scope.currentPage);
                
                if ( $scope.fetchBy.fetchLastName != ""  ||  $scope.fetchBy.fetchFirstName != "" ) {
                     
                    console.log('DEBUG: ContextListPageCtrl.fetchPage: got fetch data:' + $scope.fetchBy);
                    var tmpFetchBy = {
                                        'fetchLastName' : '',
                                        'fetchFirstName' : '',
                                        'fetchCity'  : ''
                                    };  
                    // add wildcard search params                  
                    tmpFetchBy.fetchLastName = $scope.fetchBy.fetchLastName + ".*";
                    tmpFetchBy.fetchFirstName = $scope.fetchBy.fetchFirstName + ".*"; 
                    console.log('DEBUG: ContextListPageCtrl.pageChanged: got tmp  data:', tmpFetchBy);
                    userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize , tmpFetchBy,  function(err, nxPageResults) {
                        if(err) {

                            console.log("ERROR: userNuConnect error");

                            $scope.message = "Pagination error - userNuConnect";

                        } else {

                            $scope.publicList = nxPageResults["data"];
                            //userService.usersList = nxPageResults["data"];

                            console.log("DEBUG: new usersList page contents loaded: ");
                            console.log( $scope.usersList );
                            //userService.others =  nxPageResults;  
                            console.log("FIX ERROR: ContextListPageCtrl fetchPage list length error *************** FIX");

                            $scope.contextTotalPossible =  $scope.usersList.length;   
                           
                        }
                    });
                };         
        };

        $scope.fetchConnection = function() {
                console.log('ContextListPageCtrl getting more others: currentPage: ' + $scope.currentPage );
                console.log('ContextListPageCtrl getting more others: fetch: ' + $scope.fetchBy );
                if ( $scope.fetchBy.fetchLastName !== ""  ||  $scope.fetchBy.fetchFirstName !== "" ) {

                    userService.getLocalOthersPage( $scope.currentPage , $scope.pageSize, $scope.fetchBy ,  function(err, nxPageResults) {
                        if(err) {

                            console.log("ERROR: ContextListPageCtrl userNuConnect error");

                            $scope.message = "ContextListPageCtrl: Pagination error - userNuConnect";

                        } else {

                            console.log("INFO: ContextListPageCtrl: new page loaded: " +  $scope.currentPage   );
                            // var totCount =   nxPageResults.length - 4850;
                            // var nxPage = 
                            // for(var i = 0; i < totCount  ; i++) {
                
                            $scope.others = nxPageResults["data"];

                            // userService.others =  nxPageResults;  
                            $scope.othersTotalPossible =  $scope.others.length;   
                           
                        }

                    });    
                };     
        };

        $scope.fetchContextPages = function ( component, category  ) {

          //console.log("DEBUG: MbrsiteAdminProfileManagerCtrl-initMbrsite pre getUser :", user );
          
          //var user = userService.getUser();
          var context = {'label': component };
          context.category = category;
          context.authToken = '192.168.100.235';
          console.log("DEBUG: ContextListPageCtrl.fetchContextPages-initMbrsite post getUser:", context );

          if(context && context.authToken) {
            
            var cLabels = ['Components',context.label,'Profile'];   // part: Profile,Tabset or Panes 
            //
            // NOTE: we may want to only retrieve PROFILE here??
            //
            //          call to getMbrsite using below parms will bring back all associated mbrsite relationShips
            //          and ContentSets. May need to update parms to be a bit more selective


            var parms = { where: {
                            profile: {
                                userKeyId : '',     
                                name : $scope.fetchBy.fetchContextValue,
                                pId : '', 
                                isActive : '',
                                status : '',
                                permission : ''
                                }  
                            },      
                          cLabels: cLabels
                        };
                          
                         
            console.log("DEBUG: ContextListPageCtrl.fetchContextPages-initContext parms *********** : ", parms );

            componentService.getContextProfile( context, parms, function(err, nuProfile) {
                if(err) {

                    console.log("ERROR: initContext");
                }
                else {

                    console.log("DEBUG: ContextListPageCtrl-initMbrsiteProfile nuProfile:*************************: ", nuProfile ); 

                    // $scope.contextList = componentService.getLocalComponentPart('profile', '');  // nuMbrsite.profile; // componentService.getLocalComponentPart('profile', ''); //nuMbrsite.profile;  
                   
                    $scope.contextList = componentService.contextList;  // nuMbrsite.profile; // componentService.getLocalComponentPart('profile', ''); //nuMbrsite.profile;  

                    //console.log("DEBUG: ContextListPageCtrl-getLocalComponentPart profile,contentSets via Service ************** ", $scope.profileItem );
                }
           
            });
            
          }
          else {
            console.log("DEBUG: ContextListPageCtrl.initMbrsiteProfile not authenticated ** :", context.authToken );
            $location.path('/home');
          }

        };
        
    }]);
       
    
    /*
     * Main Controller
     */
    angular.module('training.controllers').controller('MainCtrl', ['$scope', 'training.services.user', 'training.services.component', function($scope, userService, componentService) {
        
        console.log("INFO: APP Started: "  );
        //$scope.menus = "";
        userService.init();
        //componentService.init();
        
    }]);

}());