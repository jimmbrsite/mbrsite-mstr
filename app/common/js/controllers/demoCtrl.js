(function() {
  
  'use strict';

  angular.module('training.controllers').controller('CollapseDemoCtrl', ['$scope', 'training.services.user', function($scope, userServices ){
      
    $scope.isCollapsed= false;
   
  }]);


  angular.module('training.controllers').controller('obs_b2TabPaneContent', ['$scope', 'training.services.user', function($scope, userServices ){
      
      // 1.) Which template set are we processing
      // 2.) Set validation rules
      

      //
      //var user = userServices.getUser();
      //var wrkConnections = [];
      
      //wrkConnections = userServices.connections;

      $scope.items = ['Sysadmin', 'Member', 'Subscriber', 'Associate'];
      
      $scope.isAuthorized = $scope.items[0];
      
      $scope.isOK = "";

      $scope.tmpl = {
        titleone: 'Sample Title 1',
        spanone: 'Etiam sit amet leo nec nunc auctor convallis vel at est. Aenean at metus id nisl posuere varius. In vel ipsum eu risus iaculis volutpat in ut metus. Integer eget sem eu massa pharetra vehicula ut id elit. Nam sagittis malesuada est, ut tempus diam ultricies eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla urna libero, finibus in metus mollis, aliquam interdum elit. Phasellus at sagittis lectus. Vestibulum scelerisque libero eget efficitur venenatis. Etiam nunc magna, maximus vel vehicula eget, bibendum eu urna. Phasellus quis turpis id lacus mollis dictum. Ut dui elit, efficitur vel orci eu, elementum interdum erat. Cras at leo volutpat, commodo metus in, dictum justo. Cras id vulputate ex. Nullam et aliquet arcu, id elementum magna. Phasellus faucibus sapien at dolor ultrices pellentesque.',
        titletwo:  'Ssub title 2',
        spantwo:   'Donec blandit suscipit ante, quis viverra urna ornare vitae. Vestibulum dolor ligula, scelerisque at lectus in, suscipit lobortis enim. Nunc at mattis sapien, quis mollis dolor. Donec blandit diam velit, quis pellentesque ante lobortis a. Aliquam et diam eget nibh tempus vulputate non ut tellus. Mauris sagittis vehicula dapibus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Aenean a quam ut nisl sollicitudin fringilla eu faucibus massa. Duis cursus augue at sapien tempus, vitae ornare tortor euismod. Maecenas pharetra tincidunt felis non consectetur. Maecenas condimentum mi sit amet nibh tempor accumsan. In hac habitasse platea dictumst. Duis accumsan, metus dictum fringilla facilisis, purus magna dictum lacus, eget consequat erat purus sit amet quam. Nam luctus ac ipsum non rhoncus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Fusce elementum, erat vitae gravida tincidunt, justo lorem maximus leo, a iaculis purus risus sit amet felis. Donec a lorem sagittis massa consequat tristique.',  
        titlethree:  'Sub title 3',
        spanthree:   'Donec blandit suscipit ante, quis viverra urna ornare vitae. Vestibulum dolor ligula, scelerisque at lectus in, suscipit lobortis enim. Nunc at mattis sapien, quis mollis dolor. Donec blandit diam velit, quis pellentesque ante lobortis a. Aliquam et diam eget nibh tempus vulputate non ut tellus. Mauris sagittis vehicula dapibus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Aenean a quam ut nisl sollicitudin fringilla eu faucibus massa. Duis cursus augue at sapien tempus, vitae ornare tortor euismod. Maecenas pharetra tincidunt felis non consectetur. Maecenas condimentum mi sit amet nibh tempor accumsan. In hac habitasse platea dictumst. Duis accumsan, metus dictum fringilla facilisis, purus magna dictum lacus, eget consequat erat purus sit amet quam. Nam luctus ac ipsum non rhoncus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Fusce elementum, erat vitae gravida tincidunt, justo lorem maximus leo, a iaculis purus risus sit amet felis. Donec a lorem sagittis massa consequat tristique.'  

      };

      $scope.updateThis = function(){
          
            alert("data saved");
       
      };

      $scope.uploadThis = function(){
          
            alert("file uploaded");
       
      };      
     

  }]);

  // How many zip codes in my area
  angular.module('training.controllers').controller('ZipsPerCtrl', ['$scope', 'training.services.user', function( $scope, userServices ){
      var user = userServices.getUser();
      var wrkConnections = [];
      wrkConnections = userServices.connections;
    

      var userId = user._id;
      var options = { 'types' : ['FOLLOWS'],          
                            'direction' : "out" };

      
      console.log("DEBUG: training.controllers.ZipsPerCtrl  user: ", user );
      console.log("DEBUG: training.controllers.ZipsPerCtrl  connections: ",  wrkConnections );
      
      $scope.title = "Zips Per City " ;
      if ( wrkConnections.length ) {
        var tmp0 = wrkConnections.pop() ;   
        var tmp1 = wrkConnections.pop() ;   
        
        console.log("DEBUG: training.controllers.ZipsPerCtrl  tmp0: ",  tmp0 );
      

        $scope.d3Data = [
          {name: tmp0.firstName , score: tmp0._id },
           {name: tmp1.firstName , score: tmp1._id }
        ];  
      } 
      else {
         $scope.d3Data = [
             {name: 'blank' , score: 10 },
             {name: 'blank' , score: 10  }
          ];

      }
        //{name: "Luther,Ok", score:3},
        //{name: "Chicago,Il", score: 45}
        
      
  
      $scope.d3OnClick = function(item){
        alert(item.name);
      };
  
  }]);

  angular.module('training.controllers').controller('D3DirectedNodeCtrl', ['$scope', 'training.services.user', function($scope, userServices){
      
      console.log("DEBUG: training.controllers.D3DirectedNodeCtrl  strart: ", "user" );
      // var user = userServices.getUser();
      // var wrkConnections = [];
      // wrkConnections = userServices.getLocalConnections(); //push(userServices.getLocalConnections());
    
      //var userId = user._id;
      //var options = { 'types' : ['FOLLOWS'],          
      //                      'direction' : "out" };

     

      //var item = false;
      $scope.qrylists = [{title: "Select My Nodes", 
                action: "", 
                qrylists: [
                  {
                    title: "My Conections",
                    action: '<a href="#users/profile/{{dneGetConnections()}}#tab6-7" > ',
                    eofaction: "a"
                  },
                  {
                    title: "my Connects",
                    action: '<button type="submit" class="btn btn-default btn-sm " ng-click="dneGetConnections()">',
                    eoftagaction: "</button>"
                  },  
                  {
                    title: "Connections",
                    action: "#users/profile/{{whichUserId()}}#tab2-7"
                  },
                  {
                    title: "User List",
                    action: "#users/profile/{{whichUserId()}}#tab3-5"
                  },
                  {
                    title: "Stuff",
                    action: "#users/profile/{{whichUserId()}}#tab4-5"
                  },
                  {
                    title: "Stuff Stuff",
                    action: "#users/profile/{{whichUserId()}}#tab5-5"
                  }
                    
                ]
              }
        ];
 
      $scope.item =false;

      $scope.title = "Directed Node Editor";
      
      //console.log("DEBUG: training.controllers.D3DirectedNodeCtrl  userId: ",  userId ); 
      //$scope.dnedata = [];
      // create nodes , NOTE: Links are not working yet
      //$scope.dnedata = [];
      // $scope.dnedata[0] =  nodes;
      // $scope.dnedata[1] =  lastNodeId;
      // $scope.dnedata[2] =  links;
      // $scope.dnedata[3] =  item;


      // var nodes =  [{id: 0, reflexive: false, name:"Jim", dat: {city:"West Sac", zip:"95603"} }, {id: 1, reflexive: true, name:"Char", dat: {city:"West Sac", zip:"95691"}  }
      //              , {id: 2, reflexive: false, name:"Harry", dat: {city:"Sac", zip:"96774"} }, {id: 3, reflexive: false, name:"Al", dat: {city:"Davis", zip:"95666"} }];
      // var lastNodeId = 3;
      // var links =  [{source: nodes[0], target: nodes[1], left: false, right: true },
      //                       {source: nodes[1], target: nodes[2], left: false, right: true },
      //                       {source: nodes[1], target: nodes[3], left: false, right: true }
      //                      ];

      //  // $scope.dnedata[0] =  nodes;
      //  // $scope.dnedata[1] =  lastNodeId;
      //  // $scope.dnedata[2] =  links;
      //  // $scope.dnedata[3] =  item;
      
      // console.log("DEBUG: training.controllers.D3DirectedNodeCtrl  wrkConnections: ",  wrkConnections );      
      // $scope.dnedata = [];
      // if ( wrkConnections ) {
      //    var i;
      //    for ( i = 0; i < 4; i++ ) {

      //       $scope.dnedata[0] =  wrkConnections[0];
      //       $scope.dnedata[1] =  wrkConnections[1];
      //       $scope.dnedata[2] =  wrkConnections[2];
      //       $scope.dnedata[3] =  wrkConnections[3];

         
      //    }
      

      //   // var tmp0 = wrkConnections.pop() ;   
      //   // var tmp1 = wrkConnections.pop() ;   
        
      //   // console.log("DEBUG: training.controllers.ZipsPerCtrl  tmp0: ",  tmp0 );
      

      //   // $scope.d3Data = [
      //   //   {name: tmp0.firstName , score: tmp0._id },
      //   //    {name: tmp1.firstName , score: tmp1._id }
      //   // ];  
      // } 
      // else {
      //     // default of create new nodes
      //     $scope.dnedata[0] =  [];
      //     $scope.dnedata[1] =  0;
      //     $scope.dnedata[2] =  [];
      //     $scope.dnedata[3] =  $scope.item;

      // };

      $scope.dnedata = [];
      $scope.dnedata[0] =  [];
      $scope.dnedata[1] =  0;
      $scope.dnedata[2] =  [];
      $scope.dnedata[3] =  $scope.item;

      $scope.dneGetConnections = function() {
            
          console.log("DEBUG: training.controllers.D3DirectedNodeCtrl.dneGetConnections hello : ", "hello");
            
          var user = userServices.getUser();
          var wrkConnections = [];
          wrkConnections = userServices.getLocalConnections(); //push(userServices.getLocalConnections());
          if ( wrkConnections.length ) {
             $scope.dnedata[0] =  wrkConnections[0];
             $scope.dnedata[1] =  wrkConnections[1];
             $scope.dnedata[2] =  wrkConnections[2];
             $scope.dnedata[3] =  wrkConnections[3];
            
          }
      
    
      };

      // $scope.d3OnClick = function(item){
        
      //   alert(item.name);
        
      //   if ( itemm ) { 
      //     item = false; 
      //   } else {
      //     item = true; 
      //   }
        
      // };
  
  }]);
  
  angular.module('training.controllers').controller('D3NodeEditCtrl', ['$scope', function($scope ){
      $scope.title = "D3NodeEditCtrl";
      $scope.d3Nodes = [
        {name: "Greg", score:98},
        {name: "Ari", score:96},
        {name: "Loser", score: 48},
        {name: "Winner", score: 23}
      ];
  
      // $scope.d3OnClick = function(){
      //   // alert(item.name);
      //   alert("hello");
      // };
  
  }]);

  angular.module('training.controllers').controller('DemoCtrl', ['$scope', function($scope ){
      $scope.title = "DemoCtrl";
      $scope.d3Nodes = [
        {name: "Greg", score:98},
        {name: "Ari", score:96},
        {name: "Loser", score: 48},
        {name: "Winner", score: 23}
      ];
  
      // $scope.d3OnClick = function(){
      //   // alert(item.name);
      //   alert("hello");
      // };
  
  }]);
}());