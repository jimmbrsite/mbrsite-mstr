
(function() {

    'use strict';

    /*
     * Creating training module
     */

    angular.module('training', ['ngRoute', 'xeditable', 'ui.bootstrap', 'training.controllers', 'training.services','training.directives','training.filters']);

    angular.module('training').config(['$routeProvider', function($routeProvider) {
       
        $routeProvider
            .when('/home', {controller:'MainCtrl', templateUrl:'common/partials/home.html'})
            .when('/searchX', {controller:'MainCtrl', templateUrl:'common/partials/searchX.html'})
            .when('/locator', {controller:'MainCtrl', templateUrl:'common/partials/locator.html'})
            .when('/events', {controller:'MainCtrl', templateUrl:'common/partials/events.html'})
            .when('/entertainment', {controller:'MainCtrl', templateUrl:'common/partials/entertainment.html'})
            .when('/calendars', {controller:'MainCtrl', templateUrl:'common/partials/calendars.html'})
            .when('/shopping', {controller:'MainCtrl', templateUrl:'common/partials/shopping.html'})
            .when('/golfing', {controller:'MainCtrl', templateUrl:'common/partials/golfing.html'})
            .when('/signup', {controller:'MainCtrl', templateUrl:'common/partials/signup.html'})
            .when('/lodging', {controller:'MainCtrl', templateUrl:'common/partials/lodging.html'})
            .when('/dining', {controller:'MainCtrl', templateUrl:'common/partials/dining.html'})
            .when('/map', {controller:'MainCtrl', templateUrl:'common/partials/map.html'})
            .when('/realEstate', {controller:'MainCtrl', templateUrl:'common/partials/realEstate.html'})
            .when('/activities', {controller:'MainCtrl', templateUrl:'common/partials/activities.html'})
            .when('/recreation', {controller:'MainCtrl', templateUrl:'common/partials/recreation.html'})
            .when('/about', {controller:'MainCtrl', templateUrl:'common/partials/about.html'})
            .when('/contactus', {controller:'MainCtrl', templateUrl:'common/partials/contactus.html'})

            .when('/users', {controller:'UsersCtrl', templateUrl:'user/partials/user-list.html'})
            .when('/users/profile/:userId', {controller:'UserProfileCtrl', templateUrl:'user/partials/user-myprofile.html'})
            .when('/users/profile/tabs/:userId', {controller:'UserProfileCtrl', templateUrl:'user/partials/user-mytabs.html'})
            .when('/users/create', {controller:'UsersCreateCtrl', templateUrl:'user/partials/user-create.html'})
            .when('/users/edit/:userId', {controller:'UsersEditCtrl', templateUrl:'user/partials/user-edit.html'})
            .when('/d3/demo', {controller:'DemoCtrl', templateUrl:'common/partials/d3Example.html'})
            // .when('/users/profile/:userId', {controller:'UsersEditCtrl', templateUrl:'user/partials/user-myprofile.html'})
            .when('/users/login', {controller:'UsersLoginCtrl', templateUrl:'user/partials/user-login.html'})
            .when('/hello', {controller:'MainCtrl', templateUrl:'common/partials/helloPartialAlchemy.html'})
            .otherwise({redirectTo:'/home'});
    }]);

    /*
     * USER
     * Create user controller modules
     */
    angular.module('training.controllers', []);
    
    /*
     * Create  user services module
     */
    angular.module('training.services', []);

    /*
     * Create user directives
     */
    angular.module('training.directives', []);

    /*
     * Create user filters
     */
    angular.module('training.filters', []);

    
}());