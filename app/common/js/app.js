
(function() {

    'use strict';

    /*
     * Creating training module
     */

    angular.module('training', ['ngRoute', 'xeditable', 'ui.bootstrap', 'ngFileUpload', 'training.controllers', 'training.services','training.directives','training.filters']);
    // angular.module('training', ['ngRoute', 'xeditable', 'ui.bootstrap', 'training.controllers', 'training.services','training.directives','training.filters']);
    angular.module('training').config(['$routeProvider', function($routeProvider) {
       
        $routeProvider
            .when('/home', {controller:'MainCtrl', templateUrl:'public/partials/home.html'})
            .when('/searchX', {controller:'MainCtrl', templateUrl:'public/partials/searchX.html'})
            .when('/locator', {controller:'MainCtrl', templateUrl:'public/partials/locator.html'})
            .when('/events', {controller:'MainCtrl', templateUrl:'public/partials/events.html'})
            .when('/entertainment', {controller:'MainCtrl', templateUrl:'public/partials/entertainment.html'})
            .when('/calendars', {controller:'MainCtrl', templateUrl:'public/partials/calendars.html'})
            .when('/shopping', {controller:'MainCtrl', templateUrl:'public/partials/shopping.html'})
            .when('/golfing', {controller:'MainCtrl', templateUrl:'public/partials/golfing.html'})
            .when('/signup', {controller:'MainCtrl', templateUrl:'public/partials/signup.html'})
            .when('/lodging', {controller:'MainCtrl', templateUrl:'public/partials/lodging.html'})
            .when('/dining', {controller:'MainCtrl', templateUrl:'public/partials/dining.html'})
            .when('/map', {controller:'MainCtrl', templateUrl:'public/partials/map.html'})
            .when('/realEstate', {controller:'MainCtrl', templateUrl:'public/partials/realEstate.html'})
            .when('/activities', {controller:'MainCtrl', templateUrl:'public/partials/activities.html'})
            .when('/recreation', {controller:'MainCtrl', templateUrl:'public/partials/recreation.html'})
            .when('/about', {controller:'MainCtrl', templateUrl:'public/partials/about.html'})
            .when('/contact', {controller:'MainCtrl', templateUrl:'public/partials/contact.html'})
            .when('/jobs', {controller:'MainCtrl', templateUrl:'public/partials/jobs.html'})
            .when('/waterfalls', {controller:'MainCtrl', templateUrl:'public/partials/waterfalls.html'})
            .when('/store', {controller:'MainCtrl', templateUrl:'public/partials/store.html'})
            .when('/public-services', {controller:'MainCtrl', templateUrl:'public/partials/public-services.html'})

            .when('/users', {controller:'UsersCtrl', templateUrl:'user/partials/user-list.html'})
            .when('/users/adminhome', {controller:'UserAdminHomeCtrl', templateUrl:'user/partials/user-adminHome.html'})
            .when('/users/adminconsole', {controller:'UserAdminConsoleCtrl', templateUrl:'user/partials/user-adminConsole.html'})    
            //.when('/users/mbrsiteconsole', {controller:'UserMbrsiteConsoleCtrl', templateUrl:'user/partials/user-mbrsiteConsole.html'})    
            .when('/users/admanager', {controller:'UserAdminProfileCtrl', templateUrl:'user/partials/user-adManager.html'})    
            .when('/users/calendar', {controller:'UserAdminProfileCtrl', templateUrl:'user/partials/user-calendar.html'})    
            .when('/users/jobs', {controller:'UserAdminProfileCtrl', templateUrl:'user/partials/user-jobs.html'})    
            .when('/users/other', {controller:'UserAdminProfileCtrl', templateUrl:'user/partials/user-myprofile.html'})    
            .when('/users/profile/:userId', {controller:'UserAdminProfileCtrl', templateUrl:'user/partials/user-myprofile.html'})
            .when('/users/profile/tabs/:userId', {controller:'UserAdminProfileCtrl', templateUrl:'user/partials/user-mytabs.html'})
            .when('/users/create', {controller:'UserCreateCtrl', templateUrl:'user/partials/user-create.html'} )
            .when('/users/edit/:userId', {controller:'UsersEditCtrl', templateUrl:'user/partials/user-edit.html'})
            //.when('/mbrsites/mbrsiteconsole', {controller:'MbrsiteAdminConsoleCtrl', templateUrl:'component/partials/mbrsite-adminConsole.html'})    
            .when('/mbrsites/mbrsiteconsole/', {controller:'MbrsiteAdminConsoleCtrl', templateUrl:'component/partials/mbrsite-adminConsole.html'})    
            .when('/mbrsites/page', {controller:'MbrsiteAdminRelationShipManagerCtrl', templateUrl:'component/partials/mbrsite-adminRelationShipManager.html'})    
            .when('/mbrsites/profile', {controller:'MbrsiteAdminProfileManagerCtrl', templateUrl:'component/partials/mbrsite-adminProfileManager.html'})    
            .when('/d3/demo', {controller:'DemoCtrl', templateUrl:'public/partials/d3Example.html'})
            // .when('/users/profile/:userId', {controller:'UsersEditCtrl', templateUrl:'user/partials/user-myprofile.html'})
            .when('/users/login', {controller:'UsersLoginCtrl', templateUrl:'user/partials/user-login.html'})
            .when('/hello', {controller:'MainCtrl', templateUrl:'public/partials/helloPartialAlchemy.html'})
            .otherwise({redirectTo:'/home'});
    }]);

    /*
     * USER
     * Create user controller modules
     */
    angular.module('training.controllers', []);
    
    /*
     * Create  user services module
     */
    angular.module('training.services', []);

    /*
     * Create user directives
     */
    angular.module('training.directives', []);

    /*
     * Create user filters
     */
    angular.module('training.filters', []);

    
}());