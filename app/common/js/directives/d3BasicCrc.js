(function () {

  'use strict';

  angular.module('training.directives').directive('d3Bars', ['$window', '$timeout', 'd3Service', function( $window, $timeout, d3Service ) {
      console.log("DEBUG:training.directives.d3-Barse enhanced start: ");
      return {
          restrict: 'EA',
          scope: {
            data: '=',
            label: '@',
            onClick: '&'
          },
          link: function(scope, ele, attrs) {
            d3Service.d3().then(function(d3) {
              var renderTimeout;
              var margin = parseInt(attrs.margin) || 20,
                  barHeight = parseInt(attrs.barHeight) || 20,
                  barPadding = parseInt(attrs.barPadding) || 5;
     
              var svg = d3.select(ele[0])
                .append('svg')
                .style('width', '100%');
     
              $window.onresize = function() {
                scope.$apply();
              };
     
              scope.$watch(function() {
                return angular.element($window)[0].innerWidth;
              }, function() {
                scope.render(scope.data);
              });
     
              scope.$watch('data', function(newData) {
                scope.render(newData);
              }, true);
     
              scope.render = function(data) {
                svg.selectAll('*').remove();
     
                if (!data) return;
                // if (renderTimeout) clearTimeout(renderTimeout);
                // if (renderTimeout) $timeout.cancel(renderTimeout);
     
                // renderTimeout = $timeout(function() {
                //   var width = d3.select(ele[0])[0][0].offsetWidth - margin,96
                //       height = scope.data.length * (barHeight + barPadding),
                //       color = d3.scale.category20(),
                //       xScale = d3.scale.linear()
                //         .domain([0, d3.max(data, function(d) {
                //           return d.score;
                //         })])
                //         .range([0, width]);
     
                //  svg.attr('height', height);
     
                  svg.selectAll('circle')
                    .data(data)
                    .enter()
                    .append('circle')
                    .on('click', function(d,i) {
                        return scope.onClick({item: d});
                      })
                      //.attr('height', barHeight)
                      // .attr('width', 140)
                      // .attr('x', Math.round(margin/2))
                      // .attr('y', function(d,i) {
                      //   return i * (barHeight + barPadding);
                      .attr('class', 'bubble')
                      .attr('cy', function(d){
                            return 100;
                       })
                      .attr('cx', function(d){
                            return (d.score)*2;
                       })
                      .attr('r', function(d){
                            return 10 ;  // d.score
                       });
                  // d3Bars
                  //      .attr('fill', function(d) {
                  //          return color(d.score);
                  //      })
                  //      .transition()
                  //      .duration(1000)
                  //      .attr('width', function(d) {
                  //          return xScale(d.score);
                  //      });
                  svg.selectAll('text')
                    .data(data)
                    .enter()
                      .append('text')
                      .attr('fill', '#fff')
                      .attr('y', function(d,i) {
                        return i * (barHeight + barPadding) + 15;
                        //return i * ( barHeight + d.score ) + 15;
                      })
                      .attr('x', 15)
                      .text(function(d) {
                        return d.name + " (scored: " + d.score + ")";
                      });
                }    // , 200);
              }     // ;
               
            );      // });
            
          }}  //  eof link/return
   }]); // eof anglar modual
}()); // eof function