(function () {

  'use strict';

  angular.module('training.directives').directive('d3Nodeditor', ['$window', '$timeout', 'd3Service', function( $window, $timeout, d3Service ) {
      console.log("DEBUG:training.directives.d3-NodeEditor enhanced start: ");
      return {
          restrict: 'EA',
          scope: {
            data: '=',
            label: '@',
            onClick: '&'
          },
          link: function(scope, ele, attrs) {

            d3Service.d3().then(function(d3) {
                    
                    var wrkNodeId = 0;   // used as seed for nodeId
                    var wrkLinkId = 0;   // used as seed for linkId
                    //  console.log("hello");
                    // 960 x 500    
                    var width = Math.max(100, window.innerWidth),
                        height = Math.max(50, window.innerHeight),
                        fill = d3.scale.category20();

                    // mouse event vars
                    var selected_node = null,
                        selected_link = null,
                        mousedown_link = null,
                        mousedown_node = null,
                        mouseup_node = null;

                    // init svg
                    var outer = d3.select("#surface")
                        .append("svg:svg")
                        .attr("width",  '100%')   // width
                        .attr("height", height)
                        .attr("pointer-events", "all")
                        .style('width', '100%');

                    var vis = outer
                        .append('svg:g')
                        .call(d3.behavior.zoom().on("zoom", rescale))
                        .on("dblclick.zoom", null )
                        .append('svg:g')
                        .on("mousemove", mousemove)
                        .on("mousedown", mousedown)
                        .on("mouseup", mouseup);
                      

                    vis.append('svg:rect')
                        .attr('width', '100%')     // width
                        .attr('height', height)
                        .attr('fill', 'white');

                    // init force layout
                    var force = d3.layout.force()
                        .size([width, height])
                        .nodes([{nodeId: 0, text:'start' }]) // initialize with a single node 
                        .linkDistance(50)
                        .charge(-200)
                        .on("tick", tick);
                     

                    // line displayed when dragging new nodes
                    var drag_line = vis.append("line")
                        .attr("class", "drag_line")
                        .attr("x1", 0)
                        .attr("y1", 0)
                        .attr("x2", 0)
                        .attr("y2", 0);

                    // get layout properties
                    var nodes = force.nodes(),
                        links = force.links(),
                        node = vis.selectAll(".node"),
                        link = vis.selectAll(".link");

                    // add keyboard callback
                    d3.select(window)
                       .on("keydown", keydown);

                    redraw();

                    //focus on svg
                    //vis.node().focus();
                    // DEBUG used to track link/node id  
                    function mouseover_link() {






                      var tmpNode = d3.select(link);
                      var tmpLine = d3.select(this);


// 1. Need to d3.select(this)
// 2. then extract data from "__data__" array
// 3. update the contents of data array as needed
// 4. write into array. how ??????  

                      var tmpJbs = tmpLine;   ///.attr('linkId'); 
                      console.log("mouseover link tmpJbs : ",  tmpJbs );  
                      tmpJbs.html('stests'); 
                      //tmpLine.html("testss");
                      console.log("mouseover link tmpLine: ",  tmpLine[0][0].__data__ );  // tmpLine[0][0].__data__ 
                      console.log("mouseover link tmpJbs : ",  tmpJbs );  
                      console.log("mouseover link obj    : ",  tmpNode[0][0][0][0].__data__ );
                      console.log("mouseover link id     : ",  tmpNode[0][0][0][0].__data__.linkId );
                      // if (selected_node || selected_link) {

                      //tmpJbs = tmpLine.attr('linkId').value = 11;
                      //console.log("mouseover link tmpJbs:: ",  tmpJbs );  
                      
                      //   var tmp = prompt("new kink text");
                      //   if (tmp) {
                      //       if ( selected_node ){
                      //         console.log("node : ", selected_node );
                      //         selected_node.text = tmp;
                      //         //NOTE: Add logic to update the link "line"
  
                      //       } else if ( selected_link ) {
                      //         console.log("link : " , selected_link );
                      //         selected_link.text = tmp; 
                      //         //NOTE: Add logic to update the link "line"

                      //       }
                      //   }  
                            
                      // }
                      //  redraw(); 
                      //return; 

                    }

                     function mouseover_node() {

                       // var point = d3.mouse(this);
                       // console.log("selected_ node : ",  selected_node  );
                       // console.log("selected_link : ",  selected_link  );
                       
                       // console.log("mousedown_link : ",  mousedown_link  );
                       // console.log("mousedown_node : ",  mousedown_node  );
                       // console.log("mouseup_node : ",  mouseup_node  );
                       // console.log("d3.mouse.this : ",  point  );
                       
                      var tmpNode = d3.select(node);
                      console.log("mouseover node obj: ",  tmpNode[0][0][0][0].__data__ );
                      console.log("mouseover node id : ",  tmpNode[0][0][0][0].__data__.nodeId );
                      var promptTxt = tmpNode[0][0][0][0].__data__.text ;
                       
                      //only make change if node is selected 
                      if (( selected_node ) && (selected_node.nodeId == tmpNode[0][0][0][0].__data__.nodeId )) {
                        
                        //console.log("mouseover node id : ", selected_node.nodeId );
                        //if ( selected_node.text == tmpNode[0][0][0][0].__data__.text ) {
                        // console.log("selected node : ", selected_node.text );
                        //  console.log("this node : ", this.text );
                        var tmp = prompt("mouseover circle 'node' text: " + promptTxt);
                        if (tmp) {
                           tmpNode[0][0][0][0].__data__.text = tmp;
                           selected_node = tmpNode;
                           //redraw();  
                        }  
                      }
                      return; 
                    }

                    // END DEBUG
                    function mousedown() {
                      if (!mousedown_node && !mousedown_link) {
                        // allow panning if nothing is selected
                        vis.call(d3.behavior.zoom().on("zoom", rescale));

                        return;
                      }
                    }
 
                    function mousemove() {
                      if (!mousedown_node) return;

                      // update drag line
                      drag_line
                          .attr("x1", mousedown_node.x)
                          .attr("y1", mousedown_node.y)
                          .attr("x2", d3.svg.mouse(this)[0])
                          .attr("y2", d3.svg.mouse(this)[1]);
                    }

                    function mouseup() {
                      if (mousedown_node) {
                        // hide drag line
                        drag_line
                          .attr("class", "drag_line_hidden")

                        if (!mouseup_node) {
                          // add node - 
                          // MODIFIED added nodeId
                          //wrkNodeId = wrkNodeId++;
                          var point = d3.mouse(this),
                            node = {x: point[0], y: point[1], nodeId: bumpId("node"), text: 'jimsb' },
                            n = nodes.push(node);

                          // select new node
                          selected_node = node;
                          selected_link = null;
                          console.log("target node: ", node);
                         
                          // add link to mousedown node
                          links.push({source: mousedown_node, target: node, linkId:  bumpId("link"), text:"jims"});

                        }
 
                        redraw();
                      }

                      // clear mouse event vars
                      resetMouseVars();
                    }
 
                    function resetMouseVars() {
                      mousedown_node = null;
                      mouseup_node = null;
                      mousedown_link = null;
                    }

                    function tick() {
                      link.attr("x1", function(d) { return d.source.x; })
                          .attr("y1", function(d) { return d.source.y; })
                          .attr("x2", function(d) { return d.target.x; })
                          .attr("y2", function(d) { return d.target.y; });

                      node.attr("cx", function(d) { return d.x; })
                          .attr("cy", function(d) { return d.y; });
                    }

                    // rescale g
                    function rescale() {
                    
                      var trans=d3.event.translate;
                      //var trans = "197.65109824538996,88.49214818709784";
                      var scale=d3.event.scale;
                      //var scale  = "0.5140569133280332"; 
                      vis.attr("transform",
                          "translate(" + trans + ")"
                          + " scale(" + scale + ")");
                     
                    }

                    // redraw force layout
                    function redraw() {
                      // MODIFIED incrmnt wrkNodeId and wrklinkId
                      //wrkLinkId = wrkLinkId++;  
                      //wrkNodeId = wrkNodeId++;

                      link = link.data(links);
      
                      link.enter().insert("line", ".node")
                          .attr("class", "link")
                          // MODIFIFIED: added attr name

                          //.attr("linkId", bumpId("link") )
                          .on("mouseover", mouseover_link )
                          .on("mousedown", 
                            function(d) { 
                              mousedown_link = d;
                              
                              // DEBUG    
                              // alert("mousedown_link 199: " + mousedown_link.linkId);
                              

                              if (mousedown_link == selected_link) selected_link = null;
                              else selected_link = mousedown_link; 
                              selected_node = null; 
                              redraw(); 
                            })

                      link.exit().remove();

                      link
                        .classed("link_selected", function(d) { return d === selected_link; });

                      node = node.data(nodes);

                      node.enter().insert("circle")
                          .attr("class", "node")
                          .attr("r", 5)
                          // MODIFIFIED: added attr name
                          //.attr("nodeId",  bumpId("node") )
                          .attr("text","jims2")
                          .on("mouseover", mouseover_node )
                          //
                          .on("mousedown", 
                          
                            function(d) { 
                              // disable zoom
                              vis.call(d3.behavior.zoom().on("zoom", null));

                              mousedown_node = d;
                              
                              //DEBUG 
                              // //console.log("mousedown circle 196", mousedown_node);
                              // alert("mousedown_node 228: " + mousedown_node.index);
                              
                              if (mousedown_node == selected_node) selected_node = null;
                              else selected_node = mousedown_node; 
                              selected_link = null; 

                              // reposition drag line
                              drag_line
                                .attr("class", "link")
                                .attr("x1", mousedown_node.x)
                                .attr("y1", mousedown_node.y)
                                .attr("x2", mousedown_node.x)
                                .attr("y2", mousedown_node.y);

                              redraw(); 
                            })
                          .on("mousedrag", function(d) {
                                // redraw();
                            })
                          .on("mouseup", 
                            function(d) { 
                              if (mousedown_node) {
                                mouseup_node = d; 
                                // DEBUG
                                // console.log("mouseup_node d 224: ", d);
                                //alert("mouseup_link: " + mouseup_link.index);
                                
                                if (mouseup_node == mousedown_node) { resetMouseVars(); return; }
                                 

                                // add link
                                var link = {source: mousedown_node, target: mouseup_node, linkId: bumpId('link'), text: 'jims3' };
                                //DEBUG
                                //console.log("new link source: ", mousedown_node );
                                //console.log("new link target: ", mouseup_node);

                                links.push(link);

                                // select new link
                                selected_link = link;
                                selected_node = null;

                                 // enable zoom
                                vis.call(d3.behavior.zoom().on("zoom"), rescale);
                                redraw();
                              } 
                            })
                        .transition()
                              .duration(750)
                              .ease("elastic")
                              .attr("r", 6.5);

                      node.exit().transition()
                          .attr("r", 0)
                          .remove();

                      node
                        .classed("node_selected", function(d) { return d === selected_node; });

                      if (d3.event) {
                        // prevent browser's default behavior
                        d3.event.preventDefault();
                      }

                      force.start();
                    }
 
                    function spliceLinksForNode(node) {
                      toSplice = links.filter(
                        function(l) { 
                          return (l.source === node) || (l.target === node); });
                      toSplice.map(
                        function(l) {
                          links.splice(links.indexOf(l), 1); });
                    }


                    function keydown() {
                      if (!selected_node && !selected_link) return;
                      switch (d3.event.keyCode) {
                        case 8: // backspace
                        case 46: { // delete
                          if (selected_node) {
                            nodes.splice(nodes.indexOf(selected_node), 1);
                            spliceLinksForNode(selected_node);
                          }
                          else if (selected_link) {
                            links.splice(links.indexOf(selected_link), 1);
                          }
                        
                          selected_link = null;
                          selected_node = null;
                          redraw();
                          break;
                        }
                      }
                    }
                    // ADDED Function
                    function bumpId( idName) {

                       if ( idName == "node" ) {
                         wrkNodeId = wrkNodeId + 1;
                         return  wrkNodeId;
                       } else if ( idName == "link" ){
                         wrkLinkId = wrkLinkId + 1;
                         return wrkLinkId;
                       }
                    }
               
            });  // eof d3 services ({})

          }};  //  eof link/return
   }]); // eof anglar modual
}()); // eof function
     