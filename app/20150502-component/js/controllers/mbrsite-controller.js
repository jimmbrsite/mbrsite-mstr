(function() {

    'use strict';

    /* *****************************
       *****************************
     *  MBRSITE CONSOLE  Ctrl
    */
    angular.module('training.controllers').controller('MbrsiteAdminConsoleCtrl', ['$scope', '$location', 'training.services.user', 'training.services.component', function($scope, $location, userService, componentService ) {
            

    /*** Component PROFILE SECTION 
     *
     *  Admin Profile Ctrl ************************************
     * 
     *   the component edit ctrl collects remote component data and sets different content in $scope.user as determined by userId
    */
        // broadcasting test source  BOTTOM Controller
        //  added to UserListCtrl (top) , UserCreateCtrl (Middle),  UserAdminProfileCtrl  (Bottom ) 
        //$scope.mbrProfileEditForm.editItem = '';  
        $scope.topValue = 0;
        $scope.middleValue = 0;
        $scope.bottomValue = 0;

        $scope.$watch('bottomValue', function() {
            userService.updateBottomValue($scope.bottomValue);
        });

        $scope.$on('valuesUpdated', function() {
            $scope.topValue = userService.topValue;
            $scope.middleValue = userService.middleValue;
        });
        // end of Broadcast Test
       

        $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            
            }         
        };

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {
            return user._id;
            }         
        };
          
        $scope.upricategory = ""; 
        $scope.up2ndcategory = "";
        $scope.up3rdcategory = "";
        //$scope.selOrgCategory = componentService.getBizCategories();
        
        //console.log("DEBUG: MbrsiteAdminProfileCtrl selOrgCategory : ",  componentService.getBizCategories() );
        
        $scope.setUMChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'pri':
                     $scope.upricategory = $scope.selOrgCategory[choice];
                    break;
                case '2nd':
                   $scope.up2ndcategory =  $scope.selOrgCategory[choice];
                    break;
                case '3rd':
                    $scope.up3rdcategory = $scope.selOrgCategory[choice];
                    break;    
                default:
                    console.log("ERROR: MbrsiteAdminProfileCtrl setUSChoices  *************************", choice );    
            }
            return 
        };


       $scope.saveProfile = function() {
            // $scope.profileItem
            console.log("DEBUG: MbrsiteAdminProfileCtrl : ***** pre save *******", $scope.profileItem );

            //$scope.usersedititem = angular.copy(nuuser);
            // $scope.bizEditItem.profilelabelId = $scope.utype;
            // $scope.bizEditItem.role = $scope.urole;
            // $scope.bizEditItem.permissions = $scope.uperm;
         // ***************
         // NEED TO REVISIT ADD USER LOGIC TO INCLUDE NEW PROPERTIES AND LAYOUT FOR MBRSITE COMPONENT
         //****************

            // keyId : req.body.keyId || targetKeyId, 
            // lastupdated: req.body.lastupdated || "",        
            // updatedById: req.body.updatedById || "",
            //    isActive: req.body.isActive || "",      // ** Required, set false at creation-relates to state


            // "Member"   user not allowed to alter the following fields
            //$scope.bizEditItem.userKeyId:    ** set to "member" userID at creation = retdIds._id;
            //$scope.bizEditItem.dateCreated:  ** set at creation
            //$scope.bizEditItem.approveDate:  ** Required, set at time of obj creation
            //$scope.bizEditItem.approvedById: ** Required, set at time of obj creation
            // other properties not set in form
           
            
            $scope.profileItem.lastUpdated =  new Date();
            $scope.profileItem.updatedById =  userService.user.keyId; 
            $scope.profileItem.isActive = false;            
            $scope.profileItem.approveDate = new Date() ;
            $scope.profileItem.status  = 'Yes';    // ** Required, set at system level-relates to validity of   
            $scope.profileItem.isActive = 'Yes';   //  ** Required, set false at creation-relates to state of
            //$scope.bizEditItem.category = ''; 

            $scope.profileItem.category[0] = $scope.upricategory; 
            $scope.profileItem.category[1] = $scope.up2ndcategory;
            $scope.profileItem.category[2] = $scope.up3rdcategory;

            $scope.profileItem.keywords[0] = $scope.profileItem.category[0];   // temp fix for key words

            //$scope.bizEditItem.cFirstName[0] = $scope.bizEditItem.category[0];   // temp fix for key words 
            // console.log("DEBUG: MbrsiteAdminPro
        //ProfileCtrl.componentService.saveComponent :  pre save ***", $scope.bizEditItem );
        // ***************
        // NEED TO REVISIT BEFORE CREATING COMPONET PART tabsetBar and tabsetPanes 
        //                        try adding properties at this time for the mbrsite 
        //                        BarId relates to PaneId, PaneId relates to ContnetID
        //*********
           /*   ThIS iS GOOD CODE JUST NOT WANTING TO OVERWRITE TEST DATA WITH 
            $scope.bizEditItem.tabBarId = [0];
            $scope.bizEditItem.tabBarHref = ["href='#tab0-1'"];
            $scope.bizEditItem.tabBarLabelText = ["Blank 2"];
            $scope.bizEditItem.tabBarisActive = [true];
            $scope.bizEditItem.tabBarOrder = [1] ;
            // tabPanes for each tabPane there can be several ContentModel entries
            $scope.bizEditItem.tabPaneIds = ["tab0-1"];
            $scope.bizEditItem.tabPaneTitleText = ["Blank Template Two"];
            // ContentModels for each contentModel there are thre content entries title,text,imgsrc
            $scope.bizEditItem.contentPaneIds = ["tab0-1"];
            $scope.bizEditItem.contentNameIds = ["contentOne"] ;                 // ["contentOne","contenTwo", ...]
            $scope.bizEditItem.contentIsOk = ["Support"];
            
            $scope.bizEditItem.contentModelTitleText = ["Sample Title 1"];
            $scope.bizEditItem.contentModelTextArea = ['mbrs-Etiam sit amet leo nec nunc auctor convallis vel at est. Aenean at metus id nisl posuere varius. In vel ipsum eu risus iaculis volutpat in ut metus. Integer eget sem eu massa pharetra vehicula ut id elit. Nam sagittis malesuada est, ut tempus diam ultricies eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla urna libero, finibus in metus mollis, aliquam interdum elit. Phasellus at sagittis lectus. Vestibulum scelerisque libero eget efficitur venenatis. Etiam nunc magna, maximus vel vehicula eget, bibendum eu urna. Phasellus quis turpis id lacus mollis dictum. Ut dui elit, efficitur vel orci eu, elementum interdum erat. Cras at leo volutpat, commodo metus in, dictum justo. Cras id vulputate ex. Nullam et aliquet arcu, id elementum magna. Phasellus faucibus sapien at dolor ultrices pellentesque.'];
            $scope.bizEditItem.contentModelImgSrcs = ["common/bootstrap/images/customer_service2.jpg"];
            $scope.bizEditItem.contentModelImgTextArea: req.body.contentModelImgTextArea || [],  // ["common/bootstrap/images/customer_service2.jpg", ...];
            $scope.bizEditItem.contentModelLinkTarget: req.body.contentModelLinkTarget || [],  // ["href='#'"];
            $scope.bizEditItem.contentModelLinkTextArea: req.body.contentModelLinkTextArea || // ["Click here for more information"];
            */

            componentService.saveComponent($scope.profileItem, function(err, status) {
               
                if(err) {

                    console.log("ERROR: MbrsiteAdminConsoleCtrl component not updated *****: ", err);
                }
                else {

                    console.log("DEBUG:MbrsiteAdminConsoleCtrl component updated with status: ", status );
                    //$scope.usersedititem = '';
                    //$location.path('/#/users/profil/:#tab3-5');
                    //$scope.$apply(); 
                       
                      // commented $scope.usersEditItem = {};
                   // commented  });
                }

            });
        };

        $scope.cancelProfile = function () {
            
            console.log("INFO: MbrsiteAdminConsoleCtrl-canceledProfile update");
            
            //$location.path('/users/profile/' + userId );
            
                
        };

        $scope.initMbrsite = function ( part  ) {

          console.log("DEBUG: MbrsiteAdminConsoleCtrl-initMbrsite pre getUser :", user );
          
          var user = userService.getUser();
          
          console.log("DEBUG: MbrsiteAdminConsoleCtrl-initMbrsite post getUser:", user );

          if(user && user.authToken) {
            
            /* 
            var cLabels = ['Components','Mbrsite',part];     // part: Profile,Tabset or Panes 
                            
            componentService.getMbrsite( user.keyId, cLabels , function(err, mbrsite) {
            */
            var cLabels = ['Components','Mbrsite',part];     // part: Profile,Tabset or Panes 
            
            // NOTE: we may want to form cypher query here??
            var parms = { matchProfile: {
                            userKeyId : user.keyId,
                            name : '',
                            pId : '', 
                            isActive : true,
                            status : '',
                            permission : ''
                            },  
                          relationShip: {
                            isActive : true,
                            status : ''
                            },  
                          cLabels :  cLabels
                        };
                          
                         
            console.log("DEBUG: MbrsiteAdminConsoleCtrl-initMbrsite parms *********** : ", parms );

            componentService.getMbrsite( parms, function(err, mbrsite) {
                if(err) {

                    console.log("ERROR: initMbrsite ");
                }
                else {

                    // may want to make this multiple mbrsites   
                    console.log("DEBUG: MbrsiteAdminConsoleCtrl-initMbrsite got mbrsite *********** : ", mbrsite);
                    //$scope.mbrProfileEditForm.editItem = mbrsite;
                    console.log("ERROR: MbrsiteAdminConsoleCtrl-initMbrsite post mbrsite:*************************: ", mbrsite ); 
/*
          fix: using  Mbrsite.profile, Mbrsite.Bars.relationship and Mbrsite.MyHome.ContentSet
                    a. fill prompts My Home, enable x-editable prompts, update changes 
                    b. create pageManager pageManager, enable add new ContentSet and relationShip
                    c. enable item a for modal activated update launched from admin search    
*/
                    //$scope.profileItem = mbrsite.profile;
                    $scope.profileItem = componentService.getLocalComponentPart('profile', '');
                    $scope.contentSets = componentService.getLocalComponentPart('contentSets',  0 );
                    console.log("DEBUG: MbrsiteAdminConsoleCtrl-getLocalComponentPart profile,contentSets via Service ************** ", $scope.profileItem, $scope.contentSets );
                    //$scope.relationShips = mbrsite.relationShips;
                    //$scope.contentSets = mbrsite.contentSets;  
               // componentService.setLocalComponent( mbrsite) ;
                    // fixed $scope.bizEditItem.lblArray = mbrsite.groupLabel + ":" + mbrsite.typeLabel + ":" + mbrsite.partLabel;  // 'Components:Mbrsite:Profile';                              
                    // $scope.upricategory = mbrsite.profile.category[0]; 
                    // $scope.up2ndcategory = mbrsite.profile.category[1];
                    // $scope.up3rdcategory = mbrsite.profile.category[2];
                    // $scope.selOrgCategory = componentService.getProfileCategories();

                    // $scope.localSiteProfile =  componentService.getLocalComponentPart('profile', '');
                    // $scope.localSiteContentSet =  componentService.getLocalComponentPart('contentsets', 0 );
                    //  console.log("ERROR: MbrsiteAdminConsoleCtrl-initMbrsite $scope.contentSets:*************************: ", $scope.contentSets ); 
                }
           
            });
            
          }
          else {
            console.log("DEBUG: MbrsiteAdminConsoleCtrl.initMbrsite not authenticated ** :", user.authToken );
            $location.path('/home');
          }

        };

    
        $scope.items = ['Sysadmin', 'Member', 'Subscriber', 'Associate', 'Support'];

               
        //$scope.localSiteContentSet =  componentService.getLocalComponentPart('contentsets', 0 );
        $scope.isAuthorized = 'support'       //  userService.getUserPermissions(); //  $scope.items[0];  //
      
        $scope.isOK = userService.getUserPermissions();

        // console.log("DEBUG: MbrsiteAdminConsoleCtrl-localSiteProfile data via Service ************** ", $scope.contentSets.contentModelTextArea[0] );
        // try {
              
        //     $scope.tabPane = {
        //         tabId : 'TabID',                       // $scope.localSiteProfile.paneTabIds[0],            // $scope.localMbrsite.paneTabIds[0],
        //         titleText: 'Title Text',                  //  {{ profileItem.name }}',                                    //$scope.localSiteProfile.paneTabTitles[0],
        //         contentOne: {
        //                   // tabId : $scope.contentSets[0].contentTabIds[0],
        //                   // tabNameId: $scope.localSiteProfile.contentTabNameIds[0],
        //                   // isOK: $scope.localSiteProfile.contentIsOk[0],
        //                   // titleText: $scope.localSiteProfile.contentModelTitleText[0],
        //                   textArea: $scope.contentSets.contentModelTextArea[0]
        //                   // imgSrc:  $scope.localSiteProfile.contentModelImgSrcs[0],
        //                   // imgTextArea: $scope.localMbrsite.contentModelImgTextArea[0],
        //                   // linkTarget:  $scope.localSiteProfile.contentModelLinkTarget[0],
        //                   // linkTextArea: $scope.localSiteProfile.contentModelLinkTextArea[0]
        //         },
        //         contentTwo: {
        //                //    isOK:   localMbrsite.tabContentIsOk[0],
        //                // titleone:  localMbrsite.tabContentModelTitles[0],
        //                //  spanone:  localMbrsite.tabContentModelSpans[0],
        //                // imgsrcone: localMbrsite.tabContentModelImgsSrc[0]
        //                 titleText:  'tmpl-Ssub title 2',
        //                 textArea:   'tmpl-Donec blandit suscipit ante, quis viverra urna ornare vitae. Vestibulum dolor ligula, scelerisque at lectus in, suscipit lobortis enim. Nunc at mattis sapien, quis mollis dolor. Donec blandit diam velit, quis pellentesque ante lobortis a. Aliquam et diam eget nibh tempus vulputate non ut tellus. Mauris sagittis vehicula dapibus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Aenean a quam ut nisl sollicitudin fringilla eu faucibus massa. Duis cursus augue at sapien tempus, vitae ornare tortor euismod. Maecenas pharetra tincidunt felis non consectetur. Maecenas condimentum mi sit amet nibh tempor accumsan. In hac habitasse platea dictumst. Duis accumsan, metus dictum fringilla facilisis, purus magna dictum lacus, eget consequat erat purus sit amet quam. Nam luctus ac ipsum non rhoncus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Fusce elementum, erat vitae gravida tincidunt, justo lorem maximus leo, a iaculis purus risus sit amet felis. Donec a lorem sagittis massa consequat tristique.'
        //         },
        //         contentThree: {
        //                //    isOK:   localMbrsite.tabContentIsOk[0],
        //                // titleone:  localMbrsite.tabContentModelTitles[0],
        //                //  spanone:  localMbrsite.tabContentModelSpans[0],
        //                // imgsrcone: localMbrsite.tabContentModelImgsSrc[0]
        //                  titleText:  'tmpl-Sub title 3',
        //                  textArea:   'tmpl-Donec blandit suscipit ante, quis viverra urna ornare vitae. Vestibulum dolor ligula, scelerisque at lectus in, suscipit lobortis enim. Nunc at mattis sapien, quis mollis dolor. Donec blandit diam velit, quis pellentesque ante lobortis a. Aliquam et diam eget nibh tempus vulputate non ut tellus. Mauris sagittis vehicula dapibus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Aenean a quam ut nisl sollicitudin fringilla eu faucibus massa. Duis cursus augue at sapien tempus, vitae ornare tortor euismod. Maecenas pharetra tincidunt felis non consectetur. Maecenas condimentum mi sit amet nibh tempor accumsan. In hac habitasse platea dictumst. Duis accumsan, metus dictum fringilla facilisis, purus magna dictum lacus, eget consequat erat purus sit amet quam. Nam luctus ac ipsum non rhoncus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Fusce elementum, erat vitae gravida tincidunt, justo lorem maximus leo, a iaculis purus risus sit amet felis. Donec a lorem sagittis massa consequat tristique.' 
        //         }
        //     };
        // }
        // catch(err) {
       
        //  //document.getElementById("demo").innerHTML = err.message;
        //  $scope.localSiteProfile =  componentService.getLocalComponentPart('profile', '');    
        // };



        $scope.updateThis = function(){
          
            alert("data saved");
       
        };

        $scope.uploadThis = function(){
          
            alert("file uploaded");
       
        };

        $scope.saveThisTab = function(){
          
            alert("save this tab");
       
        };

        $scope.postChangesThisTab = function(){
          // editable-text="user.name"
            alert("submit this tab: check with other tabs to see if changes /nare pending. if so, then give warning user");
            console.log("DEBUG: MbrsiteAdminConsoleCtrl-.postChangesThisTab data  ************** ", $scope.tabPane );

            /* One way to submit data on server is to define onbeforesave attribute pointing to some method of scope.
             Useful when you need to send data on server first and only then update local model (e.g. $scope.user).
             New value can be passed as $data parameter (e.g. <a ... onbeforesave="updateUser($data)">).

            The main thing is that local model will be updated only if method returns true or undefined (or promise resolved
            to true/undefined). Commonly there are 3 cases depending on result type:

            true or undefined: Success. Local model will be updated automatically and form will close.
            false:   Success. But local model will not be updated and form will close.   Useful when you want to update local model manually
                                                                                         (e.g. server changed values).
            string: Error. Local model will not be updated, form will not close, string will be shown as error message. Useful for validation and processing errors.

            */

             return " Not updated";
        };      
     
    }]);
    
    
    /*  **********************************
        **********************************
         MBRSITE PROFILE MANAGER Ctrl
    */  
    angular.module('training.controllers').controller('MbrsiteAdminProfileManagerCtrl', ['$scope', 'training.services.user', 'training.services.component', function($scope, userService, componentService ) {
        
        $scope.whichUser = function() {
            var user = userService.getUser();
            if(user) {

                return user.firstName + " " + user.lastName;
            
            }         
        };

        $scope.whichUserId = function() {
            var user = userService.getUser();
            if(user) {

                return user._id;
            }         
        };
        
        console.log("DEBUG: MbrsiteAdminProfileManagerCtrl ************** " );

        $scope.upricategory = ""; 
        $scope.up2ndcategory = "";
        $scope.up3rdcategory = "";
        //$scope.selOrgCategory = componentService.getBizCategories();
        //$scope.selOrgCategory = componentService.getBizCategories();
        
        //console.log("DEBUG: MbrsiteAdminProfileCtrl selOrgCategory : ",  componentService.getBizCategories() );
        
        $scope.setUMChoices = function( param, choice) {
            // set user radio style ( single selection) choices
             switch( param) {
                case 'pri':
                     $scope.upricategory = $scope.selOrgCategory[choice];
                    break;
                case '2nd':
                   $scope.up2ndcategory =  $scope.selOrgCategory[choice];
                    break;
                case '3rd':
                    $scope.up3rdcategory = $scope.selOrgCategory[choice];
                    break;    
                default:
                    console.log("ERROR: MbrsiteAdminProfileCtrl setUSChoices  *************************", choice );    
            }
            return 
        };


       $scope.saveProfile = function() {

            console.log("DEBUG: MbrsiteAdminProfileCtrl : ***** pre save *******", $scope.profileItem );

            //$scope.usersedititem = angular.copy(nuuser);
            // $scope.bizEditItem.profilelabelId = $scope.utype;
            // $scope.bizEditItem.role = $scope.urole;
            // $scope.bizEditItem.permissions = $scope.uperm;
         // ***************
         // NEED TO REVISIT ADD USER LOGIC TO INCLUDE NEW PROPERTIES AND LAYOUT FOR MBRSITE COMPONENT
         //****************

            // keyId : req.body.keyId || targetKeyId, 
            // lastupdated: req.body.lastupdated || "",        
            // updatedById: req.body.updatedById || "",
            //    isActive: req.body.isActive || "",      // ** Required, set false at creation-relates to state


            // "Member"   user not allowed to alter the following fields
            //$scope.bizEditItem.userKeyId:    ** set to "member" userID at creation = retdIds._id;
            //$scope.bizEditItem.dateCreated:  ** set at creation
            //$scope.bizEditItem.approveDate:  ** Required, set at time of obj creation
            //$scope.bizEditItem.approvedById: ** Required, set at time of obj creation
            // other properties not set in form
           
            
            $scope.profileItem.lastUpdated =  new Date();
            $scope.profileItem.updatedById =  userService.user.keyId; 
            $scope.profileItem.isActive = false;            
            $scope.profileItem.approveDate = new Date() ;
            $scope.profileItem.status  = 'Active';    // ** Required, set at system level-relates to validity of   
            $scope.profileItem.isActive = true;   //  ** Required, set false at creation-relates to state of
            //$scope.bizEditItem.category = ''; 

            $scope.profileItem.category[0] = $scope.upricategory; 
            $scope.profileItem.category[1] = $scope.up2ndcategory;
            $scope.profileItem.category[2] = $scope.up3rdcategory;

            //$scope.bizEditItem.keywords[0] = $scope.bizEditItem.category[0];   // temp fix for key words

            //$scope.bizEditItem.cFirstName[0] = $scope.bizEditItem.category[0];   // temp fix for key words 
            // console.log("DEBUG: MbrsiteAdminPro
        //ProfileCtrl.componentService.saveComponent :  pre save ***", $scope.bizEditItem );
        // ***************
        // NEED TO REVISIT BEFORE CREATING COMPONET PART tabsetBar and tabsetPanes 
        //                        try adding properties at this time for the mbrsite 
        //                        BarId relates to PaneId, PaneId relates to ContnetID
        //*********
           /*   ThIS iS GOOD CODE JUST NOT WANTING TO OVERWRITE TEST DATA WITH 
            $scope.bizEditItem.tabBarId = [0];
            $scope.bizEditItem.tabBarHref = ["href='#tab0-1'"];
            $scope.bizEditItem.tabBarLabelText = ["Blank 2"];
            $scope.bizEditItem.tabBarisActive = [true];
            $scope.bizEditItem.tabBarOrder = [1] ;
            // tabPanes for each tabPane there can be several ContentModel entries
            $scope.bizEditItem.tabPaneIds = ["tab0-1"];
            $scope.bizEditItem.tabPaneTitleText = ["Blank Template Two"];
            // ContentModels for each contentModel there are thre content entries title,text,imgsrc
            $scope.bizEditItem.contentPaneIds = ["tab0-1"];
            $scope.bizEditItem.contentNameIds = ["contentOne"] ;                 // ["contentOne","contenTwo", ...]
            $scope.bizEditItem.contentIsOk = ["Support"];
            
            $scope.bizEditItem.contentModelTitleText = ["Sample Title 1"];
            $scope.bizEditItem.contentModelTextArea = ['mbrs-Etiam sit amet leo nec nunc auctor convallis vel at est. Aenean at metus id nisl posuere varius. In vel ipsum eu risus iaculis volutpat in ut metus. Integer eget sem eu massa pharetra vehicula ut id elit. Nam sagittis malesuada est, ut tempus diam ultricies eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla urna libero, finibus in metus mollis, aliquam interdum elit. Phasellus at sagittis lectus. Vestibulum scelerisque libero eget efficitur venenatis. Etiam nunc magna, maximus vel vehicula eget, bibendum eu urna. Phasellus quis turpis id lacus mollis dictum. Ut dui elit, efficitur vel orci eu, elementum interdum erat. Cras at leo volutpat, commodo metus in, dictum justo. Cras id vulputate ex. Nullam et aliquet arcu, id elementum magna. Phasellus faucibus sapien at dolor ultrices pellentesque.'];
            $scope.bizEditItem.contentModelImgSrcs = ["common/bootstrap/images/customer_service2.jpg"];
            $scope.bizEditItem.contentModelImgTextArea: req.body.contentModelImgTextArea || [],  // ["common/bootstrap/images/customer_service2.jpg", ...];
            $scope.bizEditItem.contentModelLinkTarget: req.body.contentModelLinkTarget || [],  // ["href='#'"];
            $scope.bizEditItem.contentModelLinkTextArea: req.body.contentModelLinkTextArea || // ["Click here for more information"];
            */

            componentService.saveComponent($scope.profileItem, function(err, status) {
               
                if(err) {

                    console.log("ERROR: MbrsiteAdminProfileManagerCtrlcomponent-saveComponent not updated *****: ", err);
                }
                else {

                    console.log("DEBUG: MbrsiteAdminProfileManagerCtrlcomponent-saveComponent updated with status: ", status );
                    //$scope.usersedititem = '';
                    //$location.path('/#/users/profil/:#tab3-5');
                    //$scope.$apply(); 
                       
                      // commented $scope.usersEditItem = {};
                   // commented  });
                }

            });
        };

        $scope.cancelProfile = function () {
            
            console.log("INFO: MbrsiteAdminProfileManagerCtrl-canceled profile update");
            
            //$location.path('/users/profile/' + userId );
            
                
        };


        $scope.initMbrsiteProfile = function ( ) {

          console.log("DEBUG: MbrsiteAdminProfileManagerCtrl-initMbrsiteProfile-getLocalComponentPart :" );
        
           // var user = userService.getUser();
            
           $scope.profileItem = componentService.getLocalComponentPart('profile', '');
           $scope.upricategory = $scope.profileItem.category[0]; 
           $scope.up2ndcategory = $scope.profileItem.category[1];
           $scope.up3rdcategory = $scope.profileItem.category[2];                  //mbrsite.profile
           $scope.selOrgCategory = componentService.getProfileCategories();
       
        }; 

     //    $scope.OBS_initMbrsiteProfile = function ( part  ) {

     //      console.log("DEBUG: initMbrsite pre getUser :", user );
          
     //      var user = userService.getUser();
          
     //      console.log("DEBUG: initMbrsite post getUser:", user );

     //      if(user && user.authToken) {
            

     //        var cLabels = ['Components','Mbrsite',part];     // part: Profile,Tabset or Panes 
            
     //        // NOTE: we may want to form cypher query here??
     //        var parms = [ { params: { 'userKeyId' : user.keyId,
     //                         'name' : '',
     //                         'pId' : '', 
     //                         'access': {
     //                           'isActive' : true,
     //                            'status' : '',
     //                            'permission' : ''
     //                          }
     //                        }
     //                      }];  
                         
     //        console.log("DEBUG: got mbrsite *********** : ", parms );
     // ******************************
     //       var cLabels = ['Components','Mbrsite',part];     // part: Profile,Tabset or Panes 
            
     //        // NOTE: we may want to form cypher query here??
     //        var parms = { matchProfile: {
     //                        userKeyId : user.keyId,
     //                        name : '',
     //                        pId : '', 
     //                        isActive : true,
     //                        status : '',
     //                        permission : ''
     //                        },  
     //                      relationShip: {
     //                        isActive : true,
     //                        status : ''
     //                        },  
     //                      cLabels :  cLabels
     //                    };
                          
                         
     //        console.log("DEBUG: MbrsiteAdminConsoleCtrl-initMbrsite parms *********** : ", parms );

     //        componentService.getMbrsite( parms, function(err, mbrsite) 

     //        ****************
     //        componentService.getMbrsite( parms, function(err, mbrsite) {

     //            if(err) {

     //                console.log("ERROR: initMbrsite ");
     //            }
     //            else {

     //                // may want to make this multiple mbrsites   
     //                console.log("DEBUG: got mbrsite *********** : ", mbrsite);
     //                //$scope.mbrProfileEditForm.editItem = mbrsite;
     //                 console.log("ERROR: UserProfileCtrl initMbrsite post  part,mbrsite:*************************: ", part, mbrsite ); 
     //                switch( part ) {
     //                    case 'Profile':
     //                        $scope.bizEditItem = mbrsite;
     //                        $scope.localMbrsite = mbrsite;
     //                        componentService.setLocalMbrsite( mbrsite) ;
     //                        // fixed $scope.bizEditItem.lblArray = mbrsite.groupLabel + ":" + mbrsite.typeLabel + ":" + mbrsite.partLabel;  // 'Components:Mbrsite:Profile';                              
     //                        $scope.upricategory = mbrsite.category[0]; 
     //                        $scope.up2ndcategory = mbrsite.category[1];
     //                        $scope.up3rdcategory = mbrsite.category[2];
     //                        $scope.selOrgCategory = componentService.getBizCategories();
     //                        break;
     //                    case 'Tabset':
     //                        //$scope.utype =  $scope.selTypes[choice];
     //                        break;
     //                    case 'Panes':
     //                        //$scope.uperm = $scope.selPermissions[choice];
     //                        break;    
     //                    default:
     //                        $scope.bizEditItem = "";
     //                        console.log("ERROR: UserProfileCtrl initMbrsite  part: *************************", part );    
     //                }

     //            }
           
     //        });
            
     //      }
     //      else {
     //        console.log("DEBUG: MbrsiteAdminProfileCtrl.initMbrsite not authenticated ** :", user.authToken );
     //        $location.path('/home');
     //      }

     //    };
    
    
    }]);

    /*  **********************************
        **********************************
         MBRSITE PAGE MANAGER Ctrl
    */  
    
    angular.module('training.controllers').controller('MbrsiteAdminPageManagerCtrl', ['$scope', 'training.services.user', 'training.services.component', function($scope, userService, componentService ) {
    

             console.log("DEBUG: MbrsiteAdminPageManagerCtrl ************** " );
              
    }]);

    angular.module('training.controllers').controller('MbrsiteAdminContenSetsCtrl', ['$scope', 'training.services.user', 'training.services.component', function( $scope, userService, componentService ) {
            
            $scope.tabPane = "";
            $scope.contentSets = componentService.getLocalComponentPart('contentSets',  0 );
            console.log("DEBUG: MbrsiteAdminContenSetsCtrl-getLocalComponentPart contentSets via Service ************** ", $scope.contentSets );
                    
            try {
                  
                $scope.tabPane = {
                    tabId : 'TabID',                       // $scope.localSiteProfile.paneTabIds[0],            // $scope.localMbrsite.paneTabIds[0],
                    titleText: 'Title Text',                  //  {{ profileItem.name }}',                                    //$scope.localSiteProfile.paneTabTitles[0],
                    contentOne: {
                              // tabId : $scope.contentSets[0].contentTabIds[0],
                              // tabNameId: $scope.localSiteProfile.contentTabNameIds[0],
                              // isOK: $scope.localSiteProfile.contentIsOk[0],
                              titleText: 'Ttile sample' , //$scope.contentSets.contentModelTitleText[0],
                              textArea:  'text area sample' //contentSets.contentModelTextArea[0]
                              // imgSrc:  $scope.localSiteProfile.contentModelImgSrcs[0],
                              // imgTextArea: $scope.localMbrsite.contentModelImgTextArea[0],
                              // linkTarget:  $scope.localSiteProfile.contentModelLinkTarget[0],
                              // linkTextArea: $scope.localSiteProfile.contentModelLinkTextArea[0]
                    },
                    contentTwo: {
                           //    isOK:   localMbrsite.tabContentIsOk[0],
                           // titleone:  localMbrsite.tabContentModelTitles[0],
                           //  spanone:  localMbrsite.tabContentModelSpans[0],
                           // imgsrcone: localMbrsite.tabContentModelImgsSrc[0]
                            titleText:  'tmpl-Ssub title 2',
                            textArea:   'tmpl-Donec blandit suscipit ante, quis viverra urna ornare vitae. Vestibulum dolor ligula, scelerisque at lectus in, suscipit lobortis enim. Nunc at mattis sapien, quis mollis dolor. Donec blandit diam velit, quis pellentesque ante lobortis a. Aliquam et diam eget nibh tempus vulputate non ut tellus. Mauris sagittis vehicula dapibus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Aenean a quam ut nisl sollicitudin fringilla eu faucibus massa. Duis cursus augue at sapien tempus, vitae ornare tortor euismod. Maecenas pharetra tincidunt felis non consectetur. Maecenas condimentum mi sit amet nibh tempor accumsan. In hac habitasse platea dictumst. Duis accumsan, metus dictum fringilla facilisis, purus magna dictum lacus, eget consequat erat purus sit amet quam. Nam luctus ac ipsum non rhoncus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Fusce elementum, erat vitae gravida tincidunt, justo lorem maximus leo, a iaculis purus risus sit amet felis. Donec a lorem sagittis massa consequat tristique.'
                    },
                    contentThree: {
                           //    isOK:   localMbrsite.tabContentIsOk[0],
                           // titleone:  localMbrsite.tabContentModelTitles[0],
                           //  spanone:  localMbrsite.tabContentModelSpans[0],
                           // imgsrcone: localMbrsite.tabContentModelImgsSrc[0]
                             titleText:  'tmpl-Sub title 3',
                             textArea:   'tmpl-Donec blandit suscipit ante, quis viverra urna ornare vitae. Vestibulum dolor ligula, scelerisque at lectus in, suscipit lobortis enim. Nunc at mattis sapien, quis mollis dolor. Donec blandit diam velit, quis pellentesque ante lobortis a. Aliquam et diam eget nibh tempus vulputate non ut tellus. Mauris sagittis vehicula dapibus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Aenean a quam ut nisl sollicitudin fringilla eu faucibus massa. Duis cursus augue at sapien tempus, vitae ornare tortor euismod. Maecenas pharetra tincidunt felis non consectetur. Maecenas condimentum mi sit amet nibh tempor accumsan. In hac habitasse platea dictumst. Duis accumsan, metus dictum fringilla facilisis, purus magna dictum lacus, eget consequat erat purus sit amet quam. Nam luctus ac ipsum non rhoncus. Maecenas quam libero, luctus eu maximus eget, dictum in leo. Nunc in metus tellus. In lacinia varius sem vitae venenatis. Aenean mi odio, imperdiet eu enim vel, convallis lobortis nibh. In et faucibus purus, quis aliquet dui. Fusce elementum, erat vitae gravida tincidunt, justo lorem maximus leo, a iaculis purus risus sit amet felis. Donec a lorem sagittis massa consequat tristique.' 
                    }
                };
            }
            catch(err) {
           
             //document.getElementById("demo").innerHTML = err.message;
             //$scope.localSiteProfile =  componentService.getLocalComponentPart('profile', '');    
            };

             console.log("DEBUG: MbrsiteAdminContentSetsCtrl ************** " );
              
    }]);
}());