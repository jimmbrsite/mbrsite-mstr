The Cashiers Historical Society
f0Designer Showhouse
The Cashiers Historical Society Designer Showhouse is held each year in mid-August and goes for two weeks.
The Showhouse event draws designers from around the southeast and nationally. Featuring a different home each year, each Showhouse selected represents the finest in mountain living on the Cashiers Plateau.
Tickets are available from the event planners and the Historical Society.
Contact the Cashiers Historical Society to find out more about this annual event!
Click Here to Visit The Cashiers Historical Society Website


Groovin On The Green
Most Friday nights on the Village Commons during the summer months.
This series of free concerts showcases music from around western North Carolina. The events are open to anyone, so grab your chairs and coolers and joinu for a fun-filled evening of music. The events start between 5:30 and 6:30PM. This series of concerts are presented by the Greater Cashiers Area Merchants Association for the entire community.
Click Here To See The Groovin On The Green Schedule


Click Here To See The GCAMA Website


The Tour de Cashiers Mountain Bicycle Ride
Early Spring in our mountains is a time of cool mornings, warm afternoons, wild flowers and flowering shrubs, and dramatic differences in leafing at different elevations � which our riders will experience along their courses. Summer residents begin returning, and the stores and restaurants are reopened. But the pace (and traffic) are at a lower level than in mid-Summer. In short, an ideal time for a solid, supported bike ride in the mountains, and for any non-riding family members to come visit also. All rides are through low-traffic rural and mountain landscapes.
On Saturday May 3rd this year The Tour de Cashiers provides a weekend of mountain immersion � great training for whatever else may be in your season�s riding plans, or just a wonderful time enjoying Spring in the mountains, up close on a bicycle. The different length courses )100 mile, 62 mile and 25 mile) are suitable for all types of riders. Last year more than 300 riders participated in one of the most beautiful rides in the US.
All riders depart the Village Green at the Cashiers Crossroads at 9:00AM, Saturday, May 3, 2014.
Click Here to Visit the Tour de Cashiers Website


The Rotary Club of Cashiers Valley
f0Spring and Fall Arts & Crafts Fair
The Rotary Club of Cashiers Valley presents two Arts & Crafts Fairs every year - one on Memorial Day weekend and the other on Labor Day weekend.
The Arts and Crafts Fairs are held on the Cashiers Village Green at the crossorads of Hwy. 107 and Hwy. 64. With over 80 vendors this year there is sure to be something for everyone at this event. All of the items here are made by the craftspeople, are of the highest quality, and represent unique and diverse tastes.
Contact the Rotary Club of Cashiers Valley to find out more about this exciting twice-a-year event!
Click Here to Visit the Cashiers Rotary Club Website


The Cashiers Valley Leaf Festival
The Leaf Festival is held each year the first weekend in October.
The 2014 Leaf Festival is on October 10-12 and features music, shopping, dining and a whole lot of fun for the whole family. This festival is presented by the Greater Cashiers Area Merchants Association for the entire community.
Event Schedules are available from the event planners and participating GCAMA merchants.
Contact the Greater Cashiers Area Merchants Association to find out more about this annual event!
Click Here to Visit The GCAMA Leaf Festival Website 


Click Here to Visit The GCAMA Website


The Village Green - Joy Garden Tour
The Joy Garden Tour is a biannual event to raise financial support for The Village Green. The tour inspires gardeners of every level of experience and enthusiasm. The Joy Garden Tour features a guest speaker event, a special patron party and garden shops with 35 vendors offering plants, garden related merchandise, unique gifts, antiques and more. The highlight is a day spent touring select and stunning private gardens in the Cashiers area. Noted Plein Air Artists will paint in these gardens with the paintings available for purchase. Plans also include Saturday activities.
The Village Green - Arts On The Green
Arts On The Green is held every other year in mid-July. The event showcases plein air artists from around the area.
The Village Green - Christmas On the Green
Visitors to The Village Green enjoy twinkling light displays and festive decorations from Thanksgiving Weekend until New Year's Day. On November 22 the park becomes a winter wonderland ans the Cashiers Christmas Tree is lit! The Festival of Trees celebrates one of the area's greatest industries and showcases local businesses. After the holiday season, the evergreen trees are planted in The Village Green for continued enjoyment.
Tickets are available from the event planners and the Village Green.
Contact the Village Green to find out more about this annual event!
Click Here to Visit The Joy Garden Tour Website


Click Here to Visit The Arts On The Green Website


Click Here to Visit Christmas On The Green Website


The Summer Colors Fine Art Show
The Art League of Highlands-Cashiers holds its "Summer Colors Fine Art Show" indoors at the Sapphire Valley Community Center, approximately two miles east of the Crossroads in the Sapphire Valley Resort. Works in oil, watercolor, acrylics, dry media and mixed media will be on display and for sale, as well as photographs, sculpture, glass, one-of-a-kind jewelry and woodturnings. All work is original. Those who have attended previous Art League shows are appreciative of the high quality and variety of the pieces on display. Virtually all of the artists have received recognition for their work, and their creations may be found in public and private collections throughout the Southeast and beyond.
For Information call (828)743-7663

The Annual Cashiers Christmas Parade
Saturday, December 13, 2014
This is a wonderful event for the whole family. The parade starts at 12:00 Noon and lasts for about an hour or so. The parade route goes from Cornucopia restauranton Hwy 107 South in Cashiers. The parade goes north on Hwy. 107 to the crossroads. Here is where the judging occurs. The parade then turns left and proceeds on Hwy 64 West ending at Frank Allen Road. Bring your whole family to see and participate in this one-of-a-kind parade!
For more information contact the Cashiers Chamber of Commerce.
Click Here to Visit The Cashiers Chamber Website